In this implementation **all HTML templates** will live under `public/assets/email-templates/account-send-activationlink`
The reason is, like this they share the images of the`img` folder and images are not stored redundantly.

Later, an email, opened by the user will load images from the this server (here the DEV, TEST or PROD AIO).

All **new HTML** templates should be stored below folder `public/assets/email-templates/account-send-activationlink`

**New images** must be placed below `public/assets/email-templates/account-send-activationlink/img`.

**New placeholdes** should follow the syntax '`###PLACEHOLDERNAME##`'. 

To use them you must edit `src/Types/EmailTemplatePlaceholderTypes.php` and `src/Services/MSMailServices/EmailService.php` accordingly to the implmentations of `mio-email-template-account-activation.html` and  `mio-email-template-shopmvp-apikey.html`.

---
This implementation is very, very old style, quick and dirty, made to show nice emails during a board meeting.

All that stuff should be moved to single microservice. In this case **MSMAIL** would be a good place to host all kind of email functionality.

Moreover, a mostly similar implementation exists in **MIO**! Moving that stuff to **MSMAIL**, we would have single place to maintain that functionality. In addition we should use **TWIG-templates** and not HTML-templates along with ###placeholders### as here, to render HTML style emails: That way we can separate headers and footers (which don't change frequently) from individual and context related body content.








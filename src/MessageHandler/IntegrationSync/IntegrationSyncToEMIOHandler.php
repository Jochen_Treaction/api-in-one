<?php

namespace App\MessageHandler\IntegrationSync;

use App\Message\IntegrationSync\IntegrationSyncToEMIO;
use App\Message\ShopDataQueue\AddChunkDataToShopDataQueue;
use App\Repository\IntegrationsRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactServices;
use App\Services\AIOServices\IntegrationServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Types\ContactPermissionTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Driver\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class IntegrationSyncToEMIOHandler implements MessageHandlerInterface
{

    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactServices;
    /**
     * @var ContactPermissionTypes
     * @author Pradeep
     */
    private ContactPermissionTypes $contactPermissionTypes;
    /**
     * @var string
     * @author Pradeep
     */
    private string $eMIOAPIKey;
    /**
     * @var IntegrationsRepository
     * @author Pradeep
     */
    private IntegrationsRepository $integrationsRepository;
    /**
     * @var UniqueObjectTypes
     * @author Pradeep
     */
    private UniqueObjectTypes $uniqueObjectTypes;
    /**
     * @var IntegrationServices
     * @author Pradeep
     */
    private IntegrationServices $integrationServices;
    /**
     * @var MSMIOServices
     * @author Pradeep
     */
    private MSMIOServices $eMioServices;
    /**
     * @var int
     * @author Pradeep
     */
    private int $companyId;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        ContactServices $contactServices,
        ContactPermissionTypes $contactPermissionTypes,
        ContactFieldServices $contactFieldServices,
        IntegrationServices $integrationServices,
        MSMIOServices $eMioServices,
        IntegrationsRepository $integrationsRepository,
        LoggerInterface $logger
    ) {
        $this->contactServices = $contactServices;
        $this->contactPermissionTypes = $contactPermissionTypes;
        $this->integrationServices = $integrationServices;
        $this->eMioServices = $eMioServices;
        $this->contactFieldServices = $contactFieldServices;
        $this->integrationsRepository = $integrationsRepository;
        $this->logger = $logger;
    }

    /**
     * @param IntegrationSyncToEMIO $integrationSyncToEMIO
     * @return array
     * @author Pradeep
     */
    public function __invoke(IntegrationSyncToEMIO $integrationSyncToEMIO): array
    {
        $contactsToSync = $integrationSyncToEMIO->getContactsToSync();
        if (!$this->setCompanyId($integrationSyncToEMIO->getCompanyId())) {
            return ['status' => false, 'message' => 'invalid MIO company id provided'];
        }
        return $this->integrate($contactsToSync);
    }


    /**
     * setCompanyId set company Id.
     * @param int $companyId
     * @return bool
     * @author Pradeep
     */
    private function setCompanyId(int $companyId): bool
    {
        if ($companyId <= 0) {
            return false;
        }
        $this->companyId = $companyId;
        return true;
    }

    /**
     * integrate contacts with eMIO.
     * @param array $contactsToSync
     * @return array
     * @author Pradeep
     */
    public function integrate(array $contactsToSync): array
    {
        $companyId = $this->getCompanyId();

        try {
            $config = $this->integrationsRepository->getIntegrationConfigForCompany($companyId,
                UniqueObjectTypes::EMAILINONE);
            if (empty($config)) {
                throw new RuntimeException('Invalid eMIO configuration provided :' .
                                           json_encode($config, JSON_THROW_ON_ERROR));
            }
            // Divide the contacts based on Permission, i,e withDOI and withOutDOI Permission.
            $contactsToSync = $this->divideContactsBasedOnPermission($contactsToSync);
            if (empty($contactsToSync)) {
                throw new RuntimeException('empty contact array provided to synchronize with eMIO :' .
                                           json_encode($contactsToSync, JSON_THROW_ON_ERROR));
            }
            // integrate contacts with DOI Permission
            if (isset($contactsToSync[ 'contactsWithDOI' ]) && !empty($contactsToSync[ 'contactsWithDOI' ])) {
                $this->integrateContactsWithDOIPermission($contactsToSync[ 'contactsWithDOI' ], $config);
            }// integrate contacts with NO DOI Permission.
            if (isset($contactsToSync[ 'contactsWithNoDOI' ]) && !empty($contactsToSync[ 'contactsWithNoDOI' ])) {
                $this->integrateContactsWithNoDOIPermission($contactsToSync[ 'contactsWithNoDOI' ], $config);
            }
            return ['status' => false, 'message' => 'failed to start eMIO Integration'];
        } catch (RuntimeException|\Doctrine\DBAL\Exception|JsonException $e) {
            $this->logger->error('integrateSync :' . $e->getMessage(), [__METHOD__, __LINE__]);
            return ['status' => false, 'message' => $e->getMessage()];
        }

    }

    /**
     * getCompanyId returns the companyId of eMIO.
     * @return int
     * @author Pradeep
     */
    private function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * divideContactsBasedOnPermission divide the contacts based on permission.
     * - DOI or NONDOI Permission.
     * @param array $contactsToSync
     * @return array[]
     * @author Pradeep
     */
    private function divideContactsBasedOnPermission(array $contactsToSync): array
    {
        $contactsWithDOI = [];
        $contactsWithNoDOI = [];

        foreach ($contactsToSync as $contactToSync) {
            if (!isset($contactToSync[ 'contact' ])) {
                continue;
            }
            $contact = $contactToSync[ 'contact' ];
            $permission = $this->contactFieldServices->getValue($contact, 'permission');
            if ($this->contactPermissionTypes->hasDoubleOptInPermision($permission)) {
                $contactsWithDOI[] = $contact;
            } else {
                $contactsWithNoDOI[] = $contactToSync;
            }
        }
        return ['contactsWithDOI' => $contactsWithDOI, 'contactsWithNoDOI' => $contactsWithNoDOI];
    }

    /**
     * integrateContactsWithDOIPermission synchronizes contacts with eMIO using REST API.
     * @param array $contactsWithDOI
     * @param array $config
     * @return array
     * @author Pradeep
     */
    public function integrateContactsWithDOIPermission(array $contactsWithDOI, array $config): array
    {
        $status = false;
        if (empty($contactsWithDOI) || empty($config)) {
            throw new RuntimeException('Invalid contacts or eMIO configuration');
        }
        $companyId = $this->getCompanyId();
        // Use Contact sync of eMIO.(bulk sync)
        try {

            $this->logger->info('integrateContactsWithDOIPermission :' . json_encode($contactsWithDOI));

            $this->eMioServices->setAPIKey($config[ 'settings' ][ 'apikey' ]);
            $this->eMioServices->syncContactFieldsOfDataTypeList($contactsWithDOI);
            $status = $this->eMioServices->syncContacts($contactsWithDOI, $config[ 'settings' ], $companyId);
            if (empty($status)) {
                $this->logger->info('Waiting for the response from eMIO ');
                $status = false;
            }
            $this->logger->info('integrateContactsWithDOIPermission Status: ' . $status);
            return ['status' => $status, 'message' => 'successfully synchronized to eMIO'];
        } catch (RuntimeException|Exception|\Doctrine\DBAL\Exception|JsonException $e) {
            $this->logger->error('integrateSync :' . $e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * integrateContactsWithNoDOIPermission synchronizes contacts which has NoDOI.
     * - create contacts in eMIO using contactCreate call.
     * @param array $contactsWithNoDOI
     * @param array $config
     * @return array
     * @author Pradeep
     */
    public function integrateContactsWithNoDOIPermission(array $contactsWithNoDOI, array $config): array
    {
        $status = false;
        if (empty($contactsWithNoDOI) || empty($config)) {
            throw new RuntimeException('Invalid contacts or eMIO configuration');
        }
        $companyId = $this->getCompanyId();
        try {
            $this->logger->info('integrateContactsWithNoDOIPermission :' . json_encode($contactsWithNoDOI));
            foreach ($contactsWithNoDOI as $contact) {
                $this->logger->info('Contact integrate :' . json_encode($contact));
                $campaignId = (int)$contact[ 'base' ][ 'campaign_id' ];
                $this->logger->info('test parmas :' . json_encode([
                        'companyId' => $companyId,
                        'cmapaignId' => $campaignId,
                        'contact' => $contact,
                        'config' => $config,
                    ], JSON_THROW_ON_ERROR));
                $this->integrationServices->eMailInOne($companyId, $campaignId, $contact, $config);
            }
            return ['status' => true, 'message' => 'successfully synchronized to eMIO'];
        } catch (JsonException|RuntimeException $e) {
            $this->logger->error('integrateSync :' . $e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

}
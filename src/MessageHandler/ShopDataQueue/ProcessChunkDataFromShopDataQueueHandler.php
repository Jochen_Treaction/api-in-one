<?php

namespace App\MessageHandler\ShopDataQueue;

use App\Message\ShopDataQueue\ProcessChunkDataFromShopDataQueue;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ShopDataQueueRepository;
use App\Services\AIOServices\IntegrationServices;
use App\Services\RedisService;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use Doctrine\DBAL\Driver\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;

class ProcessChunkDataFromShopDataQueueHandler implements MessageHandlerInterface
{
    use HandleTrait;

    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;
    /**
     * @var CompanyRepository
     * @author Pradeep
     */
    private CompanyRepository $companyRepository;
    /**
     * @var CampaignRepository
     * @author Pradeep
     */
    private CampaignRepository $campaignRepository;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private ContactRepository $contactRepository;
    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var ShopDataQueueRepository
     * @author Pradeep
     */
    private ShopDataQueueRepository $webhookQueueRepository;
    /**
     * @var ToolServices
     * @author Pradeep
     */
    private ToolServices $toolServices;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    /**
     * @var IntegrationServices
     * @author Pradeep
     */
    private IntegrationServices $integrationServices;

    /**
     * @param ContactRepository $contactRepository
     * @param RedisService $redisService
     * @param LoggerInterface $logger
     */
    public function __construct(
        ContactRepository $contactRepository,
        RedisService $redisService,
        IntegrationServices $integrationServices,
        LoggerInterface $logger
    ) {
        $this->contactRepository = $contactRepository;
        $this->redisService = $redisService;
        $this->integrationServices = $integrationServices;
        $this->logger = $logger;
    }

    public function __invoke(ProcessChunkDataFromShopDataQueue $processChunkDataFromWebhookQueue): array
    {

        $jobId = $processChunkDataFromWebhookQueue->getJobId();
        $chunkData = $processChunkDataFromWebhookQueue->getChunkData();
        $company = $processChunkDataFromWebhookQueue->getCompany();
        $campaign = $processChunkDataFromWebhookQueue->getCampaign();

        try {

            if (!isset($campaign[ 'id' ], $company[ 'id' ]) || empty($chunkData)) {
                throw new RuntimeException('Invalid Company Or CampaignId provided.');
            }
            // save the company and campaign to redis.
            $this->saveToRedis($campaign, $company);
            // converts chunk to simple key value structure.
            $contacts = $this->parseChunk($chunkData);
            // processing -> starts process required to insert contact to DB,
            if (!$this->processChunkData($campaign[ 'id' ], $company[ 'id' ], $contacts, (int)$jobId)) {
                throw new RuntimeException('Failed to sync contacts.');
            }
            return ['status' => true, 'message' => 'successfully created leads'];

        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Convert the payload structure of MIO to simple key=>value pair.
     * Key is the field name and value is the field value.
     * @param array $contacts
     * @return array
     * @author Pradeep
     */
    private function parseChunk(array $contacts): array
    {
        $parsedChunk = [];
        if (empty($contacts)) {
            return $parsedChunk;
        }
        // for all contacts
        foreach ($contacts as $contact) {
            // for all the standard fields of contact.
            foreach ($contact as $standard) {
                $parsedContact = [];
                // for all the different fields
                foreach ($standard as $fields) {
                    // ignore required, datatype, regex fields
                    // only required entry is the field name.
                    foreach ($fields as $key => $value) {
                        if (in_array($key, ['required', 'datatype', 'regex'])) {
                            continue;
                        }
                        $parsedContact[ $key ] = $value;
                    }
                }
            }
            $parsedChunk[] = $parsedContact;
        }

        return $parsedChunk;
    }

    /**
     * saveToRedis stores the campaign and company details to redis server.
     * @param array $campaign
     * @param array $company
     * @throws \JsonException
     * @author Pradeep
     */
    private function saveToRedis(array $campaign, array $company): void
    {
        $campaignKey = 'campaign-' . $campaign[ 'id' ];
        $accountKey = 'campaign-' . $company[ 'id' ];
        $this->redisService->hashesObject($campaignKey, $campaign);
        $this->redisService->hashesObject($accountKey, $company);
    }

    /**
     * processChunkData process each and every chunk of data to Leads table.
     * @param int $campaignId
     * @param int $companyId
     * @param array $contacts
     * @param int $webhookQueueId
     * @return bool
     * @author Pradeep
     */
    private function processChunkData(int $campaignId, int $companyId, array $contacts, int $webhookQueueId = 0): bool
    {
        $syncWithEMIO = $this->isChunkGoingToSyncToeMIO($companyId);
        return $this->contactRepository->multiInsert($campaignId, $companyId, $contacts, $webhookQueueId,
            $syncWithEMIO);
    }

    /**
     * isChunkGoingToSyncToeMIO checks whether che
     * @param int $companyId
     * @return bool
     * @author Pradeep
     */
    private function isChunkGoingToSyncToeMIO(int $companyId): bool
    {
        return $this->integrationServices->isEMIOIntegrationActiveForCompany($companyId);
    }

}
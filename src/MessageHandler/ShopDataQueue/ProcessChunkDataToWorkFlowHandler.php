<?php

namespace App\MessageHandler\ShopDataQueue;

use App\Message\ShopDataQueue\ProcessChunkDataToWorkFlow;
use App\Repository\Workflow\WorkflowRepository;
use App\Services\AIOServices\ProcesshookServices;
use App\Services\RedisService;
use Doctrine\DBAL\Exception;
use RuntimeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProcessChunkDataToWorkFlowHandler implements MessageHandlerInterface
{

    public function __construct(
        WorkflowRepository $workflowRepository,
        RedisService $redisService,
        ProcesshookServices $processhookServices
    ) {
        $this->workflowRepository = $workflowRepository;
        $this->redisService = $redisService;
        $this->processhookServices = $processhookServices;

    }

    public function __invoke(ProcessChunkDataToWorkFlow $processChunkDataToWorkFlow)
    {
        $campaignId = $processChunkDataToWorkFlow->getCampaignId();
        $companyId = $processChunkDataToWorkFlow->getCompanyId();
        $workflowId = $processChunkDataToWorkFlow->getWorkflowId();
        $chunkData = $processChunkDataToWorkFlow->getChunkData();
        $chunkBaseData = $processChunkDataToWorkFlow->getChunkBaseData();

        // check if the workflow id is provided in base data or not.
        // Campaign can have multiple workflowId,
        // Priority is given to the Workflow id in the base data
        $executeWorkFlow = $workflowId <= 0 ? $this->getWorkFlowId($campaignId) : $workflowId;
        // initate workflow, only if the workflow id is present.
        if ($executeWorkFlow > 0) {
            return $this->execute($companyId, $campaignId, $chunkData, $chunkBaseData);
        }
        return ['status' => true, 'message' => 'workflow is sucessfull'];
    }

    /**
     * execute the workflow for the chunk of contacts.
     * @param int $companyId
     * @param int $campaignId
     * @param array $chunkData
     * @param array $chunkBaseData
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    private function execute(int $companyId, int $campaignId, array $chunkData, array $chunkBaseData): array
    {
        try {
            $workflowId = $this->getWorkFlowId($campaignId);
            if ($workflowId <= 0) {
                throw new RuntimeException('Invalid workflow Id');
            }

            // workflow cannot take bulk data,
            // Hence dividing the chunk into inidividual contacts and appending the
            // respective base data of the payload.
            // The Base data of the payload is utilized furthur in the internal calls.

            foreach ($chunkData as $chunk) {
                // check if the chunk has the standard fields.
                // customfields are always optional for a contact.
                if (!isset($chunk[ 'standard' ]) || empty($chunk)) {
                    continue;
                }
                // prepare the complete single contact array.
                $singleContact = $this->preprocessData($chunk, $chunkBaseData);
                if (!empty($singleContact)) {
                    // Handed over to processHook services.
                    $result = $this->processhookServices->integrateWorkflow($workflowId, $singleContact, $companyId);
                    if ($result === null || (is_bool($result) && !$result)) {
                        throw new RuntimeException('failed to execute processhook');
                    }
                }
            }
            return ['status' => true, 'message' => 'successfully executed'];
        } catch (Exception|RuntimeException $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * preprocessData appends the base and contact fields of array into single array.
     * ProcessHook Services accept contact information with base section.
     * @param array $chunkWithStandardFields
     * @param array $baseData
     * @return array
     * @author Pradeep
     */
    private function preprocessData(array $chunkWithStandardFields, array $baseData): array
    {
        if (empty($chunkWithStandardFields) || empty($baseData)) {
            return [];
        }
        $chunkWithContactField[ 'contact' ] = $chunkWithStandardFields;
        return array_merge($baseData, $chunkWithContactField);
    }

    /**
     * getWorkFlowId fetches the workflow Id using campaignId.
     * fucntion also utilizes the redisServer and stores Id in interm storage.
     * @param int $campaignId
     * @return int
     * @author Pradeep
     */
    private function getWorkFlowId(int $campaignId): int
    {
        $workflowId = $this->getCampaignWorkFlowIdFromRedis($campaignId);
        if ($workflowId > 0) {
            return $workflowId;
        }
        $details = $this->workflowRepository->get(0, $campaignId);
        if (!empty($details) && isset($details[ 'id' ])) {
            $workflowId = $details[ 'id' ];
            $this->setCampaignWorkFlowIdFromRedis($campaignId, $workflowId);
        }

        return $workflowId;
    }

    /**
     * getCampaignWorkFlowIdFromRedis fetches the workflowId from redis server if present.
     *
     * @param int $campaignId
     * @return false|mixed|string
     * @author Pradeep
     */
    private function getCampaignWorkFlowIdFromRedis(int $campaignId)
    {
        $redisKey = 'workflow-id-for-campaign-' . $campaignId;
        return $this->redisService->get($redisKey);
    }

    /**
     * setCampaignWorkFlowIdFromRedis stores the workflowId to redis server.
     *
     * @param int $campaignId
     * @param int $workflowId
     * @author Pradeep
     */
    private function setCampaignWorkFlowIdFromRedis(int $campaignId, int $workflowId)
    {
        $redisKey = 'workflow-id-for-campaign-' . $campaignId;
        $this->redisService->set($redisKey, $workflowId);
    }
}
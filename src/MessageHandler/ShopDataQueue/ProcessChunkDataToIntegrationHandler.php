<?php

namespace App\MessageHandler\ShopDataQueue;

use App\Message\ShopDataQueue\ProcessChunkDataToIntegration;
use App\Repository\CampaignRepository;
use App\Repository\ContactRepository;
use App\Repository\IntegrationsRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactServices;
use App\Services\AIOServices\IntegrationServices;
use App\Services\MSMailServices\MSMailServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Services\RedisService;
use App\Types\ContactPermissionTypes;
use Doctrine\DBAL\Driver\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProcessChunkDataToIntegrationHandler implements MessageHandlerInterface
{

    /**
     * @var IntegrationsRepository
     * @author Pradeep
     */
    private IntegrationsRepository $integrationsRepository;
    /**
     * @var MSMIOServices
     * @author Pradeep
     */
    private MSMIOServices $MSMIOServices;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactService;
    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private ContactRepository $contactRepository;
    /**
     * @var ContactPermissionTypes
     * @author Pradeep
     */
    private ContactPermissionTypes $contactPermissionTypes;
    /**
     * @var IntegrationServices
     * @author Pradeep
     */
    private IntegrationServices $integrationServices;
    /**
     * @var CampaignRepository
     * @author Pradeep
     */
    private CampaignRepository $campaignRepository;

    public function __construct(
        IntegrationsRepository $integrationsRepository,
        ContactRepository $contactRepository,
        CampaignRepository $campaignRepository,
        ContactServices $contactService,
        MSMIOServices $MSMIOServices,
        ContactPermissionTypes $contactPermissionTypes,
        IntegrationServices $integrationServices,
        RedisService $redisService,
        ContactFieldServices $contactFieldServices,
        LoggerInterface $logger
    ) {
        $this->integrationsRepository = $integrationsRepository;
        $this->redisService = $redisService;
        $this->contactRepository = $contactRepository;
        $this->campaignRepository = $campaignRepository;
        $this->contactService = $contactService;
        $this->contactFieldServices = $contactFieldServices;
        $this->integrationServices = $integrationServices;
        $this->contactPermissionTypes = $contactPermissionTypes;
        $this->MSMIOServices = $MSMIOServices;
        $this->logger = $logger;
    }

    public function __invoke(ProcessChunkDataToIntegration $processChunkDataToIntegration)
    {
        $chunkData = $processChunkDataToIntegration->getChunkData();
        $companyId = $processChunkDataToIntegration->getCompanyId();
        $campaignId = $processChunkDataToIntegration->getCampaignId();
        $integrationConfig = $this->integrationsRepository->getIntegrationsForCompany($companyId);

        // integration with eMIO.
        if (isset($integrationConfig[ 'email-in-one' ][ 'settings' ]) && !empty($integrationConfig[ 'email-in-one' ][ 'settings' ])) {
            return $this->eMIO($campaignId, $companyId, $integrationConfig[ 'email-in-one' ], $chunkData);
        }

        return ['status' => true, 'message' => 'integration is sucessfull'];
    }

    /**
     * eMIO integrates the chunk of data with eMailInOne.
     * - Uses RestAPI and single contact create.
     * - Uses redisserver to store interm cache.
     * @param int $campaignId
     * @param int $companyId
     * @param array $config
     * @param array $chunkData
     * @return bool
     * @author Pradeep
     */
    private function eMIO(int $campaignId, int $companyId, array $config, array $chunkData): array
    {
        try {
            // redis unique key for contact permission based on campaign.
            $redisKeyForPermissionOfCampaign = 'permission-for-campaign-' . $campaignId;
            // Check if the contact permission for the campaign is in redis
            $permissionOfCampaign = $this->redisService->get($redisKeyForPermissionOfCampaign);
            if (is_bool($permissionOfCampaign) && !$permissionOfCampaign) {
                // fetches the permission given for the page or webhook in the wizard.
                // this permission will be same for all the contacts for the same campaignId.
                $permissionOfCampaign = $this->campaignRepository->getPermission($campaignId);
                // save the permission back to redis.
                $this->redisService->set($redisKeyForPermissionOfCampaign, $permissionOfCampaign);
            }
            // get the save the permission of campaign to redis.
            // check if the permission is present in redis based on campaignId.
            // if so get the permission
            if (!isset($config[ 'settings' ][ 'apikey' ])) {
                throw new RuntimeException('could not fetch apikey for eMIO');
            }
            // configure the eMIO APIKey
            $this->MSMIOServices->setAPIKey($config[ 'settings' ][ 'apikey' ]);
            // collects the smartTags from all the contacts and creates customfields in eMIO.
            $this->MSMIOServices->syncContactFieldsOfDataTypeList($chunkData);

            // array to collect contacts with DOI or DOI+ permission level.s
            $contactsWithDoubleOptIn = [];

            // when campaign has DOI or DOI+ permission.
            // collect all the contacts since campaign has DOI or DOI+ permission to all contacts.
            if ($this->contactPermissionTypes->hasDoubleOptInPermision($permissionOfCampaign)) {
                $contactsWithDoubleOptIn = $chunkData;
            } else {
                // checking the permission for each and every contact.
                // this is the case where the campaign has no DOI or DOI+ permission.
                // but some contacts might have DOI or DOI+ permission in their payload.
                // So Use EMIO rest api for the contacts with DOI or DOI+ permission.
                foreach ($chunkData as $contact) {
                    // get the permission for each contact.
                    $permission = $this->contactFieldServices->getValue($contact, 'permission');
                    // No permission is present in the contact array.
                    // So consider the permission offered by the campaign(page/webhook wizard.)
                    if ($permission === null) {
                        $permission = $permissionOfCampaign;
                    }
                    // check for the permission.
                    if (!$this->contactPermissionTypes->hasDoubleOptInPermision($permission)) {
                        $this->logger->info('contactPermission' . json_encode($permission));
                        // contact with no doi permission level.
                        $this->integrationServices->eMailInOne($companyId, $campaignId, $contact, $config);
                    } else {
                        // collect the contacts which has DOI or DOI+
                        $contactsWithDoubleOptIn[] = $contact;
                    }
                }
            }

            // synchronizes the contacts with eMIO.
            // synchronization is done with RestAPI, directly with permission as 'double-opt-in'
            if (!empty($contactsWithDoubleOptIn)) {
                if (!$this->MSMIOServices->syncContacts($contactsWithDoubleOptIn, $config[ 'settings' ], $companyId)) {
                    throw new RuntimeException("Failed to send request to eMIO for synchronizing contacts");
                }
            }
            return ['status' => true, 'message' => 'Integration with eMIO is successfull'];

        } catch (Exception|RuntimeException|\Doctrine\DBAL\Exception|JsonException $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    private function Hubspot()
    {

    }

    private function Digistore()
    {

    }
}
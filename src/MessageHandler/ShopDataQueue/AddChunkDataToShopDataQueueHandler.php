<?php

namespace App\MessageHandler\ShopDataQueue;

use App\Message\ShopDataQueue\AddChunkDataToShopDataQueue;
use App\Repository\CompanyRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusdefRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Repository\ShopDataQueueRepository;
use App\Services\RedisService;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\UniqueObjectTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddChunkDataToShopDataQueueHandler implements MessageHandlerInterface
{

    /**
     * @var ShopDataQueueRepository
     * @author Pradeep
     */
    private ShopDataQueueRepository $webhookQueueRepository;
    /**
     * @var ToolServices
     * @author Pradeep
     */
    private ToolServices $toolServices;
    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var UniqueObjectTypes
     * @author Pradeep
     */
    private UniqueObjectTypes $uniqueObjectTypes;
    /**
     * @var CompanyRepository
     * @author Pradeep
     */
    private CompanyRepository $companyRepository;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;
    /**
     * @var UniqueObjectTypeRepository
     * @author Pradeep
     */
    private UniqueObjectTypeRepository $uniqueObjectTypeRepository;
    /**
     * @var StatusdefRepository
     * @author Pradeep
     */
    private StatusdefRepository $statusdefRepository;

    /**
     * @param ShopDataQueueRepository $webhookQueueRepository
     * @param RedisService $redisService
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     */
    public function __construct(
        ShopDataQueueRepository $webhookQueueRepository,
        RedisService $redisService,
        ToolServices $toolServices,
        ObjectRegisterRepository $objectRegisterRepository,
        UniqueObjectTypeRepository $uniqueObjectTypeRepository,
        StatusdefRepository $statusdefRepository,
        CompanyRepository $companyRepository,
        LoggerInterface $logger
    ) {
        $this->webhookQueueRepository = $webhookQueueRepository;
        $this->toolServices = $toolServices;
        $this->redisService = $redisService;
        $this->companyRepository = $companyRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->uniqueObjectTypeRepository = $uniqueObjectTypeRepository;
        $this->statusdefRepository = $statusdefRepository;
        $this->logger = $logger;
    }

    /**
     * @param AddChunkDataToShopDataQueue $addDataToWebhookQueue
     * @author Pradeep
     */
    public function __invoke(AddChunkDataToShopDataQueue $addDataToWebhookQueue)
    {
        try {
            $chunkKey = $addDataToWebhookQueue->getChunkKey();
            $chunkData = $addDataToWebhookQueue->getChunkData();
            $readyToWriteToDB = $addDataToWebhookQueue->getReadyToWriteToDB();
            $this->logger->error('$readyToWriteToDB ' . $readyToWriteToDB, [__METHOD__, __LINE__]);
            // continue to get save all the chunks when processChunks is false.
            if ($readyToWriteToDB === 'false') {
                $this->writeChunksToRedisServer($chunkKey, $chunkData);
            }

            // All the has been received, Now get all the chunks from redis
            // and process each and every chunk to MIO.
            if ($readyToWriteToDB === 'true' || $readyToWriteToDB) {
                $this->saveChunkDataToWebHookQueue($chunkData);
            }
        } catch (JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
    }

    /**
     * @param string $chunkKey
     * @param string $chunkData
     * @throws JsonException
     * @author Pradeep
     */
    private function writeChunksToRedisServer(string $chunkKey, string $chunkData): void
    {
        $this->logger->info('writeChunksToRedisServer');
        $this->logger->info('Waiting for other Chunks');
        if (!empty($chunkKey) &&
            !$this->redisService->set($chunkKey, $chunkData)) {
            throw new RuntimeException(
                'invalid ChunkKey or failed to save chunk to redis. ' .
                json_encode([
                    'chunk' => $chunkKey,
                    'payload' => $chunkData,
                ], JSON_THROW_ON_ERROR));
        }
    }


    /**
     * @param string $allChunkKeysAsList
     * @throws JsonException
     * @author Pradeep
     */
    private function saveChunkDataToWebHookQueue(string $allChunkKeysAsList): void
    {
        $this->logger->info('saveChunkDataToWebHookQueue', [__METHOD__, __LINE__]);

        $statusTypeNew = $this->webhookQueueRepository::WB_QUEUE_STATUS_TYP_NEW;
        $start = null;
        $message = "";
        $modification = "";
        $end = null;
        if (empty($allChunkKeysAsList)) {
            $this->logger->error('invalid chunk data after decoded', [__METHOD__, __LINE__]);
        }
        $chunkKeysAsArray = $this->toolServices->decryptContentData($allChunkKeysAsList);
        // $chunkKeysAsArray = $this->simpleDecryptToArray($allChunkKeysAsList);
        $this->logger->info('descryptedChunkKeysAsArray', [__METHOD__, __LINE__]);
        if (empty($chunkKeysAsArray)) {
            $this->logger->error('invalid chunk data after decoded', [__METHOD__, __LINE__]);
        }

        foreach ($chunkKeysAsArray as $chunkKey) {
            $this->logger->info('Inside Foreach', [__METHOD__, __LINE__]);
            // Get the ChunkData from redis server using chunkKey.
            $chunkValue = $this->redisService->get($chunkKey);
            if (empty($chunkValue)) {
                continue;
            }
            $this->logger->info('geting chunk value from redis.', [__METHOD__, __LINE__]);

            $data = $this->toolServices->decryptContentData($chunkValue, true);
            $this->logger->info('descrypted Chunk data.', [__METHOD__, __LINE__]);
            $accountNumber = (string)$data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ];
            $this->logger->info('Account' . $accountNumber, [__METHOD__, __LINE__]);
            $objRegIdOfWebhook = $data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ] ?? '';
            $this->logger->info('$objRegIdOfWebhook' . json_encode($objRegIdOfWebhook), [__METHOD__, __LINE__]);
            $uuid = $data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ] ?? '';
            $this->logger->info('$uuid' . json_encode($uuid), [__METHOD__, __LINE__]);
            if (!empty($uuid)) {
                $objRegIdOfWebhook = $this->objectRegisterRepository->getObjectRegisterIdWithUUID($uuid);
            }

            // create an object register for the webhookQueue.
            $uniqueObjectTypeId = $this->uniqueObjectTypeRepository->getUniqueObjectTypeId(uniqueObjectTypes::WBHOOKQUEUE);
            $this->logger->info('$uniqueObjectTypeId' . json_encode($uniqueObjectTypeId), [__METHOD__, __LINE__]);
            $statusDefId = $this->statusdefRepository->getStatusdefId($this->statusdefRepository::NEW);
            $this->logger->info('$statusDefId' . json_encode($statusDefId), [__METHOD__, __LINE__]);
            $objectRegIdOfWebhookQueue = $this->objectRegisterRepository->create($uniqueObjectTypeId, $statusDefId);
            $this->logger->info('$objectRegIdOfWebhookQueue' . json_encode($objectRegIdOfWebhookQueue),
                [__METHOD__, __LINE__]);
            $company = $this->companyRepository->getCompanyDetails(0, (int)$accountNumber);
            $this->logger->info('$company' . json_encode($company), [__METHOD__, __LINE__]);
            if (empty($company) || !isset($company[ 'id' ]) || (int)$company[ 'id' ] <= 0) {
                // Chunk wont be processed to DB.
                // but chunk will be present in redis server.
                continue;
            }
            if ($objectRegIdOfWebhookQueue === null) {
                $objectRegIdOfWebhookQueue = 0;
                $message = 'Failed to create an object registration for webhookQueue.';
            }
            if (empty($objRegIdOfWebhook)) {
                $objRegIdOfWebhook = 0;
                $message = "Failed to get the object registerId from base section of payload";
            }
            $this->logger->info('before insert saveChunkDataToWebHookQueue');
            // Insert to webhookQueue Table.
            if (!$this->webhookQueueRepository->insert($accountNumber, (int)$company[ 'id' ],
                $objectRegIdOfWebhookQueue, (int)$objRegIdOfWebhook, $chunkValue,
                $statusTypeNew, $start, $end, $message, $modification)) {
                $this->logger->error('Failed to insert to webhook_queue table ' .
                                     json_encode([
                                         'accountNumber' => $accountNumber,
                                         'dataToProcess' => $chunkValue,
                                         'status' => $statusTypeNew,
                                     ], JSON_THROW_ON_ERROR));
                continue;
            }
            // after inserting data to table, delete the chunk details from redis.
            $this->redisService->delete($chunkKey);
        }
    }
}
<?php


namespace App\Controller;


use App\Repository\CompanyRepository;
use App\Repository\ProcesshookRepository;
use App\Services\AIOServices\ProcesshookServices;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\ProcesshookTypes;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProcesshookController
{
    private LoggerInterface $logger;
    private ProcesshookServices $processhookService;
    private ProcesshookRepository $processhookRepository;
    private ProcesshookTypes $types;
    private ToolServices $toolServices;
    private CompanyRepository $companyRepository;

    public function __construct(
        LoggerInterface       $logger,
        ProcesshookServices   $processhookService,
        ProcesshookRepository $processhookRepository,
        ProcesshookTypes      $processhookTypes,
        ToolServices          $toolServices,
        CompanyRepository     $companyRepository
    )
    {
        $this->logger = $logger;
        $this->processhookService = $processhookService;
        $this->processhookRepository = $processhookRepository;
        $this->types = $processhookTypes;
        $this->toolServices = $toolServices;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @Route("/workflow/init", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws \JsonException
     * @author Aki
     */
    public function workflow(
        Request $request
    ): Response
    {
        // This is forward Request from webhook/receive service.
        $webReceive = $request->get('wb-receive');
        // This is request generated from campaign while creating contact.
        $content = $request->getContent();
        $en_data = empty($webReceive) ? $content : $webReceive;
        $this->logger->info('contact Controller ' . json_encode($en_data, JSON_THROW_ON_ERROR));
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $data = $this->toolServices->decryptContentData($en_data);
            if (!isset($data[SourceApiTypes::BASE][SourceApiTypes::APIKEY],  // TODO: provide apikey in page: line may be removed to make it work
                $data[SourceApiTypes::BASE][SourceApiTypes::AccountNumber],
                $data[SourceApiTypes::BASE][SourceApiTypes::ObjectRegisterId])) {
                throw new RuntimeException  ('Invalid APIKey or accountNo or ObjectRegisterId');
            }
            // get CompanyDetails
            $company = $this->companyRepository->getCompanyDetails(0,
                $data[SourceApiTypes::BASE][SourceApiTypes::AccountNumber]);
            if (isset($data['base']['workflow_id']) && ($data['base']['workflow_id'] !== null)) {
                $workflowId = $data['base']['workflow_id'];
                if (!$this->processhookService->integrateWorkflow($workflowId, $data, $company['id'])) {
                    throw new RuntimeException('Failed to Integrate workflow');
                }
            }
            return $this->toolServices->setResponseWithMessage($response, 'Workflow Executed',
                true);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->toolServices->setResponseWithMessage($response, $e->getMessage(), false, []);
        }
    }
}
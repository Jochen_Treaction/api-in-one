<?php

namespace App\Controller;

ini_set('memory_limit', - 1);
ini_set('max_execution_time', - 1);
ini_set('upload_max_size', - 1);
ini_set('post_max_size', - 1);


use App\Message\IntegrationSync\IntegrationSyncToEMIO;
use App\Message\ShopDataQueue\ProcessChunkDataFromShopDataQueue;
use App\Message\ShopDataQueue\ProcessChunkDataToIntegration;
use App\Message\ShopDataQueue\ProcessChunkDataToWorkFlow;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\ContactSyncToIntegrationRepository;
use App\Repository\CustomFieldRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ShopDataQueueRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Services\AIOServices\ContactServices;
use App\Services\AIOServices\ContactLastSyncService;
use App\Services\ContactSyncToIntegrations\ContactSyncEmailCache;
use App\Services\ContactSyncToIntegrations\ContactSyncLastIdCache;
use App\Services\ContactSyncToIntegrations\ContactSyncToIntegrationService;
use App\Services\CronJobServices\ContactSyncToIntegration\ContactSyncToIntegrationsCronJobProcessor;
use App\Services\CronJobServices\ContactSyncToIntegration\ContactSyncToIntegrationsCronJobStatus;
use App\Services\CronJobServices\CronJobRetryService\ContactSyncToIntegrationCronJobRetry;
use App\Services\MessageBusServices;
use App\Services\SemaphoreService;
use App\Services\ToolServices;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CronJobController extends AbstractController
{


    /**
     * @Route("/cron/webhookqueue/execute", name="webhook_queue_action", methods={"GET"})
     * @param ToolServices $toolServices
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param ShopDataQueueRepository $webhookQueueRepository
     * @param MessageBusInterface $messageBus
     * @param MessageBusServices $messageBusService
     * @param SemaphoreService $semaphoreService
     * @return JsonResponse
     * @throws \JsonException
     * @author Pradeep
     */
    public function webhookQueueAction(
        ToolServices $toolServices,
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        ShopDataQueueRepository $webhookQueueRepository,
        MessageBusInterface $messageBus,
        MessageBusServices $messageBusService,
        SemaphoreService $semaphoreService
    ): JsonResponse {

        $semaphoreKey = 'semaphore-external-data-queue';
        if ($semaphoreService->isLocked($semaphoreKey)) {
            $t = $semaphoreService->get($semaphoreKey);
            return $this->json('Semaphore is not free for next job ' . json_encode($t), Response::HTTP_OK);
        }
        // Get the next Job details.
        $job = $webhookQueueRepository->getNextJobToProcess();
        try {
            $t1 = microtime(true);
            if (empty($job[ 'status' ]) ||
                $job[ 'status' ] !== $webhookQueueRepository::WB_QUEUE_STATUS_TYP_NEW) {
                return $this->json('Currently no new Jobs to process', Response::HTTP_OK);
            }
            // Initiate Semaphore service.
            $semaphoreService->set($semaphoreKey, true);
            if (!isset($job[ 'id' ], $job[ 'account_id' ], $job[ 'data_to_process' ])) {
                throw new RuntimeException('Invalid accountId or data_to_process is provided');
            }
            $id = $job[ 'id' ];
            $webhookObjectRegId = $job[ 'webhook_objectregister_id' ];
            $accountId = (int)$job[ 'account_id' ];
            $dataToProcess = $job[ 'data_to_process' ];

            $company = $companyRepository->getCompanyDetails(0, $accountId);
            $campaign = $campaignRepository->getCampaignDetailsByObjectRegisterId($webhookObjectRegId);

            $data = $toolServices->decryptContentData($dataToProcess, true);
            if (empty($company) || empty($campaign) || empty($data) ||
                !isset($campaign[ 'id' ], $company[ 'id' ], $data[ 'contacts' ])) {
                throw new RuntimeException('Failed to fetch Campaign or Company information');
            }
            $chunkData = $data[ 'contacts' ];

            if (!$webhookQueueRepository->updateJobStatus($id, $webhookQueueRepository::WB_QUEUE_STATUS_TYP_LOCKED)) {
                throw new RuntimeException('Failed to update webhook queue status to ' .
                                           json_encode([
                                               'id' => $id,
                                               'status ' => $webhookQueueRepository::WB_QUEUE_STATUS_TYP_LOCKED,
                                           ], JSON_THROW_ON_ERROR));
            }

            // This has to be under bus handler.
            $message = new ProcessChunkDataFromShopDataQueue($id, $accountId, $webhookObjectRegId, $chunkData, $company,
                $campaign);
            $result = $messageBusService->dispatchMessage($message);
            if (!$result[ 'status' ]) {
                throw new RuntimeException($result[ 'message' ]);
            }

            // Integrate the Chunk Data to 3rd Party Services, eMIO, Hubspot, Digistore,
            $message = new ProcessChunkDataToIntegration($company[ 'id' ], $campaign[ 'id' ], $chunkData);
            $result = $messageBusService->dispatchMessage($message);
            if (!$result[ 'status' ]) {
                throw new RuntimeException($result[ 'message' ]);
            }

            $workFlowId = $data[ 'base' ][ 'workflow_id' ] ?? 0;
            $chunkBaseData[ 'base' ] = $data[ 'base' ];
            $executeWorkFlow = true;
            // @WEB-5798: workflow will not be executed when base section has 'execute_workflow' with false,
            if (isset($data[ 'base' ][ 'execute_workflow' ]) && !$data[ 'base' ][ 'execute_workflow' ]) {
                $executeWorkFlow = false;
            }

            if ($executeWorkFlow) {
                // ProcessChunkDataToWorkFlows.
                // fetch the chunk of data while processing the workflows.
                $message = new ProcessChunkDataToWorkFlow($company[ 'id' ], $campaign[ 'id' ], $workFlowId, $chunkData,
                    $chunkBaseData);
                $result = $messageBusService->dispatchMessage($message);
                if (!$result[ 'status' ]) {
                    throw new RuntimeException($result[ 'message' ]);
                }
            }

            // update the webhookQueue table with status as Done, for the row entry.
            if (!$webhookQueueRepository->updateJobStatus($id, $webhookQueueRepository::WB_QUEUE_STATUS_TYP_DONE,
                'processed chunk successfully')) {
                throw new RuntimeException('Failed to update webhook queue status to ' . $webhookQueueRepository::WB_QUEUE_STATUS_TYP_DONE);
            }
            $semaphoreService->set($semaphoreKey, false);
            return $this->json('Contacts synchronized successfully', Response::HTTP_OK);

        } catch (RuntimeException|Exception $e) {
            $webhookQueueRepository->updateJobStatus($id, $webhookQueueRepository::WB_QUEUE_STATUS_TYP_ERROR,
                $e->getMessage());
            $semaphoreService->set($semaphoreKey, false);
            return $this->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Route("/cron/integration/sync", name="integration_sync", methods={"GET"})
     * @param ContactSyncToIntegrationsCronJobStatus $contactSyncToIntegrationsCronJobStatus
     * @param ContactSyncToIntegrationsCronJobProcessor $contactSyncToIntegrationsCronJobProcessor
     * @param ContactSyncToIntegrationService $contactSyncToIntegrationService
     * @param ContactSyncLastIdCache $contactSyncLastIdCache
     * @param ContactSyncEmailCache $contactSyncEmailCache
     * @param ContactLastSyncService $contactLastSyncService
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @param CustomFieldRepository $customFieldRepository
     * @param ContactSyncToIntegrationCronJobRetry $contactSyncToIntegrationCronJobRetry
     * @param MessageBusServices $messageBusService
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     */
    public function integrationSyncAction(
        ContactSyncToIntegrationsCronJobStatus $contactSyncToIntegrationsCronJobStatus,
        ContactSyncToIntegrationsCronJobProcessor $contactSyncToIntegrationsCronJobProcessor,
        ContactSyncToIntegrationService $contactSyncToIntegrationService,
        ContactSyncLastIdCache $contactSyncLastIdCache,
        ContactSyncEmailCache $contactSyncEmailCache,
        ContactLastSyncService $contactLastSyncService,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        CustomFieldRepository $customFieldRepository,
        ContactSyncToIntegrationCronJobRetry $contactSyncToIntegrationCronJobRetry,
        MessageBusServices $messageBusService,
        LoggerInterface $logger
    ): JsonResponse {

        $chunkLength = 1000;

        try {

            $logger->info('Started');
            if (!$contactSyncToIntegrationsCronJobProcessor->isJobPresent() ||
                $contactSyncToIntegrationsCronJobStatus->isRunning()) {
                $currentJob = $contactSyncToIntegrationsCronJobProcessor->getCurrent();
                if (empty($currentJob)) {
                    return $this->json('No Jobs to Process');
                }
                if ($contactSyncToIntegrationCronJobRetry->isLastChunkProcessed($currentJob[ 'updated' ])) {
                    $contactSyncToIntegrationCronJobRetry->setToRetry($currentJob[ 'id' ]);
                }
                return $this->json('No Jobs to Process');
            }

            $logger->info('contact-sync-started');
            $job = $contactSyncToIntegrationsCronJobProcessor->getNext();
            $logger->info('JOB Details ' . json_encode($job));
            if (!isset($job[ 'id' ], $job[ 'company_id' ], $job[ 'start_contact_id' ], $job[ 'last_contact_id' ])) {
                return $this->json('No Jobs to Process');
            }

            $logger->info('contact-sync-started-got-record');
            $lastIdCacheKey = $contactSyncLastIdCache->createOrGetKey($job[ 'id' ]);
            $emailCacheKey = $contactSyncEmailCache->createOrGetKey($job[ 'id' ]);

            if (!($lastIdCacheKey) || !($emailCacheKey)) {
                throw new RuntimeException('Failed to create redisKeys');
            }
            $logger->info('contact-sync-started-got-record');
            $logger->info('5');
            // update the status to working.
            $contactSyncToIntegrationsCronJobStatus->Lock($job[ 'id' ]);

            // fetchContacts To Sync
            $account = $job[ 'company_id' ];
            $startContactId = $job[ 'start_contact_id' ];

            $logger->info('contact-sync-locked');

            // check the job status.
            // if job status is retry => get the maxContactid from RetryService.

            $maxContactId = $contactSyncLastIdCache->get();
            if (empty($maxContactId)) {
                $maxContactId = $job[ 'last_contact_id' ];
            }
            $logger->info('contact-sync-started-got-max-contact-id');

            // fetch ProcessedEMailAddress.
            $unfilteredContact = $contactSyncToIntegrationService->getContactsForSync($account, $startContactId,
                $maxContactId, $chunkLength);
            $logger->info('contact-sync-unfilterred-contact ' . json_encode($unfilteredContact));

            // unfiltered contacts could be empty when all the contacts between the range are synchronized.
            if (empty($unfilteredContact)) {

                // All the contacts are synchronized to integration.
                $logger->info('contact-sync-unfilterred-contact-empty');
                // update status to done.
                $contactSyncLastIdCache->reset();
                $contactSyncEmailCache->reset();
                $contactSyncToIntegrationsCronJobStatus->completed($job[ 'id' ]);

                return $this->json('Successfully synchronized contacts');
            }

            // Contacts are not empty.
            // get the last contact field id. for next retrival of the range.
            $contactLastId = $contactSyncToIntegrationService->getLastId($unfilteredContact);

            // get the MIO standard fields
            $standardFields = $standardFieldMappingRepository->getMIOStandardFieldAsArray();

            // get the MIO Custom fields
            $customFields = $customFieldRepository->getCustomFieldsNames($account);
            $logger->info('contact-sync-fetchted-stadnrd-custm');

            // filter the contacts with standard and customfields.
            // remove the duplicate email address .
            $filteredContacts = $contactSyncToIntegrationService->filterDuplicateRecordsForSync($unfilteredContact,
                $standardFields, $customFields);

            $logger->info('contact-sync-filtered-contacts ' . count($filteredContacts));

            // send the message to EMIO integration.
            $message = new IntegrationSyncToEMIO($account, $filteredContacts);
            $messageBusService->dispatchMessage($message);
            $logger->info('contact-sync-sent-message');
            // check the return message from Handler.
            // when the message is false, don't update the

            // save the last contact id to redis.
            $contactSyncLastIdCache->set($contactLastId);
            $logger->info('contact-sync-set-last-contact-id');

            //Update last_sync field for the contacts which are synchronized to eMIO.
            $contactIds = $contactSyncToIntegrationService->getContactIdAsArray($filteredContacts);
            if (empty($contactIds) || !$contactLastSyncService->updateForChunk($contactIds)) {
                throw new RuntimeException('Failed to update lastSync for contactIds');
            }

            // collect the emails from filteredContacts and updateEmailCache.
            if (!$contactSyncToIntegrationService->updateEmailCache($filteredContacts)) {
                throw new RuntimeException('Failed to update emailCache');
            }

            // Unlock the semaphore and update the status for the row.
            $contactSyncToIntegrationsCronJobStatus->unLock($job[ 'id' ]);
            $logger->info('contact-sync-unlocked');

            return $this->json('Successfully synchronized contacts');

        } catch (RuntimeException|Exception  $e) {

            // remove all the cache.
            $contactSyncLastIdCache->reset();
            $contactSyncEmailCache->reset();
            $message = $e->getMessage();
            if (empty($message)) {
                $message = "failed to synchronize.";
            }
            // Unlock the semaphore and update the error.
            $contactSyncToIntegrationsCronJobStatus->error($job[ 'id' ], $message);
            return $this->json('Something went wrong while synchronizing contacts to eMIO :' . $e->getMessage());
        }
    }
}
<?php


namespace App\Controller;


use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomFieldRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\LeadCustomFieldContentRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ShopDataQueueRepository;
use App\Services\AIOServices\AIOFraudDetectionService;
use App\Services\AIOServices\AIOService;
use App\Services\MSMailServices\MsMailConfigurationService;
use App\Services\MSMIOServices\MSMIOServices;
use App\Services\RedisService;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\MioStandardFieldTypes;
use App\Types\MsMailInterfaceTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Redis;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DataTypesRepository;
use App\Services\MSMailServices\MSMailServices;

class BastelController extends AbstractController
{

    /**
     * @Route("/test/msmail/config", methods={"GET"})
     * @return void
     */
    public function testMsMailConfig(MsMailInterfaceTypes $msMailInterfaceTypes):JsonResponse
    {
        $initialConfigArray = $msMailInterfaceTypes->getInitialConfigArray();
        return $this->json(['InitialConfigArray' => $initialConfigArray]);
    }



    /**
     * @Route("/test/redis", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function testRedis(): JsonResponse
    {
       $h =  $this->getParameter('app.redis.host');
       $p =  $this->getParameter('app.redis.port');
       $u =  $this->getParameter('app.redis.user');
       $pw =  $this->getParameter('app.redis.pw');
        $redis = new Redis();
        $conn = $redis->connect($h, $p);
        $auth = $redis->auth([$u, $pw]);

        return $this->json(['$conn'=>$conn, '$auth'=> $auth]);
    }


    /**
     * @Route("/test/redis/set", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function set(): JsonResponse
    {

        /*
         * INSTALL redis on server AS ROOT
         * apt update
         * add-apt-repository ppa:redislabs/redis
         * apt-get update
         * apt-get install redis
         *
         * then configure /etc/redis/redis.conf =>
         * ################################## NETWORK #####################################
            bind 0.0.0.0
            port <some other not commonly used port number>
            ################################# GENERAL #####################################
            supervised systemd
            ################################## SECURITY ###################################
            # configure users access - PHP CURRENTLY DOES NOT SUPPORT REDIS CONNECT WITH user and password !!!
            user apiInOne on +@all -@dangerous ~* >somelongpassword
            user marketingInOne on +@all -@dangerous ~* >somelongpassword
            user msBackendInOne on +@all -@dangerous ~* >somelongpassword
            # set pw for default user - !!! PHP ONLY SUPPORTS THIS PW ONLY  => $redis = new Redis(); $redis->connect(host, port); $redis->auth(<some ultra long password min 64 chars with upper lower case chars, numbers, special chars>)
            requirepass <some ultra long password min 64 chars with upper lower case chars, numbers, special chars>
         *
         *  run this command as root:
         *  systemctl restart redis-server
         *
         *  then try to connect and run some commands (set keys, get keys, ...)
         */

        // TODO: Configure host ip, port and pw in services_dev.yaml (redis NOT setup!), services_testaio.yaml (redis NOT setup!), services_prod.yaml (redis setup on PROD @ IP 91.250.80.240!),
        // see      app.redis.host: ''
        //          app.redis.port: '8341'
        //          app.redis.auth: ''
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379); // TODO: change to '91.250.80.240',  8341

        // $redis->auth($password); // TODO: at the moment you'll have to use AUTH PW of "default" user (SUPERUSER) => see KeePass treaction_backend_keys_*.*
        $status = $redis->set('DESC-20220210071902-6204ae56791711.00788788-72809e2a32fa828346594118d3de1c2c',
            'eyJiYXNlIjp7ImFwaWtleSI6IkUyMENBRDQ4LUNDQ0MtQjQ2RS05QzQ1LTdCOUZEQTVDRUUxRSIsImFjY291bnRfbnVtYmVyIjoiMTUwNyIsImV4ZWN1dGVfd29ya2Zsb3ciOmZhbHNlLCJ1dWlkIjoiZWY4MWFiZmEtOGU1ZS0xMWVjLWJjN2QtMzZmMDE1OTRhNzg4In0sImNvbnRhY3RzIjpbeyJzdGFuZGFyZCI6W3siZW1haWwiOiJtb3h1Y2Fmb2hAbWFpbGluYXRvci5jb20iLCJyZXF1aXJlZCI6InRydWUiLCJmaWVsZF9uYW1lIjoiZW1haWwiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJtcyIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiT2xnYSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJLaXJrIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImJpcnRoZGF5IjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQWQgcXVhcyBwcmFlc2VudGl1bSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiI3Njk5MyIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjaXR5IjoiVGVtcG9yZSBxdWlzIG5vc3RydSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjb3VudHJ5IjoiRGV1dHNjaGxhbmQiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJ0b3RhbF9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF95ZWFyX29yZGVyX25ldF92YWx1ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9vcmRlcl9kYXRlIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiaXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJkb2lfaXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJkb2lfdGltZXN0YW1wIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsib3B0aW5faXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJvcHRpbl90aW1lc3RhbXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn1dfSx7InN0YW5kYXJkIjpbeyJlbWFpbCI6Im1henV3ZWxAbWFpbGluYXRvci5jb20iLCJyZXF1aXJlZCI6InRydWUiLCJmaWVsZF9uYW1lIjoiZW1haWwiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJtciIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiSmluIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfbmFtZSI6IlN1bW1lcnMiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiYmlydGhkYXkiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJzdHJlZXQiOiJFeGVyY2l0YXRpb24gYXV0ZW0gaSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiIxNzgzOCIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjaXR5IjoiVXQgZW5pbSBudWxsYSB2b2x1cHQiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InNtYXJ0X3RhZ3MiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsidG90YWxfb3JkZXJfbmV0X3ZhbHVlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfZGF0ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX25vIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfeWVhcl9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZmlyc3Rfb3JkZXJfZGF0ZSI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImlwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZG9pX2lwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZG9pX3RpbWVzdGFtcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Im9wdGluX2lwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsib3B0aW5fdGltZXN0YW1wIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9XX0seyJzdGFuZGFyZCI6W3siZW1haWwiOiJmaW5vdmVzYWNlQG1haWxpbmF0b3IuY29tIiwicmVxdWlyZWQiOiJ0cnVlIiwiZmllbGRfbmFtZSI6ImVtYWlsIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoibXIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZmlyc3RfbmFtZSI6IkJyZW5uYSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJNYXlzIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImJpcnRoZGF5IjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiRW5pbSBmdWdpdCBtb2RpIHNvbCIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiI0MTY5OCIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjaXR5IjoiUXVhbSBpZCBtb2xlc3RpYXMgbCIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjb3VudHJ5IjoiRGV1dHNjaGxhbmQiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJ0b3RhbF9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF95ZWFyX29yZGVyX25ldF92YWx1ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9vcmRlcl9kYXRlIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiaXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJkb2lfaXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJkb2lfdGltZXN0YW1wIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsib3B0aW5faXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJvcHRpbl90aW1lc3RhbXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn1dfSx7InN0YW5kYXJkIjpbeyJlbWFpbCI6ImdhaGlAbWFpbGluYXRvci5jb20iLCJyZXF1aXJlZCI6InRydWUiLCJmaWVsZF9uYW1lIjoiZW1haWwiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJtciIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiUmlhIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfbmFtZSI6IkVsbGlzIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImJpcnRoZGF5IjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiTW9kaSBzaW1pbGlxdWUgbW9kaSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiI1MTk3MSIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjaXR5IjoiVm9sdXB0YXMgZXQgY29uc2VjdGUiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InNtYXJ0X3RhZ3MiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsidG90YWxfb3JkZXJfbmV0X3ZhbHVlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfZGF0ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX25vIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfeWVhcl9vcmRlcl9uZXRfdmFsdWUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZmlyc3Rfb3JkZXJfZGF0ZSI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImlwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZG9pX2lwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZG9pX3RpbWVzdGFtcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Im9wdGluX2lwIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsib3B0aW5fdGltZXN0YW1wIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9XX0seyJzdGFuZGFyZCI6W3siZW1haWwiOiJjZWt5cWVAbWFpbGluYXRvci5jb20iLCJyZXF1aXJlZCI6InRydWUiLCJmaWVsZF9uYW1lIjoiZW1haWwiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJtciIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiSm9zZXBoIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfbmFtZSI6IkxhbmUiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiYmlydGhkYXkiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJzdHJlZXQiOiJQYXJpYXR1ciBNYWduYW0gYWRpIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImhvdXNlX251bWJlciI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6Ijk4OTU1IiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNpdHkiOiJWb2x1cHRhcyBpbmNpZGlkdW50IiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNvdW50cnkiOiJEZXV0c2NobGFuZCIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJzbWFydF90YWdzIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX2RhdGUiOiIiLCJyZXF1aXJlZCI6InRydWUiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9ubyI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX25ldF92YWx1ZSI6IiIsInJlcXVpcmVkIjoidHJ1ZSIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjoiIiwicmVxdWlyZWQiOiJ0cnVlIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X29yZGVyX2RhdGUiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJpcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImRvaV9pcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImRvaV90aW1lc3RhbXAiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJvcHRpbl9pcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Im9wdGluX3RpbWVzdGFtcCI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19XX0=');
        //$status = $redis->hset('Key', 'hashKey', 'value');
        return $this->json($status);
    }

    /**
     * @Route("/test/redis/get", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function getValue(): JsonResponse
    {
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        $value = $redis->get('DESC-20220210071902-6204ae56791711.00788788-72809e2a32fa828346594118d3de1c2c');
        //$value = $redis->hGet('Key', 'hashKey');
        return $this->json($value);
    }

    /**
     * @Route("/test/redis/del", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function flush(): JsonResponse
    {
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        //    $value = $redis->get('MyKey');
        $value = $redis->flushAll();
        return $this->json($value);
    }

    /**
     * @Route("/test/redis/get/all", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function getAll(): JsonResponse
    {
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        //$value = $redis->get('DESC-20220210071902-6204ae56791711.00788788-72809e2a32fa828346594118d3de1c2');
        $value = $redis->keys('*');
        return $this->json($value);
    }



    /**
     * @Route("/test/sd/fields", methods={"GET"})
     * @return JsonResponse
     * @author Pradeep
     */
    public function testSDFields(MIOStandardFieldRepository $mioStandardFieldRepository): JsonResponse
    {
        $value = $mioStandardFieldRepository->getFieldNameAsArray();
        return $this->json($value);
    }



    /**
     * @Route("/check/data", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @author Pradeep
     */
    public function getChunks(Request $request, LoggerInterface $logger): JsonResponse
    {
        $content = $request->getContent();
        $syncMode = $request->headers->get('SyncMode');
        $chunkId = $request->headers->get('ChunkId');
        $chunkKey = $request->headers->get('ChunkKey');
        $totalChunks = $request->headers->get('TotalNumberOfChunks');
        $logger->info('checkData' . json_encode([
                'syncMode' => $syncMode,
                'chunkId' => $chunkId,
                'chunkKey' => $chunkKey,
                'totalChunks' => $totalChunks,
                'content' => $content,
            ], JSON_THROW_ON_ERROR));
        return $this->json($content);
    }


    /**
     * @param Request $request
     * @param DataTypesRepository $dataTypesRepository
     * @return JsonResponse
     * @Route("/check/alive", methods={"GET"})
     *
     * @internal route to check AIO installation with database connection
     */
    public function checkAlive(Request $request, DataTypesRepository $dataTypesRepository): JsonResponse
    {
        $dateTime = new \DateTime();
        $dt = $dataTypesRepository->getAllDatatypes();
        return $this->json(array_merge([
                'status' => true,
                'msg' => 'I am alive',
                'yourIP' => $request->getClientIp(),
                'currentTime' => date('Y-m-d H:i:s'),
                'timezone' => $dateTime->format('T'),
            ],
            ['dataTypes' => $dt]
        ));
    }


    /**
     * @param Request $request
     * @param DataTypesRepository $dataTypesRepository
     * @return JsonResponse
     * @Route("/check/redis/alive", methods={"GET"})
     *
     * @internal route to check AIO installation with database connection
     */
    public function checkRedisAlive(Request $request, RedisService $redisService): JsonResponse
    {
        $redisService->set('test','abcd');
        $testVariable = $redisService->get('test');
        $userDetails = $redisService->getUserDetails();
        return $this->json(["get result" => $testVariable, "userDetails" => $userDetails]);
    }


    /**
     * @param Request $request
     * @param DataTypesRepository $dataTypesRepository
     * @return JsonResponse
     * @Route("/mail/config/kl1jahs9ddf8haert", methods={"GET"})
     *
     * @internal hidden route to check email server configuration
     */
    public function getEmailServerConfiguration(Request $request, MSMailServices $mailServices): JsonResponse
    {
        $mc = $mailServices->fetchMailerConfiguration();
        return $this->json($mc);
    }


    /**
     * @param Request $request
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return JsonResponse
     * @Route("/or", methods={"GET"})
     *
     */
    public function getObjRegEntry(Request $request, ObjectRegisterRepository $objectRegisterRepository): JsonResponse
    {
        //@comment- This is test function to test PDO functionality
        //todo:Remove this funciton after bugfix and testing

        $record = $objectRegisterRepository->getLeadFromObjectRegister(1);
        return $this->json($record);
    }


    /**
     * @param Request $request
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return JsonResponse
     * @Route("/post/jotform", methods={"POST"})
     *
     */
    public function getSampleDataFromJotForms(Request $request, ObjectRegisterRepository $objectRegisterRepository, MioStandardFieldTypes $mioStandardFieldTypes, MIOStandardFieldRepository $standardFieldRepository, LoggerInterface $logger): JsonResponse
    {
        /*
    [submission_id] => 5246186157911874913
    [formID] => 220882953058362
    [ip] => 95.222.30.197
    [apikey] => b48ea9cc-3e65-11ec-b9ea-c43772f7ef7d
    [account_number] => 1022
    [contact_producttest] => Array
        (
            [0] => Gratis-Beauty-Geschenkset|https://www.jotform.com/uploads/svenbanks/form_files/gratisprodukte-jotform-produktbild-3.61e58ea6b637d2.10971794.jpg
        )

    [contact_fieldofinterest] => Array
        (
            [0] => Finanzen & Geldanlage|https://www.jotform.com/uploads/svenbanks/form_files/k1.61e59005d06179.03199056.jpg
            [1] => Shopping, Lifestyle, Beauty & Mode|https://www.jotform.com/uploads/svenbanks/form_files/k2.61e59015a6db39.35938390.jpg
            [2] => Reisen & Hotels|https://www.jotform.com/uploads/svenbanks/form_files/k4.61e590253b07f1.21225477.jpg
            [3] => Haustiere|https://www.jotform.com/uploads/svenbanks/form_files/k7.61e590444dba64.93282721.jpg
            [4] => Spiele, Unterhaltung & Lotto|https://www.jotform.com/uploads/svenbanks/form_files/k8.61e5904c4513c7.54935287.jpg
        )

    [contact_salutation] => Herr
    [contact_firstname] => Aravind
    [contact_lastname] => Karri
    [contact_street] => frankfurter str 46
    [contact_postalcode] => 34121
    [contact_birthday] => Array
        (
            [0] => 21
            [1] => 11
            [2] => 1992
        )

    [contact_email] => aravind.karri@treaction.net
    [contact_phone] => Array
        (
            [0] => (173) 888-8888
        )

    [contact_newspaper] => Array
        (
            [0] => FOCUS (0€/6 Ausgaben - sonst 2,30€)|https://www.jotform.com/uploads/svenbanks/form_files/Focus.61e5928338a102.76035079.jpeg
        )

    [duhast] => Array
        (
            [0] => Ich akzeptiere die Liefervereinbarung
            [1] => Ich akzeptiere die Nutzungsbedingungen
        )

)
         */
        $data = $request->getContent();
        $postArray = json_decode($data, true, 20, JSON_OBJECT_AS_ARRAY);
        $postArrayKeys = array_keys($postArray);
        $receiveContact = [];
        $receiveContact['base']['apikey'] = $postArray['apikey'];
        $receiveContact['base']['account_number'] = $postArray['account_number'];
        $receiveContact['base']['uuid'] = $postArray['uuid'];

        foreach ($postArrayKeys as $jotFormKey ) {

            if( array_key_exists($jotFormKey, $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS)) {
                $mappedKey = $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS[$jotFormKey];
                $mappedKeyParts = explode('_', $mappedKey); //  contact_salutation => 0:contact, 1:salutation

                // get mio.miostandardfield.fieldtype
                $fieldType =  $standardFieldRepository->getFieldTypeByFieldName($mappedKey);
                $receiveContact[$mappedKeyParts[0]][$fieldType][] = $this->setKeyValuePair($mappedKeyParts[1], $postArray[$jotFormKey]);

            } elseif( array_key_exists($jotFormKey, $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS)) {

                $mappedKey = $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS[$jotFormKey];
                $mappedKeyParts = explode('_', $mappedKey); //  contact_salutation => 0:contact, 1:salutation
                $fieldType = $standardFieldRepository->getFieldTypeByFieldName($mappedKey);

                if (is_array($postArray[$jotFormKey])) {
                    $tempArray = [];
                    foreach ($postArray[$jotFormKey] as $value) {
                        $tempArray[] = explode('|', $value)[0];
                    }
                    $value = implode(',', $tempArray); // get as list
                    $receiveContact[$mappedKeyParts[0]][$fieldType][] = $this->setKeyValuePair($mappedKeyParts[1], $value);
                } else {
                    $receiveContact[$mappedKeyParts[0]][$fieldType][] = $this->setKeyValuePair($mappedKeyParts[1], $postArray[$jotFormKey]);
                }
            } else { // handle customfields
                $mappedKey = $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS[$jotFormKey];
                $mappedKeyParts = explode('_', $mappedKey); //  contact_salutation => 0:contact, 1:salutation
                $value = (is_array($postArray[$jotFormKey])) ? implode(',', $postArray[$jotFormKey]) : $postArray[$jotFormKey];
                $receiveContact['custom'][] = $this->setKeyValuePair($jotFormKey, $value);
            }
        }

        $logger->info('$receiveContact content',[$receiveContact, __METHOD__, __LINE__]);

        // see it as resonse
        return $this->json($receiveContact);

        // send it to /v2.0/contact/create
        // return $this->forward('App\Controller\ContactController::createContact', ['wb-receive' => base64_encode($receiveContact)], ['client-ip' => $request->getClientIp()] );
    }

    private function setKeyValuePair(string $field_name_value, string $field_value_value):array
    {
        return [
            'field_name' => $field_name_value,
            'field_value' => $field_value_value,
            'regex' => '.*', // this must be defined in table mio.miostandardfield
            'required' => 0, // information does not come from JOTForms
            'datatype' => 'Text', // neither provided by JOTForms, nor defined in table mio.miostandardfield
        ];
    }

    //todo: check DB connection




}

<?php


namespace App\Controller;


use App\Repository\CompanyRepository;
use App\Repository\IntegrationsRepository;
use App\Services\AIOServices\AIOService;
use App\Services\AIOServices\GeneralServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\UniqueObjectTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MIOController extends AbstractController
{
    /**
     * @Route("/blacklist", methods={"GET","POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @param AIOService $AIOService
     * @return JsonResponse
     * @throws JsonException
     * @author Pradeep
     */
    public function blackListing(
        Request $request,
        LoggerInterface $logger,
        MSMIOServices $MSMIOServices,
        AIOService $AIOService
    ): JsonResponse {
    	$status = ['change' => 'this']; // TODO: @Pradeep: change that to what you need
        $en_data = $request->get('data');
        $data = $AIOService->formatRequestData($en_data);
        if (empty($data[ 'api_key' ]) || empty($data[ 'blk_id' ]) || empty($data[ 'blk_email' ])) {
            return $this->json('Missing apikey or blacklist id or email list');
        }
        // $MSMIOService = new MSMIOServices($data[ 'api_key' ]); // does not work with deployed version 2021-03-09
		try {
			//$MSMIOService = new MSMIOServices();
			$status = $MSMIOServices->blackListEmail((int)$data['blk_id'], $data['blk_email'], $data['blk_imp_name']);
		} catch (Exception $e) {
        	; // currently do nothing, but don't block
		}
		return $this->json($status);
    }

    /**
     * @Route("/v2.0/blacklist/", methods={"POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @param MSMIOServices $MSMIOServices
     * @param ToolServices $toolServices
     * @param CompanyRepository $companyRepository
     * @param GeneralServices $generalServices
     * @param IntegrationsRepository $integrationsRepository
     * @return Response
     * @author Pradeep
     */
    public function blacklistEmail(
        Request $request,
        LoggerInterface $logger,
        MSMIOServices $MSMIOServices,
        ToolServices $toolServices,
        CompanyRepository $companyRepository,
        GeneralServices $generalServices,
        IntegrationsRepository $integrationsRepository
    ) {
        $content = $request->getContent();
        $response = new Response();
        try {
            $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
            $data = $toolServices->decryptContentData($content);
            if (!isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::APIKEY ],
                $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ])) {
                throw new RuntimeException  ('Invalid APIKey or accountNo or ObjectRegisterId');
            }
            $companyAccountNo = $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ];
            $companyApiKey = $data[ SourceApiTypes::BASE ][ SourceApiTypes::APIKEY ];
            $company = $companyRepository->getCompanyDetails(0, $companyAccountNo);
            if (! $generalServices->validateCIOAPIKeyWithCompanyId($companyApiKey, $company['id'])) {
                throw new RuntimeException('Failed to validate APIKey with Company Id');
            }

            if(! isset($data['blacklist']['email']) || empty($data['blacklist']['email']) ) {
                throw new RuntimeException('no emails provided for blacklisting');
            }
            $emails = json_encode($data[ 'blacklist' ][ 'email' ], JSON_THROW_ON_ERROR);

            //return $this->json(['accNo'=>$companyAccountNo,'apikey'=>$companyApiKey]);
            // validate APIKey with AccountNumber.

            // get companyId
            $company = $companyRepository->getCompanyDetails(0, $companyAccountNo);
            $eMIOConfig = $integrationsRepository->getIntegrationConfigForCompany($company['id'],
                UniqueObjectTypes::EMAILINONE);
            if(empty($eMIOConfig)) {
                throw new RuntimeException('eMIO integration is not configured');
            }

            $eMIOApikey = $eMIOConfig['settings']['apikey'];
            $blackListId = $eMIOConfig['settings']['blacklist_id'];
            $blacklistingFlaggedName = $eMIOConfig['settings']['blacklisting_flagged_name'];

            if(empty($eMIOApikey) || empty($blackListId) || !$MSMIOServices->setAPIKey($eMIOApikey)) {
                throw new RuntimeException('eMIO apikey or blacklist is not configured');
            }
            $status = $MSMIOServices->blackListEmail((int)$blackListId, $emails, $blacklistingFlaggedName, "/v2.0/blacklist");

            return  $toolServices->setResponseWithMessage($response, 'Emails are blacklisted successfully.',
                true, []);

        }catch (Exception | RuntimeException $e) {
            return  $toolServices->setResponseWithMessage($response, $e->getMessage(),
                false, []);
        }
    }
}

<?php


namespace App\Controller;


use App\Services\AIOServices\AIOService;
use App\Services\AIOServices\MSCIOAccountIntegrations;
use App\Services\AIOServices\MSCIOCampaignServices;
use App\Services\AIOServices\PageServices;
use App\Services\PrivateServices\DeviceDetectionService;
use App\Services\PrivateServices\SplitTestService;
use App\Services\PrivateServices\SurveyService;
use App\Types\APITypes\SourceApiTypes;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CompanyRepository;
use App\Services\ToolServices;

class PageController extends AbstractController
{
    private $logger;
    private $companyRepository;
    private $pageServices;
    private $toolServices;

    public function __construct(
        LoggerInterface   $logger,
        CompanyRepository $companyRepository,
        PageServices      $pageServices,
        ToolServices      $toolServices

    )
    {
        $this->logger = $logger;
        $this->companyRepository = $companyRepository;
        $this->pageServices = $pageServices;
        $this->toolServices = $toolServices;
    }

    /**
     * @Route("/page/config/read", methods={"GET","POST"})
     * #Route("/page/config/basic/get", methods={"GET","POST"})
     * @param Request $request
     * @param MSCIOCampaignServices $campaignServices ,
     * @param MSCIOAccountIntegrations $integrations
     * @param SurveyService $surveyService
     * @param AIOService $service
     * @param DeviceDetectionService $deviceDetector
     * @param SplitTestService $splitTestService
     * @return JsonResponse
     * @throws JsonException
     */
    public function getPageBasicConfiguration(
        Request                  $request,
        SurveyService            $surveyService,
        AIOService               $service,
        DeviceDetectionService   $deviceDetector,
        SplitTestService         $splitTestService
    ): JsonResponse
    {
        // Removed WebSpider Microservice.
        // Trying to compile the code.
        $integrations = null;
        $campaignServices = null;

        $en_data = $request->getContent();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $data = $service->formatRequestData($en_data);  // Decode and format json data to array
        if (empty($data) || empty($data['campaign_token'])) {
            return $this->json('Invalid encoded data');
        }
        $ids = $service->decryptAndFetchIds($data['campaign_token']);

        if (empty($ids) || empty($ids['campaign_id']) || empty($ids['company_id'])) {
            return $this->json('Invalid Request, Campaign could not be found');
        }
        $basic_config = $campaignServices->getCampaignParameters($ids['campaign_id']); // Get campaign parameters
        $basic_config['Survey'] = $surveyService->isSurveyConfiguredForCampaign($ids['campaign_id']);

        // check for Bot detection
        if ($deviceDetector->setDeviceDetector($request->server->get('HTTP_USER_AGENT')) && $deviceDetector->isBotDetected()) {
            $basic_config['bots'] = $integrations->fetchBotConfigByCompanyId($ids['company_id']);
        }

        if ($splitTestService->isSplitTestActive($ids['campaign_id'])) {
            $basic_config['splittest_url'] = $splitTestService->fetchNextSplitTestVariant($ids['campaign_id'], '',
                '');
        }

        return $this->json($basic_config);
    }

    /**
     * @Route("/page/splittest/read", methods={"GET","POST"})
     * #Route("/page/splittest/get", methods={"GET","POST"})
     * @param Request $request
     * @param AIOService $service
     * @param SplitTestService $splitTestService
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function getPageSplitTestConfiguration(
        Request          $request,
        AIOService       $service,
        SplitTestService $splitTestService,
        LoggerInterface  $logger
    ): Response
    {
        $en_data = $request->getContent();
        $response = new Response();
        $splitTestConfig = [];
        $sticky_ip = '';
        $sticky_email = '';
        $request_data = $service->decryptRequestData($en_data);
        $sticky_config = $request->get('sticky_configuration');
        $sticky_config_cookie = $request_data['sticky_configuration_cookie'] ?? '';
        if ($sticky_config === 'IP') {
            $sticky_ip = $request->getClientIp();
        } else if ($sticky_config === 'Email') {
            $sticky_email = $sticky_config_cookie;
        } else {
            $sticky_ip = '';
            $sticky_email = '';
        }

        try {
            if (empty($request_data) || (int)$request_data['campaign_id'] <= 0) {
                $basic_config = 'Invalid Request, Campaign could not be found';
                return $response->setContent(json_encode($basic_config, JSON_THROW_ON_ERROR));
            }
            if ($splitTestService->isSplitTestActive($request_data['campaign_id'])) {
                $splitTestConfig['splittest_url'] = $splitTestService->fetchNextSplitTestVariant(
                    $request_data['campaign_id'],
                    $sticky_ip,
                    $sticky_email
                );
            }
        } catch (JsonException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $response->setContent(json_encode($splitTestConfig, JSON_THROW_ON_ERROR));
    }

    /**
     * @Route("/page/policy/get", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws \JsonException
     * @author Aki
     */
    public function getPagePolicyInformation(
        Request    $request
    ): Response
    {
        $en_data = $request->getContent();
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);
        //get the data for the request
        $data = $this->toolServices->decryptContentData($en_data);
        $this->logger->info("Requested company info for account number" . $data[SourceApiTypes::AccountNumber],
            [__FUNCTION__, __LINE__]);

        //get the policy info from DB
        try {
            $companyInfo = $this->companyRepository->getCompanyDetails(0,$data[SourceApiTypes::AccountNumber]);
            $companyPolicyInfo = json_encode($this->pageServices->formatRequiredPolicyDetailsForClient($companyInfo),
                JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $response->setContent(json_encode($e->getMessage(), JSON_THROW_ON_ERROR));
        }
        //return the data
        return $response->setContent($companyPolicyInfo);

    }
}
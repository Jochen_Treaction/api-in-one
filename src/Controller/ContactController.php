<?php


namespace App\Controller;


use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomFieldRepository;
use App\Repository\DataTypesRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\LeadCustomFieldContentRepository;
use App\Repository\ContactSyncToIntegrationRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusdefRepository;
use App\Repository\Workflow\WorkflowRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactLastSyncService;
use App\Services\AIOServices\ContactServices;
use App\Services\AIOServices\GeneralServices;
use App\Services\AIOServices\IntegrationServices;
use App\Services\AIOServices\ProcesshookServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\ContactFieldTypes;
use App\Types\ContactPermissionTypes;
use Doctrine\DBAL\DBALException;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/v2.0/contact/create", methods={"POST"})
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param CustomFieldRepository $customFieldRepository
     * @param IntegrationServices $integrationServices
     * @param LeadCustomFieldContentRepository $leadCustomFieldContentRepository
     * @param LoggerInterface $logger
     * @param ToolServices $toolServices
     * @param GeneralServices $generalServices
     * @param ContactServices $contactService
     * @param WorkflowRepository $workflowRepository
     * @param ProcesshookServices $processhookServices
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function createContact(
        Request $request,
        ContactRepository $contactRepository,
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        CustomFieldRepository $customFieldRepository,
        IntegrationServices $integrationServices,
        LeadCustomFieldContentRepository $leadCustomFieldContentRepository,
        LoggerInterface $logger,
        ToolServices $toolServices,
        GeneralServices $generalServices,
        ContactServices $contactService,
        WorkflowRepository $workflowRepository,
        ProcesshookServices $processhookServices,
        ObjectRegisterRepository $objectRegisterRepository,
        ContactLastSyncService $contactLastSyncService,
        ContactFieldServices $contactFieldServices
    ): Response {
        // This is forward Request from webhook/receive service.
        $webReceive = $request->get('wb-receive');

        // TODO: if true: then process the new format
        /*
         [
            'field_name' => $field_name_value,
            'field_value' => $field_value_value,
            'regex' => '.*', // this must be defined in table mio.miostandardfield
            'required' => 0, // information does not come from JOTForms
            'datatype' => 'Text', // neither provided by JOTForms, nor defined in table mio.miostandardfield
        ]
         */
        $isJotForms = ($request->get('postSource') === 'JotForms') ? true : false;

        // This is request generated from campaign while creating contact.
        $content = $request->getContent();
        $en_data = empty($webReceive) ? $content : $webReceive;
        $logger->info('contact Controller ' . json_encode($en_data));
        //If the request is from 'webhook/receive' get the correct client ip
        $clientIp = $request->getClientIp();
        if ($request->get('client-ip')) {
            $clientIp = $request->get('client-ip');
        }

        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            // Ignore the contact if the Ipaddress is missing.
            if (empty($clientIp) /*|| !$contactService->validateIpAddress($clientIp)*/) {
                return $toolServices->setResponseWithMessage($response, 'Contact is created/updated successfully.',
                    true, []);
            }

            $data = $toolServices->decryptContentData($en_data);
            //if optional parameter fieldName is missing manually add it
            $data = $contactService->addOptionalParametersForContactData($data);
            //variable to identify client either UUID or object register id
            if (isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ]) ||
                isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
                $clientIdentifier = true;
            }

            if (!isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::APIKEY ],  // TODO: provide apikey in page: line may be removed to make it work
                $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ], //TODO: Make error messages more robust
                $clientIdentifier)) {
                throw new RuntimeException  ('Invalid APIKey or accountNo or ObjectRegisterId/UUID');
            }

            // Objectregister id not defined in base payload and UUID is used as identifier web- 5710
            if (!isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
                //Get the object register id and append to data
                $data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ] = $objectRegisterRepository->
                getObjectRegisterIdWithUUID($data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ]);
                //throw new run time exception that campaign is not active
                if (empty($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
                    throw new RuntimeException('Please activate the page/webhook');
                }
            }

            // get CompanyDetails
            $company = $companyRepository->getCompanyDetails(0,
                $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ]);
            // get Campaign Details
            $campaign = $campaignRepository->getCampaignDetailsByObjectRegisterId($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ]);
            $projectId = $campaignRepository->getProjectId($data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ]);
            // Validate Id's and APIKey with Account Number
            if (empty($campaign) || empty($company) || !isset ($company[ 'id' ]) ||
                !$generalServices->validateCIOAPIKeyWithCompanyId($data[ SourceApiTypes::BASE ][ 'apikey' ],
                    $company[ 'id' ])) {
                throw new RuntimeException('Failed to validate APIKey with Company Id');
            }
            $newContactEmail = $toolServices->parseFieldFromContactData($data, 'email');
            $existingContact = $contactRepository->getContactByEmail($newContactEmail, $company[ 'id' ]);

            // Check the Contact status for the existing contact.
            if (!empty($existingContact) && isset($existingContact[ 'objectregister_id' ])) {
                // Get the status of the existing Contact.
                $existingContactStatus = $objectRegisterRepository->getStatus($existingContact[ 'objectregister_id' ]);
                // Do not process the contact is not either ACTIVE or INACTIVE,
                if (!in_array($existingContactStatus,
                    [StatusdefRepository::ACTIVE, StatusdefRepository::INACTIVE], true)) {
                    $logger->info('Contact is already marked as Fraud or blacklisted. ');
                    return $toolServices->setResponseWithMessage($response, 'Contact is created/updated successfully.',
                        true, []);
                }
            }



            //add ip to contact todo:add technical fields manually from request
            if (!isset($data[ 'contact' ][ 'standard' ][ 'ip' ])) {
                $data[ 'contact' ][ 'standard' ][] = ["field_name" => "ip", "ip" => $clientIp];
            }

            // Create Contact
            $contactId = $contactRepository->insertContact($campaign[ 'id' ], $company[ 'id' ],
                $data[ 'contact' ][ 'standard' ]);

            if (is_null($contactId)) {
                throw new RuntimeException('Failed to create Contact');
            }

            if (isset($data[ 'contact' ][ 'custom' ]) && !empty($data[ 'contact' ][ 'custom' ])) {
                $customFields = $toolServices->formatContactFieldsAsKeyValuePair($data[ 'contact' ][ 'custom' ]);
                if (!$customFieldRepository->insertLeadDataToCustomField($customFields, $contactId, $campaign[ 'id' ],
                    $projectId)) {
                    throw new RuntimeException('Failed to insert lead Custom Fields');
                }
            }
            if (!empty($existingContact)) {
                $customFieldRepository->copyExistingCustomFieldsToNewContact($contactId, $existingContact[ 'id' ]);
            }

            // update the smartTags.
            // New Version of Contact has new SmartTags appended with old version of Contact while creating new contact in database.
            // Here SmartTags are again taken from DB before integrating to other 3rd Systems.
            if (!empty($contactFieldServices->getValue($data[ 'contact' ], ContactFieldTypes::SmartTags))) {
                $data[ 'contact' ][ 'standard' ][] = ['smart_tags' => $contactRepository->getSmartTags($contactId)];
            }
            /*            if(isset($data['contact']['standard']['smart_tags'])) {
                  $data[ 'contact' ][ 'standard' ][ 'smart_tags' ] = $contactRepository->getSmartTags($contactId);
              }*/

            // @WEB-5798: workflow will not be executed when base section has 'execute_workflow' with false,
            $executeWorkFlow = true;
            if (isset($data[ 'base' ][ 'execute_workflow' ]) && !$data[ 'base' ][ 'execute_workflow' ]) {
                $executeWorkFlow = false;
            }

            if ($executeWorkFlow) {
                // WorkFLow which are configured while creating campaign.
                // eg - LeadNotification,
                $workflowDetails = $workflowRepository->get(0, $campaign[ 'id' ]);
                $isDefaultWorkFlowEnabled = !empty($workflowDetails);
                // If workflow is given in base payload,
                if (isset($data[ 'base' ][ 'workflow_id' ]) && (!empty($data[ 'base' ][ 'workflow_id' ]))) {
                    $workFlowId = $data[ 'base' ][ 'workflow_id' ];
                } else {
                    if ($isDefaultWorkFlowEnabled && isset($workflowDetails[ 'id' ])) {
                        $workFlowId = $workflowDetails[ 'id' ];
                    } else {
                        $workFlowId = 0;
                    }
                }

                // if no workflow id is fetched , Don't go to Processhooks configuration.
                $logger->info('PARAMS integrateWorkflow', ['$workFlowId'=>$workFlowId, '$data'=>$data, '$companyId'=>$company[ 'id' ], __METHOD__, __LINE__]);
                if ($workFlowId > 0 && !empty($workFlowId) &&
                    !$processhookServices->integrateWorkflow($workFlowId, $data, $company[ 'id' ])) {
                    $logger->notice('THROW EXCEPTION', ['Failed to Integrate workflow', __METHOD__, __LINE__]);
                    throw new RuntimeException('Failed to Integrate workflow');
                }
            }

            // Integrate Contact to Other integration Services.
            // WorkFlow is provided to validate whether any processhook are already defined for the workflow.
            if (!$integrationServices->integrate($company[ 'id' ], $campaign[ 'id' ], $data, $workFlowId)) {
                $logger->error('Failed to integrate Contact Data to all or some of the services.',
                    [__METHOD__, __LINE__]);
            }

            // Update the last_sync value for the leads table.
            // currently last_sync is updated only when lead is synchronized with eMIO.
            if ($integrationServices->isEMIOIntegrationActiveForCompany($company[ 'id' ])) {
                if ($contactLastSyncService->updateById($contactId)) {
                    throw new RuntimeException('failed to update last sync timestamp for lead');
                }
            }

            return $toolServices->setResponseWithMessage($response, 'Contact is created/updated successfully.',
                true, ['contact_id' => $contactId]);
        } catch (RuntimeException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage(), false, []);
        }
    }


    /**
     * @Route("/contact/list/create", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function createContactList(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/contact/update", methods={"POST"})
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function updateContact(
        Request $request,
        ContactRepository $contactRepository,
        ToolServices $toolServices,
        LoggerInterface $logger
    ): Response {
        $en_data = $request->getContent();
        $response = new Response();
        try {
            $data = $toolServices->decryptDataToNameValuePair($en_data);
            if (!isset($data[ ContactFieldTypes::Email ]) || empty($data[ ContactFieldTypes::Email ])) {
                throw new RuntimeException("Invalid email address");
            }
            $contact = $contactRepository->getContactByEmail($data[ ContactFieldTypes::Email ]);
            if ($contact === null) {
                throw new RuntimeException("Invalid Request, Email Address is not present");
            }
            $status = $contactRepository->update($contact[ 'id' ], $contact[ 'campaign_id' ],
                $contact[ 'objectRegisterId' ], $contact[ 'account' ], $contact[ 'email' ], $contact[ 'salutation' ],
                $contact[ 'first_name' ], $contact[ 'last_name' ], $contact[ 'firma' ], $contact[ 'street' ],
                $contact[ 'postal_code' ], $contact[ 'city' ], $contact[ 'country' ], $contact[ 'phone' ],
                $contact[ 'campaign' ], $contact[ 'lead_reference' ], $contact[ 'url' ], $contact[ 'final_url' ],
                $contact[ 'traffic_source' ], $contact[ 'utm_parameters' ], $contact[ 'split_version' ],
                $contact[ 'target_group' ], $contact[ 'affiliate_id' ], $contact[ 'affiliateSub_id' ], $contact[ 'ip' ],
                $contact[ 'device_type' ], $contact[ 'device_browser' ], $contact[ 'device_os' ],
                $contact[ 'device_os_version' ], $contact[ 'device_browser_version' ], $contact[ 'modification' ]);
            if ($status) {
                $response = $toolServices->setResponseWithMessage($response, 'Contact is updated successfully', 'true',
                    []);
            } else {
                $response = $toolServices->setResponseWithMessage($response, 'Contact is not updated successfully',
                    'false', []);
            }
            return $response;
        } catch (RuntimeException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage(), false, []);
        }
    }

    /**
     * @Route("/contact/delete", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function deleteContact(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/contact/list/get", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function getContactList(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/contact/get", methods={"GET"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function getSingleContact(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/contact/blacklist", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function blacklistContact(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/contact/customfield/emio/sync", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CustomFieldRepository $customFieldRepository
     * @param DataTypesRepository $dataTypesRepository
     * @param MSMIOServices $eMIOServices
     * @param LoggerInterface $logger
     * @return Response|null
     * @author Pradeep
     */
    public function syncEMIOCustomFields(
        Request $request,
        ToolServices $toolServices,
        CustomFieldRepository $customFieldRepository,
        DataTypesRepository $dataTypesRepository,
        MSMIOServices $eMIOServices,
        LoggerInterface $logger
    ): ?Response {
        $response = new Response();
        $requestData = $request->getContent();
        try {
            $customFieldData = json_decode(base64_decode($requestData), true, 512, JSON_THROW_ON_ERROR);

            if (!isset($customFieldData[ 'relation' ], $customFieldData[ 'customfieldlist' ], $customFieldData[ 'integrations' ])) {
                throw new RuntimeException('Invalid Request');
            }
            foreach ($customFieldData[ 'customfieldlist' ] as &$customfield) {
                if (isset($customfield[ 'datatypes_name' ]) && !empty($customfield[ 'datatypes_name' ])) {
                    continue;
                }
                if (isset($customfield[ 'datatype' ])) {
                    $dataType = $dataTypesRepository->get($customfield[ 'datatype' ]);
                    $customfield[ 'datatypes_name' ] = $dataType[ 'name' ] ?? '';
                }
            }
            unset($customfield);
            foreach ($customFieldData[ 'integrations' ] as $integration) {
                if (isset($integration[ 'emio' ][ 'status' ], $integration[ 'emio' ][ 'apikey' ]) && $integration[ 'emio' ][ 'status' ]) {
                    $status = $eMIOServices->syncCustomFields($integration[ 'emio' ][ 'apikey' ],
                        $customFieldData[ 'customfieldlist' ]);
                    $logger->info('eMIO CustomField sync status ' . $status);
                }
            }
            return $toolServices->setResponseWithMessage($response, '', true, []);
        } catch (DBALException|JsonException|RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage(), true, []);
        }
    }

    /**
     * @Route("/contact/customfields/create", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CustomFieldRepository $customFieldRepository
     * @param MSMIOServices $eMIOServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function createCustomField(
        Request $request,
        ToolServices $toolServices,
        CustomFieldRepository $customFieldRepository,
        MSMIOServices $eMIOServices,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $requestData = $request->getContent();

        try {
            $customFieldData = json_decode(base64_decode($requestData), true, 512, JSON_THROW_ON_ERROR);
            //$customFieldArr = json_decode($toolServices->simpleDecrypt($requestData), true, 512, JSON_THROW_ON_ERROR);
            if (!isset($customFieldData[ 'relation' ], $customFieldData[ 'customfieldlist' ], $customFieldData[ 'integrations' ])) {
                throw new RuntimeException('Invalid Request');
            }
            $mioCustomFields = $customFieldRepository->getCustomFieldsForAccount($customFieldData[ 'relation' ][ 'account_no' ]);
            /* foreach ($customFieldData[ 'customFieldsList' ] as $customField) {*/
            /*                if ($customFieldRepository->getCustomFieldDetailsForCampaign($customField[ 'fieldname' ], $campaignId)) {
                                continue;
                            }*/
            //Todo:: need to insert new customfield
            //$customFieldRepository->insert($refCustomFieldsId,$objectRegisterId,$campaignId, $dataTypeId,$fieldName,0,0,'');
            /* }*/

            foreach ($customFieldData[ 'integrations' ] as $integration) {
                if (isset($integration[ 'eMIO' ][ 'status' ], $integration[ 'eMIO' ][ 'apikey' ]) && $integration[ 'eMIO' ][ 'status' ]) {
                    $eMIOServices->syncCustomFields($integration[ 'eMIO' ][ 'apikey' ],
                        $customFieldData[ 'customfieldlist' ]);
                }
            }
            return $toolServices->setResponseWithMessage($response, '', true, []);
        } catch (DBALException|JsonException|RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage(), true, []);
        }
    }

    /**
     * @Route("/contact/customfields/delete", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CustomFieldRepository $customFieldRepository
     * @param MSMIOServices $eMIOServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function deleteCustomField(
        Request $request,
        ToolServices $toolServices,
        CustomFieldRepository $customFieldRepository,
        MSMIOServices $eMIOServices,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $requestData = $request->getContent();
        return $response;

        /*        try {
                    $customFieldData = json_decode(base64_decode($requestData), true, 512, JSON_THROW_ON_ERROR);
                    //$customFieldArr = json_decode($toolServices->simpleDecrypt($requestData), true, 512, JSON_THROW_ON_ERROR);
                    if (!isset($customFieldData[ 'relation' ], $customFieldData[ 'customfieldlist' ], $customFieldData[ 'integrations' ])) {
                        throw new RuntimeException('Invalid Request');
                    }

                    foreach ($customFieldData[ 'integrations' ] as $integration) {
                        if (isset($integration[ 'eMIO' ][ 'status' ], $integration[ 'eMIO' ][ 'apikey' ]) && $integration[ 'eMIO' ][ 'status' ]) {
                            $eMIOServices->syncCustomFields($integration[ 'eMIO' ][ 'apikey' ],
                                $customFieldData[ 'customfieldlist' ]);
                        }
                    }
                    return $toolServices->setResponseWithMessage($response, '', true, []);
                } catch (DBALException | JsonException | RuntimeException $e) {
                    $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    return $toolServices->setResponseWithMessage($response, $e->getMessage(), true, []);
                }*/

    }

    /**
     * @Route("/contact/customfields/update", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function updateCustomField(Request $request): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/contact/customfields/read", methods={"GET"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function readCustomFields(Request $request): Response
    {
        return $this->json([]);
    }

    /**
     * Service is used for synchronizing contacts between MIO and eMIO.
     * i,e by pressing the 'Sync' button at contacts->overview twig.
     *
     * Contact Sync does by contact after contact.
     * Sync happens in two diffenent types.
     *  1. eMIO Sync RestAPI -> contacts with permission levels 'active'(doi or doiplus)
     *  2. createContact in eMIO -> contacts with permission level less than (doi and doiplus).
     *
     * @Route("/contact/sync", methods={"POST"})
     * @param Request $request
     * @param MSMIOServices $eMIOServices
     * @param ContactRepository $contactRepository
     * @param CampaignRepository $campaignRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param IntegrationServices $integrationServices
     * @param ContactSyncToIntegrationRepository $leadSyncLogRepository
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function syncContacts(
        Request $request,
        MSMIOServices $eMIOServices,
        ContactRepository $contactRepository,
        CampaignRepository $campaignRepository,
        IntegrationsRepository $integrationsRepository,
        IntegrationServices $integrationServices,
        ContactSyncToIntegrationRepository $leadSyncLogRepository,
        LoggerInterface $logger
    ): JsonResponse {
        $message = [];
        $content = $request->getContent();

        $content = json_decode(base64_decode($content), true, 512, JSON_THROW_ON_ERROR);
        $logger->info('Request Content ' . json_encode($content));

        $companyId = $content[ 'base' ][ 'company_id' ];
        $contacts = $content[ 'contacts' ];
        $countOfContacts = count($content[ 'contacts' ]);
        $integration = $content[ 'base' ][ 'integration_name' ];
        $lastContactId = 0;
        $integrationId = 0;

        // fetch integration details using integration Id.
        $config = $integrationsRepository->getIntegrationConfigForCompany($companyId, $integration);

        // Get Integration Id.
        if (isset($config[ 'relation' ][ 'integration_id' ])) {
            $integrationId = $config[ 'relation' ][ 'integration_id' ];
        }

        // Check if the customFields are present or not .
        // else create the customFields once again

        foreach ($contacts as $contact) {
            if (empty($contact[ 'base' ] || empty($contact[ 'contact' ]))) {
                continue;
            }
            $campaignId = (int)$contact[ 'base' ][ 'campaign_id' ];
            $logger->info('Campaign ' . json_encode(
                    [
                        'campaign_id' => $campaignId,
                        'permission' => $campaignRepository->getPermission($campaignId),
                    ]));
            // If permission is DOI or DOI+(4 or 5) , contact sync service is used.
            if ($campaignRepository->getPermission($campaignId) < ContactPermissionTypes::PERMISSION_DOUBLE_OPT_IN) {

                $status = $integrationServices->eMailInOne($companyId, $campaignId, $contact, $config);
                if (!$status) {
                    $message[][ 'failed' ] = 'Failed to sync ' . $contact[ 'contact' ][ 'standard' ][ 0 ][ 'email' ];
                } else {
                    $message[][ 'success' ] = 'Successfully sync ' . $contact[ 'contact' ][ 'standard' ][ 0 ][ 'email' ];
                }
                $logger->info('integration to eMIO ' . $status);

            } else {
                // Contacts with DOI or DOI+ Permission.
                $contactSync[] = $contact[ 'contact' ];
            }
        }

        if (!empty($contactSync)) {
            // Use Contact sync of eMIO.(bulk sync)
            $eMIOServices->setAPIKey($config[ 'settings' ][ 'apikey' ]);
            //$logger->info('apiset'. json_encode($config['email-in-one']['settings']['apikey']));
            $eMIOServices->syncContactFieldsOfDataTypeList($contactSync);
            //$logger->info('syncContactFieldsOfDataTypeList');
            $status = $eMIOServices->syncContacts($contactSync, $config[ 'settings' ]);
            //$logger->info('syncContacts'. json_encode($status));
        }

        $logger->info('$leadSyncStatus' . count($contacts));
        $logger->info('last contact email ' . json_encode($contacts[ 1 ][ 'base' ][ 'lead_id' ]));
        if (isset($contacts[ 1 ][ 'base' ][ 'lead_id' ])) {
            $lastContactId = $contacts[ 1 ][ 'base' ][ 'lead_id' ];
        }

        // insert
        $leadSyncStatus = $leadSyncLogRepository->insert($integrationId, json_encode($config),
            $countOfContacts, $lastContactId, $status, json_encode($message));

        $logger->info('$leadSyncStatus' . $leadSyncStatus);

        return $this->json(['status' => $status, 'message' => $message, 'leadSyncStatus' => $leadSyncStatus]);
    }


    /**
     * @Route("/v2.0/contacts/shop/sync", name="contacts_debug", methods={"POST"})
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @param CompanyRepository $companyRepository
     * @param CampaignRepository $campaignRepository
     * @param ToolServices $toolServices
     * @param LeadCustomFieldContentRepository $leadCustomFieldContentRepository
     * @param LoggerInterface $logger
     * @param CustomFieldRepository $customFieldRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param MSMIOServices $msmioServices
     * @return JsonResponse
     * @throws DBALException
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     * @deprecated
     * @internal -> synchronization is done from shopplugin and MIO.
     * shopware can should insert leads and customfields to Db and sync to eMIO
     * MIO should directly sync to eMIO.
     */
    public function syncContactsFromShopPlugin(
        Request $request,
        ContactRepository $contactRepository,
        CompanyRepository $companyRepository,
        CampaignRepository $campaignRepository,
        ToolServices $toolServices,
        LeadCustomFieldContentRepository $leadCustomFieldContentRepository,
        LoggerInterface $logger,
        CustomFieldRepository $customFieldRepository,
        IntegrationsRepository $integrationsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        MSMIOServices $msmioServices
    ): Response {
        $en_data = $request->getContent();
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        //'eyJiYXNlIjp7ImFwaWtleSI6IkREODA5QjY0LTI1MjItRDJCMS1BQzNDLURERjc5RkI5QkNGRCIsImFjY291bnRfbnVtYmVyIjoxMzcyLCJvYmplY3RfcmVnaXN0ZXJfaWQiOjk5Mzd9LCJjb250YWN0cyI6W3sic3RhbmRhcmQiOlt7ImVtYWlsIjoibWFyYy5rcmVzaW5AdHJlYWN0aW9uLm5ldCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJNYXJjIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9uYW1lIjoiS3Jlc2luIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQW4gZGVyIEJ1Y2htXHUwMGZjaGxlIDUiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjM0MjcwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6IlNjaGF1ZW5idXJnIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MzkzNS44NjAwMDAwMDAwMDAxMjczMjkyNTgyNDgyMDk5NTMzMDgxMDU0Njg3NSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0xNCAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE2MCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjoxMzM0LjI3OTk5OTk5OTk5OTk3MjcxNTE1ODk0NjgxMjE1Mjg2MjU0ODgyODEyNSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF95ZWFyX29yZGVyX25ldF92YWx1ZSI6MzkzNS44NTk5OTk5OTk5OTk2NzI1ODE5MDczNjE3NDU4MzQzNTA1ODU5Mzc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn1dfSx7InN0YW5kYXJkIjpbeyJlbWFpbCI6Im1hcmMua3Jlc2luQHRyZWFjdGlvbi5uZXQiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiRW1haWwiLCJyZWdleCI6IiJ9LHsic2FsdXRhdGlvbiI6IkhlcnIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiTWFyYyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfbmFtZSI6IktyZXNpbiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InN0cmVldCI6IkFuIGRlciBCdWNobVx1MDBmY2hsZSA1IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiaG91c2VfbnVtYmVyIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiIzNDI3MCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNpdHkiOiJTY2hhdWVuYnVyZyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNvdW50cnkiOiJEZXV0c2NobGFuZCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InNtYXJ0X3RhZ3MiOiJLYXRhbG9nICMxLCBGcmVpemVpdCAmIEVsZWt0cm8iLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJ0b3RhbF9vcmRlcl9uZXRfdmFsdWUiOjM5MzUuODYwMDAwMDAwMDAwMTI3MzI5MjU4MjQ4MjA5OTUzMzA4MTA1NDY4NzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfZGF0ZSI6IjIwMjEtMDktMTQgMDA6MDA6MDAiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX25vIjoiMTAxNTkiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX25ldF92YWx1ZSI6MjAzMy42MTk5OTk5OTk5OTk4OTA4NjA2MzU3ODcyNDg2MTE0NTAxOTUzMTI1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg1OTk5OTk5OTk5OTY3MjU4MTkwNzM2MTc0NTgzNDM1MDU4NTkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19LHsic3RhbmRhcmQiOlt7ImVtYWlsIjoibWFyYy5rcmVzaW5AdHJlYWN0aW9uLm5ldCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJNYXJjIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9uYW1lIjoiS3Jlc2luIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQW4gZGVyIEJ1Y2htXHUwMGZjaGxlIDUiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjM0MjcwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6IlNjaGF1ZW5idXJnIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MzkzNS44NjAwMDAwMDAwMDAxMjczMjkyNTgyNDgyMDk5NTMzMDgxMDU0Njg3NSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0xMyAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE1OCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjo0MTYuNzU5OTk5OTk5OTk5OTkwOTA1MDUyOTgyMjcwNzE3NjIwODQ5NjA5Mzc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg1OTk5OTk5OTk5OTY3MjU4MTkwNzM2MTc0NTgzNDM1MDU4NTkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19LHsic3RhbmRhcmQiOlt7ImVtYWlsIjoibWFyYy5rcmVzaW5AdHJlYWN0aW9uLm5ldCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJNYXJjIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9uYW1lIjoiS3Jlc2luIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQW4gZGVyIEJ1Y2htXHUwMGZjaGxlIDUiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjM0MjcwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6IlNjaGF1ZW5idXJnIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MzkzNS44NjAwMDAwMDAwMDAxMjczMjkyNTgyNDgyMDk5NTMzMDgxMDU0Njg3NSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0xMCAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE1NyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjo4My45ODk5OTk5OTk5OTk5OTQ4ODQwOTIzMDI1MjcyNzg2NjE3Mjc5MDUyNzM0Mzc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg1OTk5OTk5OTk5OTY3MjU4MTkwNzM2MTc0NTgzNDM1MDU4NTkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19LHsic3RhbmRhcmQiOlt7ImVtYWlsIjoiaG9ueXdAbWFpbGluYXRvci5jb20iLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiRW1haWwiLCJyZWdleCI6IiJ9LHsic2FsdXRhdGlvbiI6IkZyYXUiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJmaXJzdF9uYW1lIjoiUGxhdG8iLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJCbGV2aW5zIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQXV0ZW0gaWxsdW0gYWQgZXN0IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiaG91c2VfbnVtYmVyIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiI5MTA3MSIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNpdHkiOiJDdXBpZGF0YXQgY3VscGEgcXVpIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MjAxNi44MDk5OTk5OTk5OTk5NDU0MzAzMTc4OTM2MjQzMDU3MjUwOTc2NTYyNSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0xMCAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE1NiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjoyMDE2LjgwOTk5OTk5OTk5OTk0NTQzMDMxNzg5MzYyNDMwNTcyNTA5NzY1NjI1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjoyMDE2LjgwOTk5OTk5OTk5OTk0NTQzMDMxNzg5MzYyNDMwNTcyNTA5NzY1NjI1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn1dfSx7InN0YW5kYXJkIjpbeyJlbWFpbCI6Imt1amF3QG1haWxpbmF0b3IuY29tIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJIZXJyIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZmlyc3RfbmFtZSI6IkplbGFuaSIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfbmFtZSI6IkNoYW5nIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiRG9sb3JlcyBhZCBhcGVyaWFtIGQiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjIzOTEwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6Ik5pc2kgYWxpcXVpcCBzb2x1dGEgIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6NDMzLjU2MDAwMDAwMDAwMDAwMjI3MzczNjc1NDQzMjMyMDU5NDc4NzU5NzY1NjI1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX2RhdGUiOiIyMDIxLTA5LTEwIDAwOjAwOjAwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9ubyI6IjEwMTU1IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9uZXRfdmFsdWUiOjQxNi43NTk5OTk5OTk5OTk5OTA5MDUwNTI5ODIyNzA3MTc2MjA4NDk2MDkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfeWVhcl9vcmRlcl9uZXRfdmFsdWUiOjQzMy41NjAwMDAwMDAwMDAwMDIyNzM3MzY3NTQ0MzIzMjA1OTQ3ODc1OTc2NTYyNSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9XX0seyJzdGFuZGFyZCI6W3siZW1haWwiOiJrdWphd0BtYWlsaW5hdG9yLmNvbSIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJKZWxhbmkiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJDaGFuZyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InN0cmVldCI6IkRvbG9yZXMgYWQgYXBlcmlhbSBkIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiaG91c2VfbnVtYmVyIjoiIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsicG9zdGFsX2NvZGUiOiIyMzkxMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNpdHkiOiJOaXNpIGFsaXF1aXAgc29sdXRhICIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImNvdW50cnkiOiJEZXV0c2NobGFuZCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InNtYXJ0X3RhZ3MiOiJLYXRhbG9nICMxLCBGcmVpemVpdCAmIEVsZWt0cm8iLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJ0b3RhbF9vcmRlcl9uZXRfdmFsdWUiOjQzMy41NjAwMDAwMDAwMDAwMDIyNzM3MzY3NTQ0MzIzMjA1OTQ3ODc1OTc2NTYyNSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0xMCAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE1NCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjoxNi44MDAwMDAwMDAwMDAwMDA3MTA1NDI3MzU3NjAxMDAxODU4NzExMjQyNjc1NzgxMjUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3RfeWVhcl9vcmRlcl9uZXRfdmFsdWUiOjQzMy41NjAwMDAwMDAwMDAwMDIyNzM3MzY3NTQ0MzIzMjA1OTQ3ODc1OTc2NTYyNSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9XX0seyJzdGFuZGFyZCI6W3siZW1haWwiOiJtYXJjLmtyZXNpbkB0cmVhY3Rpb24ubmV0IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IkVtYWlsIiwicmVnZXgiOiIifSx7InNhbHV0YXRpb24iOiJIZXJyIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiZmlyc3RfbmFtZSI6Ik1hcmMiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJLcmVzaW4iLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJzdHJlZXQiOiJBbiBkZXIgQnVjaG1cdTAwZmNobGUgNSIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImhvdXNlX251bWJlciI6IiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InBvc3RhbF9jb2RlIjoiMzQyNzAiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjaXR5IjoiU2NoYXVlbmJ1cmciLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJjb3VudHJ5IjoiRGV1dHNjaGxhbmQiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJzbWFydF90YWdzIjoiS2F0YWxvZyAjMSwgRnJlaXplaXQgJiBFbGVrdHJvIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsidG90YWxfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg2MDAwMDAwMDAwMDEyNzMyOTI1ODI0ODIwOTk1MzMwODEwNTQ2ODc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX2RhdGUiOiIyMDIxLTA5LTA5IDAwOjAwOjAwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9ubyI6IjEwMTUzIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9uZXRfdmFsdWUiOjMzLjYwMDAwMDAwMDAwMDAwMTQyMTA4NTQ3MTUyMDIwMDM3MTc0MjI0ODUzNTE1NjI1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg1OTk5OTk5OTk5OTY3MjU4MTkwNzM2MTc0NTgzNDM1MDU4NTkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19LHsic3RhbmRhcmQiOlt7ImVtYWlsIjoibWFyYy5rcmVzaW5AdHJlYWN0aW9uLm5ldCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJNYXJjIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9uYW1lIjoiS3Jlc2luIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQW4gZGVyIEJ1Y2htXHUwMGZjaGxlIDUiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjM0MjcwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6IlNjaGF1ZW5idXJnIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEZyZWl6ZWl0ICYgRWxla3RybyIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MzkzNS44NjAwMDAwMDAwMDAxMjczMjkyNTgyNDgyMDk5NTMzMDgxMDU0Njg3NSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9kYXRlIjoiMjAyMS0wOS0wOSAwMDowMDowMCIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbm8iOiIxMDE1MiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7Imxhc3Rfb3JkZXJfbmV0X3ZhbHVlIjozMy42MDk5OTk5OTk5OTk5OTk0MzE1NjU4MTEzOTE5MTk4NTEzMDMxMDA1ODU5Mzc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjozOTM1Ljg1OTk5OTk5OTk5OTY3MjU4MTkwNzM2MTc0NTgzNDM1MDU4NTkzNzUsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifV19LHsic3RhbmRhcmQiOlt7ImVtYWlsIjoicXVqdUBtYWlsaW5hdG9yLmNvbSIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJFbWFpbCIsInJlZ2V4IjoiIn0seyJzYWx1dGF0aW9uIjoiSGVyciIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7ImZpcnN0X25hbWUiOiJKZW1pbWEiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X25hbWUiOiJFdmVyZXR0IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic3RyZWV0IjoiQWRpcGlzaWNpbmcgZXhwbGljYWIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJob3VzZV9udW1iZXIiOiIiLCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJwb3N0YWxfY29kZSI6IjE5NjU1IiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY2l0eSI6IklsbG8gdm9sdXB0YXR1bSB2ZWwgIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsiY291bnRyeSI6IkRldXRzY2hsYW5kIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsic21hcnRfdGFncyI6IkthdGFsb2cgIzEsIEJla2xlaWR1bmcsIEhlcnJlbiIsInJlcXVpcmVkIjoiIiwiZGF0YXR5cGUiOiJUZXh0IiwicmVnZXgiOiIifSx7InRvdGFsX29yZGVyX25ldF92YWx1ZSI6MTYuODA5OTk5OTk5OTk5OTk4NzIxMDIzMDc1NjMxODE5NjY1NDMxOTc2MzE4MzU5Mzc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X29yZGVyX2RhdGUiOiIyMDIxLTA5LTA3IDAwOjAwOjAwIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9ubyI6IjEwMTUxIiwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9LHsibGFzdF9vcmRlcl9uZXRfdmFsdWUiOjE2LjgwOTk5OTk5OTk5OTk5NTE2ODMwOTM5NjgzMTMxODczNjA3NjM1NDk4MDQ2ODc1LCJyZXF1aXJlZCI6IiIsImRhdGF0eXBlIjoiVGV4dCIsInJlZ2V4IjoiIn0seyJsYXN0X3llYXJfb3JkZXJfbmV0X3ZhbHVlIjoxNi44MDk5OTk5OTk5OTk5OTUxNjgzMDkzOTY4MzEzMTg3MzYwNzYzNTQ5ODA0Njg3NSwicmVxdWlyZWQiOiIiLCJkYXRhdHlwZSI6IlRleHQiLCJyZWdleCI6IiJ9XX1dfQ';
        // Decrypt the content.
        $data = $toolServices->decryptContentData($en_data);

        // Objectregister id not defined in base payload and UUID is used as identifier web- 5710
        if (!isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
            //Get the object register id and append to data
            $data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ] = $objectRegisterRepository->
            getObjectRegisterIdWithUUID($data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ]);
            //throw new run time exception that campaign is not active
            if (empty($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
                throw new RuntimeException('Please activate the page/webhook');
            }
        }

        // get CompanyDetails
        $company = $companyRepository->getCompanyDetails(0,
            $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ]);

        // get Campaign Details
        $campaign = $campaignRepository->getCampaignDetailsByObjectRegisterId($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ]);

        // Get Project details for the account.
        $projectId = $campaignRepository->getProjectId($data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ]);

        if (!isset($campaign[ 'id' ], $company[ 'id' ], $data[ 'contacts' ])) {
            $response = $toolServices->setResponseWithMessage($response, 'Invalid Company Or CampaignId provided.',
                false, []);
            return $response;
        }
        $contacts = $contactRepository->generateObjectRegisterIdForContactList($campaign[ 'id' ], $company[ 'id' ],
            $data[ 'contacts' ]);
        // MultiInsert For Contacts.
        if (!$contactRepository->multiInsert($campaign[ 'id' ], $company[ 'id' ], $contacts)) {
            return $toolServices->setResponseWithMessage(
                $response, 'Failed to sync contacts.', false, []);
        }

        // Since all eCommerce fields are merged to leads table, no need to insert eCommerce fields into
        // Customfields table.
        /*
                $cusFields = $customFieldRepository->getCustomFieldsForAccount($company[ 'account_no' ]);
                if (!empty($cusFields)) {
                    $leadCustomFieldContentRepository->insertCustomFieldsFromLeadSync($contacts, $cusFields);
                }
        */


        $integrationConfig = $integrationsRepository->getIntegrationsForCompany($company[ 'id' ]);
        $logger->info('integrations' . json_encode($integrationConfig));

        // integration with eMIO.
        if (isset($integrationConfig[ 'email-in-one' ][ 'settings' ]) && !empty($integrationConfig[ 'email-in-one' ][ 'settings' ])) {
            if (!isset($integrationConfig[ 'email-in-one' ][ 'settings' ][ 'apikey' ])) {
                $logger->info('Failed no emailinone integrations' . json_encode($integrationConfig));
                return $this->json('Failed to fetch APIkey for eMIO');
            }
            $msmioServices->setAPIKey($integrationConfig[ 'email-in-one' ][ 'settings' ][ 'apikey' ]);
            $logger->info('apiset' . json_encode($integrationConfig[ 'email-in-one' ][ 'settings' ][ 'apikey' ]));
            $msmioServices->syncContactFieldsOfDataTypeList($contacts);
            $logger->info('syncContactFieldsOfDataTypeList');
            $status = $msmioServices->syncContacts($contacts, $integrationConfig[ 'email-in-one' ][ 'settings' ],
                $company[ 'id' ]);
            $logger->info('syncContacts' . json_encode($status));
        }

        $response = $toolServices->setResponseWithMessage($response, 'Contacts synchronized successfully.',
            true, []);
        return $response;
    }
}

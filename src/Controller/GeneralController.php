<?php


namespace App\Controller;


use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\UserMetaRepository;
use App\Repository\UserRepository;
use App\Services\AIOServices\AIOService;
use App\Services\AIOServices\GeneralServices;

// use App\Services\AIOServices\MSCIOAccountIntegrations;
// use App\Services\AIOServices\MSCIOCompanyServices;
// use App\Services\AIOServices\MSCIOConfigurationTargets;
use App\Services\AIOServices\LanguageLocalizationService;
use App\Services\MSMailServices\MSMailServices;
use App\Services\MSManagementServices\MSManagementServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Services\PrivateServices\DeviceDetectionService;
use App\Services\PrivateServices\ProjectService;
use App\Services\ToolServices;
use App\Types\AccountStatusTypes;
use App\Types\UserStatusTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use WhichBrowser\Parser;

class GeneralController extends AbstractController
{

    /**
     * @Route("/v2.0/general/account/create", methods={"POST"})
     * @param Request $request
     * @param CompanyRepository $companyRepository
     * @param MSMailServices $mailServices
     * @param UserMetaRepository $userMetaRepository
     * @param UserRepository $userRepository
     * @param LoggerInterface $logger
     * @param ToolServices $toolServices
     * @return JsonResponse
     * @author Pradeep
     */
    public function accountCreate(
        Request $request,
        CompanyRepository $companyRepository,
        MSMailServices $mailServices,
        UserMetaRepository $userMetaRepository,
        UserRepository $userRepository,
        LoggerInterface $logger,
        ToolServices $toolServices
    ): Response {
        $data = $request->getContent();
        $headers = $request->server->getHeaders();
        $ipAddress = $request->getClientIp();
        $location = '';//$userRepository->getUserLocationByIpAddress($ipAddress);
        $whichBrowser = new Parser($headers[ 'USER_AGENT' ]);
        $browserName = $whichBrowser->browser->name;
        $browserOs = $whichBrowser->os->name;
        $device = $whichBrowser->getType();

        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);

        try {
            $nw_acc_data = json_decode(base64_decode($data), true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
            $logger->info("/general/account/create", $nw_acc_data);

            if (!isset($nw_acc_data[ 'email' ])) {
                throw new RuntimeException('Invalid email address');
            }

            $domain = $companyRepository->getDomainNameFromEmail($nw_acc_data[ 'email' ]);
            $isEmailPresent = $userRepository->getUserDetailsByEmail($nw_acc_data[ 'email' ]);

            if (!empty($isEmailPresent) || is_null($isEmailPresent)) {
                throw new RuntimeException('User with EmailAddress is already present , Please try Forgot Password to login to MIO');
            }

            if (!isset($nw_acc_data[ 'permission' ]) || !$nw_acc_data[ 'permission' ]) {
                throw new RuntimeException('Permission denied.');
            }

            $permission = 1;
            // create Company
            $company_id = $companyRepository->createCompany('', '', '', '', '',
                '', $domain, '', '', '', '', '',
                1, 1, 0);

            if (is_null($company_id) || $company_id <= 0) {
                throw new RuntimeException('Failed to create Company.');
            }

            $logger->info('Company Id', [$company_id]);
            // create User
            $user_id = $userRepository->createUser($company_id, $nw_acc_data[ 'email' ],
                ['ROLE_MASTER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'], $nw_acc_data[ 'firstName' ],
                $nw_acc_data[ 'lastName' ], $nw_acc_data[ 'salutation' ], '', '', '',
                '', '0', 0, $permission, 0, '',
                1, 1, UserStatusTypes::NEW);

            if (is_null($user_id) || $user_id <= 0) {
                throw new RuntimeException('Failed to create user');
            }

            $logger->info('Company Id', [$user_id]);
            // register User as AdminUser
            $admin_id = $companyRepository->registerAdminUserToCompany($user_id, $company_id, 1, 1);
            $logger->info('Administrator Id', [$admin_id]);

            $registration_firstStep_details = json_encode([
                'source' => 'AIO',
                'ip' => (string)$ipAddress,
                'time_stamp' => (string)date('Y-m-d H:i:s'),
                'location' => (string)$location,
                'device' => $device,
                'broswer' => (string)$browserName,
                'device_os' => (string)$browserOs,
            ], JSON_THROW_ON_ERROR);

            if (is_null($userMetaRepository->insert((int)$user_id, 'Registration_FirstStep',
                $registration_firstStep_details, (int)$user_id, (int)$user_id))) {
                $logger->critical('Failed to create User FirstStep Registration Details. ', [__METHOD__, __LINE__]);
            }

            $company = $companyRepository->getCompanyDetails($company_id, 0);
            // create doi using Account Number
            $doi = $toolServices->encryptDataToNameValuePair(
                ['accountNo' => $company[ 'account_no' ], 'email' => $nw_acc_data[ 'email' ]]);
            // generate Activation Link
            $activationLink =
                (stripos(strtolower($request->server->get('SERVER_PROTOCOL')), 'https')) ?:
                    'https://' . $request->server->get('HTTP_HOST') . '/v2.0/general/account/activate/?p=' . $doi;
            $logger->info('activationLink ', [$activationLink]);
            $logger->info('accData', [json_encode($nw_acc_data)]);

            // send activation link to User.
            if (!$mailServices->sendAccountActivationEmail($company, $nw_acc_data[ 'email' ], $nw_acc_data,
                $activationLink, (string)$company[ 'account_no' ])) {
                throw new RuntimeException("Something went wrong! Unable to send Confirmation E-Mail.");
            }

            return $toolServices->setResponseWithMessage($response,
                'Your registration is successful, Please check the details sent to E-Mail', true);

        } catch (Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage(), true);
        }


    }

    /**
     * @Route("/v2.0/general/account/activate", methods={"GET"})
     * @param Request $request
     * @param MSManagementServices $managementServices
     * @param ObjectRegisterMetaRepository $ObjectRegisterMetaRepository
     * @param CampaignRepository $campaignRepository
     * @param UserRepository $userRepository
     * @param UserMetaRepository $userMetaRepository
     * @param CompanyRepository $companyRepository
     * @param ProjectService $projectService
     * @param ToolServices $toolServices
     * @param MSMailServices $mailServices
     * @param MSMIOServices $eMIOServices
     * @param LoggerInterface $logger
     * @return RedirectResponse
     * @throws JsonException
     * @author Pradeep
     */
    public function accountActivate(
        Request $request,
        MSManagementServices $managementServices,
        ObjectRegisterMetaRepository $ObjectRegisterMetaRepository,
        CampaignRepository $campaignRepository,
        UserRepository $userRepository,
        UserMetaRepository $userMetaRepository,
        CompanyRepository $companyRepository,
        ProjectService $projectService,
        ToolServices $toolServices,
        MSMailServices $mailServices,
        MSMIOServices $eMIOServices,
        LoggerInterface $logger
    ): RedirectResponse {
        $p = $request->get('p'); // base64 encryped json with (email, account_no)
        $headers = $request->server->getHeaders();
        $ipAddress = $request->getClientIp();
        $location = $userRepository->getUserLocationByIpAddress($ipAddress);
        $whichBrowser = new Parser($headers[ 'USER_AGENT' ]);
        $browserName = $whichBrowser->browser->name;
        $browserOs = $whichBrowser->os->name;
        $device = $whichBrowser->getType();
        $registration_second_details = json_encode([
            'source' => 'AIO',
            'ip' => (string)$ipAddress,
            'time_stamp' => (string)date('Y-m-d H:i:s'),
            'location' => (string)$location,
            'device' => $device,
            'browser' => (string)$browserName,
            'device_os' => (string)$browserOs,
        ], JSON_THROW_ON_ERROR);

        try {
            $data = $toolServices->decryptDataToNameValuePair($p);
            $logger->info("this is incomming data for /general/account/activate ", $data);

            if (!isset($data[ 'accountNo' ], $data[ 'email' ]) || empty($data)) {
                throw new RuntimeException('Failed to retrieve request data');
            }

            // get user Information.
            $user = $userRepository->getUserDetailsByEmail($data[ 'email' ]);

            // User status with confirmed has already activated himself.
            // So redirect the user directly to login screen
            if (UserStatusTypes::ACTIVE === $user[ 'status' ]) {
                return $this->redirectToRoute('auth.getSigninScreen', ['s' => base64_encode('account-is-active')]);
            }

            // fetch account details by account id
            $company = $companyRepository->getCompanyDetails(0, (int)$data[ 'accountNo' ]);

            if (empty($user) || $company === null || $company[ 'objectregister_id' ] <= 0) {
                throw new RuntimeException('Failed to retrieve Company or User Information');
            }

            // Generate APIKey from ManagementServices
            $apikey = $managementServices->generateCIOAPIKey((int)$data[ 'accountNo' ]);
            $projectId = $campaignRepository->getProjectId($data[ 'accountNo' ]);
            $projectId = $projectId ?? $projectService->createProject($data[ 'accountNo' ]);

            if ($apikey === null || $projectId === null) {
                throw new RuntimeException('Failed to create project OR apikey');
            }

            // set user status to confirmed after running the activation link (received due to e.g. Shop MVP interaction),
            // added 2021-02-02, WEB-5067
            // set Account status to confirmed
            if ($companyRepository->setCompanyObjectRegisterStatusToAccountStatusType($company[ 'objectregister_id' ],
                    AccountStatusTypes::CONFIRMED) === null
                || !$userRepository->setUserStatus($user[ 'id' ], UserStatusTypes::CONFIRMED)) {
                throw new RuntimeException('Failed to update status for Company Or User');
            }

            if (!$ObjectRegisterMetaRepository->insert($company[ 'objectregister_id' ], 'account_apikey', $apikey,
                $user[ 'id' ], $user[ 'id' ])) {
                throw new RuntimeException('Failed to insert APIKey to ObjectRegisterMeta');
            }

            if (empty($user) || is_null($userMetaRepository->insert((int)$user[ 'id' ],
                    'Registration_SecondStep', $registration_second_details, (int)$user[ 'id' ],
                    (int)$user[ 'id' ]))) {
                $logger->critical('Failed to create User FirstStep Registration Details. ', [__METHOD__, __LINE__]);
            }

            // createUserToeMIO(string email);
            if ($eMIOServices->subscribeUserToeMIONewsLetter($user, $company)) {
                $logger->error('Failed to create Contact in eMIO ', [__METHOD__, __LINE__]);
            }

            // Shop system Create Webhook.
            $generateWebhookData = [
                'apikey' => $apikey,
                'accountNo' => $data[ 'accountNo' ],
                'userId' => $user[ 'id' ],
            ];
            $encryptShopSysCreate = $toolServices->encryptDataToNameValuePair($generateWebhookData);
            $shopSystemResponse = $this->forward('App\Controller\GeneralController::IntegrationShopSystemCreate',
                ['data' => $encryptShopSysCreate]);
            $responseContent = json_decode($shopSystemResponse->getContent(), true, 512,
                JSON_THROW_ON_ERROR);

            if (!isset($responseContent[ 'status' ]) || $responseContent[ 'status' ] === false) {
                throw new RuntimeException('Failed to create Shop System');
            }

            $emailEncrypted = $toolServices->simpleEncrypt($data[ 'email' ]);// generate link for User to set password.
            $user_activation_link = $_ENV[ 'MARKETINGINONE_URL' ]
                                    . '/setpassword/?p='
                                    . base64_encode($emailEncrypted);
            $logger->info('User Activation Link ', [$user_activation_link]);
            return $this->redirect($user_activation_link);

        } catch (ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | Exception $e) {

            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $error = base64_encode($e->getMessage());
            return $this->redirect($_ENV[ 'MARKETINGINONE_URL' ] . '?error=' . $error);
        }
    }

    /**
     * @Route("v2.0/general/validate/apikey", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param GeneralServices $generalServices
     * @param CompanyRepository $companyRepository
     * @param LoggerInterface $logger
     * @return Response
     * @throws JsonException
     * @internal example base64_encode({"base":{"apikey":"3D2C3A88-9628-BF5A-E7DC-5A7E35CD4B14","account_number":1046}}) "
     * @author Pradeep
     */
    public function validateCIOAPIKey(
        Request $request,
        ToolServices $toolServices,
        GeneralServices $generalServices,
        CompanyRepository $companyRepository,
        LanguageLocalizationService $languageLocalizationService,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        $responseBody['company'] = [];
        try {// check if the apikey and account_number exists
            $enData = $request->getContent();
            $data = json_decode(base64_decode($enData), true, 512, JSON_THROW_ON_ERROR);

            if (!isset($data[ 'base' ][ 'apikey' ], $data[ 'base' ][ 'account_number' ]) ||
                empty(trim($data[ 'base' ][ 'apikey' ])) || empty(trim($data[ 'base' ][ 'account_number' ]))) {
                throw new RuntimeException("invalid apikey or account number provided");
            }
            $apikey = trim($data[ 'base' ][ 'apikey' ]);
            $accountNo = trim($data[ 'base' ][ 'account_number' ]);// get company details using account number
            $isoCode = trim($data[ 'base' ][ 'iso_code' ]);
            $company = $companyRepository->getCompanyDetails(0, (int)$accountNo);
            if (empty($company) || !isset($company[ 'id' ]) || (int)$company[ 'id' ] <= 0) {
                throw new RuntimeException("no matching company found with account number " . (int)$accountNo);
            }
            $companyId = trim($company[ 'id' ]);
            if (!$generalServices->validateCIOAPIKeyWithCompanyId($apikey, (int)$companyId)) {
                throw new RuntimeException("invalid configuration provided, apikey does not match with account number.");
            }
            $companyDetails = [
                'name'                  => trim($company[ 'name' ]) ?? '',
                'street'                => trim($company[ 'street' ]) ?? '',
                'house_number'          => trim($company[ 'house_number' ]) ?? '',
                'postal_code'           => trim($company[ 'postal_code' ]) ?? '',
                'city'                  => trim($company[ 'city' ]) ?? '',
                'country'               => trim($company[ 'country' ]) ?? '',
                'website'               => trim($company[ 'website' ]) ?? '',
                'url_privacy_policy'    => trim($company[ 'url_privacy_policy' ]) ?? '',
                'url_imprint'           => trim($company[ 'url_imprint' ]) ?? '',
                'url_gtc'               => trim($company[ 'url_gtc' ]) ?? '',
                'phone'                 => trim($company[ 'phone' ]) ?? '',
            ];

            $responseBody[ 'company' ] = $companyDetails;
            $responseBody[ 'optinText' ] = $languageLocalizationService->getTranslatedOptinText($companyId, $isoCode);
            return $toolServices->setResponseWithMessage(
                $response,
                'APIKey matches with Company Account Number',
                true,
                $responseBody);
        } catch (Exception $e) {
            return $toolServices->setResponseWithMessage(
                $response,
                $e->getMessage(),
                false,
                $responseBody);
        }
    }

    /**
     * @Route("/general/integrations/cms/create", methods={"POST"})
     * @return Response
     * @author Pradeep
     */
    public function IntegrationCMSCreate(): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/general/integrations/shopsystem/create", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CompanyRepository $companyRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function IntegrationShopSystemCreate(
        Request $request,
        ToolServices $toolServices,
        LoggerInterface $logger
    ): Response {
        $requestData = $request->get('data');
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        $data = $toolServices->decryptDataToNameValuePair($requestData);
        try {
            $webhooks = ['eCommerceHook', 'NewsletterHook'];
            // Fowarding the request to create Webhook.
            foreach ($webhooks as $webhook) {
                $data[ 'webhookName' ] = $webhook;
                //create webhook
                $endata = $toolServices->encryptDataToNameValuePair($data);
                $webhookCreateResponse = $this->forward('App\Controller\WebhookController::createWebhook',
                    ['data' => $endata]);
                $responseContent = json_decode($webhookCreateResponse->getContent(), true, 512,
                    JSON_THROW_ON_ERROR);
                if ($responseContent[ 'status' ] === false) {
                    $logger->error($responseContent[ 'message' ], [__METHOD__, __LINE__]);
                    continue;
                }
                //activate webhook
                $webhookActivateResponse = $this->forward('App\Controller\WebhookController::activateWebhook',
                    ['data' => $endata]);
                $responseContent = json_decode($webhookActivateResponse->getContent(), true, 512,
                    JSON_THROW_ON_ERROR);
                if ($responseContent[ 'status' ] === false) {
                    $logger->error($responseContent[ 'message' ], [__METHOD__, __LINE__]);
                    continue;
                }
            }
            $response = $toolServices->setResponseWithMessage($response, 'Webhooks are created and activated', true);
            return $response;
        } catch (JsonException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $response;
        }
    }

    /**
     * @Route("/general/user/read", methods={"GET"})
     * @return Response
     * @author Pradeep
     */
    public function readUserDetails(): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/general/account/read", methods={"GET"})
     * @return Response
     * @author Pradeep
     */
    public function readAccountDetails(): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/general/setting/read", methods={"GET"})
     * @return Response
     * @author Pradeep
     */
    public function readFraudSettings(): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/general/integration/read", methods={"GET"})
     * @return Response
     * @author Pradeep
     */
    public function readProjectIntegrations(): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/general/accountname/validate", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CompanyRepository $companyRepository
     * @return Response
     * @author Pradeep
     */
    public function validateAccountName(
        Request $request,
        ToolServices $toolServices,
        CompanyRepository $companyRepository
    ): Response {
        $data = $request->get('data');
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        $account = $toolServices->decryptDataToNameValuePair($data);
        $accountName = $account[ 'accountName' ] ?? '';
        if (empty($accountName)) {
            return $toolServices->setResponseWithMessage($response,
                'Invalid Account Name passed for validation',
                false);
        }
        $status = $companyRepository->checkIfAccountNameIsAvailable($accountName);
        if ($status) {
            $recommendedAccountNames = $companyRepository->getAccountNameRecommendation($accountName);
            return $toolServices->setJsonResponse($response, $recommendedAccountNames,
                false,
                'Account Name is not available');
        }

        return $toolServices->setResponseWithMessage($response,
            'Account Name is available',
            true);

    }

    /**
     * @Route("/general/account/apikey/create", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param CompanyRepository $companyRepository
     * @param CampaignRepository $campaignRepository
     * @param ProjectService $projectService
     * @param MSManagementServices $managementServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     * @deprecated since APIKey is generated while creating MIO Account.
     */
    public function orderAPIKeyForExistingCustomer(
        Request $request,
        ToolServices $toolServices,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        CompanyRepository $companyRepository,
        CampaignRepository $campaignRepository,
        ProjectService $projectService,
        MSManagementServices $managementServices,
        LoggerInterface $logger
    ): Response {
        $en_data = $request->getContent();
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        $data = $toolServices->decryptDataToNameValuePair($en_data);
        if (!isset($data[ 'accountNo' ]) || empty($data)) {
            throw new RuntimeException('Failed to retrieve request data');
        }
        try {
            $company = $companyRepository->getCompanyDetails(0, (int)$data[ 'accountNo' ]);
            if ($company === null || !isset($company[ 'id' ], $company[ 'objectregister_id' ])) {
                throw new RuntimeException('Failed to retrieve company information based on account_no');
            }
            $apikey = $objectRegisterMetaRepository->getAccountAPIKey($company[ 'id' ]);
            $admin = $companyRepository->getCompanyAdminDetails((int)$company[ 'id' ]);
            // Get the project or Create a new project
            $projectId = $campaignRepository->getProjectId($data[ 'accountNo' ]);
            $projectId = $projectId ?? $projectService->createProject($data[ 'accountNo' ]);
            if ($projectId === null) {
                throw new RuntimeException('Failed to create project OR apikey');
            }
            if ($apikey === null) {
                // create a new apiKey and save to database.
                $apikey = $managementServices->generateCIOAPIKey((int)$data[ 'accountNo' ]);
                if ($apikey === null) {
                    throw new RuntimeException('Failed to create APIKey');
                }
                if (!$objectRegisterMetaRepository->insert($company[ 'objectregister_id' ], 'account_apikey',
                    $apikey)) {
                    throw new RuntimeException('Failed to insert APIKey to ObjectRegisterMeta');
                }
            }

        } catch (Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response,
                $e->getMessage(), false, ['apikey' => '']);
        }

        return $toolServices->setResponseWithMessage($response,
            'Your registration is successful, Please check the details sent to E-Mail', true, ['apikey' => $apikey]);
    }

    /**
     * @Route("/v2.0/getaccountfraudsettings", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     */

    public function getAccountFraudSettings(
        Request $request
    ): Response {
        $en_data = $request->getContent();
        $basic_config = [];
        $response = new Response();
        //response headers
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers',
            'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer');
        $response->headers->set('Access-Control-Max-Age', '86400');
        $response->headers->set('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
//         $data = $service->formatRequestData($en_data);  // Decode and format json data to array
//         if (empty($data) || empty($data[ 'campaign_token' ])) {
        // return $this->json('Invalid encoded data');
//             return $response->setContent(json_encode('failed'));
//         }
        //get ids from token
//         $ids = $service->decryptAndFetchIds($data[ 'campaign_token' ]);
        //$userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)";
//         $userAgent = $request->server->get('HTTP_USER_AGENT');
        /*         $basic_config[ 'accFraudConfig' ] = $integrations->fetchBotConfigByCompanyId($ids[ 'company_id' ]);
                 $basic_config[ 'accFraudConfig' ][ 'isbotdetected' ] = $deviceDetector->setDeviceDetector($userAgent) && $deviceDetector->isBotDetected();*/

        // Todo: Replace the bellow code and get the values from DB.
        $sampleValue = '{"maxLeadsPerDomain":"3","maxLeadsPerIp":"3","urlToReplaceLinks":"https:\/\/treaction.net","geoBlocking":"on","urlToHidePage":"","useIntermediatePage":"","scrambleUrl":"off","id":"30"}';
        $basic_config[ 'accFraudConfig' ] = json_decode($sampleValue, true, 512,
            JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
        return $response->setContent(json_encode($basic_config));
    }

}

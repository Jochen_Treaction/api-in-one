<?php


namespace App\Controller;



use App\Services\AIOServices\AIOService;
use App\Services\AIOServices\MSCIOAccountIntegrations;
use App\Services\AIOServices\MSCIOCampaignServices;
use App\Services\PrivateServices\DeviceDetectionService;
use App\Services\PrivateServices\SplitTestService;
use App\Services\PrivateServices\SurveyService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CampaignRepository;
use App\Services\ToolServices;
use App\Services\PrivateServices\DataValidationService;
use App\Services\PrivateServices\ProjectService;

/**
 * Class ProjectController
 * @package App\Controller
 * @Route( "/project")
 */
class ProjectController extends AbstractController
{
    private const AccountNumber = 'accountNo';
    private $CampaignRepository;
    private $DataValidationService;
    private $LoggerInterface;
    private $ToolService;
    private $ProjectService;


    /**
     * ProjectController constructor.
     * @param CampaignRepository $CampaignRepository
     * @param DataValidationService $DataValidationService
     * @param LoggerInterface $logger
     * @param ToolServices $ToolService
     * @param ProjectService $ProjectService
     * @author aki
     */
    public function __construct(CampaignRepository $CampaignRepository,
                                DataValidationService $DataValidationService,
                                LoggerInterface $logger,
                                ToolServices  $ToolService,
                                ProjectService $ProjectService
)
    {
        $this->CampaignRepository = $CampaignRepository;
        $this->DataValidationService = $DataValidationService;
        $this->LoggerInterface = $logger;
        $this->ToolService = $ToolService;
        $this->ProjectService = $ProjectService;
    }


    /**
     * @Route("/create", methods={"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     * @author AKI
     */
    public function createProject(
        Request $request
    ): JsonResponse
    {
        $en_data = $request->getContent();
        $response = '';
        $data = $this->ToolService->decryptDataToNameValuePair($en_data);

        //todo:for now we use company ID but need to make use of apikey i.e take account number value form api key
        //todo:validate data if additional parameters are sent.
        $accountNumber = $data[self::AccountNumber];
        //todo:check for DPA acceptance ???
        try{
            if($this->ProjectService->checkForValidAccount($accountNumber)){ //Check for valid account

                $campaignId = $this->ProjectService->createProject($accountNumber);
/*                //todo: always make entry for created by and updated by !!!
                $objectRegisterId = $this->ProjectService->makeObjectRegisterLayerEntry(); //objectregister layer entry
                $this->LoggerInterface->info("objectRegister Entry made for project with id".$objectRegisterId);

                //entry in campaign table
                $campaignId = $this->ProjectService->makeCampaignLayerEntry($accountNumber,$objectRegisterId); //campaign and campaign params entry*/
                if($campaignId === null){
                    return $response;
                }
                $response = 'Project created successfully';
            }else{
                $response = 'Account number or api key not valid';
            }
        }catch (\Exception $e) {
            $this->LoggerInterface->error($e->getMessage(), [__METHOD__, __LINE__]);
            $response = $e->getMessage();
        }

        //make campaign table entry;
        return $this->json($response);
    }
    

    /**
     * @Route("/update", methods={"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @author Aki
     */
    public function updateProject(
        Request $request
    ): JsonResponse
    {
        $en_data = $request->getContent();
        $response = '';
        $data = $this->ToolService->decryptDataToNameValuePair($en_data);
        $accountNumber = $data[self::AccountNumber];
        $count = count($data); //check if additional variables are present
        if($this->ProjectService->checkForValidAccount($accountNumber) && $count > 2){
            if($this->ProjectService->checkForValidData($data) && $this->ProjectService->updateProject($data)){
                $response = 'Project updated successfully';
            }
        }else{
            if(count < 2){
                $response = 'please add additional data';
            }else{
                $response = 'Please check the token/ account number';
            }
        }
        return $this->json($response);
    }


    /**
     * @Route("/read", methods={"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @author Aki
     */
    public function getProjectData(
        Request $request
    ): JsonResponse
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $en_data = $request->getContent();
        $settings = []; //type cast to json to return json response
        $data = $this->ToolService->decryptDataToNameValuePair($en_data);
        $accountNumber = $data[self::AccountNumber];
        if($this->ProjectService->checkForValidAccount($accountNumber)){
            if($this->ProjectService->checkForValidData($data)){
                $settings['settings'] = base64_encode($this->ProjectService->getProjectData($accountNumber));
            }
        }
        return $this->json($settings);

    }

    /**
     * @Route("/getDescription", methods={"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @author Aki
     */
    public function getProjecDescription(
        Request $request
    ): JsonResponse
    {
        $response = '';
        $response = $this->ProjectService->getDescription();
        if(empty($dataStructure)){
            return $this->json($response);
        }

        return $this->ToolService->setResponseWithMessage($response,'Please follow the data structure');

    }

}
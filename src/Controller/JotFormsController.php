<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterRepository;
use App\Services\JOTFormServices\JotFormPluginService;
use App\Services\JOTFormServices\JOTFormServices;
use App\Services\ToolServices;
use App\Types\MioStandardFieldTypes;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/v2.0/jotforms")
 */
class JotFormsController extends AbstractController
{
    /**
     * @var JotFormPluginService
     * @author Pradeep
     */
    private JotFormPluginService $jotFormPluginService;

    public function __construct(
        JotFormPluginService $jotFormPluginService,
        ToolServices $toolServices,
        LoggerInterface $logger
    ) {
        $this->jotFormPluginService = $jotFormPluginService;
        $this->toolServices = $toolServices;
        $this->logger = $logger;
    }

    /**
     * @Route("/webhooks/get", name="get_jotform_webhooks" ,methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @author Pradeep
     */
    public function getWebhooksWithJotFormConfigurations(
        Request $request,
        ToolServices $toolServices,
        CampaignRepository $campaignRepository
    ): Response {
        $status = false;
        $message = "failed to fetch webhooks which has jotform configured for the account";
        $data = $request->getContent();
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);

        try {
            $content = $toolServices->decryptContentData($data);
            if (!isset($content[ 'base' ][ 'apikey' ], $content[ 'base' ][ 'account_no' ])) {
                throw new RuntimeException("Invalid payload provided " . json_encode($content));
            }
            $result = $campaignRepository->getWebhooksWithJotFormsInstalled($content[ 'base' ][ 'account_no' ]);
            return $this->toolServices->setJsonResponse($response, $result, true, 'successfully fetched webhooks');
        } catch (Exception | RuntimeException  $e) {
            return $this->toolServices->setJsonResponse($response, [], $status, $e->getMessage());
        }
    }

    /**
     * @Route("/search", name="search_jotform_id" ,methods={"POST"})
     * Searches formId of the JotForm inside DB.
     * - This Function is triggered from Jotform Plugin.
     * - When an entry is found -> plugin skips the MIO authentication step.
     * - when no entry is found -> plugin resumes the MIO authentication step with APIKey and AccountNumber.
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function searchFormId(Request $request): Response
    {
        $this->logger->info('SEARCH-FORM-ID : STARTED');
        $status = false;
        $message = "Form Id is not found";
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);

        try {
            $en_data = $request->getContent();
            $this->logger->info('SEARCH-FORM-ID :', [$en_data, __METHOD__, __LINE__]);
            $data = $this->toolServices->decryptContentData($en_data);
            $this->logger->info('SEARCH-FORM-ID DATA:', [$data, __METHOD__, __LINE__]);
            if (empty($data) || !isset($data[ 'jt_form_id' ], $data[ 'jt_user_name' ], $data[ 'jt_apikey' ])) {
                throw new RuntimeException("Invalid payload provided");
            }
            $formId = $data[ 'jt_form_id' ];
            $userName = $data[ 'jt_user_name' ];
            $apikey = $data[ 'jt_apikey' ];
            $searchResult = $this->jotFormPluginService->search($formId, $userName, $apikey);
            $this->logger->info('SEARCH-FORM-ID SEARCH RESULT:', [$searchResult, __METHOD__, __LINE__]);
            if (!empty($searchResult)) {
                $status = true;
                $message = "FormId is found";
            }
            return $this->toolServices->setJsonResponse($response, $searchResult, $status, $message);
        } catch (Exception | RuntimeException $e) {
            return $this->toolServices->setJsonResponse($response, [], $status, $message);
        }
    }

    /**
     * @Route("/save", name="save_jotform_configuration" ,methods={"POST"})
     * Saves the JotForm Config data to ObjectRegMeta table for a specific ObjectRegId (webhook)
     * - Service is called by the JotForms plugin.
     * - Saves the field mapping between jotforms and MIO.
     * - also saves other related jotform data.
     * - The connection between jotform and MIO is successfully only when saveAction is successful.
     * @param Request $request
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     * @author Pradeep
     */
    public function saveAction(
        Request $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): Response {
        $this->logger->info('SAVE_ACTION : STARTED');
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $en_data = $request->getContent();
            $this->logger->info('SAVE_ACTION:PAYLOAD ', [$en_data, __METHOD__, __LINE__]);
            $data = $this->toolServices->decryptContentData($en_data);
            $uuid = $data[ 'base' ][ 'uuid' ];
            $apikey = $data[ 'base' ][ 'apikey' ];
            $accountNumber = $data[ 'base' ][ 'account_no' ];
            $mapping = $data[ 'config' ][ 'mapping' ];
            $jotformId = $data[ 'config' ][ 'jt_form_id' ];
            $jotFormApiKey = $data[ 'config' ][ 'jt_apikey' ];
            $jotFormUserName = $data[ 'config' ][ 'jt_user_name' ];
            $jotFormRequestUri = $data[ 'config' ][ 'jt_request_uri' ];

            $objRegId = $objectRegisterRepository->getObjectRegisterIdWithUUID($uuid);
            $this->logger->info('SAVE_ACTION:OBJREGID ', [$objRegId, __METHOD__, __LINE__]);
            // Map to jotForm Meta Keys for  objectRegMeta table.
            $jotFormPluginConfig = $this->jotFormPluginService->mapToJotFormMetaKeys(
                $jotformId, $jotFormUserName, $jotFormApiKey, $jotFormRequestUri, $mapping, $apikey, $accountNumber,
                $uuid);
            $this->logger->info('SAVE_ACTION:PLUGINCONFIG ', [$jotFormPluginConfig, __METHOD__, __LINE__]);
            // insert all the jotform data to different metakeys to objRegMetaTable.
            // all the JTFields are inserted to specific ObjRegId.
            // Reports error when atleast one of the JTFields caused error.
            if (!$this->jotFormPluginService->insert($objRegId, $jotFormPluginConfig)) {
                throw new RuntimeException("failed to save mapping");
            }
            $this->logger->info('SAVE_ACTION : COMPLETED');
            return $this->toolServices->setJsonResponse($response, [], true, 'Successfully saved ');

        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->toolServices->setJsonResponse($response, [], false, $e->getMessage());
        }
    }

    /**
     * @Route("/get", name="get_jotform_mapping" ,methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function getAction(Request $request): Response
    {
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $en_data = $request->getContent();
            $data = $this->toolServices->decryptContentData($en_data);
            $apikey = $data[ 'base' ][ 'apikey' ];
            $accountNumber = $data[ 'base' ][ 'account_no' ];
            $jotformId = $data[ 'config' ][ 'jt_form_id' ];

            $mapping = $this->jotFormPluginService->getFieldMapping($jotformId);

            return $this->toolServices->setResponseWithMessage($response, 'Successfully fetched mapping information ',
                true, ['mapping' => $mapping],);

        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->toolServices->setResponseWithMessage($response, 'failed to fetch mapping information', false,
                [$e->getMessage()]);
        }
    }

    /**
     * @Route("/remove", name="remove_jotform_mapping" ,methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function removeAction(
        Request $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): Response {
        $response = new Response();
        $response = $this->toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $en_data = $request->getContent();
            $data = $this->toolServices->decryptContentData($en_data);
            $apikey = $data[ 'base' ][ 'apikey' ];
            $accountNumber = $data[ 'base' ][ 'account_no' ];
            $uuid = $data[ 'base' ][ 'uuid' ];
            $jotformId = $data[ 'config' ][ 'jt_form_id' ];

            $objRegId = $objectRegisterRepository->getObjectRegisterIdWithUUID($uuid);
            if (!$this->jotFormPluginService->remove($objRegId, $jotformId)) {
                throw new RuntimeException('Failed to delete entry in objectRegisterMetaRepository');
            }

            return $this->toolServices->setResponseWithMessage($response, 'Successfully deleted jotforms ', true, []);

        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->toolServices->setResponseWithMessage($response, 'failed to delete jotform', false,
                [$e->getMessage()]);
        }
    }

    /**
     * takes weired jotForms-Posts, converts them into expected format for /v2.0/contact/create and forwards to /v2.0/contact/create
     * @Route("/contact/create", name="v1JotFormsContactCreate")
     * @deprecated
     */
    public function v1JotFormsContactCreate(
        Request $request,
        ObjectRegisterRepository $objectRegisterRepository,
        MioStandardFieldTypes $mioStandardFieldTypes,
        MIOStandardFieldRepository $standardFieldRepository,
        ToolServices $toolServices,
        LoggerInterface $logger): Response
    {
        /*
    [submission_id] => 5246186157911874913
    [formID] => 220882953058362
    [ip] => 95.222.30.197
    [apikey] => b48ea9cc-3e65-11ec-b9ea-c43772f7ef7d
    [account_number] => 1022
    [contact_producttest] => Array
        (
            [0] => Gratis-Beauty-Geschenkset|https://www.jotform.com/uploads/svenbanks/form_files/gratisprodukte-jotform-produktbild-3.61e58ea6b637d2.10971794.jpg
        )

    [contact_fieldofinterest] => Array
        (
            [0] => Finanzen & Geldanlage|https://www.jotform.com/uploads/svenbanks/form_files/k1.61e59005d06179.03199056.jpg
            [1] => Shopping, Lifestyle, Beauty & Mode|https://www.jotform.com/uploads/svenbanks/form_files/k2.61e59015a6db39.35938390.jpg
            [2] => Reisen & Hotels|https://www.jotform.com/uploads/svenbanks/form_files/k4.61e590253b07f1.21225477.jpg
            [3] => Haustiere|https://www.jotform.com/uploads/svenbanks/form_files/k7.61e590444dba64.93282721.jpg
            [4] => Spiele, Unterhaltung & Lotto|https://www.jotform.com/uploads/svenbanks/form_files/k8.61e5904c4513c7.54935287.jpg
        )

    [contact_salutation] => Herr
    [contact_firstname] => Aravind
    [contact_lastname] => Karri
    [contact_street] => frankfurter str 46
    [contact_postalcode] => 34121
    [contact_birthday] => Array
        (
            [0] => 21
            [1] => 11
            [2] => 1992
        )

    [contact_email] => aravind.karri@treaction.net
    [contact_phone] => Array
        (
            [0] => (173) 888-8888
        )

    [contact_newspaper] => Array
        (
            [0] => FOCUS (0€/6 Ausgaben - sonst 2,30€)|https://www.jotform.com/uploads/svenbanks/form_files/Focus.61e5928338a102.76035079.jpeg
        )

    [duhast] => Array
        (
            [0] => Ich akzeptiere die Liefervereinbarung
            [1] => Ich akzeptiere die Nutzungsbedingungen
        )

)
         */
        $data = base64_decode($request->getContent());
        $logger->info('$data', [$data, __METHOD__, __LINE__]);

        //response
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);

//        try {
        $postArray = json_decode($data, true, 20, JSON_OBJECT_AS_ARRAY);
        $logger->info('$postArray', [$postArray, __METHOD__, __LINE__]);


        // TODO: this stuff goes to JotFormsService >>>> START
        $receiveContact = [];
        $receiveContact['base']['apikey'] = $postArray['apikey'];
        $receiveContact['base']['account_number'] = $postArray['account_number'];
        $receiveContact['base']['uuid'] = $postArray['uuid'];
        //remove base elements from the post array
        unset($postArray['apikey'], $postArray['account_number'], $postArray['uuid'], $postArray['app_env']);
        //store the keys
        $postArrayKeys = array_keys($postArray);
        //loop the post array data
        foreach ($postArrayKeys as $jotFormKey) {
            //if the key has no brackets in jot forms
            if (array_key_exists($jotFormKey, $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS)) {
                $mappedKey = $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS[$jotFormKey];
                $logger->info('$mappedKey', [$mappedKey, __METHOD__, __LINE__]);

                $mappedKeyParts = explode('_', $jotFormKey); //  contact_salutation => 0:contact, 1:salutation

                // get mio.miostandardfield.fieldtype
                $standardFieldRecord = $standardFieldRepository->getFieldTypeByFieldName($mappedKey);
                $logger->info('$standardFieldRecord', [$standardFieldRecord, __METHOD__, __LINE__]);
                $logger->info('setKeyValuePair PARAMS', [$mappedKeyParts[1], $postArray[$jotFormKey], $standardFieldRecord, __METHOD__, __LINE__]);

                // TODO: phone value is an array  setKeyValuePair PARAMS ["phone",["(111) 111-1111"],{"fieldtype":"Standard","datatype":"Phone","regex":"[0-9-/ ()+]*"},"App\\Controller\\JotFormsController::v1JotFormsContactCreate",103] []
                $value = (is_array($postArray[$jotFormKey])) ? implode(',', $postArray[$jotFormKey]) : $postArray[$jotFormKey];

                $receiveContact[$mappedKeyParts[0]][$standardFieldRecord['fieldtype']][] = $this->setKeyValuePair($mappedKey, $value, $standardFieldRecord);
            } elseif (array_key_exists($jotFormKey, $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS)) {
                //if the key has brackets
                $mappedKey = $mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS[$jotFormKey];
                $mappedKeyParts = explode('_', $jotFormKey); //  contact_salutation => 0:contact, 1:salutation
                $standardFieldRecord = $standardFieldRepository->getFieldTypeByFieldName($mappedKey);
                $logger->info('$standardFieldRecord', [$standardFieldRecord, __METHOD__, __LINE__]);

                if (is_array($postArray[$jotFormKey])) {
                    $tempArray = [];
                    foreach ($postArray[$jotFormKey] as $value) {
                        $tempArray[] = explode('|', $value)[0];
                    }
                    $value = implode(',', $tempArray); // get as list
                    $receiveContact[$mappedKeyParts[0]][$standardFieldRecord['fieldtype']][] = $this->setKeyValuePair($mappedKeyParts[1], $value, $standardFieldRecord);
                } else {
                    $value = (is_array($postArray[$jotFormKey])) ? implode(',', $postArray[$jotFormKey]) : $postArray[$jotFormKey];

                    $receiveContact[$mappedKeyParts[0]][$standardFieldRecord['fieldtype']][] = $this->setKeyValuePair($mappedKey, $value, $standardFieldRecord);
                }
            } else { // handle customfields
                $value = (is_array($postArray[$jotFormKey])) ? implode(',', $postArray[$jotFormKey]) : $postArray[$jotFormKey];
                $jotFormKey = ($jotFormKey === 'submission_id') ? 'submissionId' : $jotFormKey;
                $jotFormKey = ($jotFormKey === 'formID') ? 'formId' : $jotFormKey;
                if(str_contains($jotFormKey,'_')){
                    $jotFormKey = substr($jotFormKey, strpos($jotFormKey, "_") + 1);
                }
                $receiveContact['contact']['custom'][] = $this->setKeyValuePair($jotFormKey, $value);
            }
        }
        // TODO: <<<< END this stuff goes to JotFormsService
        $logger->info('$receiveContact content', [$receiveContact, __METHOD__, __LINE__]);

        try {
            if ($_ENV['APP_ENV'] === 'dev') {
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'receiveContact.json', json_encode($receiveContact, JSON_PRETTY_PRINT));
            }
        } catch (\Exception $e) {
            $logger->info('file_put_contents failed', [$e->getMessage(), __METHOD__, __LINE__]);
        }
//        } catch (\Exception $e) {
//            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//            return $this->json(['status' => false, 'msg' => 'check your field configuration']);
//        }
        //todo: take ip from the client data

        $contactCreateResponse = $this->forward('App\Controller\ContactController::createContact',
            ['wb-receive' => base64_encode(json_encode($receiveContact)),
                'postSource' => 'JotForms'],
            ['client-ip' => $request->getClientIp()]);
        $responseContent = json_decode($contactCreateResponse->getContent(), true, 512,
            JSON_THROW_ON_ERROR);
        if ($responseContent[ 'status' ] === false) {
            $logger->error($responseContent[ 'message' ], [__METHOD__, __LINE__]);
        }

        return $toolServices->setResponseWithMessage($response, 'Contact is created or updated successfully.',
            true, []);
    }

    private function setKeyValuePair(string $field_name_value, string $field_value_value, array $standardFieldRecord = []): array
    {
        return [
            'field_name' => $field_name_value,
            $field_name_value => $field_value_value,
            'regex' => (empty($standardFieldRecord)) ? '.*' : $standardFieldRecord['regex'], // this must be defined in table mio.miostandardfield
            'required' => 0, // information does not come from JOTForms
            'datatype' => (empty($standardFieldRecord)) ? 'Text' : $standardFieldRecord['datatype'], // neither provided by JOTForms, nor defined in table mio.miostandardfield
        ];
    }

}

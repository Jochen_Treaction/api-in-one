<?php


namespace App\Controller;
ini_set('memory_limit', - 1);

use App\Message\ShopDataQueue\AddChunkDataToShopDataQueue;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\CustomFieldRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ProcesshookRepository;
use App\Repository\StatusdefRepository;
use App\Services\AIOServices\GeneralServices;
use App\Services\JOTFormServices\JOTFormServices;
use App\Services\PIPServices\ProcessIntegrationPointService;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class WebhookController extends AbstractController
{

    /**
     * #Route("/webhook/list/get",name="webhook_get_list", methods={"POST"})
     * @Route("/v2.0/webhook/list/read", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CompanyRepository $companyRepository
     * @param CampaignRepository $campaignRepository
     * @param GeneralServices $accountIntegrations
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function getListOfWebhooks(
        Request $request,
        ToolServices $toolServices,
        CompanyRepository $companyRepository,
        CampaignRepository $campaignRepository,
        GeneralServices $accountIntegrations,
        LoggerInterface $logger
    ): Response {
        $endata = $request->get('data');
        try {
            $data = json_decode(base64_decode($endata), true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY)[ SourceApiTypes::BASE ];
            $response = new Response();
            $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
            if (!isset($data[ 'apikey' ], $data[ 'account_no' ]) || (int)$data[ 'account_no' ] <= 0) {
                throw new RuntimeException('Invalid Key Or Company Account Number');
            }// Get Company Details using Account number
            $company = $companyRepository->getCompanyDetails(0, $data[ 'account_no' ]);
            //$company = $companyServices->fetchCompanyDetailsByAccountId($data[ 'accountNo' ]);
            if (empty($company) || (int)$company[ 'id' ] <= 0) {
                throw new RuntimeException('Invalid Company Account Number');
            }
            if (!$accountIntegrations->validateCIOAPIKeyWithCompanyId($data[ 'apikey' ], $company[ 'id' ])) {
                throw new RuntimeException('APIKey and CompanyId does not match');
            }// Get Active Webhook from DB
            $webhooks = $campaignRepository->getActiveWebhooks((int)$company[ 'id' ]);
            if (empty($webhooks)) {
                $webhooks = [];
            }
            return $toolServices->setJsonResponse($response, $webhooks, true, 'Key is Validated');
        } catch (JsonException|RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setJsonResponse($response, [], false, $e->getMessage());
        }
    }

    /**
     * @Route("/v2.0/webhook/read", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @return Response
     * @author Pradeep
     */
    public function getWebhookDetails(
        Request $request,
        ToolServices $toolServices,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger
    ): Response {
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $en_data = $request->getContent();
            $data = $toolServices->decryptContentData($en_data);
            if (empty($data) || !isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ])) {
                throw new RuntimeException("Invalid Webhook UUID provided");
            }
            $uuid = $data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ];
            $objRegId = $objectRegisterRepository->getObjectRegisterIdWithUUID($uuid);
            if (empty($objRegId) || $objRegId <= 0) {
                throw new RuntimeException("Invalid Webhook Object Register Id provided");
            }
            $objRegMeta = $objectRegisterMetaRepository->getMetaValue((int)$objRegId, 'webhook');
            return $toolServices->setJsonResponse($response, $objRegMeta, true, 'Successfully fetched webhook details');
        } catch (Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setJsonResponse($response, [], false, $e->getMessage());
        }
    }

    /**
     * @Route("/webhook/list/read", methods={"POST"})
     * #Route("/webhook/get", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function getWebhookList(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/webhook/insert", name="webhook_create", methods={"POST","GET"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param GeneralServices $accountIntegrations
     * @param ProcessIntegrationPointService $processIntegrationPointService
     * @param CustomFieldRepository $customFieldRepository
     * @param ProcesshookRepository $processhookRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function createWebhook(
        Request $request,
        ToolServices $toolServices,
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        GeneralServices $accountIntegrations,
        ProcessIntegrationPointService $processIntegrationPointService,
        CustomFieldRepository $customFieldRepository,
        ProcesshookRepository $processhookRepository,
        LoggerInterface $logger
    ): Response {
        $requestData = $request->get('data');
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            $data = $toolServices->decryptDataToNameValuePair($requestData);
            $company = $companyRepository->getCompanyDetails(0, $data[ 'accountNo' ]);
            $projectId = $campaignRepository->getProjectId($data[ 'accountNo' ]);
            $webhookName = empty($data[ 'webhookName' ]) ? '' : $data[ 'webhookName' ];

            // Validate APIKey and Project Id
            $logger->info('createWebhook', [json_encode($data)]);
            if (($projectId === null) || !$accountIntegrations->validateCIOAPIKeyWithCompanyId(
                    $data[ 'apikey' ], $company[ 'id' ])) {
                $response = $toolServices->setResponseWithMessage($response, 'Invalid Request', false);
                throw new RuntimeException('Invalid ProjectId or APIKey');
            }
            // Create Webhook in DB.
            $userId = $data[ 'userId' ] ?? '';
            $webhookId = $campaignRepository->createWebhook($company[ 'id' ], $webhookName, $data, $userId);

            // Since StandardCustomfields are categorized as ecommercefields and merged to leads table.
            /*
                        if (($webhookName === 'eCommerceHook') && $webhookId !== null) {
                            $customFieldRepository->createStandardCustomFields($projectId, $webhookId);
                        }
            */
            // create Webhook in Server
            // create webhook with account number instead of name
            if (!isset($_ENV[ 'WEBHOOK_SERVICE' ]) || $webhookId === null ||
                !$processhookRepository->configureProcessHooks($webhookName, $webhookId, $projectId) ||
                !$processIntegrationPointService->createProcessInServer(
                    $_ENV[ 'WEBHOOK_SERVICE' ], $webhookName, $webhookId, $company[ 'id' ],
                    'webhook', $data[ 'accountNo' ])) {
                throw new RuntimeException('Failed to insert Webhook in Server');
            }
            return $toolServices->setResponseWithMessage($response, 'Webhook created successfully', true);
        } catch (RuntimeException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setResponseWithMessage($response, $e->getMessage() , false);
        }
    }

    /**
     * @Route("webhook/activate", name="webhook_activate", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param CampaignRepository $campaignRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param CompanyRepository $companyRepository
     * @param StatusdefRepository $statusdefRepository
     * @param ProcessIntegrationPointService $processIntegrationPointService
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function activateWebhook(
        Request $request,
        ToolServices $toolServices,
        CampaignRepository $campaignRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        CompanyRepository $companyRepository,
        StatusdefRepository $statusdefRepository,
        ProcessIntegrationPointService $processIntegrationPointService,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger
    ): Response {
        $requestData = $request->get('data');
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        $data = $toolServices->decryptDataToNameValuePair($requestData);
        $statusActiveId = $statusdefRepository->getStatusdefId(StatusdefRepository::ACTIVE);
        try {
            if (!isset($data[ 'accountNo' ], $data[ 'apikey' ]) || empty($data)) {
                throw new RuntimeException('Invalid information provided for activating Webhook');
            }
            $company = $companyRepository->getCompanyDetails(0, $data[ 'accountNo' ]);
            // $projectId = $campaignRepository->getProjectId($data['accountNo']);
            //Todo:: check the validation of APIKey with account Number.
            /*if (($projectId === null) || $accountIntegrations->validateCIOAPIKeyWithCompanyId(
                    $data[ 'apikey' ], $company[ 'id' ])) {

                throw new RuntimeException('Invalid ProjectId or APIKey');
            }*/
            if (!isset($data[ 'webhookName' ]) || empty($data[ 'webhookName' ])) {
                throw new RuntimeException('Webhook name cannot be empty');
            }
            $webhook = $campaignRepository->getCampaignDetailsByName($data[ 'webhookName' ], $company[ 'id' ]);
            if ($webhook === null || $webhook[ 'objectregister_id' ] <= 0) {
                throw new RuntimeException('Webhook Details not found in Database Or Invalid objectregister_id');
            }
            // Get Webhook ObjectRegister
            $webhkObjReg = $objectRegisterRepository->get($webhook[ 'objectregister_id' ]);
            if ($webhkObjReg === null || $webhkObjReg[ 'id' ] <= 0) {
                throw new RuntimeException('Invalid ObjectRegister Details for Webhook');
            }
            // Update webhook status to Active in DB
            if (!$objectRegisterRepository->update($webhkObjReg[ 'id' ], $webhkObjReg[ 'unique_object_type_id' ],
                $statusActiveId, $webhkObjReg[ 'version' ], $webhkObjReg[ 'modification_no' ],
                $webhkObjReg[ 'comment' ])) {
                throw new RuntimeException('Failed to Update Webhook for changing status to Active.');
            }
            // Activate webhook in Server.
            $campaignSettings = $objectRegisterMetaRepository->getMetaValue($webhook[ 'objectregister_id' ],
                ObjectRegisterMetaRepository::MetaKeyWebHook);
            $campaignSettings[ 'additionalSettings' ][ 'AIOUrl' ] = $_ENV[ 'APIINONE_URL' ];
            $campaignSettings = json_encode($campaignSettings);
            if (!isset($_ENV[ 'WEBHOOK_SERVICE' ]) || $webhook[ 'id' ] === null ||
                !$processIntegrationPointService->activateProcessInServer($_ENV[ 'WEBHOOK_SERVICE' ],
                    $webhook[ 'name' ],
                    $webhkObjReg[ 'version' ], $data[ 'accountNo' ], $campaignSettings)) {
                throw new RuntimeException('Failed to insert Webhook in Server');
            }
            $response = $toolServices->setResponseWithMessage($response, 'Webhook activated successfully', true);
            return $response;
        } catch (RuntimeException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $response = $toolServices->setResponseWithMessage($response, $e->getMessage(), false);
            return $response;
        }

        // $processIntegrationPointService->activateProcessInServer($_ENV[ 'WEBHOOK_SERVICE' ],$data[ 'webhookName' ],)
    }


    /**
     * TODO <b style="color:red">@ Pradeep => don't inject what's not used</b>
     * @Route("/v2.0/webhook/receive", name="webhook_receive", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function receiveWebhook(
        Request $request,
        ToolServices $toolServices,
        MessageBusInterface $messageBus,
        JOTFormServices $JOTFormServices,
        LoggerInterface $logger
    ): Response {
        $en_data = $request->getContent();
        $clientIp = $request->getClientIp();
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
        try {
            //check for the source type
            if($request->headers->get(SourceApiTypes::SOURCE) === SourceApiTypes::JOTFORMS){
                //This is to formate jotform data into MIO payload structure
                $en_data = $JOTFormServices->structureJotFormData($en_data);
            }
            // check if the request is Async
            if ($request->headers->get(SourceApiTypes::HEADER_SYNC_MODE) === SourceApiTypes::HEADER_SYNC_MODE_ASYNC) {
                //present only shop wear data
                $chunkData = $en_data;
                $chunkKey = $request->headers->get('ChunkKey') ?? '';
                $readyToWriteToDB = $request->headers->get('ReadyToWrite');
                // Message bus to savePayloadToWebhookQueue.
                $message = new AddChunkDataToShopDataQueue($chunkData, $chunkKey, $readyToWriteToDB);
                $messageBus->dispatch($message);
            } else {

                $data = $toolServices->decryptContentData($en_data);

                //variable to identify client either UUID or object register id
                if (isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::UUID ]) ||
                    isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::ObjectRegisterId ])) {
                    $clientIdentifier = true;
                }

                if (!isset($data[ SourceApiTypes::BASE ][ SourceApiTypes::APIKEY ],
                    $data[ SourceApiTypes::BASE ][ SourceApiTypes::AccountNumber ],
                    $clientIdentifier)) {
                    throw new RuntimeException('Invalid APIKey or accountNo or ObjectRegisterId-UUID');
                }

                $logger->info('WebhookReceive Data ' . json_encode($data, JSON_THROW_ON_ERROR));

                $forwardData[ 'base' ] = $data[ SourceApiTypes::BASE ];
                // For Creating More than one Contact.
                if (isset($data[ 'contacts' ]) && !empty($data[ 'contacts' ])) {
                    $logger->info('data in IF COndition' . json_encode($data[ 'contacts' ]));
                    foreach ($data[ 'contacts' ] as $contact) {
                        if (!isset($contact[ 'standard' ])) {
                            continue;
                        }
                        $logger->info('Contact in forloop' . json_encode($contact));
                        $forwardData[ 'contact' ] = $contact;
                        $forwardBody = $toolServices->arrToBase64Encrypt($forwardData);
                        $logger->info('Single ForwardBody ' . $forwardBody);
                        $contactCreateResponse = $this->forward('App\Controller\ContactController::createContact',
                            ['wb-receive' => $forwardBody],['client-ip' => $clientIp]);
                        $responseContent = json_decode($contactCreateResponse->getContent(), true, 512,
                            JSON_THROW_ON_ERROR);
                        if ($responseContent[ 'status' ] === false) {
                            $logger->error($responseContent[ 'message' ], [__METHOD__, __LINE__]);
                            continue;
                        }
                        unset($forwardData[ 'contact' ], $forwardBody);
                    }
                } else {
                    if (isset($data[ 'contact' ]) && !empty($data[ 'contact' ])) {
                        $contactCreateResponse = $this->forward('App\Controller\ContactController::createContact',
                            ['wb-receive' => $en_data],['client-ip' => $clientIp]);
                        $responseContent = json_decode($contactCreateResponse->getContent(), true, 512,
                            JSON_THROW_ON_ERROR);
                        if ($responseContent[ 'status' ] === false) {
                            $logger->error($responseContent[ 'message' ], [__METHOD__, __LINE__]);
                        }
                    }
                }
                // To Create Or Update Single Contact.
            }


            $response = $toolServices->setResponseWithMessage($response, 'Contact is created or updated successfully.',
                true, []);
            return $response;
        } catch (RuntimeException|Exception $e) {
            $response = $toolServices->setResponseWithMessage($response, $e->getMessage(), false, []);
            return $response;
        }
    }

    /**
     * @Route("/webhook/update", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function updateWebhook(Request $request): Response
    {
        return new Response();
    }

    /**
     * @Route("/webhook/delete", methods={"POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function deleteWebhook(Request $request): Response
    {
        return new Response();
    }


}

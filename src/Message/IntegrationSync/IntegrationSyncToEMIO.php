<?php

namespace App\Message\IntegrationSync;

class IntegrationSyncToEMIO
{

    /**
     * @var int
     * @author Pradeep
     */
    private int $companyId;
    /**
     * @var string
     * @author Pradeep
     */
    private string $apikey;
    /**
     * @var array
     * @author Pradeep
     */
    private array $contactsToSync;

    public function __construct(int $companyId, array $contactsToSync)
    {
        $this->companyId = $companyId;
        $this->contactsToSync = $contactsToSync;
    }

    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    public function getContactsToSync(): array
    {
        return $this->contactsToSync;
    }
}
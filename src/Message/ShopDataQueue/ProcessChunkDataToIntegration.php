<?php

namespace App\Message\ShopDataQueue;

class ProcessChunkDataToIntegration
{

    /**
     * @var int
     * @author Pradeep
     */
    private int $companyId;
    /**
     * @var int
     * @author Pradeep
     */
    private int $campaignId;
    /**
     * @var array
     * @author Pradeep
     */
    private array $chunkData;

    public function __construct(int $companyId, int $campaignId, array $chunkData)
    {
        $this->companyId = $companyId;
        $this->campaignId = $campaignId;
        $this->chunkData = $chunkData;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function getCampaignId()
    {
        return $this->campaignId;
    }

    public function getChunkData()
    {
        return $this->chunkData;
    }
}
<?php

namespace App\Message\ShopDataQueue;

use Symfony\Component\Messenger\HandleTrait;

class ProcessChunkDataFromShopDataQueue
{

    use HandleTrait;

    /**
     * @var int
     * @author Pradeep
     */
    private int $jobId;
    /**
     * @var int
     * @author Pradeep
     */
    private int $objectRegisterId;
    /**
     * @var int
     * @author Pradeep
     */
    private int $accountId;
    /**
     * @var array
     * @author Pradeep
     */
    private array $chunkData;
    /**
     * @var array
     * @author Pradeep
     */
    private array $company;
    /**
     * @var array
     * @author Pradeep
     */
    private array $campaign;

    public function __construct(
        int $jobId,
        int $accountId,
        int $objectRegisterId,
        array $chunkData,
        array $company,
        array $campaign
    ) {
        $this->jobId = $jobId;
        $this->accountId = $accountId;
        $this->objectRegisterId = $objectRegisterId;
        $this->chunkData = $chunkData;
        $this->company = $company;
        $this->campaign = $campaign;

    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getObjectRegisterId(): int
    {
        return $this->objectRegisterId;
    }

    public function getChunkData(): array
    {
        return $this->chunkData;
    }

    public function getJobId(): int
    {
        return $this->jobId;
    }
}
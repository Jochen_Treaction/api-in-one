<?php

namespace App\Message\ShopDataQueue;

class ProcessChunkDataToWorkFlow
{

    /**
     * @var int
     * @author Pradeep
     */
    private int $companyId;
    /**
     * @var int
     * @author Pradeep
     */
    private int $campaignId;
    /**
     * @var int
     * @author Pradeep
     */
    private int $workflowId;
    /**
     * @var array
     * @author Pradeep
     */
    private array $chunkData;

    public function __construct(
        int $companyId,
        int $campaignId,
        int $workflowId,
        array $chunkData,
        array $chunkBaseData
    ) {
        $this->companyId = $companyId;
        $this->campaignId = $campaignId;
        $this->workflowId = $workflowId;
        $this->chunkData = $chunkData;
        $this->chunkBaseData = $chunkBaseData;
    }

    public function getChunkBaseData()
    {
        return $this->chunkBaseData;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function getCampaignId()
    {
        return $this->campaignId;
    }

    public function getWorkflowId()
    {
        return $this->workflowId;
    }

    public function getChunkData()
    {
        return $this->chunkData;
    }
}
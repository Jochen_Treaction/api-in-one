<?php

namespace App\Message\ShopDataQueue;

class AddChunkDataToShopDataQueue
{
    /**
     * @var string
     * @author Pradeep
     */
    private string $chunkKey;
    /**
     * @var string
     * @author Pradeep
     */
    private string $chunkData;
    /**
     * @var string
     * @author Pradeep
     */
    private string $readyToWriteToDB;

    /**
     * @param string $chunkKey
     * @param string $chunkData
     * @param bool $readyToWriteToDB
     */
    public function __construct(string $chunkData, string $chunkKey = '', string $readyToWriteToDB = 'false')
    {
        $this->chunkKey = $chunkKey;
        $this->chunkData = $chunkData;
        $this->readyToWriteToDB = $readyToWriteToDB;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getChunkKey(): string
    {
        return $this->chunkKey;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getChunkData(): string
    {
        return $this->chunkData;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getReadyToWriteToDB(): string
    {
        return $this->readyToWriteToDB;
    }

}
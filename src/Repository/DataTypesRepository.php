<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class DataTypesRepository
{

    public function __construct(Connection $connection, LoggerInterface $logger) {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    public function insert():?int{
        return 1;
    }

    public function update():?bool
    {
        return true;
    }

    public function delete():?bool
    {
        return true;
    }

    /**
     * Get DataType details based on datatype Id.
     * @param int $dataTypeId
     * @return array|null
     * @author Pradeep
     */
    public function get(int $dataTypeId):?array
    {
        $dataType = [];
        try {
            if ($dataTypeId <= 0) {
                throw new RuntimeException('Invalid DataType Id provided');
            }
            $sql = 'SELECT * FROM datatypes WHERE id=:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $dataTypeId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $dataType = $result[ 0 ];
            }
            return $dataType;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @internal used in Bastelcontroller (/check/alive) to check installation with database connection
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     */
	public function getAllDatatypes():?array
	{
		$dataTypes = [];
		try {
			$sql = 'SELECT id, name, phptype FROM datatypes';
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();
			$rowCount = $stmt->rowCount();
			$dataTypes = $stmt->fetchAll();
		} catch (DBALException | RuntimeException | Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
			return ["error" => $e->getMessage()];
		}
		return $dataTypes;
	}


    /**
     * @param string $dataTypeName
     * @return array|null
     * @author Pradeep
     */
    public function getDataTypeDetailsByName(string $dataTypeName):?array
    {
        $dataType=null;
        try {
            if (empty($dataTypeName)) {
                throw new RuntimeException("Invalid Data Type Name provided.");
            }
            $sql = 'SELECT * FROM datatypes WHERE name LIKE "'.$dataTypeName.'"';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 || !empty($result[ 0 ])) {
                $dataType = $result[ 0 ];
            }
            return $dataType;
        } catch (DBALException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $dataType;
        }
    }
}

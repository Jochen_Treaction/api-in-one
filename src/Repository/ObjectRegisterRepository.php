<?php


namespace App\Repository;

use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Exception;
use http\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Driver\ResultStatement;
use RuntimeException;

class ObjectRegisterRepository
{

    protected $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    private $Logger;
    /**
     * @var StatusModelRepository
     * @author Pradeep
     */
    private $statusModelRepository;
    /**
     * @var StatusdefRepository
     * @author Pradeep
     */
    private $statusdefRepository;
    /**
     * @var UniqueObjectTypeRepository
     * @author Pradeep
     */
    private $uniqueObjectTypeRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $Logger,
        StatusModelRepository $statusModelRepository,
        StatusdefRepository $statusdefRepository,
        UniqueObjectTypeRepository $uniqueObjectTypeRepository
    )
    {
        $this->conn = $connection;
        $this->logger = $Logger;
        $this->statusModelRepository = $statusModelRepository;
        $this->statusdefRepository = $statusdefRepository;
        $this->uniqueObjectTypeRepository = $uniqueObjectTypeRepository;
    }


    /**
     * @param string $uniqueObjectName
     * @param string $statusdefValue
     * @param int $userId
     * @return int|null
     * @author Pradeep
     */
    public function createObjectRegister(string $uniqueObjectName, string $statusdefValue, int $userId = 9999): ?int
    {
        $objectRegisterId = null;
        if (empty($uniqueObjectName) || empty($statusdefValue)) {
            return $objectRegisterId;
        }
        try {
            $uniqueObjectTypeId = $this->uniqueObjectTypeRepository->getUniqueObjectTypeId($uniqueObjectName);
            $statusdefId = $this->statusdefRepository->getStatusdefId($statusdefValue);
            if ($uniqueObjectTypeId === null || $statusdefId === null) {
                return $objectRegisterId;
            }
            return $this->create($uniqueObjectTypeId, $statusdefId, 1, '', 0, '', $userId, $userId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegisterId;
        }
    }


    /**
     * inserts an object register entry in DB.
     * -  table : objectregister.
     * @param int $uniqueObjectTypeId
     * @param int $statusdefId
     * @param int $version
     * @param string $object_unique_id
     * @param int $modification_no
     * @param string $comment
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @throws Exception
     * @author Pradeep
     */
    public function create(
        int $uniqueObjectTypeId,
        int $statusdefId,
        int $version = 1,
        string $object_unique_id = '',
        int $modification_no = 0,
        string $comment = '',
        int $createdBy = 999999,
        int $updatedBy = 999999
    ): ?int
    {
        $objectRegisterId = null;
        if (($uniqueObjectTypeId <= 0) || ($statusdefId <= 0)) {
            throw new Exception('Invalid uniqueobjectTypeId or statusdefId');
        }
        $sql = 'INSERT INTO objectregister(unique_object_type_id, statusdef_id, version, object_unique_id, modification_no, comment, created_by, updated_by)
                VALUES(:unique_object_type_id, :statusdef_id, :version, :object_unique_id ,:modification_no, :comment, :created_by, :updated_by)';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_type_id', $uniqueObjectTypeId, ParameterType::INTEGER);
            $stmt->bindParam(':statusdef_id', $statusdefId, ParameterType::INTEGER);
            $stmt->bindParam(':version', $version, ParameterType::INTEGER);
            $stmt->bindParam(':object_unique_id', $object_unique_id, ParameterType::STRING);
            $stmt->bindParam(':modification_no', $modification_no, ParameterType::INTEGER);
            $stmt->bindParam(':comment', $comment, ParameterType::INTEGER);
            $stmt->bindParam(':created_by', $createdBy, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $updatedBy, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $objectRegisterId = $this->conn->lastInsertId();
            }
            return $objectRegisterId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegisterId;
        }
    }

    /**
     * updates an objectregister entry in DB for a given objectregister id.
     * -  table : objectregister.
     * @param int $id
     * @param int $uniqueObjectTypeId
     * @param int $statusdefId
     * @param int $version
     * @param int $modification_no
     * @param string $comment
     * @return bool
     * @author Pradeep
     */
    public function update(
        int $id,
        int $uniqueObjectTypeId,
        int $statusdefId,
        int $version = 1,
        int $modification_no = 0,
        string $comment = ''
    ): bool
    {
        $status = false;
        try {
            if ($id <= 0) {
                throw new Exception('Invalid identifier provided to update objectregister');
            }
            $sql = 'UPDATE objectregister 
                    SET 
                        unique_object_type_id=:unique_object_type_id, 
                        statusdef_id=:statusdef_id, 
                        version=:version, 
                        modification_no=:modification_no, 
                        comment=:comment 
                    WHERE id=:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_type_id', $uniqueObjectTypeId, ParameterType::INTEGER);
            $stmt->bindParam(':statusdef_id', $statusdefId, ParameterType::INTEGER);
            $stmt->bindParam(':version', $version, ParameterType::INTEGER);
            $stmt->bindParam(':modification_no', $modification_no, ParameterType::INTEGER);
            $stmt->bindParam(':comment', $comment, ParameterType::STRING);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $status = true;
            }
            return $status;
        } catch (DBALException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    public function delete(): ?bool
    {

    }


    /**
     * createNewVersionForObjectRegister Creates an object Register with new Version.
     * - Version by default starts from 0
     * @param int $objectRegisterId
     * @param int $version
     * @return int|null
     * @author Pradeep
     */
    public function createNewVersionForObjectRegister(int $objectRegisterId, int $version=0): ?int
    {
        $objectRegisterDetails = $this->get($objectRegisterId);
        try {
            if ($objectRegisterDetails === null) {
                throw new RuntimeException('Failed to fetch Object register Information');
            }
            if ($version !== 0) {
                $objectRegisterDetails['version'] = $version;
            }else {
                $objectRegisterDetails['version'] = (int)$objectRegisterDetails['version'] + 1;
            }

            return $this->create($objectRegisterDetails['unique_object_type_id'],
                $objectRegisterDetails['statusdef_id'],
                $objectRegisterDetails['version'], $objectRegisterDetails['object_unique_id'], 0, 0);
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * gets the objectregister details for the given id(object register id)
     * -  table : objectregister.
     * @param int $objectRegisterId
     * @return array|null
     * @author Pradeep
     */
    public function get(int $objectRegisterId): ?array
    {
        $objectRegister = null;
        try {
            if ($objectRegisterId <= 0) {
                throw new Exception('Invalid id provided to fetch ObjectRegister details');
            }
            $sql = 'SELECT * FROM objectregister WHERE id= :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0])) {
                $objectRegister = $result[0];
            }
            return $objectRegister;
        } catch (DBALException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegister;
        }
    }

    /**
     * EXAMPLE
     * @param string $objectregister_id
     * @return array
     */
    public function getLeadFromObjectRegister(string $objectregister_id): ?array
    {

        $objectRegisterSelect = "select * from objectregister where id = :objectregister_id";
        $stmt = $this->conn->prepare($objectRegisterSelect);
        $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);

        try {
            $stmt->execute();
            $objectRegister = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $objectRegister;
    }


    /**
     * @return array|null
     * @author Pradeep
     */
    public function getUniqueObjectTypeNames(): ?array
    {
        $uniqueObjectSelect = "select id, unique_object_name from unique_object_type";
        $stmt = $this->conn->prepare($uniqueObjectSelect);

        try {
            $stmt->execute();
            $objectRegister = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount === 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $objectRegister;
    }


    public function getObjectRegisterId(int $campaign_id): ?int
    {
        $objectRegisterIdSelect = "select o.id from campaign c join objectregister o on c.objectregister_id = o.id where c.id = :id;";
        $stmt = $this->conn->prepare($objectRegisterIdSelect);
        $stmt->bindParam(':id', $campaign_id, ParameterType::INTEGER);

        try {
            $stmt->execute();
            $objectRegisterId = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $objectRegisterId;
    }

    /**
     * @return int|null
     */
    public function insertContactObjectRegisterEntry(int $version = null): ?int
    {
        $objectRegisterId = null;
        if (null === $version) {
            $objectRegisterContactInsert = "insert into objectregister (unique_object_type_id, version, statusdef_id) values (4,1,1)";
        } else {
            $objectRegisterContactInsert = "insert into objectregister (unique_object_type_id, version, statusdef_id) values (4,:version,1)";
        }
        $stmt = $this->conn->prepare($objectRegisterContactInsert);

        if (null !== $version) {
            $version++; // increment version number
            $stmt->bindParam(':version', $version, ParameterType::INTEGER);
        }
        try {
            $stmt->execute();

            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            } else {
                $objectRegisterId = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return null;
        }
        return $objectRegisterId;

    }


    /**
     * @param int $objectRegisterId
     * @return bool
     */
    public function updateLeadsObjectRegisterStatusToArchived(int $objectRegisterId): bool
    {
        $statusId = $this->getStatusdefIdByName('archived');
        $updateObjectRegister = "UPDATE objectregister SET statusdef_id = :statusId WHERE id = :objectRegisterId";
        $stmt = $this->conn->prepare($updateObjectRegister);
        $stmt->bindParam(':statusId', $statusId, ParameterType::INTEGER);
        $stmt->bindParam(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return false;
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return false;
        }
        return true;
    }


    /**
     * @param string $name
     * @return int|null
     */
    public function getStatusdefIdByName(string $name): ?int
    {
        $name = strtolower($name);
        $selectStatusDef = "select id from statusdef where value = :name";
        $stmt = $this->conn->prepare($selectStatusDef);
        $stmt->bindParam(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $record = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $record['id'];

    }

    /**
     * @param string $objectType
     * @return int|null
     * @author  aki
     */
    public function insertObjectRegisterEntry(string $objectType): ?int
    {
        $objectRegisterId = null;

        $uniqueObjectTypeId = $this->getUniqueObjectTypeIdFromName($objectType);

        if (empty($uniqueObjectTypeId)) {
            var_dump("Problem in fetching Object register type ");
            return null;
        }
        //initially statusdef is always active
        $objectRegisterInsert = "insert into objectregister (unique_object_type_id, version, object_unique_id ,statusdef_id) 
                                    values (:uniqueObjectTypeId,1,'',1)";
        $stmt = $this->conn->prepare($objectRegisterInsert);
        $stmt->bindParam(':uniqueObjectTypeId', $uniqueObjectTypeId, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }

            $objectRegisterId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->Logger->error($e->getMessage());
            var_dump($e->getMessage());
            return null;
        }
        return $objectRegisterId;
    }


    /**
     * @param string $objectType
     * @return int|null
     * @author  aki
     */
    public function getUniqueObjectTypeIdFromName(string $objectType): ?int
    {
        $uniqueObjectTypeId = null;
        if (empty($objectType)) {
            return $uniqueObjectTypeId;
        }
        $uniqueObjectTypeSql = "select u.id  from unique_object_type u where u.unique_object_name = :objectName";
        $stmt = $this->conn->prepare($uniqueObjectTypeSql);
        $stmt->bindParam(':objectName', $objectType, ParameterType::STRING);
        try {
            $stmt->execute();
            $uniqueObjectTypeFetch = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
            $uniqueObjectTypeId = $uniqueObjectTypeFetch['id'];
        } catch (Exception $e) {
            $this->Logger->error($e->getMessage());
            var_dump($e->getMessage());
            return $uniqueObjectTypeId;
        }

        return $uniqueObjectTypeId;

    }

    /**
     * @param int $objectRegisterId
     * @return int|null
     * @author  aki
     */
    public function getUniqueObjectNameFromObjectRegisterId(int $objectRegisterId): ?string
    {
        $uniqueObjectName = null;
        if ($objectRegisterId <= 0) {
            return null;
        }
        $uniqueObjectTypeSql = "SELECT unique_object_name from unique_object_type 
                                join objectregister o on unique_object_type.id = o.unique_object_type_id
                                WHERE o.id = :objectRegisterId";
        $stmt = $this->conn->prepare($uniqueObjectTypeSql);
        $stmt->bindParam(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $uniqueObjectTypeFetch = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
            $uniqueObjectName = $uniqueObjectTypeFetch['unique_object_name'];
        } catch (Exception $e) {
            $this->Logger->error($e->getMessage());
            return $uniqueObjectName;
        }

        return $uniqueObjectName;

    }

    /**
     * @param int $statusdefId
     * @param string $ModelName
     * @param int $uniqueObjectTypeId
     * @return int|null
     * @author  aki
     */
    public function insertStatusModelEntry(int $statusdefId, string $ModelName, int $uniqueObjectTypeId): ?int
    {
        $statusModelId = null;
        $statusModelInsert = "insert into status_model (unique_object_type_id, statusdef_id,modelname) values (:uniqueObjectTypeId,:statusdefId,:modelname)";
        $stmt = $this->conn->prepare($statusModelInsert);
        $stmt->bindParam(':uniqueObjectTypeId', $uniqueObjectTypeId, ParameterType::INTEGER);
        $stmt->bindParam(':statusdefId', $statusdefId, ParameterType::INTEGER);
        $stmt->bindParam(':modelname', $ModelName, ParameterType::STRING);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                var_dump('Problem in status model insertion');
                return null;
            } else {
                $statusModelId = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->Logger->error($e->getMessage());
            var_dump($e->getMessage());
            return null;
        }
        return $statusModelId;

    }


    /**
     * @param int $uniqueObjectTypeId
     * @param int $statusdefId
     * @param int $version
     * @param string $comment
     * @param int $userId
     * @return array|null
     * @author Pradeep
     */
    public function insertIntoObjectRegisterId(
        int $uniqueObjectTypeId,
        int $statusdefId,
        int $version,
        string $comment,
        int $userId = 1
    ): ?int
    {
        $objectRegisterId = null;
        if ($uniqueObjectTypeId <= 0 || $statusdefId <= 0 || $version <= 0) {
            $this->logger->info('Invalid parameters passed');
            return null;
        }
        $modification_no = 1;
        try {
            $sql = 'INSERT INTO objectregister (
                                unique_object_type_id, 
                                statusdef_id, 
                                version, 
                                modification_no, 
                                comment, 
                                created_by, 
                                updated_by
                                ) 
                                VALUES(
                                       :unique_object_type_id, 
                                       :statusdef_id, 
                                       :version, 
                                       :modification_no, 
                                       :comment, 
                                       :created_by,
                                       :updated_by
                                       ) ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_type_id', $uniqueObjectTypeId);
            $stmt->bindParam(':statusdef_id', $statusdefId);
            $stmt->bindParam(':version', $version);
            $stmt->bindParam(':modification_no', $modification_no);
            $stmt->bindParam(':comment', $comment);
            $stmt->bindParam(':created_by', $userId);
            $stmt->bindParam(':updated_by', $userId);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $objectRegisterId = $this->conn->lastInsertId();
            }
            $this->logger->info('ObjectRegister Id :' . $objectRegisterId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $objectRegisterId;
    }


    /**
     * @return int|null
     * @author Pradeep
     */
    public function getObjectRegisterIdForSuperAdmin(): ?int
    {
        $objectRegisterId = null;
        try {
            $uniqueObjectName = UniqueObjectTypes::SUPERADMIN;
            $sql = 'SELECT o.*
                    FROM objectregister o
                        JOIN unique_object_type uot on uot.id = o.unique_object_type_id
                    WHERE uot.unique_object_name = :unique_object_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_name', $uniqueObjectName);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result)) {
                $objectRegisterId = $result[0]['id'];
            }
            return $objectRegisterId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegisterId;
        }
    }

    /**
     * Gets the Status value for the ObjectRegister Object.
     * @param int $objectRegisterId
     * @return string
     * @author Pradeep
     *
     */
    public function getStatus(int $objectRegisterId): string
    {
        $status = '';
        try {
            if (empty($objectRegisterId)) {
                throw new RuntimeException('Invalid ObjectRegister Id provided');
            }
            $sql = 'select sd.value
                    from statusdef sd
                             join objectregister o on sd.id = o.statusdef_id
                    where o.id = :objectregister_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && !empty($result[0]) && isset($result[0]['value'])) {
                $status = $result[0]['value'];
            }
            return $status;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $UUID
     * @return string
     * @author Aki
     */
    public function getObjectRegisterIdWithUUID(string $UUID): string
    {
        $objectRegisterId = "";
        //only active pages/webhooks will save data using UUID, others use object register id
        $status = "active";
        try {
            if (empty($UUID)) {
                throw new RuntimeException('Invalid Unique identifier(UUID) provided');
            }
            $sql = 'select o.id
                    from objectregister o
                             join statusdef s on s.id = o.statusdef_id
                    where o.object_unique_id = :UUID and s.value = :status';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':UUID', $UUID, ParameterType::STRING);
            $stmt->bindParam(':status', $status, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && !empty($result[0]) && isset($result[0]['id'])) {
                $objectRegisterId = $result[0]['id'];
            }
            return $objectRegisterId;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegisterId;
        }


    }
}

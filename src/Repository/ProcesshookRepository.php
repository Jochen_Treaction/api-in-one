<?php


namespace App\Repository;

use App\Repository\Workflow\UserProcessHookHasWorkflow;
use App\Repository\Workflow\UserProcessHookRepository;
use App\Repository\Workflow\WorkflowRepository;
use App\Types\ProcesshookTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;

class ProcesshookRepository
{
    private Connection $conn;
    private LoggerInterface $logger;
    private ProcesshookTypes $processHookTypes;
    /**
     * @var UserProcessHookRepository
     * @author Pradeep
     */
    private UserProcessHookRepository $userProcessHookRepository;

    public function __construct(
        Connection $conn,
        LoggerInterface $logger,
        ProcesshookTypes $processhookTypes,
        UserProcessHookRepository  $userProcessHookRepository,
        WorkflowRepository $workflowRepository,
        UserProcessHookHasWorkflow $userProcessHookHasWorkflow
    )
    {
        $this->conn = $conn;
        $this->logger = $logger;
        $this->processHookTypes = $processhookTypes;
        $this->userProcessHookRepository = $userProcessHookRepository;
        $this->workflowRepository = $workflowRepository;
        $this->userProcessHookHasWorkflow = $userProcessHookHasWorkflow;
    }



    /**
     * @param $workflowId
     * @return array|null
     * @author aki
     */
    public function  getProcesshooksWithWorkflowId($workflowId): ?array
    {
        if ($workflowId <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = "select * from user_processhook_has_workflow where workflow_id = :workflow_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':workflow_id', $workflowId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result;

    }

    /**
     * @param $userProcesshookId
     * @return string|null
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     */
    public function getProcesshookType($userProcesshookId): ?string
    {
        if (empty($userProcesshookId)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = "select p.name from processhook p
                join user_processhook up on p.id = up.processhook_id
                where up.id = :user_processhook_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':user_processhook_id', $userProcesshookId);
            $stmt->execute();
            $result = $stmt->fetch();
            $name = $result['name'];
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $name;

    }

    /**
     * @param $userProcesshookId
     * @return array|null
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     */
    public function getUserProcesshook($userProcesshookId): ?array
    {
        if (empty($userProcesshookId)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = "select * from user_processhook
                where id = :user_processhook_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':user_processhook_id', $userProcesshookId);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result;
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     * @author Pradeep
     */
    public function get(int $id = 0, string $name=''):array
    {
        $result = [];
        try {
            if ($id <= 0 && empty($name)) {
                throw new Exception('Invalid Process Hook ID or Name provided');
            }
            $sql = 'select * from processhook where id = :id or name = :name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $stmt->bindValue(':name', $name);
            $stmt->execute();
            $result  = $stmt->fetchAll();
            if(empty($result) || !isset($result[0])) {
                return [];
            }
            $result = $result[0];
            $this->logger->info('get ProcessHooks '.json_encode($result));
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__,__LINE__]);
            return [];
        }
        return $result;
    }

    /**
     * @param $webhookName
     * @param int $webhookId
     * @param int $projectId
     * @return bool
     * @author Pradeep
     */
    public function configureProcessHooks($webhookName, int $webhookId, int $projectId):bool
    {
        $status = true;
        try {
            $defaultPHooks = $this->processHookTypes->getDefaultPHookForWebhook($webhookName);
            if(empty($defaultPHooks)) {
                throw new Exception('Unable to find ProcessHooks for '.$webhookName);
            }
            //Create a WorkFlow Id.
            $workflowId = $this->workflowRepository->getId($webhookId, true);
            if($workflowId <= 0 || $workflowId === null) {
                throw new Exception('Failed to create workflow for WebhookId :' .$webhookId);
            }
            foreach ($defaultPHooks as $hook) {
                $pHook = $this->get(0,$hook);
                if (empty($pHook) || !isset($pHook[ 'id' ])) {
                    $status = false;
                    $errMsg = 'Invalid ' . $pHook;
                    continue;
                }
                // Todo initiate beginTransaction
                $this->conn->beginTransaction();
                // user ProcessHook
                $userPHookId = $this->userProcessHookRepository->getId($projectId, true, $pHook[ 'id' ]);
                $this->logger->info('$userPHookId '.$userPHookId);
                // UserProcessHook has Workflow.
                if ($userPHookId === null ||
                    ($this->userProcessHookHasWorkflow->insert($userPHookId, $workflowId) <= 0) ||
                    ($this->workflowRepository->insertToObjectRegisterMeta($workflowId, $hook,"Neuer Lead",
                        $userPHookId) === null)
                ) {
                    // Todo rollback transaction $this->conn->rollBack();
                    $status = false;
                    $errMsg = 'Invalid ' . $userPHookId.' or '.$workflowId.' or failed to insert to UserProcessHookHasWorkflow';
                    continue;
                }
                if(!$status) {
                    $this->conn->rollBack();
                }
                $this->conn->commit();
            }
            if (!$status) {
                throw new Exception($errMsg);
            }
        } catch (\Doctrine\DBAL\Driver\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            return $status;
        }
        return $status;
    }

}
<?php


namespace App\Repository;


use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property ObjectTypeMetaRepository ObjectTypeMetaRepository
 * @property ObjectRegisterMetaRepository objectRegisterMetaRepository
 * @property ObjectRegisterRepository objectRegisterRepository
 */
class IntegrationsRepository
{


    public function __construct(
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
//        ObjectTypeMetaRepository $objectTypeMetaRepository,
        Connection $conn,
        LoggerInterface $logger
    ) {
//        $this->ObjectTypeMetaRepository = $objectTypeMetaRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->conn = $conn;
        $this->logger = $logger;

    }


    public function update(
        int $id,
        int $objectregisterId,
        int $companyId,
        int $campaignId,
        int $createdBy,
        int $updatedBy
    ): ?bool {

    }

    public function delete(int $id): ?bool
    {
        return null;
    }

    public function get(int $id): ?array
    {
        return null;
    }

    public function getIntegrationsForCompany(
        int $companyId,
        string $status = 'active'
    ): ?array {
        $integrationsOverview = [];
        if ($companyId <= 0) {
            throw new RuntimeException('Invalid CompanyId provided');
        }
        try {
            $sql = 'SELECT om.meta_key as integration_key, om.meta_value as integration_value
                    FROM integrations i
                        JOIN objectregister o ON i.objectregister_id = o.id
                        JOIN object_register_meta om ON o.id = om.object_register_id
                        JOIN statusdef s ON o.statusdef_id = s.id
                    WHERE i.company_id = :company_id AND s.value = :status';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':status', $status, ParameterType::STRING);
            $stmt->execute();
            $results = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($results)) {
                foreach ($results as $result) {
                    if (!isset($result[ 'integration_key' ]) && $result[ 'integration_value' ]) {
                        continue;
                    }
                    $integrationsOverview[ $result[ 'integration_key' ] ] = json_decode($result[ 'integration_value' ],
                        true, 512, JSON_THROW_ON_ERROR);
                }
            }
            return $integrationsOverview;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int $objectregisterId
     * @param int $companyId
     * @param int $campaignId
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @author Pradeep
     */
    public function insert(int $objectregisterId, int $companyId, int $campaignId, int $createdBy, int $updatedBy): ?int
    {
        $integrationsId = null;
        try {
            if ($objectregisterId <= 0 || $companyId <= 0 || $campaignId <= 0) {
                throw new RuntimeException('Invalid objectregisterId or CompanyId or CampaignId provided');
            }
            $sql = 'INSERT INTO integrations(objectregister_id, company_id, campaign_id, created_by, updated_by) 
                    VALUE(?,?,?,?,?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $objectregisterId, ParameterType::INTEGER);
            $stmt->bindParam(2, $companyId, ParameterType::INTEGER);
            $stmt->bindParam(3, $campaignId, ParameterType::INTEGER);
            $stmt->bindParam(4, $createdBy, ParameterType::INTEGER);
            $stmt->bindParam(5, $updatedBy, ParameterType::INTEGER);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $integrationsId = $this->conn->lastInsertId();
            }
            return $integrationsId;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $integrationObjectRegisterMetaId
     * @param int $integrationObjectregisterId
     * @param string $uniqueObjectName
     * @param array $integrationConfig
     * @return bool
     * @author Pradeep
     */
    public function updateIntegrationsConfigForCompany(
        int $integrationObjectRegisterMetaId,
        int $integrationObjectregisterId,
        string $uniqueObjectName,
        array $integrationConfig
    ): bool {
        try {
            if ($integrationObjectRegisterMetaId <= 0 || $integrationObjectregisterId <= 0 || empty($uniqueObjectName)) {
                throw new RuntimeException('Invalid configuration provided.');
            }
            $metaValue = json_encode($integrationConfig, JSON_THROW_ON_ERROR);
            if (!$this->objectRegisterMetaRepository->update($integrationObjectRegisterMetaId,
                $integrationObjectregisterId, $uniqueObjectName, $metaValue)) {
                throw new RuntimeException('Failed to update Object Register Meta Value');
            }
            return true;
        } catch (JsonException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param array $MIOCustomFields
     * @param int $company_id
     * @return bool|null
     * @author Pradeep
     */
    public function updateeMIOCustomFieldsFromProject(array $MIOCustomFields, int $company_id):?bool
    {
        try {
            $status = false;
            if (empty($MIOCustomFields) || $company_id <= 0) {
                throw new RuntimeException('Invalid CustomFields or Company Id provided');
            }// get the Integrations from eMIO.
            $eMIOIntegrationConfig = $this->getIntegrationConfigForCompany($company_id, UniqueObjectTypes::EMAILINONE);
            if (!isset($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ]) || empty($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ])) {
                return $status;
            }
            $eMIOCustomFields = json_decode(base64_decode($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ]), true, 512,
                JSON_THROW_ON_ERROR);
            $MIOCustomFieldList = $MIOCustomFields[ 'customfieldlist' ];
            //ForLoop to detect the deleted customfield in MIO and which is present in eMIO.
            //Accordingly remove the customfield in eMIO.
            //As of now customfields which are created automatically in eMIO are not touched.
            foreach ($eMIOCustomFields as $key => $value) {
                $eMIOFieldIsPresent = false;
                if (!isset($value[ 'marketing-in-one-custom-field' ])) {
                    continue;
                }
                $eMIOField = $value[ 'marketing-in-one-custom-field' ];
                foreach ($MIOCustomFieldList as $customfield) {
                    if ($eMIOField !== $customfield[ 'fieldname' ] || $customfield[ 'instruction' ] !== 'delete') {
                        continue;
                    }
                    $eMIOFieldIsPresent = true;
                }
                // remove the customfield from eMIO which has been deleted in MIO
                if ($eMIOFieldIsPresent) {
                    unset($eMIOCustomFields[ $key ]);
                }
            }
            unset($eMIOCustomField);
            $eMIOIntegrationConfig[ 'settings' ][ 'mapping' ] = base64_encode(json_encode($eMIOCustomFields,
                JSON_THROW_ON_ERROR));
            $metaValue = json_encode($eMIOIntegrationConfig, JSON_THROW_ON_ERROR);
            $metaKey = UniqueObjectTypes::EMAILINONE;

            $status = $this->objectRegisterMetaRepository->update((int)$eMIOIntegrationConfig['relation'][ 'integration_object_register_meta_id' ],
                (int)$eMIOIntegrationConfig['relation'][ 'integration_objectregister_id' ], $metaKey, $metaValue);
            if(!$status) {
                throw new RuntimeException('Failed to update Object register meta');
            }
            $this->logger->info('status of integration update '.$status);
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $status;
    }

    /**
     * @param int $companyId
     * @param string $uniqueObjectName
     * @param string $status
     * @return array|null
     * @author Pradeep
     */
    public function getIntegrationConfigForCompany(
        int $companyId,
        string $uniqueObjectName,
        string $status = 'active'
    ): ?array {
        $config = [];
        if ($companyId <= 0 || empty($uniqueObjectName)) {
            throw new RuntimeException('Invalid companyId or UniqueObjectName provided');
        }
        try {
            $sql = 'SELECT om.*, i.id AS integration_id
                    FROM object_register_meta om
                        JOIN objectregister o ON om.object_register_id = o.id
                        JOIN integrations i ON o.id = i.objectregister_id
                        JOIN unique_object_type uot ON o.unique_object_type_id = uot.id
                        JOIN statusdef s ON o.statusdef_id = s.id
                    WHERE i.company_id = :company_id
                      AND uot.unique_object_name LIKE :unique_object_name AND s.value=:status ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':unique_object_name', $uniqueObjectName, ParameterType::STRING);
            $stmt->bindValue(':status', $status, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
          //  var_dump(json_encode($result));
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[ 0 ]) && (!empty($result[ 0 ][ 'meta_value' ]))) {
                $emailServerJson = $result[ 0 ][ 'meta_value' ];
                $config = json_decode($emailServerJson, true, 512, JSON_THROW_ON_ERROR);
                $config[ 'relation' ][ 'integration_object_register_meta_id' ] = $result[ 0 ][ 'id' ];
                $config['relation']['integration_id'] = $result[0]['integration_id'];
            }
            return $config;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getDefaultEmailServerConfiguration():?array
    {
        $objRegId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        if($objRegId === null) {
            return [];
        }
        $emailServerConfig = $this->objectRegisterMetaRepository->getMetaValue($objRegId, UniqueObjectTypes::EMAILSERVER);
        if ($emailServerConfig === null || empty($emailServerConfig)) {
            return [];
        }
        return $emailServerConfig;
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    public function getEMailInOneAPIkeySyncUserForNewsletter(): ?string
    {
        $apikey = '';
        $objRegId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        if ($objRegId === null) {
            return $apikey;
        }
        $objRegMetaDetails = $this->objectRegisterMetaRepository->getMetaValue($objRegId,
            UniqueObjectTypes::EMAILINONE);
        if (!empty($objRegMetaDetails) && isset($objRegMetaDetails[ 'settings' ][ 'apikey' ])) {
            $apikey = $objRegMetaDetails[ 'settings' ][ 'apikey' ];
        }
        return $apikey;
    }
}

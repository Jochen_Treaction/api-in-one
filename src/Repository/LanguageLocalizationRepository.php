<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LanguageLocalizationRepository
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $connection;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * getDefaultLanguage -> fetches the row which has is_default as true.
     * Only one language for a company can be set to default.
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    public function getDefaultLanguageDetails(int $companyId): array
    {
        $isDefault = true;
        $defaultLang = [];
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid companyId of language_localization table provided');
            }
            $sql = 'select * from language_localization where is_default=:is_default and company_id = :company_id';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':is_default', $isDefault, ParameterType::BOOLEAN);
            $stmt->bindValue('company_id', $companyId, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to fetch default language');
            }
            $rows = $stmt->fetchAll();
            if (!empty($rows) && isset($rows[ 0 ][ 'id' ])) {
                $defaultLang = $rows[ 0 ];
            }
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $defaultLang;
    }

    /**
     * Fetches the optinText for the specified CompanyId and IsoCode.
     * @param int $companyId
     * @param string $isoCode
     * @return array|mixed
     * @author Pradeep
     */
    public function getOptinTextForIsoCode(int $companyId, string $isoCode)
    {
        $langDetails = [];
        try {
            if (empty($isoCode)) {
                throw new RuntimeException('Invalid isocode of language_localization table provided');
            }
            $sql = '
                    select ll.*
                    from language_localization ll
                    join languages l on l.id = ll.language_id
                    where l.iso_code = :iso_code and ll.company_id =:company_id
            ';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':iso_code', $isoCode);
            $stmt->bindValue('company_id', $companyId, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to fetch default language');
            }
            $rows = $stmt->fetchAll();
            if (!empty($rows) && isset($rows[ 0 ][ 'id' ])) {
                $langDetails = $rows[ 0 ];
            }
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $langDetails;
    }
}
<?php


namespace App\Repository;

use App\Entity\Users;
use App\Types\UserStatusTypes;
use DateTime;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
/*use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\AuthenticationException;
use GeoIp2\Exception\GeoIp2Exception;
use GeoIp2\Exception\HttpException;
use GeoIp2\Exception\InvalidRequestException;
use GeoIp2\Exception\OutOfQueriesException;
use GeoIp2\WebService\Client;

use http\Client\Curl\User;
*/
use Psr\Log\LoggerInterface;
use RuntimeException;

class UserRepository
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;

    /**
     * UserRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $logger
     */
    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger = $logger;
        //$this->geoIpClient = new Client(346418, 'NrHslBO4wNzcA53C');
    }

    /**
     * @param int $companyId
     * @param string $email
     * @param array $roles
     * @param string $firstName
     * @param string $lastName
     * @param string $salutation
     * @param string $registration
     * @param string $loginDate
     * @param string $phone
     * @param string $mobile
     * @param string $failedLogins
     * @param int $superAdmin
     * @param int $permission
     * @param int $dpa
     * @param string $dpaDate
     * @param int $createdBy
     * @param int $updatedBy
     * @param string $status
     * @return int|null
     * @author Pradeep
     */
    public function createUser(
        int $companyId,
        string $email,
        array $roles,
        string $firstName,
        string $lastName,
        string $salutation,
        string $registration,
        string $loginDate,
        string $phone,
        string $mobile,
        string $failedLogins,
        int $superAdmin,
        int $permission,
        int $dpa,
        string $dpaDate,
        int $createdBy,
        int $updatedBy,
        string $status
    ): ?int {
        if(!isset($loginDate) || empty($loginDate)){
            $loginDate = date('Y-m-d H:i:s');
        }
        $user_id = null;
        $sql = "INSERT INTO users(company_id, email, password, roles, first_name, last_name, salutation, registration, login_date, phone, mobile, failed_logins, super_admin, permission, dpa, dpa_date, created_by, updated_by, status) 
                VALUES(:company_id, :email, :password, :roles,:first_name, :last_name, :salutation, :registration, :login_date, :phone, :mobile, :failed_logins, :super_admin, :permission, :dpa, :dpa_date, :created_by, :updated_by, :status)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
        $stmt->bindParam(':email', $email, ParameterType::STRING);
        $password = '';
        $stmt->bindParam(':password', $password, ParameterType::STRING);
        $js_en_roles = json_encode($roles);
        $stmt->bindParam(':roles', $js_en_roles, ParameterType::STRING);
        $stmt->bindParam(':first_name', $firstName, ParameterType::STRING);
        $stmt->bindParam(':last_name', $lastName, ParameterType::STRING);
        $stmt->bindParam(':salutation', $salutation, ParameterType::STRING);
        $stmt->bindParam(':registration', $registration, ParameterType::STRING);
        $stmt->bindParam(':login_date', $loginDate);
        $stmt->bindParam(':phone', $phone, ParameterType::STRING);
        $stmt->bindParam(':mobile', $mobile, ParameterType::STRING);
        $stmt->bindParam(':failed_logins',$failedLogins,ParameterType::STRING);
        $stmt->bindParam(':super_admin', $superAdmin, ParameterType::STRING);
        $stmt->bindParam(':permission', $permission, ParameterType::STRING);
        $stmt->bindParam(':dpa', $dpa, ParameterType::STRING);
        if(empty($dpaDate)) {
            $dpaDate = null;
        }
        $stmt->bindParam(':dpa_date', $dpaDate);
        $stmt->bindParam(':created_by', $createdBy, ParameterType::STRING);
        $stmt->bindParam(':updated_by', $updatedBy, ParameterType::STRING);
        $stmt->bindParam(':status', $status, ParameterType::STRING);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount === 0) {
                $this->logger->error('Failed to Insert User for the Company.', [__METHOD__, __LINE__]);
                return null;
            }
            $user_id = $this->conn->lastInsertId();
            return $user_id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * @param int $userId
	 * @param string $status const from \App\Types\UserStatusTypes
     * @return bool|null
     * @author Pradeep, jsr
	 * @see \App\Types\UserStatusTypes
     */
    public function setUserStatus(int $userId, string $userStatusType) :bool
    {
		$lcUserStatusType = strtolower($userStatusType);

        if($userId <= 0 || !in_array($lcUserStatusType, UserStatusTypes::getTypes())){
            return false;
        }

        try {
            $sql = 'UPDATE users SET status= :userStatusType WHERE id=:user_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':user_id', $userId, ParameterType::INTEGER);
            $stmt->bindParam(':userStatusType', $lcUserStatusType, ParameterType::STRING);
            return $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
            return false;
        }
    }

    /**
     * @param string $ipAddress
     * @return string|null
     * @author Pradeep
     */
    public function getUserLocationByIpAddress(string $ipAddress): ?string
    {
        return '';
/*        if (empty($ipAddress) || empty($this->geoIpClient)) {
            return null;
        }
        try {
            $geo_client = $this->geoIpClient->city($ipAddress);
            return $geo_client->country->name;
        } catch (AddressNotFoundException | AuthenticationException | InvalidRequestException | HttpException
        | OutOfQueriesException | GeoIp2Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }*/
    }

    /**
     * @param string $email
     * @return array|null
     */
    public function getUserDetailsByEmail(string $email): ?array
    {
        $user = [];
        try {
            if (empty($email)) {
                throw new RuntimeException('Invalid EmailAddress provided');
            }
            $sql = 'SELECT * FROM users WHERE email = :email';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':email', $email, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if (!empty($result[ 0 ]) && $rowCount > 0) {
                $user = $result[ 0 ];
            }
            return $user;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }
}

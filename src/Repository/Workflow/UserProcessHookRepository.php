<?php

namespace App\Repository\Workflow;

use App\Repository\ObjectRegisterRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;

class UserProcessHookRepository
{

    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $db;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        Connection $connection,
        ObjectRegisterRepository $objectRegisterRepository,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->db = $connection;
        $this->objectRegisterRepository = $objectRegisterRepository;
    }

    public function getId(int $projectCampaignId, bool $createIfNotExists = false, int $processHookId = 0)
    {
        $userProcessHookId = 0;
        try {
            $userProcessHook = $this->get($projectCampaignId);
            if (!empty($userProcessHook) && isset($userProcessHook[ 'id' ])) {
                return $userProcessHook[ 'id' ];
            }
            if (!$createIfNotExists) {
                return $userProcessHookId;
            }
            $userProcessHookId = $this->create($projectCampaignId, 'Neuer Lead', $processHookId);
            $this->logger->info('UPHRepository getId '.$userProcessHookId);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $userProcessHookId;
        }
        return $userProcessHookId;
    }

    /**
     * @param int $id
     * @param int $projectCampaignId
     * @return array
     * @author Pradeep
     */
    public function get(int $projectCampaignId = 0): array
    {
        $result = [];
        try {
            $sql = 'select * from user_processhook where project_campaign_id = ?';
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $projectCampaignId, ParameterType::INTEGER);
            $status = $stmt->execute();
            if(!$status) {
                return $result;
            }
            $result = $stmt->fetchAll();
            if(!empty($result) && isset($result[0]['id'])) {
                $result = $result[0];
            }
        } catch (\Doctrine\DBAL\Driver\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $result;
        }
        return $result;
    }

    public function create(int $projectCampaignId, $processHookName, $processHookId)
    {
        $userProcessHookId = null;
        try {
            if ($projectCampaignId === 0 || empty($processHookName) || $processHookId === 0) {
                throw new Exception('Invalid Parameter provided');
            }
            $objectRegisterId = $this->createProcessHookObjectRegisterId();
            if ($objectRegisterId === null || $objectRegisterId === 0) {
                throw new Exception('Failed to create ObjectRegister for User ProcessHook');
            }
            $userProcessHookId = $this->insert($processHookId, $projectCampaignId, $objectRegisterId, $processHookName);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $userProcessHookId;
        }
        return $userProcessHookId;
    }

    private function createProcessHookObjectRegisterId(): ?int
    {
        $uniqueObjectName = 'send_lead_notification';
        $statusDefValue = 'defined';
        return $this->objectRegisterRepository->createObjectRegister($uniqueObjectName, $statusDefValue);
    }

    public function insert(int $processHookId, int $projectCampaignId, int $objectRegisterId, string $processHookName)
    {
        $userProcessHookId = 0;
        try {
            if ($processHookId <= 0 || $projectCampaignId <= 0 || $objectRegisterId <= 0 || empty($processHookName)) {
                throw new Exception('Invalid Parameters provided to insert new User');
            }
            $defaultUserId = 999999;
            $sql = "insert into user_processhook(processhook_id, project_campaign_id, objectregister_id, processhook_name, created_by, updated_by)
                    value(?, ?, ?, ?, ?, ?)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $processHookId, ParameterType::INTEGER);
            $stmt->bindValue(2, $projectCampaignId, ParameterType::INTEGER);
            $stmt->bindValue(3, $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(4, $processHookName);
            $stmt->bindValue(5, $defaultUserId, ParameterType::INTEGER);
            $stmt->bindValue(6, $defaultUserId, ParameterType::INTEGER);
            if ($stmt->execute()) {
                $userProcessHookId = $this->db->lastInsertId();
                $this->logger->info('UPHRepository '.$userProcessHookId);
            }
        } catch (\Doctrine\DBAL\Driver\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $userProcessHookId;
        }
        return $userProcessHookId;
    }
}
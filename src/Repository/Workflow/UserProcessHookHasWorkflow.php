<?php

namespace App\Repository\Workflow;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use Exception;

class UserProcessHookHasWorkflow
{

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->db = $connection;
    }

    public function get()
    {

    }

    /**
     * @param int $userProcessHookId
     * @param int $workflowId
     * @param int|null $predecessorUphId
     * @param int|null $successorUphId
     * @return int|string
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function insert(
        int $userProcessHookId,
        int $workflowId,
        int $predecessorUphId = null,
        int $successorUphId = null
    ):bool  {
        try {
            if ($userProcessHookId <= 0 || $workflowId <= 0) {
                throw new Exception('Invalid UserProcessHookId or WorkFlowId provided');
            }
            $sql = "INSERT INTO `user_processhook_has_workflow`
                    (`user_processhook_id`,
                    `workflow_id`,
                    `predecessor_uph_id`,
                    `successor_uph_id`)
                    VALUES (?,?,?,?)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $userProcessHookId, ParameterType::INTEGER);
            $stmt->bindValue(2, $workflowId, ParameterType::INTEGER);
            $stmt->bindValue(3, $predecessorUphId);
            $stmt->bindValue(4, $successorUphId);

            return $stmt->execute();
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__,__METHOD__, __LINE__]);
            return false;
        }
    }
}
<?php

namespace App\Repository\Workflow;

use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;

class WorkflowRepository
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $db;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository
    ) {
        $this->logger = $logger;
        $this->db = $connection;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;

    }

    /**
     * @param int $pageWebhookCampaignId
     * @param false $createIfNotExists
     * @return int
     * @author Pradeep
     * @internal
     *     - Get the workflow id based on campaign/ webhook Id,
     *     - If `$createIfNotExists` is true, then a new workflow is created for the
     *       '$pageWebhookCampaignId' and return workflow Id,
     */
    public function getId(int $pageWebhookCampaignId, bool $createIfNotExists = false): ?int
    {
        $workflowId = 0;
        try {
            $workflow = $this->get(0, $pageWebhookCampaignId);
            if (!empty($workflow) && isset($workflow[ 'id' ])) {
                return $workflow[ 'id' ];
            }
            if (!$createIfNotExists) {
                return $workflowId;
            }
            $workflowId = $this->create($pageWebhookCampaignId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            return $workflowId;
        }
        return $workflowId;
    }

    /**
     * @param int $id
     * @param int $pageWebhookCampaignId
     * @return array
     * @author Pradeep
     */
    public function get(int $id = 0, int $pageWebhookCampaignId = 0): array
    {
        $result = [];
        try {
            $sql = 'select * from workflow where id = ? or page_webhook_campaign_id = ? order by id desc limit 1';
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $id, ParameterType::INTEGER);
            $stmt->bindValue(2, $pageWebhookCampaignId, ParameterType::INTEGER);
            $status = $stmt->execute();
            $res  =$stmt->fetchAll();
            if ($status && isset($res[0]) && !empty($res[0])) {
                $result = $res[0];
            }
        } catch (\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $result;
        }
        return $result;
    }

    /**
     * @param int $workFlowId
     * @param string $processHookType
     * @param string $processHookName
     * @param int $userProcessHookId
     * @author Pradeep
     */
    public function insertToObjectRegisterMeta(int $workFlowId, string $processHookType , string $processHookName,
        int $userProcessHookId)
    {

        $metaValue = $this->getMetaValue($processHookType, $processHookName, $userProcessHookId);
        $metaKey = "workflow";
        $webhookDetails = $this->get($workFlowId,0);
        if(empty($webhookDetails) || $webhookDetails === null) {
            return null;
        }
        $objectRegisterId = $webhookDetails['objectregister_id'];
        return $this->objectRegisterMetaRepository->insert($objectRegisterId,$metaKey,$metaValue);
    }

    /**
     * @param $processHookType
     * @param $processHookName
     * @param $userProcessHookId
     * @return string|null
     * @author Pradeep
     */
    private function getMetaValue($processHookType , $processHookName, $userProcessHookId):?string
    {
        $jsStructure = '{"data_received":[{"processhookType":"","processhookName":"","userProcesshookId":""}]}';
        $arrStructure = json_decode($jsStructure, true);

        $arrStructure['data_received'][0]['processhookType'] = $processHookType;
        $arrStructure['data_received'][0]['processhookName'] = $processHookName;
        $arrStructure['data_received'][0]['userProcesshookId'] = $userProcessHookId;

        return json_encode($arrStructure);
    }

    /**
     * @param int $pageWebhookCampaignId
     * @return int|null
     * @author Pradeep
     */
    public function create(int $pageWebhookCampaignId):?int
    {
        $workflowId = 0;
        try {
            if ($pageWebhookCampaignId === 0) {
                throw new Exception('Invalid Webhook / CampaignId provided ' . $pageWebhookCampaignId);
            }// create ObjectRegister Id.
            $objectRegisterId = $this->createWorkflowObjectRegister();
            if ($objectRegisterId === null || $objectRegisterId <= 0) {
                throw new Exception('Failed to create objectRegisterId for Workflow');
            }
            $workflowId = $this->insert($objectRegisterId, $pageWebhookCampaignId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $workflowId;
        }
        return $workflowId;
    }

    /**
     * @param int $objectRegisterId
     * @param int $pageWebhookCampaignId
     * @return int|string
     * @throws Exception
     * @author Pradeep
     */
    public function insert(int $objectRegisterId, int $pageWebhookCampaignId):?int
    {
        $workflowId = 0;
        try {
            if ($objectRegisterId <= 0 || $pageWebhookCampaignId <= 0) {
                throw new Exception("Invalid ObjectRegisterId or PageWebhookCampaignId provided");
            }
            $name = $this->getName();
            $description = $this->getDescription($pageWebhookCampaignId);
            $defaultUserId = 999999;
            $sql = 'insert into workflow(objectregister_id, page_webhook_campaign_id, name, description, created_by, updated_by)
                    values(?,?,?,?,?,?)';
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(2, $pageWebhookCampaignId, ParameterType::INTEGER);
            $stmt->bindValue(3, $name);
            $stmt->bindValue(4, $description);
            $stmt->bindValue(5, $defaultUserId);
            $stmt->bindValue(6, $defaultUserId);
            if ($stmt->execute()) {
                $workflowId = $this->db->lastInsertId();
            }
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__,__METHOD__, __LINE__]);
            return $workflowId;
        }
        return $workflowId;
    }

    private function getName()
    {
        return 'data_received';
    }

    private function getDescription(int $pageWebhookCampaignId)
    {
        return 'This is workflow for campaign_id' . $pageWebhookCampaignId;
    }

    private function createWorkflowObjectRegister(): ?int
    {
        $uniqueObjectName = 'workflow';
        $statusDefValue = 'active';
        return $this->objectRegisterRepository->createObjectRegister($uniqueObjectName, $statusDefValue);
    }

    /**
     * Verifies whether eMIO processHook is defined for the given workflow.
     * returns boolean
     *   true -> eMIO processHook is configured.
     *   false -> eMIO processHook is not configured.
     * @param int $pageWebhookCampaignId
     * @param int $workflowId
     * @return bool
     * @author Pradeep
     */
    public function hasEMIOProcessHook(int $pageWebhookCampaignId, int $workflowId): bool
    {
        if ($pageWebhookCampaignId <= 0) {
            return false;
        }

        try {
            $sql = '
            select 
                   uphw.user_processhook_id, 
                   wf.id, 
                   up.processhook_id 
            from workflow wf
                join user_processhook_has_workflow uphw on wf.id = uphw.workflow_id
                join user_processhook up on uphw.user_processhook_id = up.id
            where 
                  wf.page_webhook_campaign_id = ? and 
                  up.processhook_id IN (2,3,4,5) and 
                  wf.id=?;
            ';
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $pageWebhookCampaignId, ParameterType::INTEGER);
            $stmt->bindValue(2, $workflowId, ParameterType::INTEGER);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            return !(!$status || empty($result));
        } catch (\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }
}
<?php


namespace App\Repository;

use App\Services\AIOServices\AIOFraudDetectionService;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactServices;
use App\Services\RedisService;
use App\Types\ContactPermissionTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Driver\Connection;
use Exception;
use Exception\ExceptionInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Driver\ResultStatement;
use RuntimeException;
use App\Types\ContactFieldTypes;


class ContactRepository
{
    protected $conn;
    /**
     * @var AIOFraudDetectionService
     * @author Pradeep
     */
    private $aioFraudService;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;
    /**
     * @var CampaignRepository
     * @author Pradeep
     */
    private $campaignRepository;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    /**
     * @var MIOStandardFieldRepository
     * @author Pradeep
     */
    private MIOStandardFieldRepository $mioStandardFieldRepository;
    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactService;
    /**
     * @var ContactPermissionTypes
     * @author Pradeep
     */
    private ContactPermissionTypes $contactPermissionTypes;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        CampaignRepository $campaignRepository,
        AIOFraudDetectionService $aioFraudService,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        ContactServices $contactService,
        ContactFieldServices $contactFieldServices,
        ContactPermissionTypes $contactPermissionTypes,
        // CustomFieldRepository $customFieldRepository,
        RedisService $redisService
    ) {
        $this->logger = $logger;
        $this->conn = $connection;
        $this->aioFraudService = $aioFraudService;
        $this->campaignRepository = $campaignRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
        $this->contactService = $contactService;
        $this->contactPermissionTypes = $contactPermissionTypes;
        //$this->contactFieldServices = $contactFieldServices;
        // $this->customFieldRepository = $customFieldRepository;
        $this->redisService = $redisService;
    }

    /**
     * insertContact Data to Database.
     * - Creates ObjectRegisterID by checking if the contact with same email address present in DB.
     * - Validates and generates required Technical fields (Permission, SOIIP, SOITimeStamp, DOIIP, DOITimeStamp)
     * before inserting to DB.
     *
     * @param int $campaignId
     * @param int $companyId
     * @param array $contactStandardFields
     * @return int|null
     * @author Pradeep
     */
    public function insertContact(int $campaignId, int $companyId, array $contactStandardFields): ?int
    {
        try {
            // validates the standard fields
            // validates the input value.
            $contact = $this->parseStandardFields($campaignId, $companyId, $contactStandardFields);

            if (is_null($contact[ 'objectregister_id' ]) || empty($contact[ ContactFieldTypes::Email ])) {
                throw new RuntimeException('Failed to create ObjectRegisterId');
            }

            return $this->insert(
                $campaignId,
                $contact[ 'objectregister_id' ],
                $companyId,
                $contact[ ContactFieldTypes::Email ],
                $contact[ ContactFieldTypes::Salutation ],
                $contact[ ContactFieldTypes::FirstName ],
                $contact[ ContactFieldTypes::LastName ],
                $contact[ ContactFieldTypes::BirthDay ],
                $contact[ ContactFieldTypes::Firma ],
                $contact[ ContactFieldTypes::Street ],
                $contact[ ContactFieldTypes::HouseNumber ],
                $contact[ ContactFieldTypes::PostalCode ],
                $contact[ ContactFieldTypes::City ],
                $contact[ ContactFieldTypes::Country ],
                $contact[ ContactFieldTypes::Phone ],
                $contact[ 'campaign' ],
                '',
                $contact[ ContactFieldTypes::URL ],
                $contact[ ContactFieldTypes::URL ],
                $contact[ ContactFieldTypes::TrafficSource ],
                $contact[ ContactFieldTypes::UTMParameters ],
                $contact[ 'split_version' ],
                $contact[ ContactFieldTypes::TargetGroup ],
                $contact[ ContactFieldTypes::AffiliateID ],
                $contact[ ContactFieldTypes::AffiliateSubID ],
                $contact[ ContactFieldTypes::IP ],
                $contact[ ContactFieldTypes::DeviceType ],
                $contact[ ContactFieldTypes::DeviceBrowser ],
                $contact[ ContactFieldTypes::DeviceOs ],
                $contact[ ContactFieldTypes::DeviceOsVersion ],
                $contact[ ContactFieldTypes::DeviceBrowserVersion ],
                //
                $contact[ ContactFieldTypes::DOIIp ],
                $contact[ ContactFieldTypes::DOITimestamp ],
                $contact[ ContactFieldTypes::OptinTimeStamp ],
                $contact[ ContactFieldTypes::OptinIp ],
                $contact[ ContactFieldTypes::Permission ],
                $contact[ ContactFieldTypes::Webpage ],
                $contact[ ContactFieldTypes::ReferrerID ],

                $contact[ ContactFieldTypes::LastOrderNo ],
                $contact[ ContactFieldTypes::LastOrderDate ],
                $contact[ ContactFieldTypes::FirstOrderDate ],
                (float)$contact[ ContactFieldTypes::TotalOrderNetValue ],
                (float)$contact[ ContactFieldTypes::LastYearOrderNetValue ],
                (float)$contact[ ContactFieldTypes::LastOrderNetValue ],
                $contact[ ContactFieldTypes::SmartTags ],
                $contact[ ContactFieldTypes::ProductAttributes ],
                $contact[ ContactFieldTypes::SubShops ],
                $contact[ ContactFieldTypes::ShopUrl ],
                $contact[ ContactFieldTypes::Group ],
                (int)$contact[ ContactFieldTypes::TotalNumberOfOrders ],
                $contact[ ContactFieldTypes::Language ],
                0,
                $contact[ 'created' ]);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * parseStandardFields is helper function for validating contact fields before inserting into DB.
     * - Checks if the contact with same email present in the DB and updates the contact accordingly.
     * - Creates the ObjectRegister for the contact accordingly.
     * - Determines Permission for the Contact.
     * - Determines SOIIP, SOITimestamp, DOIIP, DOITimeStamp for the Contact.
     *
     * @param int $campaignId
     * @param int $companyId
     * @param array $contactStandardFields
     * @param bool $createObjectRegisterId
     * @return array|null
     * @throws Exception
     * @author Pradeep
     */
    private function parseStandardFields(
        int $campaignId,
        int $companyId,
        array $contactStandardFields,
        bool $createObjectRegisterId = true
    ): ?array {
        $contact = [];
        $parsedContact = [];
        if ($campaignId <= 0 || $companyId <= 0 || empty($contactStandardFields)) {
            throw new RuntimeException('Invalid CampaignId or Contact Information');
        }
        // check for the interim memory location for campaignId.
        $campaignDetails = $this->campaignRepository->getCampaignDetailsById($campaignId);
        if ($campaignDetails === null || !isset($campaignDetails[ 'name' ])) {
            throw new RuntimeException('Failed to getMetaValue CampaignDetails by Id');
        }

        // Filter the StandardFields.
        foreach ($contactStandardFields as $contactFields) {

            foreach ($contactFields as $key => $value) {
                if ($key === 'objectregister_id') {
                    $parsedContact[ 'objectregister_id' ] = $value;
                }
                if (in_array($key, ['required', 'datatype', 'regex']) ||
                    !$this->mioStandardFieldRepository->isStandardField($key)) {
                    continue;
                }
                $contact[ $key ] = $value;
                break;
            }
        }

        // Check Contact in DB
        $existingContact = $this->getContactByEmail($contact[ ContactFieldTypes::Email ], $companyId);

        // Get all the standard fields.
        $standardFields = $this->mioStandardFieldRepository->getFieldNames();

        // Compare newContact fieldValue with existingContact fieldValue.
        foreach ($standardFields as $stdField) {
            if (!isset($stdField[ 'name' ])) {
                continue;
            }
            $parsedContact[ $stdField[ 'name' ] ] =
                $this->contactService->checkFieldValueFromExistingContact($contact, $stdField[ 'name' ],
                    $existingContact);
        }
        // By default, Permission for the contact is obtained from the campaign.
        // check for interm memory location. for Optin Permission
        $parsedContact[ ContactFieldTypes::Permission ] = $this->getContactOptinPermission($campaignId);
        $parsedContact = $this->contactService->updateOptinPermission($parsedContact);

        // Created date has to be similar to old contact created date, Updated dated is always currentdate.
        $parsedContact[ 'created' ] = !empty($existingContact) ? $existingContact[ 'created' ] : '';
        $parsedContact[ ContactFieldTypes::SplitVersion ] = $contact[ ContactFieldTypes::SplitVersion ] ?? '';
        $parsedContact[ 'campaign' ] = $campaignDetails[ 'name' ];

        // LeadReference is automatically generated before inserting to DB (i,e in latter steps).
        $parsedContact[ ContactFieldTypes::LeadRefernece ] = '';
        $parsedContact[ 'modification' ] = 0;

        if ($createObjectRegisterId) {
            if ($existingContact === null) {
                $parsedContact[ 'objectregister_id' ] = $this->createObjectRegisterForLead($companyId, $campaignId,
                    $parsedContact[ ContactFieldTypes::Email ], $parsedContact[ ContactFieldTypes::IP ]);
            } else {
                $parsedContact[ 'objectregister_id' ] = $this->objectRegisterRepository->createNewVersionForObjectRegister
                ($existingContact[ 'objectregister_id' ]);
            }
        }
        $this->logger->info('parsedContact ' . json_encode($parsedContact, JSON_THROW_ON_ERROR));
        return $parsedContact;
    }

    /**
     * jochen
     * @param string $email
     * @param int|null $companyId
     * @return array|null
     * @author Pradeep
     *
     */
    public function getContactByEmail(string $email, int $companyId = null): ?array
    {
        $contact = null;
        try {
            if (empty($email)) {
                throw new RuntimeException('Invalid Email address provided.');
            }

            if (null === $companyId) {
                $sql = 'SELECT * FROM leads WHERE email = :eMail ORDER BY id DESC';
                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':eMail', $email, ParameterType::STRING);
            } else {
                // leads.account represents company.id
                $sql = 'SELECT * FROM leads WHERE email = :eMail AND account = :companyId ORDER BY id DESC LIMIT 1';
                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':eMail', $email, ParameterType::STRING);
                $stmt->bindValue(':companyId', $companyId, ParameterType::INTEGER);
            }

            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $contact = $result[ 0 ];
            }

            return $contact;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contact;
        }
    }

    /**
     * getContactOptinPermission fetches the permission level given by the webhooks or campaign in the wizard.
     * - The permission level is attached to the contact created from the webhook or campaign.
     * - the permission level has to be in between 1 and 5.
     * @param int $campaignId
     * @return int|null
     * @author Pradeep
     */
    public function getContactOptinPermission(int $campaignId): ?int
    {
        $supportedMetaKeys = [UniqueObjectTypes::PAGE, UniqueObjectTypes::WEBHOOK];
        $supportedPermission = $this->contactPermissionTypes->getSupportedContactPermission();
        try {
            // Get campaign details
            $campaign = $this->campaignRepository->getCampaignDetailsById($campaignId);
            if (empty($campaign)) {
                throw new RuntimeException('failed to get campaign details with campaign id , ' . $campaignId);
            }
            // Check if the page or webhook.
            $uniqueObjectName = $this->objectRegisterRepository->getUniqueObjectNameFromObjectRegisterId($campaign[ 'objectregister_id' ]);
            if (!in_array($uniqueObjectName, $supportedMetaKeys, true)) {
                throw new RuntimeException('invalid uniqueObjectName, only page and webhook are supported');
            }
            // get Campaign Wizard details.
            $campaignSettings = $this->objectRegisterMetaRepository->getMetaValue($campaign[ 'objectregister_id' ],
                $uniqueObjectName);
            if (empty($campaignSettings)) {
                throw new RuntimeException('failed to get campaign settings with objectregister_id , ' . $campaignId);
            }
            // check for permission inside the wizard settings.
            // permission is stored as a string in the DB.
            if (isset($campaignSettings[ 'settings' ][ 'permission' ]) &&
                in_array((int)$campaignSettings[ 'settings' ][ 'permission' ], $supportedPermission, true)) {
                $permission = (int)$campaignSettings[ 'settings' ][ 'permission' ];
            } else {
                // if no permission is set, make none as default.
                $permission = ContactPermissionTypes::PERMISSION_NONE;
            }
            return $permission;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return ContactPermissionTypes::PERMISSION_NONE;
        }
    }


    /**
     * Object register id for lead is generated only after checking status of the lead,
     *  which is done by fraudDetection
     * @param int $companyId
     * @param int $campaignId
     * @param string $eMail
     * @param string $ipAddress
     * @return int|null
     * @author Pradeep
     *
     */
    public function createObjectRegisterForLead(
        int $companyId,
        int $campaignId,
        string $eMail,
        string $ipAddress = ''
    ): ?int {

        try {
            if (empty($eMail) || $companyId <= 0 || $campaignId <= 0) {
                throw new RuntimeException('Invalid Email or CompanyId or CampaignId provided');
            }
            $uniqueObjectName = UniqueObjectTypeRepository::CUSTOMER;
            $statusdefValue = $this->getStatusDefValueForContact($companyId, $campaignId, $eMail, $ipAddress);
            return $this->objectRegisterRepository->createObjectRegister($uniqueObjectName, $statusdefValue);
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * getStatusDefValueForContact gets the status of the contact.
     * - status could be ['active', 'inactive', 'fraud' ... ]
     * - If there are many emails with same IP address or same Domain -> fraud.
     * @param int $companyId
     * @param int $campaignId
     * @param string $eMail
     * @param string $ipAddress
     * @return string
     * @author Pradeep
     */
    public function getStatusDefValueForContact(
        int $companyId,
        int $campaignId,
        string $eMail,
        string $ipAddress = ''
    ): ?string {
        try {
            $statusDefValue = StatusdefRepository::ACTIVE;
            $currentDate = $this->contactService->getCurrentDate();// Get the domain Name from EMail address.
            if (empty($eMail) || $campaignId <= 0 || $companyId <= 0) {
                throw new RuntimeException('Invalid Email or CampaignId or CompanyId provided');
            }
            // Get the Count of Contacts with same Ip address
            $domainName = $this->contactService->getDomainName($eMail);

            $countOfContactsWithSameIpAddr = empty($ipAddress) ? 0 :
                $this->getCountOfContactsWithSameIP($campaignId, $ipAddress, $currentDate);
            // Get the count of contacts with same DomainName
            $countOfContactsWithSameDomain =
                $this->getCountOfContactsWithSameDomain($campaignId, $domainName, $currentDate);

            if ($countOfContactsWithSameIpAddr === null || $countOfContactsWithSameDomain === null) {
                throw new RuntimeException('Failed to getMetaValue Count of contacts with same DomainName or IpAddress');
            }
            // Fraud checking
            if ($this->aioFraudService->isFraudContact($companyId, $campaignId, $eMail, $countOfContactsWithSameIpAddr,
                $countOfContactsWithSameDomain)) {
                $statusDefValue = $this->aioFraudService->getContactStatus();
            }
            return $statusDefValue;
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * getCountOfContactsWithSameIP gets count of rows with same IP.
     * - Does query to DB with Ipaddr, date, and campaignId.
     * @param int $campaignId
     * @param string $ipAddr
     * @param string $date
     * @return int
     */
    public function getCountOfContactsWithSameIP(int $campaignId, string $ipAddr, string $date): ?int
    {
        $count = 0;
        try {
            // If the Ipaddress is not provided by Campaign.
            if (empty($ipAddr)) {
                return $count;
            }
            if (empty($date) || $campaignId <= 0) {
                throw new RuntimeException('Invalid IpAddress or Campaign or Date parameter provided');
            }
            $sql = 'SELECT COUNT(*) as count
                    FROM leads
                             JOIN objectregister o on leads.objectregister_id = o.id
                    WHERE ip LIKE :ip_address AND DATE(o.created_at) = :date AND campaign_id = :campaign_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':ip_address', $ipAddr, ParameterType::STRING);
            $stmt->bindParam(':date', $date, ParameterType::STRING);
            $stmt->bindParam(':campaign_id', $campaignId, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && isset($result[ 0 ][ 'count' ])) {
                $count = $result[ 0 ][ 'count' ];
            }
            return $count;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $campaignId
     * @param string $domain
     * @param string $date
     * @return int
     */
    public function getCountOfContactsWithSameDomain(int $campaignId, string $domain, string $date): ?int
    {
        $count = 0;
        try {
            if (empty($domain) || empty($date) || $campaignId <= 0) {
                throw new RuntimeException('Invalid DomainName or Campaign or Date parameter provided');
            }
            $sql = "SELECT COUNT(*) as count
                    FROM leads
                             JOIN objectregister o on leads.objectregister_id = o.id
                    WHERE email LIKE CONCAT('%',:domain,'%') AND DATE(o.created_at) = :date AND campaign_id = :campaign_id";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':domain', $domain, ParameterType::STRING);
            $stmt->bindParam(':date', $date, ParameterType::STRING);
            $stmt->bindParam(':campaign_id', $campaignId, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && isset($result[ 0 ][ 'count' ])) {
                $count = $result[ 0 ][ 'count' ];
            }
            return $count;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $campaignId
     * @param int $ObjectRegisterId
     * @param int $account
     * @param string $email
     * @param string $salutation
     * @param string $firstName
     * @param string $lastName
     * @param string $birthDay
     * @param string $firma
     * @param string $street
     * @param string $houseNumber
     * @param string $postalCode
     * @param string $city
     * @param string $country
     * @param string $phone
     * @param string $campaign
     * @param string $lead_reference
     * @param string $url
     * @param string $finalUrl
     * @param string $trafficSource
     * @param string $utmParameters
     * @param string $splitVersion
     * @param string $targetGroup
     * @param string $affiliateId
     * @param string $affiliateSubId
     * @param string $ip
     * @param string $deviceType
     * @param string $deviceBrowser
     * @param string $deviceOs
     * @param string $deviceOsVersion
     * @param string $deviceBrowserVersion
     * @param string $doiIp
     * @param string $doiTimeStamp
     * @param string $optinTimeStamp
     * @param string $optinIp
     * @param string $permission
     * @param string $webPage
     * @param string|null $referrerId
     * @param string|null $lastOrderNo
     * @param string|null $lastOrderDate
     * @param string|null $firstOrderDate
     * @param float $totalOrderNetValue
     * @param float $lastYearOrderNetValue
     * @param float $lastOrderNetValue
     * @param string|null $smartTags
     * @param string|null $productAttributes
     * @param int|null $modification
     * @param string|null $created
     * @return int
     * @author Pradeep
     */
    private function insert(
        int $campaignId,
        int $ObjectRegisterId,
        int $account,
        string $email,
        string $salutation,
        string $firstName,
        string $lastName,
        string $birthDay, // WEB:5610 Work Package Contacts.
        string $firma,
        string $street,
        string $houseNumber, // WEB:5610 Work Package Contacts.
        string $postalCode,
        string $city,
        string $country,
        string $phone,
        string $campaign,
        string $lead_reference,
        string $url,
        string $finalUrl,
        string $trafficSource,
        string $utmParameters,
        string $splitVersion,
        string $targetGroup,
        string $affiliateId,
        string $affiliateSubId,
        string $ip,
        string $deviceType,
        string $deviceBrowser,
        string $deviceOs,
        string $deviceOsVersion,
        string $deviceBrowserVersion,
        string $doiIp,
        string $doiTimeStamp = null,
        string $optinTimeStamp,
        string $optinIp,
        string $permission,
        string $webPage,
        string $referrerId = null, // WEB:5610 Work Package Contacts.

        // eCommerceFields are merged to lead table as per ticket WEB:5610 Work Package Contacts.
        string $lastOrderNo = null,
        string $lastOrderDate = null,
        string $firstOrderDate = null,
        float $totalOrderNetValue = 0,
        float $lastYearOrderNetValue = 0,
        float $lastOrderNetValue = 0,
        string $smartTags = null,
        string $productAttributes = null,

        // new eCommerceFields to lead table as per the ticket WEB-5687,
        string $subShops = null,
        string $shopUrl = null,
        string $customerGroup = null,
        int $totalNumberOfOrders = 0,

        //language
        string $language = null,

        // General Fields.
        int $modification = null,
        string $created = null
    ): ?int {
        $id = null;
        $userId = 99999;

        if (empty($created)) {
            $created = $this->contactService->getCurrentDateTime();
        }

        try {
            $this->conn->beginTransaction();
            if (!isset($lead_reference) || empty($lead_reference)) {
                $lead_reference = $this->generateUniqueLeadReference($campaignId) ?? '';
            }
            if ($campaignId <= 0 || $ObjectRegisterId <= 0 || empty($lead_reference) || $account <= 0 || empty($email)) {
                $this->logger->info('error log ' . json_encode([
                        $campaignId,
                        $ObjectRegisterId,
                        $lead_reference,
                        $account,
                        $email,
                    ], JSON_THROW_ON_ERROR));
                throw new RuntimeException('Invalid arguments provided while creating Contact');
            }


            $sql =
                'INSERT INTO leads (
                   campaign_id, objectregister_id, account, email, salutation, first_name, last_name, 
                   firma, postal_code, city, country, phone, street, campaign, lead_reference, url, final_url, 
                   traffic_source, utm_parameters, split_version, target_group, affiliate_id, affiliate_sub_id, ip, 
                   device_type, device_browser, device_os, device_os_version, device_browser_version, 
                   doi_ip,  optin_ip, permission, webpage, birthday, house_number,referrer_id,last_order_no,
                   last_order_date, first_order_date, total_order_net_value, last_year_order_net_value, 
                   last_order_net_value, smart_tags, product_attributes,doi_timestamp, optin_timestamp, 
                   total_number_of_orders,sub_shops, shop_url, customer_group,language,modification, created, created_by,
                   updated, updated_by) 
                VALUES (
                        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
                        ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
                        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?)';

            $stmt = $this->conn->prepare($sql);
            // relations parameters
            $stmt->bindParam(1, $campaignId, ParameterType::INTEGER);
            $stmt->bindParam(2, $ObjectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(3, $account, ParameterType::INTEGER);
            // lead parameters
            $stmt->bindParam(4, $email, ParameterType::STRING);
            $stmt->bindParam(5, $salutation, ParameterType::STRING);
            $stmt->bindParam(6, $firstName, ParameterType::STRING);
            $stmt->bindParam(7, $lastName, ParameterType::STRING);
            $stmt->bindParam(8, $firma, ParameterType::STRING);
            $stmt->bindParam(9, $postalCode, ParameterType::STRING);
            $stmt->bindParam(10, $city, ParameterType::STRING);
            $stmt->bindParam(11, $country, ParameterType::STRING);
            $stmt->bindParam(12, $phone, ParameterType::STRING);
            $stmt->bindParam(13, $street, ParameterType::STRING);
            $stmt->bindParam(14, $campaign, ParameterType::STRING);
            $stmt->bindParam(15, $lead_reference, ParameterType::STRING);
            $stmt->bindParam(16, $url, ParameterType::STRING);
            $stmt->bindParam(17, $finalUrl, ParameterType::STRING);
            // tracking parameters
            $stmt->bindParam(18, $trafficSource, ParameterType::STRING);
            $stmt->bindParam(19, $utmParameters, ParameterType::STRING);
            $stmt->bindParam(20, $splitVersion, ParameterType::STRING);
            $stmt->bindParam(21, $targetGroup, ParameterType::STRING);
            $stmt->bindParam(22, $affiliateId, ParameterType::STRING);
            $stmt->bindParam(23, $affiliateSubId, ParameterType::STRING);
            $stmt->bindParam(24, $ip, ParameterType::STRING);
            // device parameters.
            $stmt->bindParam(25, $deviceType, ParameterType::STRING);
            $stmt->bindParam(26, $deviceBrowser, ParameterType::STRING);
            $stmt->bindParam(27, $deviceOs, ParameterType::STRING);
            $stmt->bindParam(28, $deviceOsVersion, ParameterType::STRING);
            $stmt->bindParam(29, $deviceBrowserVersion, ParameterType::STRING);
            // New Parameters for DOI and permission
            $stmt->bindParam(30, $doiIp, ParameterType::STRING);
            $stmt->bindParam(31, $optinIp, ParameterType::STRING);
            $stmt->bindParam(32, $permission, ParameterType::INTEGER);
            $stmt->bindParam(33, $webPage, ParameterType::STRING);
            // other parameters

            $stmt->bindParam(34, $birthDay, ParameterType::STRING);
            $stmt->bindParam(35, $houseNumber, ParameterType::STRING);
            $stmt->bindParam(36, $referrerId, ParameterType::STRING);
            // eCommerce Fields
            $stmt->bindParam(37, $lastOrderNo, ParameterType::STRING);
            $stmt->bindParam(38, $lastOrderDate, ParameterType::STRING);
            $stmt->bindParam(39, $firstOrderDate, ParameterType::STRING);
            $stmt->bindParam(40, $totalOrderNetValue, ParameterType::STRING);
            $stmt->bindParam(41, $lastYearOrderNetValue, ParameterType::STRING);
            $stmt->bindParam(42, $lastOrderNetValue, ParameterType::STRING);
            $stmt->bindParam(43, $smartTags, ParameterType::STRING);
            $stmt->bindParam(44, $productAttributes, ParameterType::STRING);
            $stmt->bindParam(45, $doiTimeStamp, ParameterType::STRING);
            $stmt->bindParam(46, $optinTimeStamp, ParameterType::STRING);

            $stmt->bindParam(47, $totalNumberOfOrders, ParameterType::INTEGER);
            $stmt->bindParam(48, $subShops, ParameterType::STRING);
            $stmt->bindParam(49, $shopUrl, ParameterType::STRING);
            $stmt->bindParam(50, $customerGroup, ParameterType::STRING);
            // User details
            $stmt->bindParam(51, $language, ParameterType::STRING);
            $stmt->bindParam(52, $modification, ParameterType::INTEGER);
            $stmt->bindParam(53, $created, ParameterType::STRING);
            $stmt->bindParam(54, $userId, ParameterType::INTEGER);
            $stmt->bindParam(55, $created, ParameterType::STRING);
            $stmt->bindParam(56, $userId, ParameterType::INTEGER);

            $result = $stmt->execute();
            $this->logger->info('LeadInsert Query ' . json_encode([$stmt]));
            if ($result) {
                $id = $this->conn->lastInsertId();
                $this->conn->commit();
            }
            return $id;
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->conn->rollback();
            return $id;
        }
    }

    /**
     * @param int $campaignId
     * @param string $latestLeadReference
     * @return string
     * @author Pradeep
     */
    public
    function generateUniqueLeadReference(
        int $campaignId,
        string $latestLeadReference = ''
    ): ?string {
        $lead_ref = null;
        // getMetaValue the object_unique_id.
        $isLatestLeadRefProvided = !empty($latestLeadReference);
        try {
            if ($campaignId <= 0) {
                throw new RuntimeException('Invalid CampaignId provided');
            }

            $campaign = $this->campaignRepository->get($campaignId);
            if (empty($campaign) ||
                !isset(
                    $campaign[ 'name' ],
                    $campaign[ 'objectregister_id' ],
                    $campaign[ 'object_unique_id' ])
            ) {
                throw new RuntimeException('Failed to getMetaValue Campaign Details');
            }

            /*            $campaign = $this->campaignRepository->getCampaignDetailsById($campaignId);

                        $campaignObjRegDetails = $this->objectRegisterRepository->get($campaign[ 'objectregister_id' ]);
                        if ($campaignObjRegDetails === null || !isset($campaignObjRegDetails[ 'object_unique_id' ])) {
                            throw new RuntimeException('Failed to getMetaValue Campaign Object Register Details');
                        }*/
            if ($isLatestLeadRefProvided) {
                $latest_lead_reference = $latestLeadReference;
            } else {
                $latest_lead_reference = $this->getLatestLeadReference($campaign[ 'object_unique_id' ]);
            }

            if (empty($latest_lead_reference)) {
                $ref_counter = 1;
            } else {
                $ld_buf = explode('-', $latest_lead_reference);
                $ref_counter = (int)$ld_buf[ 2 ] + 1;
            }

            $lead_ref = $campaign[ 'company_id' ] . '-' . $campaign[ 'name' ] . '-' . sprintf("%'.08d\n", $ref_counter);

            return trim($lead_ref);
        } catch (RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $lead_ref;
        }
    }

    /**
     * @param string $campaignObjectUniqueId
     * @return string|null
     * @author Pradeep
     */
    public
    function getLatestLeadReference(
        string $campaignObjectUniqueId
    ): ?string {
        $latestLeadReference = '';
        $leadSelect = "
                SELECT l.lead_reference
                FROM leads l
                         JOIN campaign c ON l.campaign_id = c.id
                         JOIN objectregister o ON c.objectregister_id = o.id
                WHERE o.object_unique_id = :object_unique_id
                ORDER BY l.id DESC LIMIT 1";
        $stmt = $this->conn->prepare($leadSelect);
        $stmt->bindParam(':object_unique_id', $campaignObjectUniqueId, ParameterType::STRING);
        try {
            $stmt->execute();
            $latestLeadReferenceResult = $stmt->fetch(FetchMode::ASSOCIATIVE);
            $rowCount = $stmt->rowCount();
            if ($rowCount === 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
        $latestLeadReference = $latestLeadReferenceResult[ 'lead_reference' ];
        return $latestLeadReference;
    }

    /**
     * @param int $campaignId
     * @param int $companyId
     * @param array $contacts
     * @param int $asyncJobId some unique id for example - webhookQueueId.
     * @return bool
     * @author Pradeep
     */
    public
    function multiInsert(
        int $campaignId,
        int $companyId,
        array $contacts,
        bool $isChunkSyncWithEMIO = false,
        int $asyncJobId = 0
    ): bool {
        $values = [];
        $lastSync = '';
        $userId = 99999;
        $this->conn->beginTransaction();
        $count = 1;
        $contactsInChunk = [];
        $currentDate = (string)date('Y-m-d H:i:s');
        if ($isChunkSyncWithEMIO) {
            $lastSync = $currentDate;
        }
        try {
            $stmt = 'INSERT INTO leads (
                       campaign_id, objectregister_id, account, email, salutation, first_name, last_name, 
                       firma, postal_code, city, country, phone, street, campaign, lead_reference, url, final_url, 
                       traffic_source, utm_parameters, split_version, target_group, affiliate_id, affiliate_sub_id, ip, 
                       device_type, device_browser, device_os, device_os_version, device_browser_version, doi_ip, 
                       doi_timestamp, optin_timestamp, optin_ip, permission, webpage,birthday, house_number,referrer_id,
                       last_order_no, last_order_date, first_order_date, total_order_net_value, last_year_order_net_value, 
                       last_order_net_value, last_sync, smart_tags, product_attributes,total_number_of_orders,sub_shops, shop_url, 
                       customer_group,language,modification, created ,created_by, updated_by
                    )';
            foreach ($contacts as $contact) {

                $contactVersion = 0;
                $uniqueObjectTypeId = 4;
                $statusDefId = 1;
                $existingContact = [];
                $insert = [];

                $contactRedisKey = 'contact-multi-insert-' . $contact[ 'email' ];
                // check if the contact present in redis.
                $contactVersion = $this->redisService->hashesGet($contactRedisKey, 'version');
                if (is_bool($contactVersion) && !$contactVersion) {
                    // get the contact information from DB,
                    $existingContact = $this->get($contact[ 'email' ], $companyId);
                    // get version of contact if contact is found in DB.
                    if (!empty($existingContact) && isset($existingContact[ 'version' ])
                        && (int)$existingContact[ 'version' ] >= 1) {
                        $contactVersion = (int)$existingContact[ 'version' ];
                    }
                    //  or save the contact which is found in db to redis.
                    $this->redisService->hashesObject($contactRedisKey, $existingContact);
                }

                // create obejctregister Id for the contact.
                $newVersion = (int)$contactVersion + 1;
                $contact[ 'objectregister_id' ] = $this->objectRegisterRepository->create(
                    $uniqueObjectTypeId,
                    $statusDefId,
                    $newVersion
                );
                $contact[ 'version' ] = $newVersion;

                // when failed to create objectregister_id for the contact.
                // ignore the contact and move to another contact
                if (!isset($contact[ 'objectregister_id' ])) {
                    $this->logger->error('failed to create objectregister_id for contact ',
                        [__CLASS__, __METHOD__, __LINE__]);
                    continue;
                }

                // generate leadReference Number for the contact.
                // try to fetch the previous leadRefNumber from redis.
                $leadRefCounter = $this->redisService->get('lead-ref-latest-for-' . $campaignId);
                // fetch campaignName from redis.
                $campaignName = $this->redisService->hashesGet('campaign-' . $campaignId, 'name');
                // generate the new lead ref when campaignName and previous lead ref is found in redis.
                if (!empty($leadRefCounter) && !empty($campaignName)) {
                    $nwLeadRefCounter = (int)$leadRefCounter + 1;
                    $contact[ 'lead_reference' ] =
                        $companyId .
                        '-' .
                        $campaignName .
                        '-' .
                        sprintf("%'.08d\n", $nwLeadRefCounter);
                } else {
                    // generate the lead ref number using Db.
                    $contact[ 'lead_reference' ] = $this->generateUniqueLeadReference($campaignId);

                    $ld_buf = explode('-', $contact[ 'lead_reference' ]);
                    $nwLeadRefCounter = (int)$ld_buf[ 2 ] + 1;
                }
                // save the new lead ref number to redis for future usage
                $this->redisService->set('lead-ref-latest-for-' . $campaignId, $nwLeadRefCounter);
                //  the contact in redis,
                //$this->redisService->hashesObject($contactRedisKey, $contact);

                // fetch standard fields from redis.
                $standardFields = $this->redisService->get('mio-standard-fields');
                // when not present fetch from DB.
                if (empty($standardFields)) {
                    $standardFields = $this->mioStandardFieldRepository->getFieldNames();
                    $this->redisService->set('mio-standard-fields', json_encode($standardFields));
                } else {
                    // redis always store the value in string.
                    // so convert the standardfields to array.
                    $standardFields = json_decode($standardFields, true, 512, JSON_THROW_ON_ERROR);
                }

                // defaultContactPermission is the permission configured in the wizard while created a page or webhook.
                $defaultContactPermission = $this->redisService->get('default-contact-permission');
                // when no value is found in redis, try to fetch from DB,
                // store back the value to redis for futrue usage.
                if (empty($defaultContactPermission)) {
                    $defaultCampaignPermission = $this->getContactOptinPermission($campaignId);
                    $this->redisService->set('default-contact-permission', $defaultCampaignPermission);
                }

                // here we compare the two versions of the contact fields and consider the values accordingly.
                foreach ($standardFields as $field) {
                    // Compare the Permission of the Contact
                    // Permission is only upgraded and never downgraded.
                    if ($field[ 'name' ] === ContactFieldTypes::Permission) {
                        // fetch the permission from existing contact.
                        $existingContactPermission = (int)$this->redisService->hashesGet($contactRedisKey,
                            $field[ 'name' ]);
                        // If the permission is not specified in the contact chuck array.
                        if (!isset($contact[ $field[ 'name' ] ])) {
                            $contact[ $field[ 'name' ] ] = 0;
                        }
                        // evaluate the permission with new permission and existing permission of the contact
                        // and assign the correct permission to the current contact version.
                        $insert[ $field[ 'name' ] ] = $this->contactService->assignPermission(
                            (int)$contact[ $field[ 'name' ] ],
                            (int)$existingContactPermission,
                            (int)$defaultContactPermission);

                    } elseif ($field[ 'name' ] === ContactFieldTypes::SubShops) {
                        // fetch the subshops of existing contact.
                        // Subshops are not appended from the shops
                        $existingContactSubShops = $this->redisService->hashesGet($contactRedisKey, $field[ 'name' ]);
                        // Check if the contact is present in the redis or not.
                        if (is_bool($existingContactSubShops) && !($existingContactSubShops)) {
                            $filteredSubShops = '';
                        } else {
                            $filteredSubShops = $this->contactService->filterSubShops($contact[ $field[ 'name' ] ],
                                $existingContactSubShops);
                        }

                        $insert[ $field[ 'name' ] ] = $filteredSubShops;
                    } elseif (isset($contact[ $field[ 'name' ] ]) && !empty($contact[ $field[ 'name' ] ])) {
                        // Simply consider the values from the new version of the contact.
                        // when the new version of the contact has the value for the field.
                        $insert[ $field[ 'name' ] ] = $contact[ $field[ 'name' ] ];
                    } else {
                        // When the value for the contact is not specified, Simply get the value from DB
                        // if the value is fetched from redis, then converting the redis default return value 'false' to string.
                        $valueFromExistingContact = $this->redisService->hashesGet($contactRedisKey, $field[ 'name' ]);
                        if (is_bool($valueFromExistingContact) && !$valueFromExistingContact) {
                            $valueFromExistingContact = '';
                        }
                        $insert[ $field[ 'name' ] ] = $valueFromExistingContact;
                    }
                }

                $insert[ ContactFieldTypes::LastSync ] = $lastSync;
                $insert[ 'modification' ] = 0;
                $this->redisService->hashesObject($contactRedisKey, $insert);

                $values[] = "(" .
                            $campaignId . ',' .
                            $contact[ 'objectregister_id' ] . ',' .
                            $companyId . ',' .
                            '"' . $insert[ ContactFieldTypes::Email ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Salutation ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::FirstName ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastName ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Firma ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::PostalCode ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::City ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Country ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Phone ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Street ] . '"' . ',' .

                            '"' . $campaignName . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LeadRefernece ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::URL ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::URL ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::TrafficSource ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::UTMParameters ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::SplitVersion ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::TargetGroup ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::AffiliateID ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::AffiliateSubID ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::IP ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DeviceType ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DeviceBrowser ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DeviceOs ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DeviceOsVersion ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DeviceBrowserVersion ] . '"' . ',' .

                            '"' . $insert[ ContactFieldTypes::DOIIp ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::DOITimestamp ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::OptinTimeStamp ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::OptinIp ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Permission ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Webpage ] . '"' . ',' .

                            '"' . $insert[ ContactFieldTypes::BirthDay ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::HouseNumber ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::ReferrerID ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastOrderNo ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastOrderDate ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::FirstOrderDate ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::TotalOrderNetValue ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastYearOrderNetValue ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastOrderNetValue ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::LastSync ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::SmartTags ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::ProductAttributes ] . '"' . ',' .

                            '"' . $insert[ ContactFieldTypes::TotalNumberOfOrders ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::SubShops ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::ShopUrl ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Group ] . '"' . ',' .
                            '"' . $insert[ ContactFieldTypes::Language ] . '"' . ',' .
                            $insert[ 'modification' ] . ',' .
                            '"' . $currentDate . '"' . ',' .
                            $userId . ',' .
                            $userId . ")";

                // Collect all the emails into an array. with chunkName as 'JobId-AccountNo-ObjRegId'
                $contactsInChunk[] = $insert;
                $count ++;
            }
            $this->logger->info('multiinsertvalues ' . json_encode($values));

            $this->redisService->delete('lead-ref-latest-for-' . $campaignId);
            $this->redisService->delete('lead-ref-latest-for-' . $campaignId);
            $this->redisService->delete('default-contact-permission');
            $query = $stmt . "VALUES " . implode(',', $values);
            $this->logger->info('multiinsertQuery ' . $query);
            $status = $this->conn->exec($query);
            $this->conn->commit();

            // save back to redis with all the chunk of contact information.
            // Chunk should have an expiration of 1hr .
            if ($asyncJobId > 0) {
                $redisValue = json_encode($contactsInChunk, JSON_THROW_ON_ERROR);
                $redisKey = 'multiInsert-' . $asyncJobId . '-' . $campaignId . '-' . $companyId;
                $this->redisService->set($redisKey, $redisValue);
            }

            return $status > 0;
        } catch (Exception $e) {
            //$this->conn->rollBack();
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * get fetches the contact details with objectregister details of the contact.
     * - returns
     *      found : contact details mixed with object register details
     *      not-found : empty array.
     * @param $email
     * @param int $companyId
     * @return array|mixed
     * @author Pradeep
     */
    private
    function get(
        $email,
        int $companyId
    ): array {
        $result = [];
        if (empty($email) || $companyId <= 0) {
            return $result;
        }

        $sql = 'select *
                from leads l
                         join objectregister o on l.objectregister_id = o.id
                where l.email = ?
                  and l.account = ?
                order by l.id
                desc limit 1';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $email);
        $stmt->bindParam(2, $companyId);
        $status = $stmt->execute();
        $result = $stmt->fetchAll();

        if (isset($result[ 0 ]) && !empty($result[ 0 ])) {
            return $result[ 0 ];
        }
        return [];

    }

    /**
     * @param int $Id
     * @param int $campaignId
     * @param int $ObjectRegisterId
     * @param int $account
     * @param string $email
     * @param string $salutation
     * @param string $firstName
     * @param string $lastName
     * @param string $firma
     * @param string $street
     * @param string $postalCode
     * @param string $city
     * @param string $country
     * @param string $phone
     * @param string $campaign
     * @param string $lead_reference
     * @param string $url
     * @param string $finalUrl
     * @param string $trafficSource
     * @param string $utmParameters
     * @param string $splitVersion
     * @param string $targetGroup
     * @param string $affiliateId
     * @param string $affiliateSubId
     * @param string $ip
     * @param string $deviceType
     * @param string $deviceBrowser
     * @param string $deviceOs
     * @param string $deviceOsVersion
     * @param string $deviceBrowserVersion
     * @param int $modification
     * @return bool
     * @author Pradeep
     */
    public
    function update(
        int $Id,
        int $campaignId,
        int $ObjectRegisterId,
        int $account,
        string $email,
        string $salutation,
        string $firstName,
        string $lastName,
        string $firma,
        string $street,
        string $postalCode,
        string $city,
        string $country,
        string $phone,
        string $campaign,
        string $lead_reference,
        string $url,
        string $finalUrl,
        string $trafficSource,
        string $utmParameters,
        string $splitVersion,
        string $targetGroup,
        string $affiliateId,
        string $affiliateSubId,
        string $ip,
        string $deviceType,
        string $deviceBrowser,
        string $deviceOs,
        string $deviceOsVersion,
        string $deviceBrowserVersion,
        int $modification
    ): bool {
        $status = false;
        try {
            if ($Id <= 0 || $ObjectRegisterId <= 0) {
                throw new Exception('Invalid Contact Id provided to  Contact');
            }
            $update_sql = '
                    UPDATE leads 
                    SET salutation=?, first_name=?, last_name=?, firma=?, postal_code=?, city=?, country=?, phone=? ,street=?, 
                        campaign=?, lead_reference=?, url=?, final_url=?, traffic_source=?, utm_parameters=?, 
                        split_version=?, target_group=?, affiliate_id=?, affiliate_sub_id=?, ip=?, device_type=?, 
                        device_browser=?, device_os=?, device_os_version=?, device_browser_version=?, modification=?, 
                        updated_by=?
                    WHERE id=?';
            $stmt = $this->conn->prepare($update_sql);// lead parameters
            $stmt->bindParam(1, $salutation, ParameterType::STRING);
            $stmt->bindParam(2, $firstName, ParameterType::STRING);
            $stmt->bindParam(3, $lastName, ParameterType::STRING);
            $stmt->bindParam(4, $firma, ParameterType::STRING);
            $stmt->bindParam(5, $postalCode, ParameterType::STRING);
            $stmt->bindParam(6, $city, ParameterType::STRING);
            $stmt->bindParam(7, $country, ParameterType::STRING);
            $stmt->bindParam(8, $phone, ParameterType::STRING);
            $stmt->bindParam(9, $street, ParameterType::STRING);
            $stmt->bindParam(10, $campaign, ParameterType::STRING);
            $stmt->bindParam(11, $lead_reference, ParameterType::STRING);
            $stmt->bindParam(12, $url, ParameterType::STRING);
            $stmt->bindParam(13, $finalUrl, ParameterType::STRING);// tracking parameters
            $stmt->bindParam(14, $trafficSource, ParameterType::STRING);
            $stmt->bindParam(15, $utmParameters, ParameterType::STRING);
            $stmt->bindParam(16, $splitVersion, ParameterType::STRING);
            $stmt->bindParam(17, $targetGroup, ParameterType::STRING);
            $stmt->bindParam(18, $affiliateId, ParameterType::STRING);
            $stmt->bindParam(19, $affiliateSubId, ParameterType::STRING);
            $stmt->bindParam(20, $ip, ParameterType::STRING);// device parameters.
            $stmt->bindParam(21, $deviceType, ParameterType::STRING);
            $stmt->bindParam(22, $deviceBrowser, ParameterType::STRING);
            $stmt->bindParam(23, $deviceOs, ParameterType::STRING);
            $stmt->bindParam(24, $deviceOsVersion, ParameterType::STRING);
            $stmt->bindParam(25, $deviceBrowserVersion, ParameterType::STRING);// User details
            $stmt->bindParam(26, $modification, ParameterType::INTEGER);
            $stmt->bindParam(27, $userId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $status = $result;
            }
            return $status;
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $contactId
     * @return bool
     * @author Pradeep
     */
    public
    function delete(
        int $contactId
    ): bool {
        $status = false;
        try {
            if ($contactId <= 0) {
                throw new RuntimeException('Invalid Contact Id provided');
            }
            $sql = 'DELETE FROM leads WHERE id=:contact_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':contact_id', $contactId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result) {
                $status = true;
            }
            return $status;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $email
     * @param int $campaignId
     * @return array|null
     * @author Pradeep
     */
    public
    function isContactExistsForCampaign(
        string $email,
        int $campaignId
    ): ?array {
        $contacts = null;
        try {
            if (empty($email) || $campaignId <= 0) {
                throw new RuntimeException('Invalid EMail address or campaignId provided.');
            }
            $sql = 'SELECT * FROM leads where email LIKE :email AND campaign_id =:campaign_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':email', $email, ParameterType::STRING);
            $stmt->bindParam(':campaign_id', $campaign_id, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if (!empty($result[ 0 ]) && $rowCount > 0) {
                $contacts = $result[ 0 ];
            }
            return $contacts;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contacts;
        }
    }

    /**
     * @param int $id
     * @return array|null
     * @author Pradeep
     */
    public
    function getContactById(
        int $id
    ): ?array {
        $contact = null;
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid ContactID provided.');
            }
            $sql = 'SELECT * FROM leads WHERE id=:contact_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':contact_id', $id, ParameterType::INTEGER);
            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $contact = $result[ 0 ];
            }
            return $contact;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contact;
        }
    }

    /**
     * @param string $email
     * @param int $campaignId
     * @return array|null lead id and latest version
     * @internal
     * a. view to receive the lastest (in sense of objectregister highest version number) active lead-entry from leads.
     * b. used in AIO for lead handling.
     * insert view v_aio_get_lastest_lead_entry as
     * select l.*, o.version, o.id objreg_objectregister_id from objectregister o
     * join leads l on (o.id = l.objectregister_id)
     * join statusdef s on (o.statusdef_id = s.id and s.value = 'active')
     * order by o.version desc limit 1;
     *
     * @deprecated views are not currently in use with functionality,
     */
    public
    function checkIfLeadExists(
        string $email,
        int $campaignId
    ): ?array {
        $leadIdSelect = "select id,version from v_aio_get_lastest_lead_entry v  where v.email = :email and v.campaign_id = :campaignId;";
        $stmt = $this->conn->prepare($leadIdSelect);
        $stmt->bindParam(':email', $email, ParameterType::STRING);
        $stmt->bindParam(':campaignId', $campaignId, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $leadRecord = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $leadRecord;
    }

    /**
     * @param int $accountId
     * @return array|null
     * @author Pradeep
     */
    public
    function getContactsForAccount(
        int $accountId
    ): ?array {
        $contacts = null;
        try {
            if ($accountId <= 0) {
                throw new RuntimeException('Invalid account Id provided');
            }
            $accountSql = 'SELECT * FROM leads where account =:accountId';
            $stmt = $this->conn->prepare($accountSql);
            $stmt->bindParam(':accountId', $accountId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->execute();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $contacts = $result[ 0 ];
            }
            return $contacts;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contacts;
        }
    }

    /**
     * @param int $campaignId
     * @return array|null
     * @author Pradeep
     */
    public
    function getContactForCampaign(
        int $campaignId
    ): ?array {
        $contacts = null;
        try {
            if ($campaignId <= 0) {
                throw new RuntimeException('Invalid account Id provided');
            }
            $campaignSql = 'SELECT * FROM leads WHERE campaign_id=:campaign_id';
            $stmt = $this->conn->prepare($campaignSql);
            $stmt->bindParam(':campaign_id', $campaignId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->execute();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $contacts = $result[ 0 ];
            }
            return $contacts;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contacts;
        }
    }

    /**
     * @param string $email
     * @return bool
     * @author Pradeep
     */
    public
    function deleteContactByEmail(
        string $email
    ): bool {
        $status = false;
        try {
            if (empty($email)) {
                throw new RuntimeException('Invalid E-Mail address provided');
            }
            $sqlDelete = 'DELETE FROM leads WHERE email LIKE :email';
            $stmt = $this->conn->prepare($sqlDelete);
            $stmt->bindParam(':email', $email, ParameterType::STRING);
            $result = $stmt->execute();
            if ($result) {
                $status = $result;
            }
            return $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $id
     * @return bool
     * @author Pradeep
     */
    public
    function deleteContactById(
        int $id
    ): bool {
        $status = false;
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid Contact ID provided');
            }
            $sqlDelete = 'DELETE FROM leads WHERE id = :id';
            $stmt = $this->conn->prepare($sqlDelete);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result) {
                $status = $result;
            }
            return $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $campaign_id
     * @param string $domain
     * @param string $today
     * @return int
     */
    public
    function getDuplicateDomainCount(
        int $campaign_id,
        string $domain,
        string $today
    ): ?int {
        $count = null;
        if (empty($domain) || $campaign_id <= 0) {
            throw new RuntimeException('Invalid domain or campaignId provided');
        }

        return $count;
    }

    /**
     * @param int $objectRegisterId
     * @return array|mixed
     * @author Pradeep
     */
    public
    function getContactByObjectRegisterId(
        int $objectRegisterId
    ) {
        if ($objectRegisterId <= 0) {
            return [];
        }

        $sql = "select * from leads where objectregister_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $objectRegisterId);
        $status = $stmt->execute();
        $result = $stmt->fetchAll();
        if ($status && !empty($result[ 0 ])) {
            return $result[ 0 ];
        }
        return [];
    }

    /**
     * - generateObjectRegisterIdForContactList creates objectRegisterId's for all contacts.
     * - ignores when contact with same email and order number arrives again.
     * - Used while synchronizing the contacts from shop plugin.
     * @param int $campaignId
     * @param int $companyId
     * @param array $contacts
     * @return array
     * @throws Exception
     * @author Pradeep
     */
    public
    function generateObjectRegisterIdForContactList(
        int $campaignId,
        int $companyId,
        array $contacts
    ): array {
        $temp = [];
        foreach ($contacts as &$contact) {
            // check if the contact is present in redis server
            // if not present check in DB.
            /* if (!isset($contact[ 'standard' ])) {
                    continue;
                }*/
            $email = $contact[ 'email' ];//$this->contactService->getValue($contact, ContactFieldTypes::Email);

            // TODO: save the contact detials in Redis server in hash format(existing contact details.).
            $existingContact = $this->get($email, $companyId);
            // $existingContact = "";
            if (!isset($temp[ $email ])) {
                $contactVersionFromDB = $existingContact === null ? 1 : $existingContact[ 'version' ];
                $contactNewVersion = $contactVersionFromDB + 1;
            } else {
                $contactNewVersion = ($temp[ $email ]) + 1;
            }
            $temp[ $email ] = $contactNewVersion;

            $uniqueObjectTypeId = 4;
            $statusDefId = 1;
            /*            $objectRegisterId[ 'objectregister_id' ] = $this->objectRegisterRepository->create(
                            $uniqueObjectTypeId,
                            $statusDefId,
                            $contactNewVersion
                        );*/
            /*
                        if ($this->isContactAlreadySynchronized($contact, $campaignId) || !isset($contact[ 'standard' ])) {
                            continue;
                        }

                        $email = $this->contactService->getValue($contact, ContactFieldTypes::Email);
                        $existingContact = $this->getContactByEmail($email, $companyId);
                        if (!isset($temp[ $email ])) {
                            $contactVersionFromDB = $this->getContactVersion($email);
                            $contactNewVersion = $contactVersionFromDB + 1;
                        } else {
                            $contactNewVersion = ($temp[ $email ]) + 1;
                        }
                        $temp[ $email ] = $contactNewVersion;
                        if ($existingContact === null) {

                            $objectRegisterId[ 'objectregister_id' ] = $this->createObjectRegisterForLead(
                                $companyId, $campaignId, $email, '');

                        } else {
                            $objectRegisterId[ 'objectregister_id' ] =
                                $this->objectRegisterRepository->createNewVersionForObjectRegister(
                                    $existingContact[ 'objectregister_id' ], $contactNewVersion);
                        }

            $objectRegisterId[ 'objectregister_id' ] = $this->objectRegisterRepository->create($uniqueObjectTypeId, $statusDefId, $version);
    */
            //array_unshift($contact[ 'standard' ], $objectRegisterId);

        }
        return $contacts;
    }

    /**
     * @param int $companyId
     * @param int $lastSyncContactId
     * @param string $lastSyncDate
     * @return array
     * @author Pradeep
     */
    public function getContactsForSync(
        int $companyId,
        int $lastSyncContactId = 0,
        string $lastSyncDate = '',
        int $offset = 0,
        int $limit = 0
    ): array {

        $this->logger->info('getContactsForSync ' . json_encode([
                'companyId' => $companyId,
                '$lastSyncContactId' => $lastSyncContactId,
                '$lastSyncDate' => $lastSyncDate,
                'offset' => $offset,
                'limit' => $limit,
            ], JSON_THROW_ON_ERROR));

        return $this->getContactsSync($companyId, $offset, $limit);
        /*        if ($lastSyncContactId > 0) {
                    $result = $this->getContactAfterId($lastSyncContactId, $companyId);
                } else {
                    if ($lastSyncDate > 0) {
                        $result = $this->getContactsAfterDate($lastSyncDate, $companyId);
                    } else {
                        $result = $this->getContactsSync($companyId, $offset, $limit);
                    }
                }*/

        // return $result;
        /*        $t1 = microtime(true);
                $t =  $this->filterDuplicateRecords($result, $companyId);
                $t2 = microtime(true);
                $this->logger->info('getContactsForSync time taken' . json_encode([$t2-$t1], JSON_THROW_ON_ERROR));*/
        return $result;
    }

    /**
     * Fetches all the contacts for synchronizing with eMIO.
     * Contacts with only status active are synchronized
     * @param int $companyId
     * @param int $startContactId
     * @param int $endContactId
     * @param string $filterEmails
     * @param int $limit
     * @return array
     * @author Pradeep
     */
    public function getContactsSync(
        int $companyId,
        int $startContactId,
        int $endContactId,
        string $filterEmails = "",
        int $limit = 0
    ): array {
        $this->logger->info('getContactsSync');
        $t1 = microtime(true);
        $filterEmailStmt = '';
        if (!empty($filterEmails)) {
            $filterEmailStmt = 'where tmp.email NOT IN (' . $filterEmails . ')';
        }
        $sql = "
                SELECT *
                FROM (
                         SELECT l.*,
                                l.id                         as lead_id,
                                l.account                    as `CompanyId`,
                                s.value                      as lead_status,
                                cf.fieldname                 as `CustomFieldQuestion`,
                                lcf.lead_customfield_content as `CustomFieldAnswer`
                    	 from leads l 
                    			  join lead_customfield_content lcf on lcf.leads_id = l.id
                                  join customfields cf on lcf.customfields_id = cf.id
                                  JOIN objectregister o on l.objectregister_id = o.id
                                  JOIN statusdef s on o.statusdef_id = s.id
                         where l.account = :account_id
                           and l.id > :start_contact_id and l.id < :end_contact_id
                           and s.value = :lead_status
                         union
                         SELECT l.*,
                                l.id                  as lead_id,
                                l.account             as `CompanyId`,
                                s.value               as lead_status,
                                'CustomFieldQuestion' as `CustomFieldQuestion`,
                                'CustomFieldAnswer'   as `CustomFieldAnswer`
                         from leads l
                                  join (select email, MAX(id) as id from leads where account = :account_id group by email) as lj
                                       on lj.id = l.id
                                  JOIN objectregister o on l.objectregister_id = o.id
                                  JOIN statusdef s on o.statusdef_id = s.id
                         where l.id not in (select lcf.leads_id from lead_customfield_content lcf)
                          and l.id > :start_contact_id and l.id < :end_contact_id
                           and s.value = :lead_status
                     ) as tmp $filterEmailStmt order by lead_id desc LIMIT :limit ;
            ";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('account_id', $companyId);
            $stmt->bindValue('lead_status', StatusDefRepository::ACTIVE);
            $stmt->bindValue('start_contact_id', $startContactId, ParameterType::INTEGER);
            $stmt->bindValue('end_contact_id', $endContactId, ParameterType::INTEGER);
            $stmt->bindValue('limit', $limit, ParameterType::INTEGER);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    /**
     * updateLead is generic update sql statement for lead table.
     * @param string $set
     * @param string $where
     * @return bool
     * @author Pradeep
     */
    public function updateLead(string $set, string $where)
    {
        try {
            if (empty($set) || empty($where)) {
                throw new RuntimeException('Invalid  sql for leads');
            }
            $sql = 'update leads set ' . $set . ' where ' . $where;
            $this->logger->info('UpdateLead ' . $sql);
            return $this->conn->prepare($sql)->execute();
        } catch (RuntimeException|Exception $e) {
            $this->logger->error('updateLead error: ' . $e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * updateLastSyncWithTimeStamp updates the lastSync parameter with timestamp.
     * @param array $contactIds
     * @param string $timeStamp
     * @return bool
     * @author Pradeep
     */
    public function updateLastSyncWithTimeStamp(array $contactIds, string $timeStamp = ""): bool
    {
        try {
            if (empty($contactIds)) {
                throw new RuntimeException("No ContactIds provided.");
            }// Fetch TimeStamp when not provided.
            if (empty($timeStamp)) {
                $timeStamp = $this->contactService->getCurrentDateTime();
            }// Convert Contact/lead Ids from array to String( ', ' saperated. )
            $contactIdsAsString = implode(",", $contactIds);
            if (strlen($contactIdsAsString) <= 0) {
                throw new RuntimeException("empty contactIds as string.");
            }// Update all the rows at a time, where id is provided in $contactIdsAsString.
            $sql = 'Update leads set last_sync = ? where id IN (' . $contactIdsAsString . ')';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $timeStamp);
            return $stmt->execute();
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * - isContactAlreadySynchronized checks if the contact with same EMAIL and ORDER NUMBER present in the Database.
     * - searches based on email and order number.
     * - only useful if the source has eCommerceFields configured.
     * - returns
     *      true if the contact is present.
     *      false if the contact is not present.
     * @param array $contact
     * @return bool
     * @author Pradeep
     */
    private
    function isContactAlreadySynchronized(
        array $contact,
        int $campaignId
    ): bool
    {
        $synchronized = false;
        if (empty ($contact) || $campaignId <= 0) {
            return $synchronized;
        }

        $email = '';//$this->contactFieldServices->getValue($contact, ContactFieldTypes::Email);
        $lastOrderNumber = '';//$this->contactFieldServices->getValue($contact, ContactFieldTypes::LastOrderNo);

        $sql = '
            select count(*) as count
            from leads
            where email = ?
              and last_order_no = ?
              and campaign_id = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $email);
        $stmt->bindParam(2, $lastOrderNumber);
        $stmt->bindParam(3, $campaignId, ParameterType::INTEGER);
        $stmt->execute();
        $count = $stmt->rowCount();
        $result = $stmt->fetchAll();
        if (isset($result[ 0 ][ 'count' ])) {
            return $result[ 0 ][ 'count' ] > 0;
        }
        return false;
    }

    /**
     * @param string $email
     * @return int|null
     * @author Pradeep
     */
    private
    function getContactVersion(
        string $email
    ): ?int {
        if (empty($email)) {
            return 0;
        }
        $sql = "
            select o.version from leads l
            join objectregister o on l.objectregister_id = o.id
            where l.email LIKE ? Order by l.id desc limit 1
           ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $email);
        $status = $stmt->execute();
        $result = $stmt->fetchAll();
        if (!$status || empty($result) || !isset($result[ 0 ][ 'version' ])) {
            return 0;
        }
        return (int)$result[ 0 ][ 'version' ];
    }


    /**
     * getSmartTags fetches the smartTags for the Contact based on ID.
     * @param int $contactId
     * @return mixed|string
     * @author Pradeep
     */
    public function getSmartTags(int $contactId)
    {
        $smartTags = '';
        $contact = $this->getContactById($contactId);
        if (empty($contact) || !isset($contact[ ContactFieldTypes::SmartTags ])) {
            return $smartTags;
        }
        return $contact[ ContactFieldTypes::SmartTags ];
    }
}

<?php


namespace App\Repository;


use App\Types\UniqueObjectTypes;
use App\Types\WebhookWizardTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class CustomFieldRepository
{
    public const SmartTags = 'smart_tags';

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var WebhookWizardTypes
     * @author Pradeep
     */
    private $webhookWizardTypes;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;
    /**
     * @var LeadCustomFieldContentRepository
     * @author Pradeep
     */
    private $leadCustomFieldContentRepository;
    /**
     * @var DataTypesRepository
     * @author Pradeep
     */
    private $dataTypesRepository;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private $contactRepository;
    private $uniqueObjectTypeRepository;
    /**
     * @var StatusdefRepository
     * @author Pradeep
     */
    private $statusdefRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        WebhookWizardTypes $wizardTypes,
        ContactRepository $contactRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        DataTypesRepository $dataTypesRepository,
        LeadCustomFieldContentRepository $leadCustomFieldContentRepository,
        UniqueObjectTypeRepository $uniqueObjectTypeRepository,
        StatusdefRepository $statusdefRepository
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
        $this->webhookWizardTypes = $wizardTypes;
        $this->leadCustomFieldContentRepository = $leadCustomFieldContentRepository;
        $this->dataTypesRepository = $dataTypesRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->contactRepository = $contactRepository;
        $this->uniqueObjectTypeRepository = $uniqueObjectTypeRepository;
        $this->statusdefRepository = $statusdefRepository;
    }


    /**
     * @param array $contactFields
     * @param int $contactId
     * @param int $campaignId
     * @param int $projectId
     * @return bool|null
     * @throws ConnectionException
     * @author Pradeep
     * @deprecated Standard CustomFields are categoried as ecommerce fields and merged to leads table.
     */
    public function insertLeadDataToStandardCustomFields(
        array $contactFields,
        int $contactId,
        int $campaignId,
        int $projectId,
        int $existingContactId=0
    ): ?bool {
        $status = false;
        $uniqueObjNameCustomField = UniqueObjectTypeRepository::STANDARDCUSTOMFIELDS;
        $statusDefActive = StatusdefRepository::ACTIVE;
        $this->logger->info('insertLeadDataToStandardCustomFields ' . json_encode([
                'contactFields' => $contactFields,
                'contactId' => $contactId,
                'campaignId' => $campaignId,
                'projectId' => $projectId
            ], JSON_THROW_ON_ERROR)
        );
        try {
            if (empty($contactFields) || $contactId <= 0 ) {
                throw new RuntimeException('Invalid Contact Information or Contact Id or DataType provided');
            }
            $webhookCustomFields = $this->filterWebhookCustomFields($contactFields);
            $this->logger->info('webhookCustomFields ' . json_encode([$webhookCustomFields], JSON_THROW_ON_ERROR));
            // there were no Standard CustomFields
            if (empty($webhookCustomFields)) {
                $status = true;
            } else {
                // there were Standard Customfields
                $this->conn->beginTransaction();
                foreach ($webhookCustomFields as $key => $value) {
                    $this->logger->info('Inside forloop ' . json_encode(['key' => $key,'value' => $value], JSON_THROW_ON_ERROR));
                    if (empty($key)) {
                        continue;
                    }
                    $customFieldForCampaign = $this->getCustomFieldDetailsForCampaign($key, $projectId);
                    $this->logger->info('CustomFieldForCampaign '. json_encode([$customFieldForCampaign]));
                    $dataType = $this->dataTypesRepository->getDataTypeDetailsByName($value[ 'datatype' ]);
                    // Check and get the customField id if already configured.
                    if (empty($customFieldForCampaign)) {
                        $this->logger->info('Creating New CustomFieldForCampaign ');
                        //ObjectRegisterId for CustomField.
                        $objectRegisterId = $this->objectRegisterRepository->createObjectRegister($uniqueObjNameCustomField,
                            $statusDefActive);
                        $this->logger->info('objectRegisterId '.json_encode($objectRegisterId));
                        // Insert CustomField
                        $customFieldId = $this->insert($campaignId, $objectRegisterId, $projectId, $dataType[ 'id' ],
                            $key, 0, 0, '');
                        $this->logger->info('New customFieldId '.json_encode($customFieldId));
                    } else {
                        $this->logger->info('Existing CustomFieldForCampaign Info '.json_encode($customFieldForCampaign));
                        // SMARTTAGS ARE ALWAYS APPENDED TO EXISTING SMARTTAGS VALUE
                        // OTHER CUSTOM STANDARD FIELDS ARE UPDATED TO NEW CONTENT.
                        $customFieldId = $customFieldForCampaign[ 'id' ];
                        if($key === self::SmartTags && $existingContactId > 0) {
                            $leadSmartTagContent = $this->leadCustomFieldContentRepository->getLatestEntry($customFieldId,$existingContactId);
                            // APPEND EXISTING SMARTTAGS TO END.
                            if(!empty($leadSmartTagContent) && !empty($leadSmartTagContent['lead_customfield_content'])
                               && isset($value[$key])) {
                                // EXAMPLE STRUCTURE OF VALUE FOR SMARTTAG
                                //  'smart_tags' => string 'shoes,computer,Handy,Laptop' (length=41)
                                //  'required' => string 'true' (length=4)
                                //  'datatype' => string 'Text' (length=4)
                                //  'regex' => string '' (length=0)
                                $filteredSmartTags  = $this->filterDuplicateTags($value[$key], $leadSmartTagContent['lead_customfield_content']);
                                $value[$key] = $filteredSmartTags;
                            }
                        }
                    }
                    $this->logger->info('Before CustomFieldContentRepository ' .$value[ $key ]);
                    // Insert Lead CustomField.
                    $leadCustomFieldId = $this->leadCustomFieldContentRepository->insert($contactId, $customFieldId,
                        $value[ $key ], '');

                    if ($leadCustomFieldId === null || $leadCustomFieldId <= 0) {
                        throw new RuntimeException("Failed to create CustomField");
                    }
                    $status = true;
                }
                $this->conn->commit();
            }
            return $status;
        } catch (ConnectionException | RuntimeException | DBALException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->conn->rollBack();
            return $status;
        }
    }

    /**
     * @return string[]
     * @author Pradeep
     * @depreacted Since StandardCustomfields are categorized as ecommercefields and merged to leads table.
     */
    public function getWebhookStandardFields(): array
    {
        return [
            'last_order_date',
            'total_order_net_value',
            'smart_tags',
            'last_order_no',
            'last_order_net_value',
            'total_order_net_value',
            'last_year_order_net_value',
            'first_order_date'
        ];
    }

    /**
     * @param array $contactFields
     * @return array
     * @author Pradeep
     * @depreacted Since StandardCustomfields are categorized as ecommercefields and merged to leads table.
     */
    private function filterWebhookCustomFields(array $contactFields): array
    {
        $contact = [];
        if (empty($contactFields)) {
            return $contact;
        }
        $webhookStandardFields = $this->getWebhookStandardFields();
        foreach ($contactFields as $key => $value) {
            if (!in_array($key, $webhookStandardFields, true)) {
                continue;
            }
            $contact[ $key ] = $value;
        }
        return $contact;
    }

    /**
     * @param string $customFieldName
     * @param int $campaignId
     * @return int|null
     * @author Pradeep
     */
    public function getCustomFieldDetailsForCampaign(string $customFieldName, int $campaignId): ?array
    {
        $customFieldDetails = [];
        try {
            if (empty($customFieldName) || $campaignId <= 0) {
                throw new RuntimeException('Invalid Custom Field name or Campaign id provided.');
            }
            $sql = 'SELECT * FROM customfields WHERE fieldname LIKE :fieldname AND campaign_id = :campaign_id ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':fieldname', $customFieldName, PARAMETERType::STRING);
            $stmt->bindParam(':campaign_id', $campaignId, PARAMETERType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result)) {
                $customFieldDetails = $result[ 0 ];
            }
            return $customFieldDetails;
        } catch (DBALException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function insert(
        int $refCustomFieldsId,
        int $objectRegisterId,
        int $campaignId,
        int $dataTypesId,
        string $fieldName,
        int $hideInUISelection = 0,
        int $mandatory = 0,
        string $regexpValidation = ''
    ): ?int {
        $id = null;
        $userId = 999999;
        try {
            $sql = 'INSERT INTO customfields(ref_customfields_id, objectregister_id, campaign_id, datatypes_id, fieldname, hide_in_ui_selection, mandatory, regexp_validation,  created_by, updated_by) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $refCustomFieldsId, ParameterType::INTEGER);
            $stmt->bindParam(2, $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(3, $campaignId, ParameterType::INTEGER);
            $stmt->bindParam(4, $dataTypesId, ParameterType::INTEGER);
            $stmt->bindParam(5, $fieldName, ParameterType::STRING);
            $stmt->bindParam(6, $hideInUISelection, ParameterType::INTEGER);
            $stmt->bindParam(7, $mandatory, ParameterType::INTEGER);
            $stmt->bindParam(8, $regexpValidation, ParameterType::INTEGER);
            $stmt->bindParam(9, $userId, ParameterType::INTEGER);
            $stmt->bindParam(10, $userId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (DBALException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $id;
        }
    }

    /**
     * @param array $contactCustomFields
     * @param int $leadId
     * @param int $campaignId
     * @param int $projectId
     * @return bool
     *
     * @author Pradeep
     */
    public function insertLeadDataToCustomField(
        array $contactCustomFields,
        int $leadId,
        int $campaignId,
        int $projectId
    ): bool {
        try {
            if ($leadId <= 0 || empty($contactCustomFields) || $campaignId <= 0) {
                throw new RuntimeException('Invalid LeadId Or contactCustomFields provided');
            }
            foreach ($contactCustomFields as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                $customFieldDetails = $this->getCustomFieldDetailsForCampaign($key, $projectId);
                // CustomFields has to inserted from MIO not here.
                if (empty($customFieldDetails)) {
                    continue;
                }
                $customFieldId = $customFieldDetails[ 'id' ];
                $leadCustomFieldContent = $value[ $key ];
                if (is_null($this->leadCustomFieldContentRepository->insert($leadId, $customFieldId,
                    $leadCustomFieldContent))) {
                    $this->logger->error('Failed to insert CustomField  ' . $customFieldId . 'to leadCustomFieldContent');
                    continue;
                }
            }
            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $newContactId
     * @param int $oldContactId
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function copyExistingCustomFieldsToNewContact(int $newContactId, int $oldContactId):bool
    {
        // Incase if there is no Old contact with same email address.
        if(empty($oldContactId)) {
            return true;
        }
        $existingContactCFields = $this->leadCustomFieldContentRepository->getFieldsByLeadId($oldContactId);
        $newContactCFields = $this->leadCustomFieldContentRepository->getFieldsByLeadId($newContactId);
        $this->logger->info('existingFIelds '.json_encode($existingContactCFields));
        $this->logger->info('new Customfield '.json_encode($newContactCFields));
        foreach($existingContactCFields as $fields) {
            if(empty($fields['customfields_id'])) {
                continue;
            }
            $this->logger->info('Check for CustomField id '.$fields['customfields_id']);
            if(!$this->isFieldPresent($fields['customfields_id'], $newContactCFields)) {
                if(is_null($this->leadCustomFieldContentRepository->insert($newContactId,$fields['customfields_id'],
                    $fields['lead_customfield_content']))){
                    $this->logger->error('Failed to insert existing CustomField to new Contact');
                    continue;
                }
            }
        }
        return true;
    }

    /**
     * @param int $customFieldId
     * @param array $newContactCFields
     * @return bool
     * @author Pradeep
     * @internal Helper Function to check the customFieldId is present in array of Customfields.
     */
    private function isFieldPresent(int $customFieldId, array $newContactCFields):bool
    {
        foreach($newContactCFields as $cFields) {
            if((int)$cFields['customfields_id'] === $customFieldId) {
                $this->logger->info('Found Missing '.$customFieldId );
                return true;
            }
        }
        return false;
    }

    public function update(): bool
    {

    }

    public function delete(int $customFieldId): ?bool
    {

    }

    public function get(int $customFieldId): ?array
    {

    }

    public function getCustomFieldReferenceForCampaign(int $campaignId): ?array
    {

    }

    /**
     * @param int $accountNo
     * @return array|null
     * @throws DBALException
     * @author Pradeep
     */
    public function getCustomFieldsForAccount(int $accountNo): ?array
    {
        $customFields = [];
        try {
            if ($accountNo <= 0) {
                throw new RuntimeException('Invalid Account number provided.');
            }
            $sql = 'SELECT cs.id as id, cs.fieldname AS customfield, d.name AS datatype
                    FROM customfields cs
                             JOIN campaign c ON c.id = cs.campaign_id
                             JOIN datatypes d on cs.datatypes_id = d.id
                    WHERE c.account_no LIKE :account_no';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':account_no', $accountNo, PARAMETERType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0) {
                $customFields = $result;
            }
            return $customFields;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $projectId
     * @param int $webhookId
     * @return bool|null
     * @author Pradeep
     * @depreacted Since StandardCustomfields are categorized as ecommercefields and merged to leads table.
     */
    public function createStandardCustomFields(int $projectId, int $webhookId): bool
    {
        try {
            $customFieldUniqueObjId = $this->uniqueObjectTypeRepository->getUniqueObjectTypeId(UniqueObjectTypes::STANDARDCUSTOMFIELDS);
            $standardCustomFields = $this->getWebhookStandardFields();
            $statusdefId = $this->statusdefRepository->getStatusdefId(StatusdefRepository::ACTIVE);
            if ($statusdefId <= 0 || is_null($statusdefId)) {
                // consider the active as default.
                $statusdefId = 1;
            }
            foreach ($standardCustomFields as $field) {
                if (empty($field)) {
                    continue;
                }
                $cusFldObjRegId = $this->objectRegisterRepository->create($customFieldUniqueObjId, $statusdefId);
                if ($cusFldObjRegId <= 0 || is_null($cusFldObjRegId)
                    || $this->insert($webhookId, $cusFldObjRegId, $projectId, 8, $field) === null) {
                    $this->logger->error('Failed to create ObjectRegisterId for custom field ' . $field);
                    continue;
                }
            }
            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }


    /**
     * get all customfields of a project
     * @param int $company_id
     * @param string|null $uniqueObjectName
     * @return array|null if no rows where found [], else [customfields.id, customfields.object_register_id objectregister_id, customfields.campaign_id, customfields.ref_customfields_id, customfields.fieldname, customfields.datatypes_id, datatypes.name datatypes_name, customfields.mandatory, customfields.hide_in_ui_selection, customfields.regexp_validation]
     */
    public function getProjectCustomfields(int $company_id, string $uniqueObjectName = null): ?array
    {
        if (!empty($uniqueObjectName)) {
            $sql = "SELECT cf.id, cf.objectregister_id, cf.fieldname, cf.datatypes_id, dt.name datatypes_name, cf.mandatory, cf.mandatory required, cf.hide_in_ui_selection ,dt.name datatype , dt.base_regex regex ,dt.phptype php_type
                    FROM mio.customfields cf
                    JOIN mio.datatypes dt ON cf.datatypes_id = dt.id
                    JOIN mio.campaign cn ON cf.campaign_id = cn.id
                    JOIN mio.objectregister o ON cf.objectregister_id = o.id
                    JOIN mio.unique_object_type uot ON uot.id = o.unique_object_type_id
                    WHERE cn.company_id = :company_id and uot.unique_object_name = :unique_object_name 
                    ORDER BY cf.fieldname";

        } else {
            $sql = "SELECT cf.id, cf.objectregister_id, cf.fieldname, cf.datatypes_id, dt.name datatypes_name, cf.mandatory, cf.hide_in_ui_selection, cf.regexp_validation 
                    FROM mio.customfields cf
                    JOIN mio.datatypes dt ON cf.datatypes_id = dt.id
                    JOIN mio.campaign cn ON cf.campaign_id = cn.id
                    JOIN mio.objectregister o ON cf.objectregister_id = o.id
                    WHERE cn.company_id = :company_id and cf.fieldname != ''
                    ORDER BY cf.fieldname";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
        if (!empty($uniqueObjectName)) {
            $stmt->bindParam(':unique_object_name', $uniqueObjectName);
        }

        try {
            $stmt->execute();
            $customfields = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in customfields for (project) company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return [];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $customfields;
    }

    public function getCustomFieldsNames(int $companyId)
    {
        $fields = $this->getProjectCustomfields($companyId);
        $keys = array_column($fields, 'fieldname');
        $trimmedArray = array_map('trim', $keys);
        return array_unique($trimmedArray);
    }

}
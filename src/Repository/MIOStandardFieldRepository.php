<?php

namespace App\Repository;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Exception;
use PDO;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * Class MIOStandardFieldRepository
 * @package App\Repository
 * @author Pradeep
 * @internal Repository for 'miostandardfield' table
 */
class MIOStandardFieldRepository
{

    public const STANDARD_FIELDS = "Standard";
    public const TECHNICAL_FIELDS = "Technical";
    public const ECOMMERCE_FIELDS = "eCommerce";

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $conn;


    public function __construct(LoggerInterface $logger, Connection $conn)
    {
        $this->logger = $logger;
        $this->conn = $conn;
    }

    /**
     * Fetches all the supported standard fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'Standard'
     */
    public function getStandardFields(): ?array
    {
        return $this->getAllFieldsByType(self::STANDARD_FIELDS);
    }

    /**
     * Fetches all the MIO fields related to the fieldtype.
     * @param string $fieldType
     * @return array
     * @author Pradeep
     * @internal Field type should be one of ['Standard','Technical', 'eCommerce']
     */
    public function getAllFieldsByType(string $fieldType): ?array
    {
        try {
            if (!$this->isValidFieldType($fieldType)) {
                throw new RuntimeException("Invalid field type: " . $fieldType);
            }

            $sql = '
                select m.*, d.name as dtype_label, d.phptype as dtype_name
                from miostandardfield m
                join datatypes d on m.datatypes_id = d.id
                where fieldtype=? order by m.fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldType);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[ 0 ])) {
                throw new RuntimeException("Failed to get fields of type " . $fieldType);
            }
            return $result;
        } catch (Exception|RuntimeException|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * Check whether fieldtype is valid or not
     * @param $fieldType
     * @return bool
     * @author Pradeep
     * @internal Field type should be one of ['Standard','Technical', 'eCommerce']
     */
    public function isValidFieldType($fieldType): bool
    {
        if (in_array($fieldType, $this->getAllFieldTypes(), true)) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     * @author Pradeep
     * @internal returns all the supported field types for MIO
     * i,e ['Standard','Technical', 'eCommerce'] as present in DB.
     */
    public function getAllFieldTypes(): array
    {
        return [
            self::STANDARD_FIELDS,
            self::TECHNICAL_FIELDS,
            self::ECOMMERCE_FIELDS,
        ];
    }

    /**
     * Fetches all the fields present in miostandardfields table.
     * @return array|null
     * @author Pradeep
     */
    public function getAll(): ?array
    {
        try {

            $sql = '
                select m.*, d.name as dtype_label, d.phptype as dtype_name
                from miostandardfield m
                join datatypes d on m.datatypes_id = d.id
                order by m.fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[ 0 ])) {
                throw new RuntimeException("Failed to get fields  ");
            }
            return $result;
        } catch (Exception|RuntimeException|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * Fetches all the supported technical fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'Technical'
     */
    public function getTechnicalFields(): ?array
    {
        return $this->getAllFieldsByType(self::TECHNICAL_FIELDS);
    }

    /**
     * Fetches all the supported eCommerce fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'eCommerce'
     */
    public function getECommerceFields(): ?array
    {
        return $this->getAllFieldsByType(self::ECOMMERCE_FIELDS);
    }

    /**
     * For a given fieldName,fetches the PlaceHolder column from miostandardfield table
     * @param string $fieldName
     * @return string|null
     * @author Pradeep
     */
    public function getPlaceHolderName(string $fieldName): ?string
    {
        try {
            if (empty($fieldName)) {
                throw new RuntimeException("empty fieldname provided to fetch placeholder ");
            }
            $sql = 'select html_placeholder from miostandardfield where fieldname = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldName);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[ 0 ][ 'html_placeholder' ])) {
                throw new RuntimeException("no placeholder found for the given fieldname = " . $fieldName);
            }

            return $result[ 0 ][ 'html_placeholder' ];
        } catch (RuntimeException|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

    }

    /**
     * Fetches the FieldType for the given fieldname from table 'miostandardfield'
     * Field type should be one of ['Standard','Technical', 'eCommerce']
     * @param string $fieldName
     * @return string|null
     * @author Pradeep
     */
    public function getFieldType(string $fieldName): ?string
    {
        try {
            if (empty($fieldName)) {
                throw new RuntimeException("empty fieldName provided to fetch fieldtype ");
            }
            $sql = 'select fieldtype from miostandardfield where fieldname = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldName);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[ 0 ][ 'fieldtype' ])) {
                throw new RuntimeException("no category found for the given fieldname = " . $fieldName);
            }
            return $result[ 0 ][ 'fieldtype' ];
        } catch (RuntimeException|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function getFieldNameAsArray(): array
    {
        $fieldNames = [];
        $fields = $this->getFieldNames();
        foreach ($fields as $field) {
            $fieldNames[] = $field[ 'name' ];
        }
        return $fieldNames;
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getFieldNames(): ?array
    {
        try {
            $sql = '
                select fieldname as name
                from miostandardfield m
                order by fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();

            if (empty($result) || !isset($result[ 0 ])) {
                throw new RuntimeException("Failed to get fields  ");
            }
            $this->logger->info('getFieldNames ' . json_encode($result));
            return $result;
        } catch (Exception|RuntimeException|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * @param string $fieldName
     * @return bool
     * @author Pradeep
     */
    public function isStandardField(string $fieldName): bool
    {
        try {
            $sql = 'select id, fieldname from miostandardfield where fieldname=?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldName);
            $stmt->execute();
            $stmt->rowCount();
            return $stmt->rowCount() > 0;
        } catch (\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }


    /**
     * @param string $fieldName miostandardfield.fieldname
     * @return array|null miostandardfield.fieldtype | null <pre>[fieldtype, datatype, regex]</pre>
     */
    public function getFieldTypeByFieldName(string $fieldName): ?array
    {
        $return = null;
        try {
            $sql = 'SELECT DISTINCT 
                    lower(sf.fieldtype) as fieldtype, 
                    d.name as datatype, 
                    d.base_regex as regex
                FROM miostandardfield sf
                JOIN datatypes d ON sf.datatypes_id = d.id 
                WHERE sf.fieldname=:fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':fieldname', $fieldName);
            $stmt->execute();
            $record = $stmt->fetchAll(PDO::FETCH_ASSOC);
           // $this->logger->info('$record', [$record, __METHOD__, __LINE__]);

            if (!empty($record) && $stmt->rowCount() > 0) {
                $return = $record[ 0 ];
            }

        } catch (\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        //$this->logger->info('$return', [$return, __METHOD__, __LINE__]);
        return $return;
    }

    /**
     * @param string $dataType
     * @return array|mixed[]
     * @author Pradeep
     */
    public function getFieldsOfDataType(string $dataType): ?array
    {
        if (empty($dataType)) {
            return [];
        }

        try {
            $sql = '
                SELECT sf.*
                FROM miostandardfield sf
                         JOIN datatypes d ON sf.datatypes_id = d.id
                WHERE d.name=:data_type';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':data_type', $dataType);
            $stmt->execute();
            $record = $stmt->fetchAll();
            if (empty($record)) {
                return [];
            }
            return $record;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }
}


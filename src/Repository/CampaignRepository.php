<?php


namespace App\Repository;


// use App\Services\MSMIOServices\MSMIOServices;
use App\Services\PrivateServices\ProjectService;
use App\Types\UniqueObjectTypes;
use App\Types\WebhookWizardTypes;
// use BadFunctionCallException;
use DateTime;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Driver\Connection;
use Exception;
use JsonException;
use LogicException;
use Psr\Log\LoggerInterface;
// use Doctrine\DBAL\Driver\ResultStatement;
use RuntimeException;
use UnexpectedValueException;
// use App\Repository\ObjectRegisterMetaRepository;


class CampaignRepository
{
    private const ObjectType = "project";
    protected $conn;
    private $logger;
    /**
     * @var ProjectService
     * @author Pradeep
     */
    // private $projectService;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;
    /**
     * @var UniqueObjectTypeRepository
     * @author Pradeep
     */
    private $uniqueObjectTypeRepository;

    /**
     * @var CompanyRepository
     * @author Pradeep
     */
    private $companyRepository;
    /**
     * @var WebhookWizardTypes
     * @author Pradeep
     */
    private $webhookWizardTypes;
    /**
     * @var CustomFieldRepository
     * @author Pradeep
     */
    private $customFieldRepository;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private $objectRegisterMetaRepository;
    /**
     * @var StatusdefRepository
     * @author Pradeep
     */
    private StatusdefRepository $statusdefRepository;
    /**
     * @var MIOStandardFieldRepository
     * @author Pradeep
     */
    private MIOStandardFieldRepository $mioStandardFieldRepository;


    public function __construct(
        Connection $connection,
        LoggerInterface $LoggerInterface,
        ObjectRegisterRepository $objectRegisterRepository,
        StatusdefRepository $statusdefRepository,
        CompanyRepository $companyRepository,
        UniqueObjectTypeRepository $uniqueObjectTypeRepository,
        WebhookWizardTypes $webhookWizardTypes,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository
    ) {
        $this->conn = $connection;
        $this->companyRepository = $companyRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->statusdefRepository = $statusdefRepository;
        $this->uniqueObjectTypeRepository = $uniqueObjectTypeRepository;
        $this->logger = $LoggerInterface;
        $this->webhookWizardTypes = $webhookWizardTypes;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
    }

    public function get(int $campaignId): array
    {
        $campaign = [];
        $sql =
            '
                select c.id as campaign_id, o.id as objectregister_id, c.*, o.*
                from campaign c
                         join objectregister o on c.objectregister_id = o.id
                where c.id = ? order by c.id desc ';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $campaignId, ParameterType::INTEGER);
        $stmt->execute();
        $rowCount = $stmt->rowCount();
        $result = $stmt->fetchAll();
        if ($rowCount > 0 && isset($result[ 0 ]) && !empty($result[ 0 ])) {
            $campaign = $result[ 0 ];
        }
        return $campaign;
    }

    /**
     * @param int $accountNumber
     * @return array|null
     */
    public function getAccountidAndAccountName(int $accountNumber): ?array
    {
        $accountSelect = "select c.id,name  from company c where c.account_no = :accountNumber;";
        $stmt = $this->conn->prepare($accountSelect);
        $stmt->bindParam(':accountNumber', $accountNumber, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $accountDetails = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }

        return $accountDetails;
    }



    /**
     * @param string $cnf_tgt
     * @return int
     * @author aki
     * @deprecated table configuration_targets does not exist anymore
     */
    private function fetchConfigurationTargetId(string $cnf_tgt): int
    {
        $cnf_tgt_id = 0;
        if (empty($cnf_tgt)) {
            $this->logger->critical('Invalid Configuration Target', [__METHOD__, __LINE__]);
            return $cnf_tgt_id;
        }
        $sql = 'SELECT id 
                FROM configuration_targets 
                WHERE name LIKE :cnf_tgt';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('cnf_tgt', $cnf_tgt);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['id'])) {
                $cnf_tgt_id = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $cnf_tgt_id;
    }


    /**
     * @internal: 2021-11-12 TODO: @Aravind:  check statement!!! col usecase_type does not exist in tab campaign => method may cause ERRORS
     * @param $companyId
     * @return int|null
     */
    public function getCampaignId($companyId): ?int
    {
        $campaignId = null;
        if (empty(($companyId))) {
            $this->logger->critical('Invalid companyId', [__METHOD__, __LINE__]);
            return $campaignId;
        }
        $sqlSelect = 'select id from campaign where company_id =:companyId and usecase_type =:useCaseType';
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindValue('companyId', $companyId);
            $stmt->bindValue('useCaseType', self::ObjectType);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['id'])) {
                $campaignId = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $campaignId;
    }


    /**
     * @param int   $campaignId
     * @param array $data
     * @return bool|null
     * @throws JsonException
     * @author Aki
     * @deprecated since 2021-11-12 => table campaign_params does not exist anymore => TODO @Aravind: remove method or correct it
     */
    public function updateProject(int $campaignId, array $data): ?bool
    {
        $status = null;
        $configurationTargetId = $this->fetchConfigurationTargetId(self::ObjectType);
        $campaignParamsId = $this->getCampaignParamsId($campaignId, $configurationTargetId);
        if (empty($campaignId) || empty($data) || empty($configurationTargetId)) {
            $this->logger->critical('Invalid companyId or data', [__METHOD__, __LINE__]);
            return null;
        }
        $encodedData = json_encode($data, JSON_THROW_ON_ERROR | true, 512);
        $updateSql = 'update campaign_params set value = :data where id = :campaignParamsId';
        try {
            $stmt = $this->conn->prepare($updateSql);
            $stmt->bindParam(':data', $encodedData);
            $stmt->bindParam(':campaignParamsId', $campaignParamsId);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }

    /*
     * @param $companyId
     * @return int|null
     * @author aki
     */

    /**
     * @param int $campaignId
     * @param int $configurationTargetId
     * @return int|null
     * @author aki
     * @deprecated since 2021-11-12 => table campaign_params does not exist anymore => TODO @Aravind: remove method or correct it
     */
    private function getCampaignParamsId(int $campaignId, int $configurationTargetId): ?int
    {
        $campaignParamsId = null;
        if (empty($campaignId) && empty($configurationTargetId)) {
            $this->logger->critical('Invalid companyId or data', [__METHOD__, __LINE__]);
            return null;
        }
        $sqlSelect = 'select id from campaign_params where campaign_Id = :campaignId and configuration_target_id = :configurationTargetId';
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindValue('campaignId', $campaignId);
            $stmt->bindValue('configurationTargetId', $configurationTargetId);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['id'])) {
                $campaignParamsId = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $campaignParamsId;
    }


    /**
     * @param int $campaignId
     * @return string
     * @author Aki
     * @deprecated since 2021-11-12 => table campaign_params does not exist anymore => TODO @Aravind: remove method or correct it
     */
    public function getProjectdata(int $campaignId): string
    {
        $data = [];
        if (empty($campaignId)) {
            return $data; // TODO @Aravind => check if expected types are returned
        }
        $configurationTargetId = $this->fetchConfigurationTargetId(self::ObjectType);
        $campaignParamsId = $this->getCampaignParamsId($campaignId, $configurationTargetId);
        if (empty($campaignId) || empty($configurationTargetId)) { // TODO: @Aravind this condition is always false !!!
            $this->logger->critical('Invalid companyId or data', [__METHOD__, __LINE__]);
            return $data; // TODO: @Aravind WRONG return type => MUST BE string
        }
        $sqlSelect = 'select value from campaign_params where id = :campaignParamsId';
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindValue('campaignParamsId', $campaignParamsId);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['value'])) {
                $data = $res[0]['value'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $data;
    }


    /**
     * getActiveWebhooks fetches the list of all the webhooks configured for company.
     *
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    public function getActiveWebhooks(int $companyId): ?array
    {
        $ac_webhooks = null;
        $statusActive = StatusdefRepository::ACTIVE;
        $uniqueObjectWebhook = UniqueObjectTypeRepository::WEBHOOK;

        if ($companyId <= 0 || $this->conn === null) {
            return $ac_webhooks;
        }
        try {
            $sql = '
            select c.*, o.object_unique_id as uuid
            from campaign c
                join objectregister o on c.objectregister_id = o.id
                join statusdef s on o.statusdef_id = s.id
                join unique_object_type uot on o.unique_object_type_id = uot.id
            where s.value=:active and uot.unique_object_name LIKE :webhook AND c.company_id = :company_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindParam(':active', $statusActive, ParameterType::STRING);
            $stmt->bindParam(':webhook', $uniqueObjectWebhook, ParameterType::STRING);
            $stmt->execute();

            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[0])) {
                $ac_webhooks = $result;
            }
            return $ac_webhooks;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $ac_webhooks;
        }
    }


    /**
     * @param int    $companyId
     * @param string $webhookName
     * @param array  $data
     * @param int    $userId
     * @return int|null
     * @author Pradeep
     */
    public function createWebhook(int $companyId, string $webhookName, array $data, int $userId = 999999): ?int
    {
        $uniqueObjectName = $this->uniqueObjectTypeRepository::WEBHOOK;
        $statusdefValue = $this->statusdefRepository::ACTIVE;
        $webhookConfig = null;
        $webhookId = null;
        try {
            if ($companyId <= 0 || empty($webhookName)) {
                throw new RuntimeException('Invalid Company Id or Webhook Name');
            }

            // get Company Details.
            $this->conn->beginTransaction();
            $company = $this->companyRepository->getCompanyDetails($companyId, 0);
            $projectId = $this->getProjectId($company['account_no']);// create ObjectRegisterId.
            $objectRegisterId = $this->objectRegisterRepository->createObjectRegister($uniqueObjectName, $statusdefValue, $userId);

            // Intialize Webhook wizard
            $company['name'] = $company['name'] ?? 'shopSystemWebhook';
            if ((!$this->webhookWizardTypes->initialize($company['name'], $company['id'], $projectId))) {
                throw new RuntimeException('Invalid Webhook Configurations');
            }
            $mioStandardFields = $this->mioStandardFieldRepository->getAll();
            // NewsLetter and eCommerce Webhooks
            if (trim($webhookName) === 'NewsletterHook' /*strpos($webhookName, 'newsletter')*/) {
                $webhookConfig = $this->webhookWizardTypes->getDefaultConfigForNewsLetterWebhook($webhookName,
                    $mioStandardFields);
            }
            if (trim($webhookName) === 'eCommerceHook' /*strpos($webhookName, 'ecommerce')*/) {
                $webhookConfig = $this->webhookWizardTypes->getDefaultConfigForeCommerceWebhook($webhookName,
                    $mioStandardFields);
            }

            //create Webhook in DB.
            //todo:add url to webhook-make default urls each type of webhook.
            $webhookId = $this->createCampaignEntry($company['id'], $webhookName, $company['account_no'], '', $objectRegisterId, '', $userId, $userId);
            //append additional settings for webhook config
            $webhookConfig['additionalSettings']['objectregister_id'] = $objectRegisterId;
            $webhookConfig['additionalSettings']['accountNumber'] = $data['accountNo'];
            $webhookConfig['additionalSettings']['apiKey'] = $data['apikey'];
            $webhookConfig['additionalSettings']['CampaignName'] = $webhookName;
            $metaValue = json_encode($webhookConfig, JSON_THROW_ON_ERROR);
            if ($this->objectRegisterMetaRepository->insert($objectRegisterId, UniqueObjectTypeRepository::WEBHOOK, $metaValue) === null) {
                throw new RuntimeException('Failed to store webhook configuration to ObjectRegisterMeta.');
            }
            // Commit the whole transaction.
            $this->conn->commit();
            return $webhookId;
        } catch (LogicException | UnexpectedValueException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->conn->rollback();
            return $webhookId;
        }
    }


    /**
     * @param int $companyId
     * @return array|null
     * @author Pradeep
     */
    public function getProjectDataFromCompanyId(int $companyId): ?array
    {
        $projectData = [];
        $unique_object_name = UniqueObjectTypes::PROJECT;
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid CompanyId provided');
            }
            $sql = '
                SELECT c.*
                FROM campaign c
                         JOIN objectregister o on c.objectregister_id = o.id
                         JOIN unique_object_type uot on o.unique_object_type_id = uot.id
                WHERE uot.unique_object_name = :unique_object_name
                  AND c.company_id = :company_id;';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('unique_object_name', $unique_object_name, ParameterType::STRING);
            $stmt->bindParam('company_id', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result[0]) && $stmt->rowCount() > 0) {
                $projectData = $result[0];
            }
            return $projectData;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int $accountNumber
     * @return int|null
     * @author Pradeep
     */
    public function getProjectId(int $accountNumber): ?int
    {
        $projectId = null;
        $unique_object_name = $this->uniqueObjectTypeRepository::PROJECT;

        if ($accountNumber <= 0) {
            return null;
        }
        $sql = 'SELECT c.id FROM campaign c
                JOIN objectregister o ON c.objectregister_id = o.id
                JOIN unique_object_type uot on o.unique_object_type_id = uot.id
                JOIN company c2 on c.company_id = c2.id
                WHERE uot.unique_object_name LIKE :unique_object_name and c2.account_no=:account_no';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_name', $unique_object_name, ParameterType::STRING);
            $stmt->bindParam(':account_no', $accountNumber, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && isset($result[0]['id'])) {
                $projectId = $result[0]['id'];
            }
            return $projectId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int    $companyId
     * @param string $name
     * @param string $accountNo
     * @param string $url
     * @param int    $objectRegisterId
     * @param string $comment
     * @param int    $createdBy
     * @param int    $updatedBy
     * @return int|null
     * @throws Exception
     * @author Aki
     */
    public function createCampaignEntry(
        int $companyId,
        string $name,
        string $accountNo,
        string $url,
        int $objectRegisterId,
        string $comment = '',
        int $createdBy = 99999,
        int $updatedBy = 99999
    ): ?int {
        $campaignId = null;

        // get unique-object-name to set a value for campaign.url (not null constraint)
        $uniqueObjectName = $this->objectRegisterRepository->getUniqueObjectNameFromObjectRegisterId($objectRegisterId);

        $company = $this->companyRepository->getCompanyDetails($companyId, 0);
        if ($company === null || empty($name) || empty($accountNo)) {
            throw new Exception('Invalid Parameters Passed to createCampaignEntity');
        }

        if (empty($url)) {
            $companyName = empty($company['name']) ? $accountNo : $company['name'];

            switch ($uniqueObjectName) {
                case 'page' :
                    $domain = $_ENV['CAMPAIGN_DOMAIN'];
                    break;
                case 'webhook':
                    $domain = $_ENV['WEBHOOK_DOMAIN'];
                    break;
                case 'project':
                    $domain = 'https://dummy-project-domain.net';
                    break;
                default:
                    $domain = 'https://dummy-domain.net';
                    break;
            }

            $url = $this->generateUrlForCampaign($companyName, $name, '', $domain);
        }
        $sql = "insert into campaign (company_id, objectregister_id, name, account_no, url , comment, created_by, updated_by)
            values (:company_id, :objectregister_id,:name, :account_no,:url,:comment, :created_by, :updated_by)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
        $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
        $stmt->bindParam(':name', $name, ParameterType::STRING);
        $stmt->bindParam(':account_no', $accountNo, ParameterType::STRING);
        $stmt->bindParam(':url', $url, ParameterType::STRING);
        $stmt->bindParam(':comment', $comment, ParameterType::STRING);
        $stmt->bindParam(':created_by', $createdBy, ParameterType::INTEGER);
        $stmt->bindParam(':updated_by', $updatedBy, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                $campaignId = $this->conn->lastInsertId();
            }
            return $campaignId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaignId;
        }
    }


    /**
     * @param string $companyName
     * @param string $campaignName
     * @param string $host
     * @param string $domain
     * @return string|null
     * @author Pradeep
     */
    private function generateUrlForCampaign(string $companyName, string $campaignName, string $host = '', string $domain): string
    {
        $campaignUrl = '';
        $defaultHostName = $domain;
        if (empty($companyName) || empty($campaignName)) {
            return $campaignUrl;
        }
        $host = !empty($host) ? $host : $defaultHostName;
        return $host . '/' . $companyName . '/' . $campaignName;
    }


    /**
     * @param string $campaignName
     * @param int    $companyId
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignDetailsByName(string $campaignName, int $companyId): ?array
    {
        $campaign = null;
        try {
            if (empty($campaignName)) {
                throw new Exception("Campaign name cannot be empty.");
            }
            $sql = 'SELECT * FROM campaign WHERE name LIKE :campaign_name and company_id = :companyId';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':campaign_name', $campaignName, ParameterType::STRING);
            $stmt->bindParam(':companyId', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0])) {
                $campaign = $result[0];
            }
            return $campaign;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaign;
        }
    }


    /**
     * @param int $campaignId
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignDetailsById(int $campaignId): ?array
    {
        $campaign = null;
        try {
            if ($campaignId <= 0) {
                throw new Exception("Campaign Id cannot be empty.");
            }
            $sql = 'SELECT * FROM campaign WHERE id = :campaign_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':campaign_id', $campaignId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0])) {
                $campaign = $result[0];
            }
            return $campaign;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaign;
        }
    }


    /**
     * @param int $campaignId
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignStatusAndVersion(int $campaignId): ?array
    {
        $details = null;
        try {
            if ($campaignId <= 0) {
                throw new Exception('Invalid CampaignId provided');
            }
            $sql = 'SELECT o.version, s.value as status FROM campaign c
                        JOIN objectregister o on c.objectregister_id = o.id
                        JOIN statusdef s on o.statusdef_id = s.id
                    WHERE c.id=:campaign_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':campaign_id', $campaign_id, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($return[0])) {
                $details = $result[0];
            }
            return $details;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $details;
        }
    }


    /**
     * @param int $objectRegisterId
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignDetailsByObjectRegisterId(int $objectRegisterId): ?array
    {
        $campaign = null;
        try {
            if ($objectRegisterId <= 0) {
                throw new RuntimeException('Invalid objectregister_id provided');
            }
            $sql = 'SELECT * FROM campaign WHERE objectregister_id=:object_register_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':object_register_id', $objectRegisterId, PARAMETERType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0])) {
                $campaign = $result[0];
            }
            return $campaign;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaign;
        }
    }


    /**
     * @param int $companyId
     * @return array|null
     * @author Pradeep
     */
    public function getProjectConfigurationForCompany(int $companyId): ?array
    {
        $projectConfiguration = [];
        $unique_object_name = 'project';
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid CompanyId provided');
            }
            $sql = 'select om.meta_value
                    from object_register_meta om
                             Join objectregister o on o.id = om.object_register_id
                             JOIN campaign c on o.id = c.objectregister_id
                             JOIN unique_object_type uot on o.unique_object_type_id = uot.id
                    Where c.company_id = :company_id
                      AND uot.unique_object_name =:unique_object_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':unique_object_name', $unique_object_name, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0]) && !empty($result[0]['meta_value'])) {
                $projectConfiguration = json_decode($result[0]['meta_value'], true, 512, JSON_THROW_ON_ERROR);
            }
            return $projectConfiguration;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    public function getDraftWebhooks(int $companyId): ?array
    {
        $ac_webhooks = null;
        $statusDraft = StatusdefRepository::DRAFT;
        $uniqueObjectWebhook = UniqueObjectTypeRepository::WEBHOOK;

        if ($companyId <= 0 || $this->conn === null) {
            return $ac_webhooks;
        }
        try {
            $sql = '
            select c.* from campaign c
                join objectregister o on c.objectregister_id = o.id
                join statusdef s on o.statusdef_id = s.id
                join unique_object_type uot on o.unique_object_type_id = uot.id
            where s.value=:draft and uot.unique_object_name LIKE :webhook AND c.company_id = :company_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindParam(':draft', $statusDraft, ParameterType::STRING);
            $stmt->bindParam(':webhook', $uniqueObjectWebhook, ParameterType::STRING);
            $stmt->execute();

            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[0])) {
                $ac_webhooks = $result;
            }
            return $ac_webhooks;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int    $campaignObjRegId
     * @param string $dataSource
     * @return bool|null
     * @author Pradeep
     *    Validations->
     *    1. start and end date of campaign
     *    2. status of the campaign
     * @deprecated 2021-11-12 => used nowhere
     */
    public function validateCampaign(int $campaignObjRegId, string $dataSource): ?bool
    {
        $isActive = false;
        try {
            if ($campaignObjRegId <= 0 || !in_array($dataSource, ['webhook', 'page'])) {
                throw new RuntimeException('Invalid ObjectRegister Id or dataSource provided');
            }
            // get Setting related to page or webhook
            $metaValue = $this->objectRegisterMetaRepository->getMetaValue($campaignObjRegId, $dataSource);
            if (empty($metaValue) || empty($metaValue['settings'])) {
                throw new RuntimeException('Failed to retrieve MetaValue ');
            }

            // Start and End Date Validation
            $dateValidation = $this->validateStartAndEndDate($metaValue['settings']['beginDate'], $metaValue['settings']['beginDate']);
            // Status Validation , could be 'active,draft, archived'
            $statusValidation = $this->validateStatus($campaignObjRegId);
            if ($dateValidation && $statusValidation) {
                $isActive = true;
            }
            return $isActive;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * TODO: <b style='color:red'>remove $startDate</b>
     * @internal startDate <b>not used in code!!!</b>
     * @param string|null $startDate
     * @param string|null $endDate
     * @return bool
     * @throws Exception
     * @author Pradeep
     * 1. start and end date are optional.
     * 2. false only if the webhook end date is more than current date
     */
    private function validateStartAndEndDate(string $startDate = null, string $endDate = null): bool
    {
        $status = false;
        try {
            if (empty($endDate)) {
                $this->logger->info('EndDate for the webhook is empty');
                return true;
            }
            // Date format 'y/m/d H:i' is considered in Page/Webhook/Project.
            $date = DateTime::createFromFormat('y/m/d H:i', $endDate);
            if (!$date) {
                throw new RuntimeException('Invalid EndDate provided');
            }
            $endTimeStamp = $date->getTimestamp();
            $currentTimeStamp = time();
            if ($endTimeStamp >= $currentTimeStamp) {
                $status = true;
            }
            return $status;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }


    /**
     * @param int $campaignObjRegId
     * @return bool
     * @author Pradeep
     */
    private function validateStatus(int $campaignObjRegId): bool
    {
        $status = false;
        try {
            if ($campaignObjRegId <= 0) {
                throw new RuntimeException('Invalid Campaign Object Register Id provided');
            }
            $objStatus = $this->objectRegisterRepository->getStatus($campaignObjRegId);
            if ($objStatus === StatusdefRepository::ACTIVE) {
                $status = true;
            }
            return $status;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }


    /**
     * @param int $campaignId
     * @return int|null
     * @author   Pradeep
     * @internal Get permission for contact, i,e DOI,
     */
    public function getPermission(int $campaignId): ?int
    {
        $permission = 1;
        try {
            if ($campaignId <= 0) {
                throw new RuntimeException('Invalid Campaign Id provided');
            }
            $sql = 'SELECT orm.meta_value FROM campaign c
                    join objectregister o on o.id = c.objectregister_id
                    join object_register_meta orm ON orm.object_register_id = o.id
                    where c.id = :campaign_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':campaign_id', $campaignId);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result[0]['meta_value']) || $stmt->rowCount() < 0) {
                return $permission;
            }
            $result = json_decode($result[0]['meta_value'], true, 512, JSON_THROW_ON_ERROR);
            if (empty($result) || empty($result['settings']['permission'])) {
                return $permission;
            }
            $permission = $result['settings']['permission'];
            return $permission;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $permission;
        }
    }


    /**
     * @param int $campaignId
     * @return bool
     * @author Pradeep
     * @internal
     * TRUE - Is eCommerceHook (auto generated by SHOP MVP)
     * FALSE - Not eCommerceHook
     */
    public function iseCommerceHook(int $campaignId): bool
    {
        $campaign = $this->getCampaignDetailsById($campaignId);
        if (empty($campaign) || $campaign[ 'name' ] !== WebhookWizardTypes::ECOMMERCEHOOK) {
            return false;
        }
        return true;
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getWebhooksWithJotFormsInstalled(int $accountNumber): array
    {
        if ($accountNumber <= 0) {
            return [];
        }
        $active = "active";
        $uniqueObjName = "webhook";
        $metaKey = "jotform";

        $sql = '
            select c.* , o.object_unique_id as uuid
            from campaign c
                join objectregister o on c.objectregister_id = o.id
                join object_register_meta orm  on orm.object_register_id = o.id
                join statusdef s on o.statusdef_id = s.id
                join unique_object_type uot on o.unique_object_type_id = uot.id
            where 
                  s.value=:webhook_active and 
                  uot.unique_object_name = :webhook and 
                  orm.meta_key LIKE "%jotform%" and 
                  c.account_no = :account_no group by uuid
        ';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':webhook_active', $active);
        $stmt->bindValue(':webhook', $uniqueObjName);
        //$stmt->bindValue(':jotform', $metaKey);
        $stmt->bindValue(':account_no', $accountNumber);

        $stmt->execute();
        return $stmt->fetchAll();
    }
}

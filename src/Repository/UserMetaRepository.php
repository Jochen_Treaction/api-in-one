<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class UserMetaRepository
{
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;


    /**
     * UserMetaRepository constructor.
     * @param LoggerInterface $logger
     * @param Connection $connection
     */
    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->conn = $connection;
    }

    /**
     * @param int $id
     * @param int $userId
     * @param string $metaKey
     * @param string $metaValue
     * @return bool|null
     * @author Pradeep
     */
    public function update(int $id, int $userId, string $metaKey, string $metaValue): ?bool
    {

    }

    /**
     * @param $id
     * @return array|null
     * @author Pradeep
     */
    public function get($id): ?array
    {

    }

    /**
     * @param int $userId
     * @param string $metaKey
     * @param string $metaValue
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @author Pradeep
     */
    public function insert(
        int $userId,
        string $metaKey,
        string $metaValue,
        int $createdBy = 1,
        int $updatedBy = 1
    ): ?int {
        $userMetaId = null;
        try {
            if ($userId <= 0 || empty($metaKey) || empty($metaValue)) {
                throw new RuntimeException('Invalid UserId or MetaKey Or MetaValue provided');
            }
            $sql = 'insert into users_meta(users_id, meta_key, meta_value, created_by, updated_by)
                    value(:users_id, :meta_key, :meta_value, :created_by, :updated_by)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':users_id', $userId);
            $stmt->bindParam(':meta_key', $metaKey);
            $stmt->bindParam(':meta_value', $metaValue);
            $stmt->bindParam(':created_by', $createdBy);
            $stmt->bindParam(':updated_by', $updatedBy);
            $result = $stmt->execute();
            if ($result === true) {
                $userMetaId = $this->conn->lastInsertId();
            }
        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $userMetaId;
    }
}
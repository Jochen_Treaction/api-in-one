<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class CampaignParamsRepository
{

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    /**
     * @param int $campaignId
     * @param int $configurationTargetId
     * @param array $value
     * @return int|null
     * @author Pradeep
     */
    public function insert(int $campaignId, int $configurationTargetId, array $value) :?int
    {
        $campaignParamsId=null;
        if($campaignId===null || $configurationTargetId <= 0) {
            throw new Exception('Missing CampaignId or ConfigurationTargetId');
        }
        try {
            $value = json_encode($value);
            $defaultUserId = 999999;
            $sql = 'INSERT INTO campaign_params(campaign_Id, configuration_target_id, value, created_by, updated_by) 
                    VALUE(:campaign_id, :configuration_target_id, :value, :created_by, :updated_by)';

            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':campaign_id', $campaignId, ParameterType::INTEGER);
            $stmt->bindParam(':configuration_target_id', $configurationTargetId, ParameterType::INTEGER);
            $stmt->bindParam(':value', $value, ParameterType::STRING);
            $stmt->bindParam(':created_by', $defaultUserId, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $defaultUserId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $campaignParamsId = $this->conn->lastInsertId();
            }
            return $campaignParamsId;
        } catch (DBALException | Exception $e) {
            var_dump($e->getMessage(), [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaignParamsId;
        }
    }

    public function update() :?bool
    {

    }

    public function delete() :?bool
    {

    }

    public function get():?array
    {

    }

    /**
     * @param int $campaignId
     * @param string $configurationTargetName
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignParams(int $campaignId, string $configurationTargetName): ?array
    {
        $campaignParams=null;
        try {
            if ($campaignId <= 0 || empty($configurationTargetName)) {
                throw new RuntimeException('Invalid CampaignId or Configurationtarget provided');
            }
            $sql = 'SELECT cp.value FROM campaign_params cp
                        JOIN configuration_targets ct ON cp.configuration_target_id = ct.id
                    WHERE cp.campaign_Id=:campaign_id AND ct.name LIKE :configurationTargetName';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':configurationTargetName', $configurationTargetName, ParameterType::STRING);
            $stmt->bindParam('campaign_id', $campaignId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $campaignParams = $result[ 0 ];
            }
            return $campaignParams;
        } catch (DBALException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $campaignParams;
        }
    }

}
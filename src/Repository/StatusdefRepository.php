<?php


namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;

class StatusdefRepository
{

    public const ACTIVE = 'active';
    public const ARCHIVED = 'archived';
    public const DRAFT = 'draft';
    public const INACTIVE = 'inactive';
    public const FRAUD = 'fraud';
    public const NEW = 'new';
    public const CONFIRMED = 'confirmed';
    public const MARKED_FOR_DELETE = 'markedfordeletion';
    public const UNSUBSCRIBED = 'unsubscribed';
    public const BLACKLISTED = 'blacklisted';

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * StatusModelRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $Logger
     */
    public function __construct(Connection $connection, LoggerInterface $Logger)
    {
        $this->conn = $connection;
        $this->logger = $Logger;
    }

    public function create():?int
    {

    }

    public function delete():?bool
    {

    }

    public function update():?bool
    {

    }

    public function get():?array
    {

    }

    /**
     * @param string $value
     * @return int|null
     * @author Pradeep
     */
    public function getStatusdefId(string $value) :?int
    {
        $statusdefId = null;
        if(empty($value)) {
            return null;
        }
        $sql = 'SELECT id FROM statusdef WHERE value = :value';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':value', $value, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ]) && isset($result[ 0 ][ 'id' ])) {
                $statusdefId = $result[ 0 ][ 'id' ];
            }
            return $statusdefId;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(),  [__METHOD__, __LINE__]);
            return $statusdefId;
        }
    }
}
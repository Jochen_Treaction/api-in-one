<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;

class ContactSyncToIntegrationRepository
{

    /**
     * Supported status for the table `lead_sync_log`.
     */
    public const SYNC_STATUS_TYP_NEW = 'new';
    public const SYNC_STATUS_TYP_WAITING = 'waiting';
    public const SYNC_STATUS_TYP_WORKING = 'working';
    public const SYNC_STATUS_TYP_DONE = 'done';
    public const SYNC_STATUS_TYP_ERROR = 'failed';
    public const SYNC_STATUS_TYP_RETRY = 'retry';

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    public function getByIntegrationId(int $integrationId): array
    {
        try {
            if ($integrationId <= 0) {
                throw new Exception('Invalid Integration Id provided');
            }
            $stmt = 'select * from lead_sync_log where integration_id = ?';
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(1, $integrationId);
            $sql->execute();
            $result = $sql->fetchAll();
            if (!is_array($result) || empty($result) || !isset($result[ 0 ])) {
                return [];
            }
            return $result[ 0 ];
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    public function getCountOfOpenJobs()
    {
        $count = 0;
        $stmt = 'SELECT count(*) as count FROM lead_sync_log WHERE status = ? OR status = ? OR status = ?';
        $sql = $this->conn->prepare($stmt);
        $sql->bindValue(1, self::SYNC_STATUS_TYP_NEW);
        $sql->bindValue(2, self::SYNC_STATUS_TYP_WAITING);
        $sql->bindValue(3, self::SYNC_STATUS_TYP_RETRY);
        $sql->execute();
        $rows = $sql->fetchAll();
        if (isset($rows[ 0 ][ 'count' ]) && !empty($rows[ 0 ][ 'count' ])) {
            $count = $rows[ 0 ][ 'count' ];
        }
        return $count;
    }

    public function getAll(): array
    {
        $stmt = 'SELECT * FROM lead_sync_log WHERE id > 0';
        $sql = $this->conn->prepare($stmt);
        $sql->execute();
        return $sql->fetchAll();
    }

    public function update(
        $id,
        $integrationId,
        $settings,
        $lastSyncDate,
        $lastSyncContactId,
        $totalContactsToSync,
        $totalContactsSyncSuccessfully,
        $totalContactsPendingToSync,
        $status,
        $message = ''
    ) {
        if ($id <= 0) {
            return false;
        }
        $sql = '
            update lead_sync_log
            set 
                integration_id = :integration_id,
                settings = :settings,
                last_sync_date = :last_sync_date,
                last_sync_contact_id = :last_sync_contact_id,
                total_contacts_to_sync = :total_contacts_to_sync,
                total_contacts_sync_succcessfully = :total_contacts_sync_succcessfully,
                total_contacts_pending_to_sync = :total,
                status =:lead_sync_status,
                message = :message
            where
                id =:id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':integration_id', $integrationId);
        $stmt->bindValue(':settings', $settings);
        $stmt->bindValue(':last_sync_date', $lastSyncDate);
        $stmt->bindValue(':last_sync_contact_id', $lastSyncContactId);
        $stmt->bindValue(':total_contacts_to_sync', $totalContactsToSync);
        $stmt->bindValue(':total_contacts_sync_succcessfully', $totalContactsSyncSuccessfully);
        $stmt->bindValue(':total_contacts_pending_to_sync', $totalContactsPendingToSync);
        $stmt->bindValue(':lead_sync_status', $status);
        $stmt->bindValue(':message', $message);
        $stmt->bindValue(':id', $id);
        return $stmt->execute();
    }

    /**
     * @param string $status
     * @return array
     * @author Pradeep
     */
    public function getByStatus(string $status): array
    {
        $result = [];
        try {
            $stmt = '
                SELECT lsl.*, i.company_id 
                FROM lead_sync_log lsl 
                JOIN integrations i on lsl.integration_id = i.id
                WHERE lsl.status = ? order by lsl.id desc limit 1';
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue('1', $status);
            $sql->execute();
            $rows = $sql->fetchAll();
            if (!empty($rows[ 0 ])) {
                $result = $rows[ 0 ];
            }
            return $result;
        } catch (\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $result;
        }
    }


    public function insert(
        int $integrationId,
        array $settings,
        int $totalContactsToSync = 0,
        int $totalContactsSyncSuccessfully = 0,
        int $totalContactsPendingToSync = 0,
        string $status = self::SYNC_STATUS_TYP_NEW,
        string $message = ""
    ): bool {

        $this->logger->info('insert :' . json_encode([
                'IntegrationId' => $integrationId,
                'setting' => $settings,
                'lastSyncContactId' => $lastSyncContactId,
            ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        try {
            $stmt = "insert into `lead_sync_log` (
                             integration_id, 
                             settings, 
                             total_contacts_to_sync, 
                             total_contacts_sync_succcessfully,
                             total_contacts_pending_to_sync, 
                             status, 
                             message
                             ) values (?,?,?,?,?,?,?,?);";
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(1, $integrationId, ParameterType::INTEGER);
            $sql->bindValue(2, $settings);
            $sql->bindValue(3, $lastSyncContactId, ParameterType::INTEGER);
            $sql->bindValue(4, $totalContactsToSync, ParameterType::INTEGER);
            $sql->bindValue(5, $totalContactsSyncSuccessfully, ParameterType::INTEGER);
            $sql->bindValue(6, $totalContactsPendingToSync, ParameterType::INTEGER);
            $sql->bindValue(7, $status);
            $sql->bindValue(8, $message);
            $result = $sql->execute();
            $this->logger->info('insert :' . json_encode($result), [__METHOD__, __LINE__]);
            return $result;
        } catch (Exception|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }


    public function getCurrentTimeStamp(): string
    {
        return date("Y-m-d H:i:s");
    }

    public function start(int $id): bool
    {
        $timeStamp = $this->getCurrentTimeStamp();
        $status = self::SYNC_STATUS_TYP_WORKING;
        $stmt = "
            UPDATE lead_sync_log
            SET status = :status, start = :start , updated = :updated
            WHERE id = :id
        ";
        $sql = $this->conn->prepare($stmt);
        $sql->bindValue(':status', $status);
        $sql->bindValue(':start', $timeStamp);
        $sql->bindValue(':updated', $timeStamp);
        $sql->bindValue(':id', $id, ParameterType::INTEGER);
        return $sql->execute();
    }

    public function updateStatus(int $id, string $status, string $message = ""): bool
    {
        $timeStamp = $this->getCurrentTimeStamp();

        if ($status === self::SYNC_STATUS_TYP_DONE) {
            $stmt = "
            UPDATE lead_sync_log
            SET status = :status, message = :message, end=:end , updated = :updated
            WHERE id = :id
        ";
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(':status', $status);
            $sql->bindValue(':message', $message);
            $sql->bindValue(':end', $timeStamp);
            $sql->bindValue(':updated', $timeStamp);
            $sql->bindValue(':id', $id, ParameterType::INTEGER);
        } else {
            $stmt = "
            UPDATE lead_sync_log
            SET status = :status, message = :message,  updated = :updated
            WHERE id = :id
        ";
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(':status', $status);
            $sql->bindValue(':message', $message);
            $sql->bindValue(':updated', $timeStamp);
            $sql->bindValue(':id', $id, ParameterType::INTEGER);
        }


        return $sql->execute();
    }
}
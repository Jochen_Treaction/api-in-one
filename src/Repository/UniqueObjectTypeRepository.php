<?php


namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;

class UniqueObjectTypeRepository
{
    public const WEBHOOK = 'webhook';
    public const PROJECT = 'project';
    public const PAGE = 'page';
    public const CUSTOMER = 'customer';
    public const CUSTOMFIELDS = 'customfields';
    public const STANDARDCUSTOMFIELDS = 'standardcustomfields';
    public const SURVEY = 'survey';
    public const ACCOUNT = 'account';
    public const EMAILSERVER = 'emailserver';
    public const EMAILINONE = 'email-in-one';
    public const HUBSPOT = 'hubspot';
    public const WBHOOKQUEUE = 'externaldata_queue';

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * StatusModelRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $Logger
     */
    public function __construct(Connection $connection, LoggerInterface $Logger)
    {
        $this->conn = $connection;
        $this->logger = $Logger;
    }
    
    public function create( string $assignedTable, string $uniqueObjectName, int $hasVersioning=1, int $hasEncryption=0, string $description=''):?int
    {
        $uniqueObjectTypeId = null;
        if(empty($assignedTable) || empty($uniqueObjectName)) {
            return $uniqueObjectTypeId;
        }
        return $uniqueObjectTypeId;
    }

    public function update()
    {

    }

    public function getUniqueObjectTypeId(string $uniqueObjectName) :?int
    {
        $uniqueObjectTypeId=null;
        if(empty($uniqueObjectName)) {
            return $uniqueObjectTypeId;
        }
        $sql = 'SELECT id FROM unique_object_type WHERE unique_object_name LIKE :unique_object_name';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_name', $uniqueObjectName, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if (($rowCount > 0) && !empty($result[ 0 ][ 'id' ])) {
                $uniqueObjectTypeId = $result[ 0 ][ 'id' ];
            }
            return $uniqueObjectTypeId;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $uniqueObjectTypeId;
        }
    }

    public function deleteUniqueObjectType()
    {

    }
}
<?php


namespace App\Repository;


use App\Types\AccountStatusTypes;
use App\Types\UserStatusTypes;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class CompanyRepository
{

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var string
     * @author Pradeep
     */
    private $uniqueAccountObject;
    /**
     * @var string
     * @author Pradeep
     */
    private $statusdefActive;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;

    /**
     * CompanyRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $logger
     * @param ObjectRegisterRepository $objectRegisterRepository
     */
    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        StatusdefRepository $statusdefRepository
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->statusdefRepository = $statusdefRepository;
    }

    /**
     * @param int $companyId
     * @param int $accountNo
     * @return array|null
     * @author Pradeep
     */
    public function getCompanyDetails(int $companyId = 0, int $accountNo = 0): ?array
    {
        $company_details = null;
        try {
            if ($companyId > 0 && $accountNo > 0) {
                $sql = 'SELECT * FROM company WHERE id=:company_id AND account_no=:account_no';
                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam(':account_no', $accountNo, ParameterType::INTEGER);
                $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            }
            if ($companyId <= 0 && $accountNo <= 0) {
                $sql = 'SELECT * FROM company';
                $stmt = $this->conn->prepare($sql);
            }
            if ($companyId <= 0 && $accountNo > 0) {
                $sql = 'SELECT * FROM company where account_no=:account_no';
                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam(':account_no', $accountNo, ParameterType::INTEGER);
            }
            if ($accountNo <= 0 && $companyId > 0) {
                $sql = 'SELECT * FROM company where id=:company_id';
                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            }
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (is_array($result) && !empty($result[ 0 ])) {
                $company_details = $result[ 0 ];
            }
            return $company_details;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $company_details;
        }
    }

    /**
     * @param string $website
     * @return int|null
     * @author Pradeep
     */
    public function isCompanyExists(string $website): ?int
    {
        $company_id = null;
        if (empty($website)) {
            return null;
        }
        try {
            $sql = 'SELECT * FROM company WHERE website LIKE "%' . $website . '%"';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $return = $stmt->fetchAll();
            if (!empty($return) && !empty($return[ 0 ]) && isset($return[ 0 ][ 'id' ])) {
                $company_id = $return[ 0 ][ 'id' ];
            }
            return $company_id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $email
     * @return string|null
     * @author Pradeep
     */
    public function getDomainNameFromEmail(string $email): ?string
    {
        $domain = null;
        if (empty($email)) {
            return null;
        }
        try {
            $parse_email = explode('@', $email);
            if (isset($parse_email[ 0 ], $parse_email[ 1 ]) && is_array($parse_email)) {
                $domain = $parse_email[ 1 ];
            }
            return $domain;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $domain;
        }
    }

    /**
     * @param string $name
     * @param string $street
     * @param string $hNo
     * @param string $postalCode
     * @param string $city
     * @param string $country
     * @param string $website
     * @param string $phone
     * @param string $vat
     * @param string $loginUrl
     * @param string $delegatedDomain
     * @param string $urlOption
     * @param int $createdBy
     * @param int $updatedBy
     * @param string $modification
     * @return int|null
     * @author Pradeep
     */
    public function createCompany(
        string $name,
        string $street,
        string $hNo,
        string $postalCode,
        string $city,
        string $country,
        string $website,
        string $phone,
        string $vat,
        string $loginUrl,
        string $delegatedDomain,
        string $urlOption,
        int $createdBy,
        int $updatedBy,
        string $modification
    ): ?int {
        $company_id = null;
        try {
            $objectRegisterId = $this->objectRegisterRepository->createObjectRegister(
                UniqueObjectTypeRepository::ACCOUNT, StatusdefRepository::NEW);
            if ($objectRegisterId === null) {
                throw new RuntimeException('Failed to create ObjectRegisterId for Account');
            }
            if (empty($website)) {
                throw new RuntimeException('Invalid Parameters provided ');
            }
            // generate AccountNumber.
            $accountNo = $this->generateAccountNo();
            if ($accountNo === null) {
                throw new RuntimeException('Failed to generate Account Number');
            }
            // Consider Account name as Account number
            if (empty($name)) {
                $name = (string)$accountNo;
            }
            $sql = "INSERT INTO company(objectregister_id ,account_no, name, street, house_number, postal_code, city, country, website, phone, 
                    vat, login_url, delegated_domain, url_option, created_by, updated_by, modification) 
                VALUES (:objectregister_id, :account_no,:name, :street, :house_number, :postal_code, :city, :country, :website, :phone, :vat, 
                        :login_url, :delegated_domain, :url_option, :created_by, :updated_by, :modification)";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::STRING);
            $stmt->bindParam(':account_no', $accountNo, ParameterType::STRING);
            $stmt->bindParam(':name', $name, ParameterType::STRING);
            $stmt->bindParam(':street', $street, ParameterType::STRING);
            $stmt->bindParam(':house_number', $hNo, ParameterType::STRING);
            $stmt->bindParam(':postal_code', $postalCode, ParameterType::STRING);
            $stmt->bindParam(':city', $city, ParameterType::STRING);
            $stmt->bindParam(':country', $country, ParameterType::STRING);
            $stmt->bindParam(':website', $website, ParameterType::STRING);
            $stmt->bindParam(':phone', $phone, ParameterType::STRING);
            $stmt->bindParam(':vat', $vat, ParameterType::STRING);
            $stmt->bindParam(':login_url', $loginUrl, ParameterType::STRING);
            $stmt->bindParam(':delegated_domain', $delegatedDomain, ParameterType::STRING);
            $stmt->bindParam(':url_option', $urlOption, ParameterType::STRING);
            $stmt->bindParam(':created_by', $createdBy, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $updatedBy, ParameterType::INTEGER);
            $stmt->bindParam(':modification', $modification, ParameterType::INTEGER);

            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount === 0) {
                $this->logger->error('Failed to Insert Company.', [__METHOD__, __LINE__]);
                return null;
            }
            $company_id = $this->conn->lastInsertId();
            return $company_id;
        } catch (PDOException | DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return int
     * @author Pradeep
     */
    private function generateAccountNo(): ?int
    {
        /**
         * new, since 2021-02-03: account_no is now generated based on the autoincrement column "id" of table "company"
         * to create a unique, use "once only" account_no
         *
         */
        $account_no = 1000;
        try {
            $sql = "SELECT database()";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $database = $stmt->fetchOne();
            $sql = "SELECT auto_increment FROM information_schema.TABLES WHERE TABLE_NAME ='company' and TABLE_SCHEMA=:database";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':database', $database);
            $stmt->execute();
            $result = $stmt->fetchOne();
            /*
                     OLD:
                        $em = $this->getEntityManager();
                        $query = $em->createQuery('SELECT c
                            FROM App\Entity\Company c
                            ORDER BY c.accountNo DESC');
                        $em->flush();
                        $em->clear();
                        $result = $query->getResult();
            */
            // $acc_id = (!empty($result)) ? $result[0]->getId() : $account_no; // OLD
            $acc_id = (!empty($result)) ? $result : $account_no;
            // $this->logger->info('$query->getResult()=', [$query->getResult(), __METHOD__, __LINE__]); // OLD
            $this->logger->info('$result=', [$result, __METHOD__, __LINE__]);
            if (empty($acc_id)) {
                $acc_id = 0;
            }
            if ($account_no != $acc_id) {
                $account_no += (int)$acc_id;
            }
            return $account_no;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . "no company record present. returning with account_no #$account_no#",
                [__FUNCTION__, __LINE__]);
            return $account_no;
        }
    }

    /**
     * @param int $userId
     * @param int $companyId
     * @param int $createdBy
     * @param int $updateBy
     * @return int|null
     * @author Pradeep
     */
    public function registerAdminUserToCompany(int $userId, int $companyId, int $createdBy, int $updateBy): ?int
    {
        $admin_id = null;
        if ($userId <= 0 || $companyId <= 0) {
            return null;
        }
        $sql = "INSERT INTO administrates_companies(user_id, company_id, created_by, updated_by) 
                VALUES (:user_id, :company_id, :created_by, :updated_by)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':user_id', $userId, ParameterType::STRING);
        $stmt->bindParam(':company_id', $companyId, ParameterType::STRING);
        $stmt->bindParam(':created_by', $createdBy, ParameterType::INTEGER);
        $stmt->bindParam(':updated_by', $updateBy, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount === 0) {
                $this->logger->error('Failed to Insert to administrates Company.', [__METHOD__, __LINE__]);
                return null;
            }
            $admin_id = $this->conn->lastInsertId();
            return $admin_id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return bool|null
     * @author Pradeep
     */
    public function updateCompany(): ?bool
    {
        $sql = 'SELECT id, account_no FROM company ORDER BY id DESC LIMIT 1';

    }

    /**
     * @param int $companyId
     * @return array|null
     * @author Pradeep
     */
    public function getCompanyAdminDetails(int $companyId): ?array
    {
        $admin_details = null;
        if ($companyId <= 0) {
            return $admin_details;
        }
        $sql = 'SELECT u.* from administrates_companies ac
                JOIN company c on c.id = ac.company_id
                JOIN users u on u.id = ac.user_id WHERE ac.company_id=:company_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (is_array($result) && isset($result[ 0 ])) {
                $admin_details = $result[ 0 ];
            }
            return $admin_details;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $admin_details;
        }
    }

    /**
     * @param string $accountName
     * @return bool
     * @author Pradeep
     */
    public function checkIfAccountNameIsAvailable(string $accountName): bool
    {
        $status = false;
        if (empty($accountName)) {
            return $status;
        }
        $sql = "SELECT EXISTS(SELECT * FROM company where name LIKE ?) as accNameStatus;";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $accountName, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && isset($result[ 0 ][ 'accNameStatus' ])) {
                $status = $result[ 0 ][ 'accNameStatus' ] === '1';
            }
            return $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $accountName
     * @return array|null
     * @author Pradeep
     */
    public function getAccountNameRecommendation(string $accountName): ?array
    {
        $recommendations = [];
        if (empty($accountName)) {
            return null;
        }
        // prepare recommendations List
        $extension = ['01', '02', '03', '04', '05', '06', '07', '08', '09'];
        foreach ($extension as $ext) {
            $accNameWithExtension[ $ext ] = $accountName . $ext;
        }
        $sql = "SELECT name FROM company where name LIKE ?";
        try {
            $stmt = $this->conn->prepare($sql);
            // Check if the recommended list avilable.
            foreach ($accNameWithExtension as $key => $value) {
                $stmt->bindParam(1, $value, ParameterType::STRING);
                $stmt->execute();
                $rowCount = $stmt->rowCount();
                if ($rowCount === 0) {
                    $recommendations[] = $value;
                }
            }
            return $recommendations;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * select * from company
     * @param int $id
     * @return array
     * @author PVA
     */
    public function getCompanyDetailById(int $id): array
    {
        $cmp_details = [];
        if (empty($id) || $id <= 0) {
            return $cmp_details;
        }

        try {
            $sql = 'SELECT *
                    FROM company
                    WHERE id=:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $this->logger->info('$res', [$res, __METHOD__, __LINE__]);

            if (!empty($res) && !empty($res[ 0 ])) {
                $cmp_details = $res[ 0 ];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        $this->logger->info('$cmp_details', [$cmp_details, __METHOD__, __LINE__]);
        return $cmp_details;
    }

    /**
     * @param int $company_objectregister_id
     * @param string $statusType
     * @return int|null
     * @author Pradeep
     */
    public function setCompanyObjectRegisterStatusToAccountStatusType(
        int $company_objectregister_id,
        string $statusType
    ): ?int {
        if (!AccountStatusTypes::isCorrectType($statusType)) {
            return null;
        }

        $accountStatusId = $this->statusdefRepository->getStatusdefId($statusType);
        if (null === $accountStatusId) {
            $this->logger->error('no accountStatusId found for ' . AccountStatusTypes::MARKEDFORDELETION,
                [__METHOD__, __LINE__]);
            return null;
        }

        $updateCompanyObjectRegister = "UPDATE objectregister SET statusdef_id = :statusdef_id WHERE id = :company_objectregister_id";

        $stmt = $this->conn->prepare($updateCompanyObjectRegister);
        $stmt->bindParam(':statusdef_id', $accountStatusId, ParameterType::INTEGER);
        $stmt->bindParam(':company_objectregister_id', $company_objectregister_id, ParameterType::INTEGER);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->error('could not update objectregister with markedfordeletion for objectregister_id' . $company_objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $accountStatusId;
    }


}
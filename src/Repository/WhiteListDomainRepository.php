<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class WhiteListDomainRepository
{

    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * WhiteListDomainRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $logger
     */
    public function __construct(Connection $connection, LoggerInterface $logger) {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    /**
     * isWhitelisted checks whether the given domain name is present in the whitelisted table.
     * - table : whitelisted.
     * @param string $domain
     * @return bool
     * @author Pradeep
     */
    public function isWhitelisted(string $domain): bool
    {
        $status = false;
        try {
            if (empty($domain)) {
                throw new RuntimeException('Invalid Domain provided.');
            }
            $sql = 'select * from whitelist_domains where domain_name LIKE :domain';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':domain', $domain, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $status = true;
            }
            return $status;
        } catch (Exception|\Doctrine\DBAL\Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }
}
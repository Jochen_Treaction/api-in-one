<?php


namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;

class StatusModelRepository
{
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * StatusModelRepository constructor.
     * @param Connection $connection
     * @param LoggerInterface $Logger
     */
    public function __construct(Connection $connection, LoggerInterface $Logger)
    {
        $this->conn = $connection;
        $this->logger = $Logger;
    }

    /**
     * @param int $uniqueObjectTypeId
     * @param int $statusDefId
     * @param string $modelName
     * @return int|null
     * @author Pradeep
     */
    public function create(int $uniqueObjectTypeId, int $statusDefId, string $modelName): ?int
    {
        $statusModelId = null;
        if (empty($uniqueObjectTypeId) || empty($statusDefId) || empty($modelName)) {
            return null;
        }
        $sql = 'INSERT INTO status_model(unique_object_type_id, statusdef_id, modelname, created_by, updated_by)
                VALUES(:unique_object_type_id, :statusdef_id, :modelname, :created_by,:updated_by)';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_type_id', $uniqueObjectTypeId, ParameterType::INTEGER);
            $stmt->bindParam(':statusdef_id', $statusDefId, ParameterType::INTEGER);
            $stmt->bindParam(':modelname', $modelName, ParameterType::STRING);
            $result = $stmt->execute();
            if ($result === true) {
                $statusModelId = $this->conn->lastInsertId();
            }
            return $statusModelId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $statusModelId;
        }
    }

    /**
     * @return bool|null
     * @author Pradeep
     */
    public function update(): ?bool
    {

    }

    /**
     * @return bool|null
     * @author Pradeep
     */
    public function delete(): ?bool
    {

    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function get(): ?array
    {

    }

    public function getStatusModelId(int $uniqueObjectTypeId, int $statusDefId, string $modelName): ?int
    {
        $statusModelId = null;
        if (empty($uniqueObjectTypeId) || empty($statusDefId) || empty($modelName)) {
            return null;
        }
        $sql = 'SELECT id from status_model WHERE unique_object_id=:unique_object_id AND statusdef_id = :statusdef_id 
                              AND model_name = :model_name';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_type_id', $uniqueObjectTypeId, ParameterType::INTEGER);
            $stmt->bindParam(':statusdef_id', $statusDefId, ParameterType::INTEGER);
            $stmt->bindParam(':modelname', $modelName, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $statusDefId = $result[ 0 ][ 'id' ];
            }
            return $statusDefId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $statusDefId;
        }
    }
}
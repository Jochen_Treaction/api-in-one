<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\ParameterType;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * Class ShopDataQueueRepository
 * @package App\Repository
 * @author Pradeep
 */
class ShopDataQueueRepository
{

    /**
     * Supported status for the table `externaldata_queue`.
     */
    public const WB_QUEUE_STATUS_TYP_NEW = 'new';
    public const WB_QUEUE_STATUS_TYP_LOCKED = 'working';
    public const WB_QUEUE_STATUS_TYP_DONE = 'done';
    public const WB_QUEUE_STATUS_TYP_ERROR = 'failed';
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $connection;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;
    /**
     * @var
     * @author Pradeep
     */
    private $uniqueObjectTypeId;
    /**
     * @var StatusdefRepository
     * @author Pradeep
     */
    private StatusdefRepository $statusdefRepository;
    /**
     * @var UniqueObjectTypeRepository
     * @author Pradeep
     */
    private UniqueObjectTypeRepository $uniqueObjectTypeRepository;


    public function __construct(
        Connection $connection,
        ObjectRegisterRepository $objectRegisterRepository,
        UniqueObjectTypeRepository $uniqueObjectTypeRepository,
        StatusdefRepository $statusdefRepository,
        LoggerInterface $logger
    ) {
        $this->connection = $connection;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->uniqueObjectTypeRepository = $uniqueObjectTypeRepository;
        $this->statusdefRepository = $statusdefRepository;
        $this->logger = $logger;
    }

    /**
     * insert data to externaldata_queue table.
     * @param string $accountNumber
     * @param int $objectRegisterId
     * @param string $dataToProcess
     * @param string $status
     * @param string|null $start
     * @param string|null $end
     * @param string $message
     * @param string $modification
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function insert(
        string $accountNumber,
        int $companyId,
        int $objectRegisterId,
        int $webhookObjectRegId,
        string $dataToProcess,
        string $status = self::WB_QUEUE_STATUS_TYP_NEW,
        string $start = null,
        string $end = null,
        string $message = "",
        string $modification = ""
    ): bool {
        try {
            $this->logger->info('insert', [__METHOD__, __LINE__]);
            $timeStamp = $this->getCurrentTimeStamp();
            $sql = '
                    insert
                    into `externaldata_queue`(account_id,
                                       company_id,
                                       objectregister_id,
                                       webhook_objectregister_id,
                                       data_to_process,
                                       start,
                                       end,
                                       status,
                                       message,
                                       created,
                                       updated,
                                       modification)
                    values (:account_id,
                            :company_id,
                            :objectregister_id,
                            :webhook_objectregister_id,
                            :data_to_process,
                            :start,
                            :end,
                            :status,
                            :message,
                            :created,
                            :updated,
                            :modification)';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':account_id', $accountNumber);
            $stmt->bindParam(':company_id', $companyId);
            $stmt->bindParam(':objectregister_id', $objectRegisterId);
            $stmt->bindParam(':webhook_objectregister_id', $webhookObjectRegId);
            $stmt->bindParam(':data_to_process', $dataToProcess);
            $stmt->bindParam(':start', $start);
            $stmt->bindParam(':end', $end);
            $stmt->bindParam(':status', $status);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':created', $timeStamp);
            $stmt->bindParam(':updated', $timeStamp);
            $stmt->bindParam(':modification', $modification);
            return $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, $e->getCode()]);
            return false;
        }
    }

    /**
     * getCurrentTimeStamp helper function to get current time stamp
     * @return string
     * @author Pradeep
     */
    private function getCurrentTimeStamp(): string
    {
        $date = date('Y-m-d H:i:s');

        // date function could return 'false|string' ,
        // hence return an empty string in case of 'false'.
        if (is_bool($date) && !$date) {
            return '';
        }
        return $date;
    }

    /**
     * getNextJobToProcess fetches the next row with status as 'new' to process the cronJob.
     * @return array|null
     * @author Pradeep
     */
    public function getNextJobToProcess(): ?array
    {
        $result = [];
        $statusTypeNew = self::WB_QUEUE_STATUS_TYP_NEW;
        try {
            $sql = '
                    SELECT *
                    FROM `externaldata_queue` 
                    WHERE status = :job_status_type
                    ORDER BY id ASC LIMIT 1
                   ';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':job_status_type', $statusTypeNew, ParameterType::STRING);
            $stmt->executeStatement();
            $rowCount = $stmt->rowCount();
            $rows = $stmt->fetchAll();
            if ($rowCount > 0 && isset($rows[ 0 ])) {
                $result = $rows[ 0 ];
            }
            return $result;
        } catch (Exception|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, $e->getCode()]);
            return null;
        }
    }

    /**
     * updateJobStatus update the status of the row.
     * - Once the cron job has started to process the data row is updated to -> working.
     * - Once the cron job is completed , the status is changed to -> done
     * - The status for the `externaldata_queue` and status of the object_register is the same.
     * @param int $id
     * @param string $status
     * @return bool
     * @author Pradeep
     */
    public function updateJobStatus(
        int $id,
        string $status,
        string $message = null
    ): bool {

        try {
            if ($id <= 0 || !$this->isValidStatus($status)) {
                return false;
            }
            $job = $this->get($id);
            if (!isset($job[ 'account_id' ], $job[ 'data_to_process' ], $job[ 'objectregister_id' ])) {
                throw new InvalidArgumentException('failed to fetch account_id or data_to_process');
            }
            // Check the objectregister_Id of the chunnk.
            if (empty($job[ 'objectregister_id' ])) {
                throw new InvalidArgumentException(' objectregister_id cannot be empty ');
            }
            // update the start time for the job.
            if ($status === self::WB_QUEUE_STATUS_TYP_LOCKED) {
                $start = $this->getCurrentTimeStamp();
                $end = $job[ 'end' ];
            }
            // update the end time for the job.
            if ($status === self::WB_QUEUE_STATUS_TYP_ERROR
                || $status === self::WB_QUEUE_STATUS_TYP_DONE
            ) {
                $start = $job[ 'start' ];
                $end = $this->getCurrentTimeStamp();
            }
            // update the ObjectRegister status.
            // the same status value is updated to the object_register Id as well.
            $uniqueObjectTypeId = $this->uniqueObjectTypeRepository->getUniqueObjectTypeId($this->uniqueObjectTypeRepository::WBHOOKQUEUE);
            $statusDefId = $this->statusdefRepository->getStatusdefId($status);
            $this->logger->info('update webhookQueue Status ' . json_encode([
                    'statusDefId' => $statusDefId,
                    'uniqueObjectTypeId' => $uniqueObjectTypeId,
                ], JSON_THROW_ON_ERROR));
            if ($statusDefId === null || $statusDefId === 0) {
                throw new InvalidArgumentException('failed to fetch statusdefId for ' . $status);
            }
            if (!$this->objectRegisterRepository->update(
                $job[ 'objectregister_id' ], $uniqueObjectTypeId, $statusDefId)) {
                throw new RuntimeException('failed to update status of object_register_id to ' . $this->statusdefRepository::ACTIVE);
            }

            // Update the status message.
            $statusMessage = !empty($message) ? $message : $job[ 'message' ];

            // update the Job row, after everything is updated successfully.
            return $this->update(
                $id, $job[ 'account_id' ], $job[ 'webhook_objectregister_id' ],
                $job[ 'data_to_process' ], $start, $end, $status, $statusMessage);

        } catch (InvalidArgumentException|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * isValidStatus checks whether parameter is valid status or not.
     * @param string $status
     * @return bool
     * @author Pradeep
     */
    private function isValidStatus(string $status): bool
    {
        $supportedStatusEntries = $this->getStatusEntries();
        return in_array($status, $supportedStatusEntries, true);
    }

    /**
     * getStatusEntries list of valid status entries
     * @return string[]
     * @author Pradeep
     */
    public function getStatusEntries(): array
    {
        return [
            self::WB_QUEUE_STATUS_TYP_NEW,
            self::WB_QUEUE_STATUS_TYP_DONE,
            self::WB_QUEUE_STATUS_TYP_ERROR,
            self::WB_QUEUE_STATUS_TYP_LOCKED,
        ];
    }

    /**
     * get fetches the entry from `externaldata_queue` table for specified Id.
     * @param int $id
     * @return array|null
     * @author Pradeep
     */
    public function get(int $id): ?array
    {
        $result = [];
        try {
            $sql = 'SELECT * FROM `externaldata_queue` WHERE id = ?';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(1, $id, ParameterType::INTEGER);
            $status = $stmt->executeStatement();
            $rowCount = $stmt->rowCount();
            $rows = $stmt->fetchAll();
            if ($rowCount > 0 && isset($rows[ 0 ])) {
                $result = $rows[ 0 ];
            }
            return $result;
        } catch (Exception|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, $e->getCode()]);
            return null;
        }
    }

    /**
     * updates the row for the table '`externaldata_queue`'.
     * @param int $id
     * @param string $accountId
     * @param int $webhookObjRegId
     * @param string $dataToProcess
     * @param string|null $start
     * @param string|null $end
     * @param string $status
     * @param string|null $message
     * @return bool
     * @author Pradeep
     */
    public function update(
        int $id,
        string $accountId,
        int $webhookObjRegId,
        string $dataToProcess,
        string $start = null,
        string $end = null,
        string $status,
        string $message = null
    ): bool {

        try {
            $timeStamp = $this->getCurrentTimeStamp();
            $sql = '
             UPDATE `externaldata_queue`
             SET 
                 `account_id`= :account_id,
                 `webhook_objectregister_id`= :webhook_objreg_id,
                 `data_to_process`= :data_to_process,
                 `start`=:start,
                 `end`=:end,
                 `status`=:status,
                 `message`=:message,
                 `updated`=:updated,
                 `modification`=:modification 
             WHERE `id`=:id 
                ';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':account_id', $accountId);
            $stmt->bindParam(':webhook_objreg_id', $webhookObjRegId);
            $stmt->bindParam(':data_to_process', $dataToProcess);
            $stmt->bindParam(':start', $start);
            $stmt->bindParam(':end', $end);
            $stmt->bindParam(':status', $status);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':updated', $timeStamp);
            $stmt->bindParam(':modification', $modification);
            return $stmt->executeStatement();
        } catch (\Doctrine\DBAL\Driver\Exception|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, $e->getCode()]);
            return false;
        }
    }

}
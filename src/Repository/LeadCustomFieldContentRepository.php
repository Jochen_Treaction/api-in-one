<?php


namespace App\Repository;


use App\Types\WebhookWizardTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LeadCustomFieldContentRepository
{

    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private $contactRepository;
    /**
     * @var DataTypesRepository
     * @author Pradeep
     */
    private $dataTypesRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        ContactRepository $contactRepository,
        DataTypesRepository $dataTypesRepository
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
        $this->contactRepository = $contactRepository;
        $this->dataTypesRepository = $dataTypesRepository;
    }

    /**
     * @param int $oldLeadId
     * @param int $newLeadId
     * @return bool|null
     * @author Pradeep
     */
    public function updateLeadColumn(int $oldLeadId, int $newLeadId): ?bool
    {
        $status = false;
        try {
            if ($oldLeadId <= 0 || $newLeadId <= 0) {
                throw new RuntimeException('Invalid LeadId provided');
            }
            $sql = 'update lead_customfield_content SET leads_id=:new_lead_id WHERE leads_id=:old_lead_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':new_lead_id', $newLeadId, ParameterType::INTEGER);
            $stmt->bindValue(':old_lead_id', $oldLeadId, ParameterType::INTEGER);
            $status = $stmt->execute();
            return $status;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param array $leadCustomFieldContents
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function multiInsert(array $leadCustomFieldContents) :bool
    {
        $userId = 999999;
        $values = [];
        if (empty($leadCustomFieldContents)) {
            return false;
        }
        $stmt = 'INSERT INTO lead_customfield_content(leads_id, customfields_id, lead_customfield_content,
                                     hash_lead_customfield_content, created_by, updated_by)';
        foreach ($leadCustomFieldContents as $leadCustomFieldContent) {
            $values[] = '(' .
                        $leadCustomFieldContent[ 'lead_id' ] . ',' .
                        $leadCustomFieldContent[ 'customfields_id' ] . ',' .
                        '"' . $leadCustomFieldContent[ 'lead_customfield_content' ] . '"' . "," .
                        '"' . $leadCustomFieldContent[ 'hash_lead_customfield_content' ] . '"' . "," .
                        $userId . "," .
                        $userId . ")";

        }
        $query = $stmt . "VALUES " . implode(',', $values);
        $status = $this->conn->exec($query);
        return $status > 0;
    }

    /**
     * @param int $leadId
     * @param int $customFieldId
     * @param string $leadCustomFieldContent
     * @param string $hashLeadCustomFieldContent
     * @return int|null
     * @author Pradeep
     */
    public function insert(
        int $leadId,
        int $customFieldId,
        string $leadCustomFieldContent,
        string $hashLeadCustomFieldContent = ''
    ): ?int {
        $id = null;
        $userId = 999999;
        try {
            // lead custom field could be empty value.
            if ($leadId <= 0 || $customFieldId <= 0 /*|| empty($leadCustomFieldContent)*/) {
                throw new RuntimeException('Invalid LeadId Or CustomFieldId or CustomFieldContent provided');
            }
            $sql = 'INSERT INTO lead_customfield_content(leads_id, customfields_id, lead_customfield_content,
                                     hash_lead_customfield_content, created_by, updated_by)
                    VALUES (?, ?, ?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('1', $leadId);
            $stmt->bindParam('2', $customFieldId);
            $stmt->bindParam('3', $leadCustomFieldContent);
            $stmt->bindParam('4', $hashLeadCustomFieldContent);
            $stmt->bindParam('5', $userId);
            $stmt->bindParam('6', $userId);
            $result = $stmt->execute();
            if ($result) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (\Doctrine\DBAL\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $id;
        }
    }

    public function update(): ?bool
    {
        return false;
    }

    public function delete(): ?bool
    {
        return false;
    }

    /**
     * @param int $leadId
     * @return array
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function getFieldsByLeadId(int $leadId): array
    {
        if (empty($leadId)) {
            return [];
        }
        $sql = ' SELECT * FROM lead_customfield_content WHERE leads_id = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam('1', $leadId, ParameterType::INTEGER);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt->fetchAll();
        }
        return [];
    }

    /**
     * @param int $customFieldId
     * @return array|null
     * @author Pradeep
     */
    public function getLatestEntry(int $customFieldId, int $leadId): ?array
    {
        $leadCustomFieldContent = [];
        try {
            if ($customFieldId <= 0) {
                throw new RuntimeException('Invalid Custom Field Id');
            }
            $sql = 'SELECT *
                    FROM lead_customfield_content
                    WHERE customfields_id = ? AND leads_id = ?
                    order by id desc
                    limit 1';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('1', $customFieldId, ParameterType::INTEGER);
            $stmt->bindParam('2', $leadId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result[ 0 ]) && $stmt->rowCount() > 0) {
                $leadCustomFieldContent = $result[ 0 ];
            }
            return $leadCustomFieldContent;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $leadCustomFieldContent;
        }
    }

    /**
     * @param array $contacts
     * @param array $standardCustomFieldsForAccount
     * @throws \Doctrine\DBAL\Exception
     * @throws \JsonException
     * @author Pradeep
     * @internal  helper function for modifying the standard customfields for multi insert.
     * @depreacted since the ecommercefields (standardCustomfields ) are merged into leads table.
     */
    public function insertCustomFieldsFromLeadSync(array $contacts, array $standardCustomFieldsForAccount):bool
    {
        $leadCustomFieldContent = [];
        $customFieldDetails = [];

        // Convert Standard customfield to key value pair
        foreach($standardCustomFieldsForAccount as $field) {
            if(!isset($field['customfield'])) {
                continue;
            }
            $customFieldDetails[$field['customfield']] = $field;
        }

        // Search each fields in the contact for the custom field.
        foreach ($contacts as $contact) {
            if (!isset($contact[ 'standard' ])) {
                continue;
            }
            $field = [];
            foreach ($contact[ 'standard' ] as $k => $value1) {
                foreach($value1 as $key=> $value) {
                    if (empty($key) || in_array($key, ['required', 'datatype', 'regex'])) {
                        continue;
                    }
                    if($key === 'objectregister_id') {
                        $contactDetails = $this->contactRepository->getContactByObjectRegisterId($value);
                        if(empty($contactDetails) || !isset($contactDetails['id'])) {
                            continue;
                        }
                        $field[ 'lead_id' ] = $contactDetails['id'] ;
                        break;
                    }
                    if(isset( $customFieldDetails[$key])) {
                        $field[ 'customfields_id' ] = $customFieldDetails[$key]['id'];
                        $field[ 'lead_customfield_content' ] = $value;
                        $field[ 'hash_lead_customfield_content' ] = "";

                       // $leadCustomFieldContent[] = $field;
                    }
                    if(isset($field['lead_id'], $field['customfields_id'])) {
                        $leadCustomFieldContent[] = $field;
                    }
                }
            }
         //   $leadCustomFieldContent[] = $field;
        }
        $this->logger->info('insertCustomFieldsFromLeadSync ' . json_encode($leadCustomFieldContent, JSON_THROW_ON_ERROR));
        return  $this->multiInsert($leadCustomFieldContent);
    }

}
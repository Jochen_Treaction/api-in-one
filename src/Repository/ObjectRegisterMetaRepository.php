<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class ObjectRegisterMetaRepository
{

    public const MetaKeyProject = 'project';
    public const MetaKeyWebHook = 'webhook';
    public const MetaKeyPage = 'page';
    public const MetaKeyAPIkey = 'account_apikey';
    public const MetaKeyFraudSettings = 'fraud_settings';


    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    public function insert(int $objectregisterId, string $metaKey, string $metaValue, int $created_by=999999, $updated_by=999999): ?int
    {
        $id = null;
        $userId = 999999;
        $this->logger->info('OBJ_REG_META:INSERT', [$objectregisterId, $metaKey, $metaValue, __METHOD__, __LINE__]);
        try {
            if ($objectregisterId <= 0 || empty($metaKey) || empty($metaValue)) {
                throw new RuntimeException('Invalid Parameters provided' . json_encode([
                        'objId' => $objectregisterId,
                        '$metaKey' => $metaKey,
                        'metavlue' => $metaValue,
                    ]));
            }
            $sql = 'INSERT INTO object_register_meta(object_register_id, meta_key, meta_value, created_by, updated_by) 
                    VALUE(:objectregister_id, :meta_key, :meta_value, :created_by,:updated_by)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectregisterId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $metaKey, ParameterType::STRING);
            $stmt->bindParam(':meta_value', $metaValue, ParameterType::STRING);
            $stmt->bindParam(':created_by', $created_by, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $updated_by, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (DBALException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $id;
        }
    }

    /**
     * Update object_register_meta table entries based on object_register_id and meta_key.
     * @param int $objectregisterId
     * @param string $metaKey
     * @param string $metaValue
     * @return bool
     * @author Pradeep
     */
    public function update(int $objectRegisterId, string $metaKey, string $metaValue): bool
    {
        $this->logger->info('OBJ_REG_META:UPDATE', [$objectRegisterId, $metaKey, $metaValue, __METHOD__, __LINE__]);
        if ($objectRegisterId <= 0 || empty($metaKey)) {
            return false;
        }
        $sql = "
                UPDATE 
                    object_register_meta 
                SET meta_value = :meta_value 
                WHERE meta_key =:meta_key AND object_register_id =:object_register_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':object_register_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $metaKey);
            $stmt->bindParam(':meta_value', $metaValue);
            return $stmt->execute();
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $objectRegisterId
     * @param string $metaKey
     * @return array|null
     * @author Pradeep
     */
    public function getMetaValue(int $objectRegisterId, string $metaKey): ?array
    {
        $metaValue = null;
        $this->logger->info("this is object register get ",["ojbID"=>$objectRegisterId,"metaKey"=>$metaKey]);
        try {
            if ($objectRegisterId <= 0 || empty($metaKey)) {
                throw new RuntimeException('Invalid ObjectRegisterId or MetaKey provided');
            }
            $sql = 'SELECT meta_value
                    FROM object_register_meta
                    WHERE object_register_id = :object_register_id AND meta_key = :meta_key ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':object_register_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $metaKey, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $metaValueAsJson = $result[ 0 ][ 'meta_value' ];
                $metaValue = json_decode($metaValueAsJson, true, 512,
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
            }

            // ObjectRegister meta can hold any type of data,
            // So the return type has to be mixed , since our php is not update to 8.0 the return type is of this function.
            // is forced to send the return type to array.
            if (!empty($metaValue) && !is_array($metaValue)) {
                $metaValue = [$metaValue];
            }
            $this->logger->info("ObjRegMetaValue", [$metaValue, __METHOD__, __LINE__]);
            return $metaValue;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $companyId
     * @return string
     * @author Pradeep
     */
    public function getAccountAPIKey(int $companyId): ?string
    {
        $apikey = null;
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid ObjectRegisterId');
            }
            $sql = 'SELECT object_register_meta.meta_value as apikey FROM object_register_meta
                        JOIN objectregister o on object_register_meta.object_register_id = o.id
                        JOIN company c on o.id = c.objectregister_id
                    WHERE c.id=:company_id AND object_register_meta.meta_key LIKE "account_apikey"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $apikey = $result[ 0 ][ 'apikey' ];
            }
            return $apikey;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $apikey;
        }
    }

    /**
     * Searches metakey and metavalue for active (by default) Campaign or Webhook.
     * @param string $metaKey
     * @param string $metaValue
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function search(string $metaKey, string $metaValue = '', string $status = 'active'): array
    {

        $searchResult = [];
        $sql = "
                SELECT orm.*
                FROM `object_register_meta` orm
                JOIN objectregister o ON o.id = orm.object_register_id
                JOIN statusdef s ON s.id = o.statusdef_id
                WHERE orm.meta_key = :meta_key and orm.`meta_value` = :meta_value and s.value= :status_active; 
            ";
        if (empty($metaValue)) {
            $sql = "
                SELECT orm.* 
                FROM `object_register_meta` orm
                JOIN objectregister o ON o.id = orm.object_register_id
                JOIN statusdef s ON s.id = o.statusdef_id
                WHERE orm.meta_key = :meta_key and s.value= :status_active; 
            ";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':meta_key', $metaKey);
        $stmt->bindParam(':status_active', $status);
        if (!empty($metaValue)) {
            $stmt->bindParam(':meta_value', $metaValue);
        }
        $status = $stmt->execute();
        if (!$status) {
            throw new RuntimeException("failed to execute SQL statement");
        }
        $rowCount = $stmt->rowCount();
        $this->logger->info('SEARCH RESULT ROW COUNT', [$rowCount, __METHOD__, __LINE__]);
        if ($rowCount <= 0) {
            return [];
        }
        $result = $stmt->fetchAll();
        $this->logger->info('SEARCH RESULT', [$result, __METHOD__, __LINE__]);
        if (!empty($result) && isset($result[ 0 ][ 'id' ])) {
            $searchResult = $result[ 0 ];
        }
        return $searchResult;
    }


    public function getMultipleMetaValues(int $objectRegisterId): ?array
    {
        $multiMetaValues = '';
        if ($objectRegisterId <= 0) {
            throw new RuntimeException("Invalid ObjectRegisterId provided");
        }

        $sql = "SELECT * FROM object_register_meta WHERE object_register_id = :object_register_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':object_register_id', $objectRegisterId, ParameterType::INTEGER);
        $status = $stmt->execute();
        if (!$status) {
            throw new RuntimeException("failed to execute SQL statement");
        }
        $rowCount = $stmt->rowCount();
        $this->logger->info('SEARCH RESULT ROW COUNT getMultipleMetaValues', [$rowCount, __METHOD__, __LINE__]);
        if ($rowCount <= 0) {
            return [];
        }
        $result = $stmt->fetchAll();
        $this->logger->info('SEARCH RESULT getMultipleMetaValues', [$result, __METHOD__, __LINE__]);
        if (!empty($result) && isset($result[ 0 ][ 'id' ])) {
            $multiMetaValues = $result;
        }
        return $multiMetaValues;
    }

    /**
     * @param int $objRegId
     * @param string $jtMetaKey
     * @return bool
     * @author Pradeep
     */
    public function delete(int $objRegId, string $jtMetaKey): bool
    {
        if ($objRegId <= 0 || empty($jtMetaKey)) {
            throw new RuntimeException("Invalid ObjectRegisterId provided");
        }
        try {
            $sql = "DELETE FROM `object_register_meta` WHERE object_register_id=:object_register_id AND meta_key =:meta_key";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':object_register_id', $objRegId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $jtMetaKey);
            return $stmt->execute();
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

    }

}
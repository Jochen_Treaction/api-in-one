<?php


namespace App\Types;

class ContactFieldTypes
{
    public const LastName = 'last_name';
    public const FirstName = 'first_name';
    public const Email = 'email';
    public const Salutation = 'salutation';
    public const ReferenceNo = 'reference_no';
    public const AccountName = 'account_name';
    public const Street = 'street';
    public const HouseNumber = 'house_number';
    public const PostalCode = 'postal_code';
    public const City = 'city';
    public const Firma = 'firma';
    public const Country = 'country';
    public const Phone = 'phone';
    public const URL = 'url';
    public const Company = 'company';
    public const TrafficSource = 'traffic_source';
    public const UTMParameters = 'utm_parameters';
    public const TargetGroup = 'target_group';
    public const AffiliateID = 'affiliate_id';
    public const AffiliateSubID = 'affiliate_sub_id';
    public const IP = 'ip';

    public const LeadRefernece = 'lead_reference';
    public const SplitVersion = 'split_version';

    public const LastOrderNo = 'last_order_no';
    public const LastOrderDate = 'last_order_date';
    public const FirstOrderDate = 'first_order_date';
    public const TotalOrderNetValue = 'total_order_net_value';
    public const LastYearOrderNetValue = 'last_year_order_net_value';
    public const LastOrderNetValue = 'last_order_net_value';
    public const SmartTags = 'smart_tags';
    public const ProductAttributes = 'product_attributes';
    public const ShopUrl = 'shop_url';
    public const SubShops = 'sub_shops';
    public const Group = 'customer_group';
    public const TotalNumberOfOrders = 'total_number_of_orders';

    public const ReferrerID = 'referrer_id';
    public const BirthDay = 'birthday';

    public const DeviceType = 'device_type';
    public const DeviceBrowser = 'device_browser';
    public const DeviceOs = 'device_os';
    public const DeviceOsVersion = 'device_os_version';
    public const DeviceBrowserVersion = 'device_browser_version';
    public const Device = 'device';

    public const DOIIp = "doi_ip";
    public const DOITimestamp = "doi_timestamp";
    public const OptinTimeStamp = "optin_timestamp";
    public const OptinIp = "optin_ip";
    public const Permission = 'permission';
    public const Webpage = "webpage";
    public const Language = 'language';

    // @WEB-5982 SHOPWARE:5 New Technical Field for Contact - lastSync.
    public const LastSync = 'last_sync'; /*Time stamp of Contact synchronized with eMIO.*/

}
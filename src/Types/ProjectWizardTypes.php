<?php


namespace App\Types;


class ProjectWizardTypes
{

    private $type;
    private $typeDesc;
    public $emailPattern;
    public $uniqueObjectName;


    public function __construct()
    {
        $this->uniqueObjectName = 'project';

        $this->emailPattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9äöüß]+(?:-[a-z0-9äöüß]+)*\.){1,126}){1,}(?:(?:[a-zäöüß][a-z0-9äöüß]*)|(?:(?:xn--)[a-z0-9äöüß]+))(?:-[a-z0-9äöüß]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        $this->type = [

            "relation" => [
                "company_name" => '',
                "company_id" => '',
                "project_object_unique_id" => '',
                "project_objectregister_id" => '',
            ],

            "settings" => [
                "description" => '',
                "url_option" => '', // folder, sub-domain, delegate-domain
                "beginDate" => '',
                "expireDate" => '',
            ],

            "advancedsettings" => [
                "commercial_relationships" => '', // b2b, b2c, both
                "register_multiple_times" => '', // true, false
                "country" => '', // e.g. Germany
                "fraud_prevention_level" => '', // low, medium, high
                "forward_lead_by_email" => '',
                "bool_redirect_after_registration" => '',
                "redirect_after_registration" => '',
            ],

            "customfieldlist" => '',

            "tracker" => [
                "goolge_apikey" => '',
                "facebook_pixel" => '',
                "webgains_id" => '',
                "outbrain_id" => '',
                "bool_outbrain_track_after_30_seconds" => '',
            ],
        ];


        $this->typeDesc = [

            "relation" => [
                "company_name" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "company_id" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[\d]*$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => null,
                ],
                "project_object_unique_id" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[\d]*$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => null,
                ],
                "project_objectregister_id" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[\d]*$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => null,
                ],
            ],

            "settings" => [
                "description" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "url_option" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^((folder)|(sub-domain)|(custom-domain)|(delegate-domain)){1}$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => 'folder',
                ],
                "beginDate" => [ //e.g. 2020-01-10 10:20:50 => TODO: adapt to disallow days > 29 in February
                    "phpdatatype" => "string",
                    "pattern" => "/^(2[0-9]{3})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) (0[0-9]|1[0-9]|2[0123])\:([012345][0-9])\:([012345][0-9])$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "expireDate" => [ //e.g. 2020-01-10 10:20:50 => TODO: adapt to disallow days > 29 in February
                    "phpdatatype" => "string",
                    "pattern" => "/^(2[0-9]{3})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) (0[0-9]|1[0-9]|2[0123])\:([012345][0-9])\:([012345][0-9])$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
            ],


            "advancedsettings" => [
                "commercial_relationships" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^((b2b)|(b2c)|(both)){1}$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => 'both',
                ],

                "register_multiple_times" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[0-1]{1}$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => true,
                ], // true, false

                "country" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[a-z]*$/i",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => 'Germany',
                ], // e.g. Germany

                "fraud_prevention_level" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^((low)|(medium)|(high)){1}$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => 'low',
                ], // low, medium, high

                "forward_lead_by_email" => [
                    "phpdatatype" => "string",
                    "pattern" => $this->emailPattern, // lists of email must pass check:  $passed = (preg_grep($pattern, explode(',' $stringOfCommaSeparatedEmails), PREG_GREP_INVERT) === [])
                    "filterValidate" => null, // would fail for emails with umlaut
                    "filterSanitize" => null, // would fail for emails with umlaut
                    "defaultvalue" => null,
                ],

                "bool_redirect_after_registration" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[0-1]{1}$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => false,
                ],

                "redirect_after_registration" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^(?:(?:(?:https?):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/",
                    "filterValidate" => FILTER_VALIDATE_URL,
                    "filterSanitize" => FILTER_SANITIZE_URL,
                    "defaultvalue" => null,
                ],
            ],

            "customfieldlist" => [
                "phpdatatype" => "array",
                "pattern" => null,
                "filterValidate" => null,
                "filterSanitize" => null,
                "defaultvalue" => null,
            ],

            "tracker" => [
                "goolge_apikey" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "facebook_pixel" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "webgains_id" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "outbrain_id" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[\w,\. -]*$/",
                    "filterValidate" => null,
                    "filterSanitize" => FILTER_SANITIZE_STRING,
                    "defaultvalue" => null,
                ],
                "bool_outbrain_track_after_30_seconds" => [
                    "phpdatatype" => "int",
                    "pattern" => "/^[0-1]{1}$/",
                    "filterValidate" => FILTER_VALIDATE_INT,
                    "filterSanitize" => FILTER_SANITIZE_NUMBER_INT,
                    "defaultvalue" => null,
                ],
            ],

        ];
    }


    /**
     * @return string
     * @author jsr
     */
    public function getJson(): string
    {
        return json_encode($this->type);
    }


    /**
     * @return array
     * @author jsr
     */
    public function getArray(): array
    {
        return $this->type;
    }


    /**
     * @return array [
     *
     * "relation" => [
     * "company_name" => '',
     * "company_id" => '',
     * "project_object_unique_id" => '',
     * ],
     *
     *
     * "settings" => [
     * "description" => '',
     * "beginDate" => '',
     * "expireDate" => '',
     * ],
     *
     * "advancedsettings" => [
     * "commercial_relationships" => 'both',
     * "register_multiple_times" => 'true',
     * "country" => 'Germany',
     * "fraud_prevention_level" => 'low',
     * "forward_lead_by_email" => '',
     * "bool_redirect_after_registration" => false,
     * "redirect_after_registration" => '',
     * ],
     *
     * "customfieldlist" => '',
     *
     * "tracker" => [
     * "goolge_apikey" => '',
     * "facebook_pixel" => '',
     * "webgains_id" => '',
     * "outbrain_id" => '',
     * "bool_outbrain_track_after_30_seconds" => '',
     * ],
     * ]
     * @author jsr
     *
     */
    public function getArrayWithDefaultValues(): array
    {
        // requirement: {"commercial_relationships":"both","register_multiple_times":true,"country":"Germany","fraud_prevention_level":"low","redirect_after_registration":false}
        $default = $this->type;

        $default["settings"]["url_options"] = 'folder';
        $default["advancedsettings"]["commercial_relationships"] = 'both';
        $default["advancedsettings"]["register_multiple_times"] = true;
        $default["advancedsettings"]["country"] = 'Germany';
        $default["advancedsettings"]["fraud_prevention_level"] = 'low';
        $default["advancedsettings"]["bool_redirect_after_registration"] = false;
        return $default;
    }


    /**
     * @return string
     * @author jsr
     */
    public function getJsonWithDefaultValues(): string
    {
        return json_encode($this->getArrayWithDefaultValues());
    }


    /**
     * @return string
     */
    public function getJsonDecription():string
    {
        return json_encode($this->typeDesc);
    }


    /**
     * @return array
     */
    public function getArrayDecription():array
    {
        return $this->typeDesc;
    }


}

<?php


namespace App\Types;

class ProcesshookTypes
{
    public const SEND_LEAD_NOTIFICATION = 'send_lead_notification';
    public const START_MARKETING_AUTOMATION = 'start_marketing_automation';
    public const SEND_DOI_EMAIL = 'send_double_opt_in_mail';
    public const CALL_CONTACT_EVENT = 'call_contact_event';
    public const SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT = 'send_double_opt_in_mail_or_call_contact_event';
    public const SET_SMART_TAG = 'set_smart_tag';
    public const REMOVE_SMART_TAG = 'remove_smart_tag';
    public const SHOW_POPUP = 'show_popup';
    public const WORKFLOW = 'workflow';


    public function getDefaultNewsLetterPHooks(): array
    {
        return [
            self::SEND_LEAD_NOTIFICATION
        ];
    }

    public function getDefaultECommercePHooks(): array
    {
        return [ ];
    }

    public function getDefaultPHookForWebhook(string $webhookName): array
    {
        if($webhookName === WebhookWizardTypes::ECOMMERCEHOOK) {
            return $this->getDefaultECommercePHooks();
        }
        if($webhookName === WebhookWizardTypes::NEWSLETTERHOOK) {
            return $this->getDefaultNewsLetterPHooks();
        }
        return [];
    }

    /**
     * @param string $phType
     * @return string|void
     * @author Pradeep
     */
    public function getName(string $phType){
        if($phType === self::SEND_LEAD_NOTIFICATION) {
            return 'Neuer Lead';
        }
    }

}
<?php


namespace App\Types;


class MessageParameterTypes
{
	/**
	 * @var string
	 */
	public string $firstName = '';

	/**
	 * @var string
	 */
	public string $lastName = '';

	/**
	 * @var string
	 */
	public string $email = '';

	/**
	 * @var string
	 */
	public string $apiKey = '';

	/**
	 * @var string
	 */
	public string $accountNo = '';

	/**
	 * @var string
	 */
	public string $activationLink = '';

	/**
	 * @var string
	 */
	public string $customContent = '';


	public function __construct()
	{
	}


	public function getMessageParameterTypesArray():array
	{
		return (array) $this;
	}
}

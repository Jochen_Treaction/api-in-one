<?php


namespace App\Types\APITypes;


class AccountApiTypes
{
    //payload variables
    private $CreatePayloadtype;
    private $ActivatePayloadtype;
    private $CreatePayloadtypeDesc;
    private $ActivatePayloadtypeDesc;
    private $ApikeyCreatePayloadType;
    private $ApikeyCreatePayloadTypeDesc;
    //pattern constants
    private const StringPattern = '/^[\w,\. -]*$/';
    private const EmailPattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9äöüß]+(?:-[a-z0-9äöüß]+)*\.){1,126}){1,}(?:(?:[a-zäöüß][a-z0-9äöüß]*)|(?:(?:xn--)[a-z0-9äöüß]+))(?:-[a-z0-9äöüß]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    //database variable constants
    private const saluation = 'Salutation';
    private const firstName = 'FirstName';
    private const email = 'Email';
    private const permission = 'permission';
    private const accountNumber = 'accountNo';



    public function __construct()
    {

        $this->CreatePayloadtype = [
            self::saluation => '',
            self::firstName => '',
            self::email => '',
            self::permission => '',
        ];
        $this->ActivatePayloadtype = [
            self::accountNumber=>'',
            self::email=>'',
        ];

        $this->ApikeyCreatePayloadType = [
            self::accountNumber=>'',
        ];
        //todo:Change permission type to bool
        $this->CreatePayloadtypeDesc = [
            [
                'salutation' => '',
                'required' => 'true',
                'datatype' => 'String',
                'regex' => self::StringPattern,
            ],
            [
                'last-name' => '',
                'required' => 'true',
                'datatype' => 'String',
                'regex' =>  self::StringPattern,

            ],
            [
                'email' => '',
                'required' => 'true',
                'datatype' => 'email',
                'regex' => self::EmailPattern,

            ],
            [
                'permission' => '',
                'required' => 'true',
                'datatype' => 'Boolean',
                'regex' =>  self::StringPattern,

            ],
        ];
        $this->ActivatePayloadtypeDesc = [
            [
                'account_number'=>'',
                'required' => 'true',
                'datatype' => 'String',
                'regex' => self::StringPattern,
            ],
            [
                "email"=>'',
                'required' => 'true',
                'datatype' => 'Text',
                'regex' => self::EmailPattern,
            ],
        ];
        $this->ApikeyCreatePayloadTypeDesc = [
            [
                'account_number'=>'',
                'required' => 'true',
                'datatype' => 'String',
                'regex' => self::StringPattern,
            ],
        ];
    }

    /**
     * @return string
     * @author Aki
     */
    public function getCreatePayloadType(): string
    {
        return json_encode($this->CreatePayloadtype);
    }

    /**
     * @return array
     * @author Aki
     */
    public function getCreatePayloadTypeDesc():array
    {
        return $this->CreatePayloadtypeDesc;
    }

    /**
     * @return string
     * @author Aki
     */
    public function getActivatePayloadType(): string
    {
        return json_encode($this->CreatePayloadtype);
    }

    /**
     * @return array
     * @author Aki
     */
    public function getActivatePayloadTypeDesc():array
    {
        return $this->CreatePayloadtypeDesc;
    }

}
<?php


namespace App\Types\APITypes;


class SourceApiTypes
{
    //payload variables
    private $ListPayloadtype;
    private $ListPayloadtypeDesc;
    //pattern constants
    private const StringPattern = '/^[\w,\. -]*$/';
    private const EmailPattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9äöüß]+(?:-[a-z0-9äöüß]+)*\.){1,126}){1,}(?:(?:[a-zäöüß][a-z0-9äöüß]*)|(?:(?:xn--)[a-z0-9äöüß]+))(?:-[a-z0-9äöüß]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    //database variable constants
    private const apiKey = 'apikey';
    private const accountNumber = 'accountNo';

    public const BASE = "base";
    public const AccountNumber = "account_number";
    public const APIKEY = "apikey";
    public const ObjectRegisterId = "object_register_id";
    public const UUID = "uuid";

    public const HEADER_SYNC_MODE = 'SyncMode';
    public const HEADER_SYNC_MODE_ASYNC = 'Async';
    public const HEADER_SYNC_STATUS = 'QueueStatus';
    public const HEADER_SYNC_MODE_NONASYNC = 'Sync';
    public const HEADER_CHUNKID = 'ChunkId';
    public const HEADER_CHUNK_KEY = 'ChunkKey';
    public const HEADER_TOTAL_NUMBER_OF_CHUNKS = 'TotalNumberOfChunks';
    public const SOURCE = 'Source';
    public const JOTFORMS = 'JotForms';

    public function __construct()
    {
        $this->ListPayloadtype = [
            [
                self::accountNumber => '',
                self::apiKey => '',
            ],
        ];
        $this->ListPayloadtypeDesc = [
            [
                self::accountNumber => '',
                'required' => 'true',
                'datatype' => 'String',
                'regex' => self::StringPattern,
            ],
            [
                self::apiKey => '',
                'required' => 'true',
                'datatype' => 'String',
                'regex' => self::EmailPattern,
            ],
        ];

    }

}
<?php


namespace App\Types;


use Exception;
use JsonException;
use Monolog\Logger;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use App\Types\APITypes\SourceApiTypes;

class WebhookWizardTypes implements LoggerAwareInterface
{

    private $type;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public const ECOMMERCEHOOK = 'eCommerceHook';
    public const NEWSLETTERHOOK = 'NewsletterHook';

    //todo:take regex from the database

    public function __construct()
    {
        $this->type = [

            "relation" => [
                "company_name" => '',
                "company_id" => '',
                "project_id" => '',
                'permission' => ''
            ],

            "settings" => [
                "name" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[a-zA-Z0-9_]+$/",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
                "description" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
                "beginDate" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
                "expireDate" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
            ],

            "mapping" => [
                'newsletter' => [
                    'contact' => [
                        'standard' => [
                            [
                                ContactFieldTypes::Salutation => '',
                                "field_name" => ContactFieldTypes::Salutation,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Salutation,
                            ],
                            [
                                ContactFieldTypes::FirstName => '',
                                "field_name" => ContactFieldTypes::FirstName,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::FirstName,
                            ],
                            [
                                ContactFieldTypes::LastName => '',
                                "field_name" => ContactFieldTypes::LastName,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastName,
                            ],
                            [
                                ContactFieldTypes::Email => '',
                                "field_name" => ContactFieldTypes::Email,
                                'required' => 'true',
                                'datatype' => 'Email',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Email,
                            ],
                            [
                                ContactFieldTypes::Language => '',
                                'field_name' => ContactFieldTypes::Language,
                                'required' => '',
                                'datatype' => 'Text',
                                'regex' => '',
                            ],

                        ],
                    ],
                ],
                'eCommerce' => [
                    'contact' => [
                        'standard' => [
                            [
                                ContactFieldTypes::Salutation => '',
                                "field_name" => ContactFieldTypes::Salutation,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Salutation,
                            ],
                            [
                                ContactFieldTypes::FirstName => '',
                                "field_name" => ContactFieldTypes::FirstName,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::FirstName,
                            ],
                            [
                                ContactFieldTypes::LastName => '',
                                "field_name" => ContactFieldTypes::LastName,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastName,
                            ],
                            [
                                ContactFieldTypes::Email => '',
                                "field_name" => ContactFieldTypes::Email,
                                'required' => 'true',
                                'datatype' => 'Email',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Email,
                            ],
                            [
                                ContactFieldTypes::Street => '',
                                "field_name" => ContactFieldTypes::Street,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Street,
                            ],
                            [
                                ContactFieldTypes::HouseNumber => '',
                                "field_name" => ContactFieldTypes::HouseNumber,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::HouseNumber,
                            ],
                            [
                                ContactFieldTypes::PostalCode => '',
                                "field_name" => ContactFieldTypes::PostalCode,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::PostalCode,
                            ],
                            [
                                ContactFieldTypes::City => '',
                                "field_name" => ContactFieldTypes::City,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::City,
                            ],
                            [
                                ContactFieldTypes::Country => '',
                                "field_name" => ContactFieldTypes::Country,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Country,
                            ],
                            [
                                ContactFieldTypes::Phone => '',
                                "field_name" => ContactFieldTypes::Phone,
                                'required' => 'true',
                                'datatype' => 'Phone',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Phone,
                            ],
                            [
                                ContactFieldTypes::Permission => '',
                                "field_name" => ContactFieldTypes::Permission,
                                'required' => 'true',
                                'datatype' => 'Integer',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::Permission,
                            ],
                            [
                                ContactFieldTypes::LastOrderDate => '',
                                "field_name" => ContactFieldTypes::LastOrderDate,
                                'required' => 'true',
                                'datatype' => 'DateTime',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastOrderDate,
                            ],
                            [
                                ContactFieldTypes::LastOrderNo => '',
                                "field_name" => ContactFieldTypes::LastOrderNo,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastOrderNo,
                            ],
                            [
                                ContactFieldTypes::SmartTags => '',
                                "field_name" => ContactFieldTypes::SmartTags,
                                'required' => 'true',
                                'datatype' => 'List',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::SmartTags,
                            ],
                            [
                                ContactFieldTypes::ProductAttributes => '',
                                "field_name" => ContactFieldTypes::ProductAttributes,
                                'required' => 'true',
                                'datatype' => 'Text',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::ProductAttributes,
                            ],
                            [
                                ContactFieldTypes::TotalOrderNetValue => '',
                                "field_name" => ContactFieldTypes::TotalOrderNetValue,
                                'required' => 'true',
                                'datatype' => 'Decimal',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::TotalOrderNetValue,
                            ],
                            [
                                ContactFieldTypes::LastYearOrderNetValue => '',
                                "field_name" => ContactFieldTypes::LastYearOrderNetValue,
                                'required' => 'true',
                                'datatype' => 'Decimal',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastYearOrderNetValue,
                            ],
                            [
                                ContactFieldTypes::LastOrderNetValue => '',
                                "field_name" => ContactFieldTypes::LastOrderNetValue,
                                'required' => 'true',
                                'datatype' => 'Decimal',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::LastOrderNetValue,
                            ],
                            [
                                ContactFieldTypes::FirstOrderDate => '',
                                "field_name" => ContactFieldTypes::FirstOrderDate,
                                'required' => 'true',
                                'datatype' => 'DateTime',
                                'regex' => '',
                                'html_placeholder' => ContactFieldTypes::FirstOrderDate,
                            ],
                            [
                                ContactFieldTypes::Group => '',
                                'field_name' => ContactFieldTypes::Group,
                                'required' => '',
                                'datatype' => 'Text',
                                'regex' => '',
                            ],
                            [
                                ContactFieldTypes::SubShops => '',
                                'field_name' => ContactFieldTypes::SubShops,
                                'required' => '',
                                'datatype' => 'List',
                                'regex' => '',
                            ],
                            [
                                ContactFieldTypes::ShopUrl => '',
                                'field_name' => ContactFieldTypes::ShopUrl,
                                'required' => '',
                                'datatype' => 'Text',
                                'regex' => '',
                            ],
                            [
                                ContactFieldTypes::TotalNumberOfOrders => '',
                                'field_name' => ContactFieldTypes::TotalNumberOfOrders,
                                'required' => '',
                                'datatype' => 'Text',
                                'regex' => '',
                            ],
                            [
                                ContactFieldTypes::ProductAttributes => '',
                                'field_name' => ContactFieldTypes::ProductAttributes,
                                'required' => '',
                                'datatype' => 'List',
                                'regex' => '',
                            ],
                            [
                                ContactFieldTypes::Language => '',
                                'field_name' => ContactFieldTypes::Language,
                                'required' => '',
                                'datatype' => 'Text',
                                'regex' => '',
                            ],
                        ],
                    ],
                ],
            ],
            "additionalSettings" => [
                "objectregister_id" =>"",
                "accountNumber"=>"",
                "apiKey" =>"",
                "CampaignName"=>"",
            ],
        ];
    }

    /**
     * @param string $companyName
     * @param int $companyId
     * @param string $projectId
     * @return bool
     * @author Pradeep
     */
    public function initialize(string $companyName, int $companyId, string $projectId): bool
    {
        try {

            $this->type[ 'relation' ][ 'company_name' ] = $companyName;
            $this->type[ 'relation' ][ 'company_id' ] = $companyId;
            $this->type[ 'relation' ][ 'project_id' ] = $projectId;
            return true;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $webhookName
     * @return array
     * @author Pradeep
     */
    public function getDefaultConfigForeCommerceWebhook(string $webhookName, array $standardFields): ?array
    {
        try {
            $eCommerce [ 'relation' ] = $this->getRelation();
            $eCommerce [ 'settings' ] = $this->setSettings($webhookName);
            $eCommerce[ 'mapping' ] = base64_encode(json_encode($this->getMappingForeCommerceWebhook($standardFields),
                JSON_THROW_ON_ERROR));
            return $eCommerce;
        } catch (JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

    }

    private function getRelation(): ?array
    {
        return $this->type[ 'relation' ];
    }

    /**
     * @param string $webhookName
     * @param string $description
     * @param string $beginDate
     * @param string $expireDate
     * @return array|null
     * @author Pradeep
     */
    private function setSettings(
        string $webhookName,
        string $description = '',
        string $beginDate = '',
        string $expireDate = ''
    ): array {
        // TODO :: Update Filter validation with filter_var,  sanitization and pattern
        /*        $this->type[ 'settings' ][ 'name' ][ 'value' ] = preg_match($this->type[ 'settings' ][ 'name' ][ 'pattern' ],
                    $webhookName) ? $webhookName : '';*/
        try {
            $this->type[ 'settings' ][ 'name' ][ 'value' ] = $webhookName;
            $this->type[ 'settings' ][ 'description' ][ 'value' ] = $description;
            $this->type[ 'settings' ][ 'beginDate' ] = $beginDate;
            $this->type[ 'settings' ][ 'expireDate' ] = $expireDate;
            if($webhookName === self::ECOMMERCEHOOK) {
                // TODO:: change hardcoded permission to constant
                $this->type[ 'settings' ][ 'permission' ] = 5;
            }
            if($webhookName === self::NEWSLETTERHOOK) {
                // TODO:: change hardcoded permission to constant
                $this->type[ 'settings' ][ 'permission' ] = 1;
            }
            return $this->type[ 'settings' ];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getMappingForNewsLetterWebhook(array $standardFields): array
    {
        $newsletterFields = ['salutation', 'first_name', 'last_name', 'email', 'language', ''];
        return $this->filterFieldsForMapping($newsletterFields, $standardFields);
    }

    /**
     * @param string $webhookName
     * @return array|null
     * @author Pradeep
     */
    public function getDefaultConfigForNewsLetterWebhook(string $webhookName, array $standardFields): ?array
    {
        try {
            $newsLetter[ 'relation' ] = $this->getRelation();
            $newsLetter[ 'settings' ] = $this->setSettings($webhookName);
            $newsLetter[ 'mapping' ] = base64_encode(json_encode($this->getMappingForNewsLetterWebhook($standardFields),
                JSON_THROW_ON_ERROR));
            return $newsLetter;
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param array $standardFields
     * @return array
     * @author Pradeep
     */
    private function getMappingForeCommerceWebhook(array $standardFields): array
    {
        $eCFields = [
            'salutation',
            'first_name',
            'last_name',
            'email',
            'birthday',
            'street',
            'house_number',
            'postal_code',
            'city',
            'country',
            'smart_tags',
            'total_order_net_value',
            'last_order_date',
            'last_order_number',
            'last_order_net_value',
            'last_year_order_net_value',
            'first_order_date',
            'ip',
            'doi_ip',
            'doi_timestamp',
            'optin_ip',
            'optin_timestamp',
            'customer_group',
            'sub_shops',
            'shop_url',
            'total_number_of_orders',
            'product_attributes',
            'language',

        ];
        return $this->filterFieldsForMapping($eCFields, $standardFields);
    }

    /**
     * @param array $wbHookFilds
     * @param array $stndrdFilds
     * @return array
     * @author Pradeep
     */
    private function filterFieldsForMapping(array $wbHookFilds, array $stndrdFilds)
    {
        $mppdFilds = [];
        $nlFields = [];
        foreach ($stndrdFilds as $fields) {
            if (!in_array($fields[ 'fieldname' ], $wbHookFilds, true)) {
                continue;
            }
            $nlFilds [] = [
                $fields[ 'fieldname' ] => '',
                'field_name' => $fields[ 'fieldname' ],
                'required' => 'true',
                'datatype' => $fields[ 'dtype_label' ],
                'regex' => '',
                'html_placeholder' => $fields[ 'htmllabel' ],
            ];
        }
        // 'contact' and 'standard' are appended as per the payload expected by the client
        $mppdFilds[ 'contact' ][ 'standard' ] = $nlFilds;
        return $mppdFilds;
    }

    /**
     * @param string $pattern
     * @param string $value
     * @return bool
     * @author Pradeep
     */
    public function validateWithPattern(string $pattern, string $value): bool
    {
        $isValid = false;
        if (empty($pattern) || empty($value)) {
            return $isValid;
        }
        if (preg_match($pattern, $value)) {
            $isValid = true;
        }
        return $isValid;
    }

    /**
     * @param LoggerInterface $logger
     * @author Pradeep
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
<?php

namespace App\Types;

use App\Services\MSMailServices\MsMailConfigurationService;

class MsMailInterfaceTypes
{
	public $ccRecipientEmailList = '';
	public $messageBody = '';
	public $messageSubject = '';
	public $recipientEmailList = '';
	public $recipientIsBcc = false;
	public $senderEmailAddress = '';
	public $senderFullname = '';
	public $senderUsername = '';
	public $senderPassword = '';
	public $smtpPort = 25;
	public $smtpServer = '';

	private bool $configured;
	private $msMail;


	public function __construct(MsMailConfigurationService $msMail)
	{
		$this->configured = false;
		$this->msMail = $msMail;
		$mailParameters = $msMail->getMailParameters(); // gets default configuration from server configured in .env as MSMAIL_URL => e.g. email.msbackend-in-one.net (production)
		$this->init($mailParameters);
	}


	/**
     * @depreacted 2021
	 * @return array [ccRecipientEmailList,messageBody,messageSubject,recipientEmailList,recipientIsBcc,senderEmailAddress,senderFullname,senderPassword,smtpPort,smtpServer]<br>with basic configuration
	 */
	public function getInitialConfigArray(): array
	{
		return [
			'ccRecipientEmailList' => $this->ccRecipientEmailList,
			'messageBody' => $this->messageBody,
			'messageSubject' => $this->messageSubject,
			'recipientEmailList' => $this->recipientEmailList,
			'recipientIsBcc' => $this->recipientIsBcc,
			'senderEmailAddress' => $this->senderEmailAddress,
			'senderFullname' => $this->senderFullname,
            'senderUsername' => $this->senderUsername,
			'senderPassword' => $this->senderPassword,
			'smtpPort' => $this->smtpPort,
			'smtpServer' => $this->smtpServer,
		];
	}


	/**
     * @depreacted 2021
	 * @return MsMailInterfaceTypes with  basic configuration
	 */
	public function getInitialConfigObject(): MsMailInterfaceTypes
	{
		$obj = clone $this;
		return $obj;
	}


	/**
     * @depreacted
	 * @param array $myconfig
	 * @return $this
	 */
	public function setConfiguration(array $myconfig): MsMailInterfaceTypes
	{
		foreach ($myconfig as $k => $v) {
			$this->{$k} = $v;
		}

		$this->configured = true;
		return $this;
	}


	/**
	 * @return $this|null
	 */
	public function getConfigurationArray(): array
	{
		if ($this->configured) {
			$config = [];
			foreach (get_object_vars($this) as $varname => $value) {
				$rp = new \ReflectionProperty($this,$varname);
				if($rp->isPublic()) {
					$config[$varname] = $this->{$varname};
				}
			}
			return $config;
		} else {
			return [];
		}
	}


	/**
	 * @param array|null $mailParameters default null => not used here in MSMAIL, but in MIO, AIO
	 */
	private function init(array $mailParameters = null)
	{
		if (!empty($mailParameters['mailInterface'])) {
			foreach ($mailParameters['mailInterface'] as $type => $value) {
				$this->{$type} = $value;
			}
			$this->configured = true;
		} else {
			$this->configured = false;
		}
	}

}


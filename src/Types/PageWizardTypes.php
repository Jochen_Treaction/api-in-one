<?php


namespace App\Types;


class PageWizardTypes
{
    private $type;
    private $typeDesc;
    public $uniqueObjectName;


    public function __construct()
    {
        $this->uniqueObjectName = 'pages';

        $this->type = [
            "Flow Control" => [
                "BasicControl" => [
                    "value" => [
                        "is_redirect" => "",
                        "re_after_registration" => "",
                        "su_pg_url" => "",
                    ]
                ],
                "PageFlow" => [
                    "value" => [
                        "success_page" => "",
                        "error_page" => "",
                        "redirect_page" => "",
                        "pages" => [
                            "custom"=>[
                                [
                                    "name" => "index.html",
                                ],
                            ]
                        ],
                    ]
                ],
                "Splittest" => [
                    "value" => [
                        "status" => "",
                        "name" => "",
                        "total_variants" => "",
                        "cookie_expiration" => "",
                        "sticky_config" => "",
                        "expiration_criteria" => "",
                        "expiration_value_visitors" => "",
                        "expiration_value_visitor" => "",
                        "expiration_value_date" => "",
                        "variants" => []
                    ]
                ]
            ],
            "Integrations" => [
                "Email" => [
                    "value" => [
                        "forwardEmail" => "",
                        "forwardEmailRecipient" => "",
                    ]
                ],
                "Mapping" => [
                    "value" => [
                        "mappingTable" => ""
                    ]
                ]
            ],
            "Tracking" => [
                "Facebook"=>[
                    "value" => [
                        "pixel" => ""
                    ]
                ],
                "Google" => [
                    "value" => [
                        "apikey" => ""
                    ]
                ]
            ],
            "Settings" => [
                "FraudSettings" => [
                    "value" => [
                        "trade_show" => "", // bool
                        "level" => "", // low, medium, high
                        "max_leads_per_ip" => "", // int
                        "max_leads_per_domain" => "", // int
                        "check_whitelisted_domain" => "", //bool
                        "unique_user_registration" => "", // bool
                        "prohibit_free_mailer_address" => "" //bool
                    ]
                ],
                "General" => [
                    "value" => [
                        "beginDate" => "",
                        "expireDate" => "",
                        "advertising" => "",
                        "localization" => "",
                        "keep_url_parms" => "",
                        "track_cus_jour" => ""
                    ]
                ],
                "Messages" => [
                    "value" => [
                        'singleSubmitErrorText' => '',
                        'countdownExpireMessage' => '',
                        'ErrorMsg_Color' => '',
                        'ErrorMsgDuplicate' => '',
                        'ErrorMsgEmail' => '',
                        'ErrorMsgTelefon' => '',
                        'ErrorMsgimageUpload' => '',
                        'ErrorMsgPostalcode' => '',
                        'ErrorMsgBlockFreeMailer' => '',
                        'ErrorMsgAddress' => ' ',
                        'ErrorMsgCampaignEndDate' => '',
                        'ExpirationColor' => '',
                        'ExpirationTransparency' => ''
                    ]
                ],
            ]
        ];
        //todo:make type description
        $this->typeDesc = [];

    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->type);
    }


    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->type;
    }

    public function getArrayWithDefaultValues(): array
    {
        $default = $this->type;

        $default['pageflow']['success_page'] = 'success.html';
        $default['pageflow']['error_page'] = 'error.html';
        $default['pageflow']['redirect_page'] = 'treaction.net';
        $default['pageflow']['pages'] = array(
            'custom' =>
                array(
                    0 =>
                        array(
                            'name' => 'index.html',
                        ),
                ),
        );
        $default['splittest']['status'] = NULL;
        $default['splittest']['total_variants'] = 'Please select';
        $default['splittest']['sticky_config'] = NULL;
        $default['splittest']['expiration_criteria'] = '90';
        $default['splittest']['variants'] = NULL;

        return $default;
    }

    public function getDatastructure()
    {
        $dataModel = [
            "Flow Control" => [
                "BasicControl" => [
                    "value" => [
                        "is_redirect" => "",
                        "re_after_registration" => "",
                        "su_pg_url" => "",
                    ]
                ],
                "PageFlow" => [
                    "value" => [
                        "success_page" => "",
                        "error_page" => "",
                        "redirect_page" => "",
                        "pages" => [
                            "custom"=>[
                                [
                                    "name" => "index.html",
                                ],
                            ]
                        ],
                    ]
                ],
                "Splittest" => [
                    "value" => [
                        "status" => "",
                        "name" => "",
                        "total_variants" => "",
                        "cookie_expiration" => "",
                        "sticky_config" => "",
                        "expiration_criteria" => "",
                        "expiration_value_visitors" => "",
                        "expiration_value_visitor" => "",
                        "expiration_value_date" => "",
                        "variants" => []
                    ]
                ]
            ],
            "Integrations" => [
                "Email" => [
                    "value" => [
                        "forwardEmail" => "",
                        "forwardEmailRecipient" => "",
                    ]
                ],
                "Mapping" => [
                    "value" => [
                        "mappingTable" => ""
                    ]
                ]
            ],
            "Tracking" => [
                "Facebook"=>[
                    "value" => [
                        "pixel" => ""
                    ]
                ],
                "Google" => [
                    "value" => [
                        "apikey" => ""
                    ]
                ]
            ],
            "Settings" => [
                "FraudSettings" => [
                    "value" => [
                        "singleSubmit" => "",
                        "trade_show" => "",
                        "level" => "",
                        "max_leads_per_ip" => "",
                        "max_leads_per_domain" => "",
                    ]
                ],
                "General" => [
                    "value" => [
                        "beginDate" => "",
                        "expireDate" => "",
                        "advertising" => "",
                        "localization" => "",
                        "keep_url_parms" => "",
                        "track_cus_jour" => ""
                    ]
                ],
                "Messages" => [
                    "value" => [
                        'singleSubmitErrorText' => 'You have already registred.',
                        'countdownExpireMessage' => 'Die Aktion ist beendet',
                        'ErrorMsg_Color' => '#FF0000',
                        'ErrorMsgDuplicate' => '* Sie haben bereits teilgenommen',
                        'ErrorMsgEmail' => '* Bitte überprüfen Sie die E-Mail-Adresse',
                        'ErrorMsgTelefon' => '* Bitte überprüfen Sie die Telefonnummer',
                        'ErrorMsgimageUpload' => '* Bitte überprüfen Sie die Bildformat nur png, jpg sind erlaubt',
                        'ErrorMsgPostalcode' => '* Bitte überprüfen Sie die Postleitzahl',
                        'ErrorMsgBlockFreeMailer' => '* Bitte nutzen Sie Ihre geschäftliche E-Mail-Adresse',
                        'ErrorMsgAddress' => ' * Bitte überprüfen Sie die Adresse',
                        'ErrorMsgCampaignEndDate' => 'Die Aktion ist beendet',
                        'ExpirationColor' => '#FF0000',
                        'ExpirationTransparency' => '0.1'
                    ]
                ],
            ]
        ];

    }

}
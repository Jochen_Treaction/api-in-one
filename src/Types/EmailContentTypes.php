<?php


namespace App\Types;



/**
 * MIO, AIO, MSMAIL
 * Class EmailContentTypes
 * @package App\Types
 */
class EmailContentTypes
{
	public string $accountFinishSetupNotification = 'account-finish-setup-notification';
	public string $accountReactivationNotification = 'account-reactivation-notification';
	public string $accountDeletionNotification = 'account-deletion-notification';
	public string $accountRegistrationNotification = 'account-registration-notification';
	public string $accountSendActivationlink = 'account-send-activationlink';
	public string $accountSendActivationReminder = 'account-send-activation-reminder';
	public string $accountSendPasswordResetLink = 'account-send-password-reset-link';
	public string $accountTestMailserverNotification = 'account-test-mailserver-notification';
	public string $accountVerifyApikey = 'account-verify-apikey';
	public string $aioAccountSendActivationlink = 'aio-account-send-activationlink';
	public string $aioSendLeadNotification = 'aio-send-lead-notification';

	public function __construct()
	{
	}


	/**
	 * @return array
	 */
	public function getEmailContentTypesArray():array
	{
		$emailContentTypesArray = [];
		foreach ($this as $k => $v) {
			$emailContentTypesArray[$k] = $v;
		}
		return $emailContentTypesArray;
	}


	public function getPropertyName(string $emailContentType): ?string
	{
		foreach ($this as $k => $v) {
			if ($v === $emailContentType) {
				return $k;
			}
		}
		return null;
	}
}

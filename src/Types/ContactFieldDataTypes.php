<?php

namespace App\Types;

class ContactFieldDataTypes
{
    public const FIELD_TYPE_DATE = 'Date';
    public const FIELD_TYPE_DATETIME = 'DateTime';
    public const FIELD_TYPE_DECIMAL = 'Decimal';
    public const FIELD_TYPE_EMAIL = 'Email';
    public const FIELD_TYPE_INTEGER = 'Integer';
    public const FIELD_TYPE_LIST = 'List';
    public const FIELD_TYPE_PHONE = 'Phone';
    public const FIELD_TYPE_TEXT = 'Text';
    public const FIELD_TYPE_URL = 'URL';
    public const FIELD_TYPE_BOOLEAN = 'Boolean';

}
<?php


namespace App\Types;


class UniqueObjectTypes
{
    public const UNIQUE_OBJECT_NAME = "unique_object_name";
    public const PROJECT = "project";
    public const PAGE = "page";
    public const WEBHOOK = "webhook";
    public const CUSTOMER = "customer";
    public const CUSTOMFIELDS = "customfields";
    public const STANDARDCUSTOMFIELDS = "standardcustomfields";
    public const SURVEY = "survey";
    public const STANDALONE_SURVEY = "standalone survey";
    public const ACCOUNT = "account";
    public const EMAILSERVER = 'emailserver';
    public const EMAILINONE = 'email-in-one';
    public const SUPERADMIN = 'superadmin';
    public const WBHOOKQUEUE = 'externaldata_queue';

    /**
     * @return string[] all possible status types of App\Types\UniqueObjectTypes
     * @auther jsr
     */
    public static function getTypes(): array
    {
        return [
            self::UNIQUE_OBJECT_NAME,
            self::PROJECT,
            self::PAGE,
            self::WEBHOOK,
            self::CUSTOMER,
            self::CUSTOMFIELDS,
            //  self::SMARTTAGS,
            self::SURVEY,
            self::STANDALONE_SURVEY,
            self::ACCOUNT,
        ];
    }


    /**
     * @param string $uniqueObjectType
     * @return bool
     * @author jsr
     */
    public static function isCorrectType(string $uniqueObjectType): bool
    {
        return in_array($uniqueObjectType, self::getTypes());
    }

}


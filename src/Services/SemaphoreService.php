<?php

namespace App\Services;

use Monolog\Logger;
use Psr\Log\LoggerInterface;

class SemaphoreService
{

    public const TWO_DAYS_IN_SECONDS = (24 * 60 * 60) * 2;
    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        RedisService $redisService,
        LoggerInterface $logger
    ) {
        $this->redisService = $redisService;
        $this->logger = $logger;
    }

    /**
     * set activate or deactivate the semaphore, by updating $value to with bool.
     * @param string $semaphoreKey
     * @param bool $value
     * @return bool
     * @author Pradeep
     */
    public function set(string $semaphoreKey, bool $value)
    {
        return $this->redisService->set($semaphoreKey, $value, $this->redisService::ONE_HOUR_IN_SECONDS);
    }

    public function get(string $semaphoreKey)
    {
        return $this->redisService->get($semaphoreKey);
    }

    /**
     * isLocked tels about the semaphore status, whether its active to inactive.
     * @param string $semaphoreKey
     * @return bool
     * @author Pradeep
     */
    public function isLocked(string $semaphoreKey): bool
    {
        $value = $this->redisService->get($semaphoreKey);

        // May be before redisKey has expired.
        // Hence there is no semaphore working.
        if (empty($value)) {
            return false;
        }
        // redisKey is present.
        // Semaphore is set to false, hence the semaphore is ready to take up another job.
        if (is_bool($value) && !$value) {
            return false;
        }
        return true;
    }

}
<?php

namespace App\Services\AIOServices\Contact;

use Psr\Log\LoggerInterface;

class ContactFieldServices
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * - getValue is a helper function
     * - searches for the field and returns the value of the field.
     * - Used for processing the synchronizing contacts from shop plugin.
     * - field name is search for case-insensitive.
     * @param array $cntct
     * @param string $schKy
     * @return mixed|void
     * @author Pradeep
     */
    public function getValue(array $cntct, string $schKy)
    {
        if (!isset($cntct[ 'standard' ]) ||
            !is_array($cntct[ 'standard' ]) ||
            empty($schKy)) {
            return null;
        }
        // fetching the field value for contact standard field.
        foreach ($cntct[ 'standard' ] as $stdFld) {
            // Check if the standardField is of type array or not.
            if (!is_array($stdFld)) {
                continue;
            }
            foreach ($stdFld as $stdKey => $stdVlu) {
                if (strtolower($stdKey) === strtolower($schKy)) {
                    return $stdVlu;
                }
            }
        }
        // Custom Fields of the contact.
        if (isset($cntct[ 'custom' ]) && is_array($cntct[ 'custom' ])) {
            // fetching the field value for contact custom field.
            foreach ($cntct[ 'custom' ] as $cstmFld) {
                // Check if the CustomField is of type array or not.
                if (!is_array($cstmFld)) {
                    continue;
                }
                foreach ($cstmFld as $cstmKey => $cstmVlu) {
                    if (strtolower($cstmKey) === strtolower($schKy)) {
                        return $cstmVlu;
                    }
                }
            }
        }

        // return null when field key is not found in either standard or
        // customfield list.
        return null;
    }
}
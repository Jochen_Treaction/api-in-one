<?php


namespace App\Services\AIOServices;


use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ProcesshookRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\MSMailServices\MSMailServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Types\ProcesshookTypes;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyAccess\Exception\RuntimeException;

/**
 * @property MSMIOServices msmioServices
 * @property IntegrationServices integrationServices
 * @property ObjectRegisterRepository objectRegisterRepository
 */
class ProcesshookServices
{

    private LoggerInterface $logger;
    private ProcesshookRepository $repository;
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private MSMailServices $mailServices;
    private IntegrationsRepository $integrationsRepository;


    public function __construct(
        LoggerInterface $logger,
        ProcesshookRepository $processhookRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        MSMailServices $mailServices,
        IntegrationsRepository $integrationsRepository,
        MSMIOServices $msmioServices,
        IntegrationServices $integrationServices,
        ObjectRegisterRepository $objectRegisterRepository,
        ContactFieldServices $contactFieldServices,
        ContactServices $contactServices
    )
    {
        $this->logger = $logger;
        $this->repository = $processhookRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->mailServices = $mailServices;
        $this->integrationsRepository = $integrationsRepository;
        $this->msmioServices = $msmioServices;
        $this->integrationServices = $integrationServices;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->contactFieldServices = $contactFieldServices;
        $this->contactServices = $contactServices;
    }

    /**
     * @param int $workflowId
     * @param array $data
     * @param int $companyId
     * @return bool|null
     * @throws Exception
     * @author Aki
     */
    public function integrateWorkflow(int $workflowId, array $data, int $companyId): ?bool
    {
        $this->logger->info('integrateWorkflow' . json_encode([
                'WFid' => $workflowId,
                'companyId' => $companyId,
                'data' => $data
            ], JSON_THROW_ON_ERROR));
        //basic check
        if ($workflowId <= 0) {
            return false;
        }
        $processhooks = $this->repository->getProcesshooksWithWorkflowId($workflowId);
        $this->logger->info('ProcessHooks' . json_encode([
                $processhooks
            ], JSON_THROW_ON_ERROR));
        //todo:In case of one processhook failure other processhooks show continue working
        foreach ($processhooks as $processhook) {
            $userProcesshookId = $processhook['user_processhook_id'];
            $this->logger->info('userProcesshookId' . json_encode([
                    $userProcesshookId
                ], JSON_THROW_ON_ERROR));
            if ($this->integrateProcesshook($userProcesshookId, $data, $companyId) === false) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param int $userProcesshookId
     * @param array $data
     * @param int $companyId
     * @return bool|null
     * @throws Exception
     * @author Aki
     */
    private function integrateProcesshook(int $userProcesshookId, array $data, int $companyId): ?bool
    {
        $this->logger->info('integrateProcesshook' . json_encode([
                'userProcesshookId' =>$userProcesshookId, 'data' =>$data, 'companyId' =>$companyId
            ], JSON_THROW_ON_ERROR));
        //basic check
        if ($userProcesshookId <= 0) {
            return false;
        }
        $processhookType = $this->repository->getProcesshookType($userProcesshookId);
        $this->logger->info('integrate ProcesHook '.json_encode($processhookType),[__METHOD__, __LINE__]);
        switch ($processhookType) {
            case ProcesshookTypes::SEND_LEAD_NOTIFICATION:
                $this->sendLeadNotificationProcesshook($userProcesshookId, $data, $companyId);
                break;
            case ProcesshookTypes::START_MARKETING_AUTOMATION:
                $this->startMarketingAutomation($userProcesshookId, $data, $companyId);
                break;
            case ProcesshookTypes::SEND_DOI_EMAIL:
                $this->sendDOImail($userProcesshookId, $data, $companyId);
                break;
            case ProcesshookTypes::CALL_CONTACT_EVENT:
                $this->callContactEvent($userProcesshookId, $data, $companyId);
                break;
            case ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT:
                $this->sendDOIMailorCallContacctEvent($userProcesshookId, $data, $companyId);
                break;
            //todo:add other processhooks

        }
        return true;
    }


    /**
     * @param int $userProcesshookId
     * @param array $contact
     * @param int $companyId
     * @throws Exception
     * @throws \Exception
     * @author Aki
     */
    private function sendLeadNotificationProcesshook(int $userProcesshookId, array $contact, int $companyId): void
    {
        $this->logger->info('sendLeadNotificationProcesshook' . json_encode([
                '$userProcesshookId' =>$userProcesshookId, 'contact' =>$contact, 'companyId' =>$companyId
            ], JSON_THROW_ON_ERROR));

        $this->logger->info("Send Lead Notification processhook started", [__METHOD__, __LINE__]);
        //get object register id of user processhook
        $objectRegisterId = $this->repository->getUserProcesshook($userProcesshookId)['objectregister_id'];
        $this->logger->info("getUserProcesshook params / response", ['param $userProcesshookId' => $userProcesshookId, 'response $objectRegisterId'=>$objectRegisterId, __METHOD__, __LINE__ ]);
        //get processhook settings
        $processhookSettings = $this->objectRegisterMetaRepository->getMetaValue($objectRegisterId,ProcesshookTypes::SEND_LEAD_NOTIFICATION)['settings'];
        $this->logger->info('$this->objectRegisterMetaRepository->getMetaValue params / response', ['param $objectRegisterId' => $objectRegisterId, 'ProcesshookTypes::SEND_LEAD_NOTIFICATION' => ProcesshookTypes::SEND_LEAD_NOTIFICATION, 'response $processhookSettings'=>$processhookSettings, __METHOD__, __LINE__ ]);
        $this->logger->info('$processhookSettings', ['$processhookSettings' =>$processhookSettings, __METHOD__, __LINE__]);

        //check for empty settings
        if (!empty($processhookSettings['list_of_recipients']['value'])) {
            //get subject line
            $subjectLine = $processhookSettings['subject_line']['value'];
            //get recipients from settings
            $recipients = $processhookSettings['list_of_recipients']['value'];

            // FROM HERE ON THE CODE WILL NOT WORK DUE TO ERRORS IN MIO-CODE when you set up an emailserver configuration in integrations OR archive one and/or setup a new one => there must be exactly one record in objectregister / object_register_mata, but there are more records created => that must be fixed still (Status of 2022-07-01)

            //get integrations
            $integrations = $this->integrationsRepository->getIntegrationsForCompany($companyId); // returns 1..n records (!!!!) due to several errors in emailserver integrations setup ==> check MIO IntegrationsController::postEmailServerIntegrations and attached processes => check my comments in that code
                                                                                                            // 'emailserver' => '{"overview":{"title":"Email Server","subTitle":"Newsletter,DOI,Trigger,Automation","type":["email","all"],"releaseDate":"26.05.2020","lastUpdate":"26.05.2020","version":"1.0"},"relation":{"company_id":"161","integration_object_unique_id":10,"integration_objectregister_id":747599,"integration_name":"EmailServer","integration_type":["email","all"],"integration_object_register_meta_id":"1786"},"settings":{"server_type":"custom_server","default_server":{"smtp_server_domain":"default.com","sender_user_email":"test@default.com","sender_password":"default","port":"21"},"custom_server":{"smtp_server_domain":"smtp.web.de","sender_user_email":"zentrale-stellar-online@web.de","sender_password":"aiphojeed2EiSoonguW","port":"587"}}}'
                                                                                                            // 'emailserver' => '{"overview":{"title":"Email Server","subTitle":"Newsletter,DOI,Trigger,Automation","type":["email","all"],"releaseDate":"26.05.2020","lastUpdate":"26.05.2020","version":"1.0"},"relation":{"company_id":"","integration_object_unique_id":10,"integration_objectregister_id":747600,"integration_name":"EmailServer","integration_type":["email","all"]},"settings":{"server_type":"default_server","default_server":{"smtp_server_domain":"default.com","sender_user_email":"test@default.com","sender_password":"default","port":"21"},"custom_server":{"smtp_server_domain":"","sender_user_email":"","sender_password":"","port":""}}}'

            $this->logger->info('$integrations', ['$integrations' =>$integrations, __METHOD__, __LINE__]);

            // todo: seems not to get or use email server config from integrations (jochen)

            //check for integrations
            if (isset($integrations[UniqueObjectTypeRepository::EMAILSERVER])) { // might be even set twice or more times ...!!!
                //get email server integrations
                $emailServerIntegrations = $integrations[UniqueObjectTypeRepository::EMAILSERVER]; // which one of that two or more records of the same type??? => That does not work!
                //take settings from email server
                $this->logger->info('$emailServerIntegrations', ['$emailServerIntegrations' =>$emailServerIntegrations, __METHOD__, __LINE__]);

                $eMailServerConfiguration = $emailServerIntegrations['settings'];
                $this->logger->info('$eMailServerConfiguration', ['$eMailServerConfiguration' =>$eMailServerConfiguration, __METHOD__, __LINE__]);
                $this->logger->info('$eMailServerConfiguration[server_type]', ['server_type' =>$eMailServerConfiguration['server_type'], __METHOD__, __LINE__]);

                //check for custom server or use default server
                if ($eMailServerConfiguration['server_type'] === 'custom_server') { // TODO: fix before => servertype is always "default_server", even if seetings of "custom_server" are present

                    $eMailServerConfiguration = $eMailServerConfiguration['custom_server'];

                } elseif ($eMailServerConfiguration['server_type'] === 'default_server') {

                    $defaultMailServerConfig = $this->integrationsRepository->getDefaultEmailServerConfiguration();

                    if (empty($defaultMailServerConfig) || !isset($defaultMailServerConfig['settings']['default_server'])) {
                        $this->logger->notice('THROW EXCEPTION', ['Invalid Default eMailServer.', __METHOD__, __LINE__]);
                        throw new RuntimeException('Invalid Default eMailServer.');
                    }

                    $eMailServerConfiguration = $defaultMailServerConfig['settings']['default_server'];
                } else {
                    $this->logger->notice('THROW EXCEPTION', ['Failed to get Server Type from Installed EmailServer.', __METHOD__, __LINE__]);
                    throw new RuntimeException('Failed to get Server Type from Installed EmailServer.');
                }

                $this->logger->info('$eMailServerConfiguration used in sendForwardEmail', ['$eMailServerConfiguration' =>$eMailServerConfiguration, __METHOD__, __LINE__]);
                //send email
                $sendForwardEmail = $this->mailServices->sendForwardEmail(
                    $recipients,
                    $eMailServerConfiguration,
                    $contact,
                    $subjectLine
                );
                $this->logger->info('$sendForwardEmail',[$sendForwardEmail, __METHOD__,__LINE__]);
            }

        }
    }

    /**
     * @param int $userProcesshookId
     * @param array $contact
     * @param int $companyId
     * @throws Exception
     * @author Aki
     */
    private function startMarketingAutomation(int $userProcesshookId, array $contact, int $companyId): void
    {
        $this->logger->info("Start Marketing Automation processhook started");
        //get object register id of user processhook
        $objectRegisterId = $this->repository->getUserProcesshook($userProcesshookId)['objectregister_id'];
        //get processhook settings
        $processhookSettings = $this->objectRegisterMetaRepository->getMetaValue($objectRegisterId,
            ProcesshookTypes::START_MARKETING_AUTOMATION)['settings'];
        //type of service in eMIO
        $typeOfIntegration = MSMIOServices::ITYPE_MA;
        if (!empty($processhookSettings['marketing_automation_number']['value'])) {
            //get the marketing automation ID
            $marketingAutomationId = $processhookSettings['marketing_automation_number']['value'];
            $status = $this->integrateToEmailInOne(
                $companyId,
                $typeOfIntegration,
                $contact,
                $contactEventId = "",
                (int)$marketingAutomationId,
                $permission = 0,
                $doiMailingKey = "",
                $contactEventFields = []
            );
            if (is_null($status)) {
                $this->logger->error("Marketing Automation processhook has not executed successfully");
            }
        }
    }

    /**
     * @param int $userProcesshookId
     * @param array $contact
     * @param int $companyId
     * @throws Exception
     * @author Aki
     */
    private function sendDOImail(int $userProcesshookId, array $contact, int $companyId): void
    {
        $this->logger->info("Send DOI mail processhook started");
        //get object register id of user processhook
        $objectRegisterId = $this->repository->getUserProcesshook($userProcesshookId)['objectregister_id'];
        //get processhook settings
        $processhookSettings = $this->objectRegisterMetaRepository->getMetaValue($objectRegisterId,
            ProcesshookTypes::SEND_DOI_EMAIL)['settings'];

        $this->logger->info('sendDOI processhookSettings' . json_encode([
                '$processhookSettings' =>$processhookSettings,
            ], JSON_THROW_ON_ERROR));

        //type of service in eMIO
        $typeOfIntegration = MSMIOServices::ITYPE_DOI;
        if (!empty($processhookSettings['DOI_mailing']['value'])) {
            $doiMailingKey = $processhookSettings[ 'DOI_mailing' ][ 'value' ];
            $permission = $this->getPermission($contact);
            $status = $this->integrateToEmailInOne(
                $companyId,
                $typeOfIntegration,
                $contact,
                $contactEventId = "",
                $marketingAutomationId = "",
                $permission,
                $doiMailingKey,
                $contactEventFields = []
            );
            if (is_null($status)) {
                $this->logger->error("Marketing Automation processhook has not executed successfully");
            }
        }
    }


    /**
     * getContactPermission is a helper function for the processHooks.
     * - Fetches the permission present in the contact as well the permission in the
     * campaign.
     * - Both the permissions are evaluated and return the permission for the eMIO.
     * @param array $contact
     * @return int
     * @author Pradeep
     */
    protected function getPermission(array $contact): int
    {
        $perFromCampaign = 0;
        $perOfContact = 0;

        $this->logger->info('GetPermission Contact Infor', [$contact, __METHOD__, __LINE__]);

        if (!isset($contact[ 'base' ][ 'object_register_id' ])) {
            return $perOfContact;
        }

        //campaign is both webhook and page
        $campaignObjectRegisterId = $contact[ 'base' ][ 'object_register_id' ];
        //get campaign type
        $uniqueObjectType =
            $this->objectRegisterRepository->getUniqueObjectNameFromObjectRegisterId($campaignObjectRegisterId);

        //get campaign settings
        $campaignSettings = $this->objectRegisterMetaRepository->getMetaValue(
            $campaignObjectRegisterId,
            $uniqueObjectType
        );
        // Permission from the campaign.
        $perFromCampaign = $campaignSettings[ 'settings' ][ 'permission' ];

        // Permission of the contact.
        $perOfContact = $this->contactFieldServices->getValue($contact, 'permission');

        // evaluate both the permission levels.
        return $this->contactServices->evaluatePermissionLevel($perFromCampaign, $perOfContact);
    }

    /**
     * @param int $userProcesshookId
     * @param array $contact
     * @param int $companyId
     * @throws Exception
     * @author Aki
     */
    private function callContactEvent(int $userProcesshookId, array $contact, int $companyId): void
    {
        $permission = 0;
        $this->logger->info("Call contact event processhook started");
        //get object register id of user processhook
        $objectRegisterId = $this->repository->getUserProcesshook($userProcesshookId)['objectregister_id'];
        //get processhook settings
        $processhookSettings = $this->objectRegisterMetaRepository->getMetaValue($objectRegisterId,
            ProcesshookTypes::CALL_CONTACT_EVENT)['settings'];
        //type of service in eMIO
        $typeOfIntegration = MSMIOServices::ITYPE_CE;
        //Take mapping table
        $mappingTable = $processhookSettings[ 'field_mappings' ];
        $permission = $this->getPermission($contact);
        //Check if contact has that fields
        $contactEventFields = $this->mapEmioandMIOFileds($contact['contact'], $mappingTable);
        if (!empty($processhookSettings['contact_event_id'])) {
            $contactEventId = $processhookSettings['contact_event_id'];
            $status = $this->integrateToEmailInOne(
                $companyId,
                $typeOfIntegration,
                $contact,
                $contactEventId,
                $marketingAutomationId = "",
                $permission,
                $doiMailingKey = "",
                $contactEventFields
            );
            if (is_null($status)) {
                $this->logger->error("Marketing Automation processhook has not executed successfully");
            }
        }
    }

    /**
     * @param int $userProcesshookId
     * @param array $contact
     * @param int $companyId
     * @throws Exception
     * @author Aki
     */
    private function sendDOIMailorCallContacctEvent(int $userProcesshookId, array $contact, int $companyId): void
    {
        $this->logger->info("Call contact event or send DOI mail processhook started");
        //get object register id of user processhook
        $objectRegisterId = $this->repository->getUserProcesshook($userProcesshookId)['objectregister_id'];
        //get processhook settings
        $processhookSettings = $this->objectRegisterMetaRepository->getMetaValue($objectRegisterId,
            ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT)['settings'];
        //type of service in eMIO
        $typeOfIntegration = MSMIOServices::ITYPE_DOI_XOR_CE;

        if (!empty($processhookSettings['contact_event_id']) && !empty($processhookSettings['doi_mailing_key'])) {
            $contactEventId = $processhookSettings['contact_event_id'];
            $doiMailingKey = $processhookSettings['doi_mailing_key'];
            //take permission
            $permission = $this->getPermission($contact);
            //Take mapping table
            $mappingTable = $processhookSettings['field_mappings'];
            //Check if contact has that fields
            $contactEventFields = $this->mapEmioandMIOFileds($contact['contact'], $mappingTable);
            $status = $this->integrateToEmailInOne(
                $companyId,
                $typeOfIntegration,
                $contact,
                $contactEventId,
                $marketingAutomationId = "",
                $permission,
                $doiMailingKey,
                $contactEventFields
            );
            if (is_null($status)) {
                $this->logger->error("Marketing Automation processhook has not executed successfully");
            }
        }
    }

    private function integrateToEmailInOne(int $companyId,
                                           string $typeOfIntegration,
                                           array $contact,
                                           string $contactEventId,
                                           string $marketingAutomationId,
                                           int $permission,
                                           string $doiMailingKey,
                                           array $contactEventFields
    ): ?bool
    {
        $integrations = $this->integrationsRepository->getIntegrationsForCompany($companyId);
        $eMIOConfig = $integrations[UniqueObjectTypeRepository::EMAILINONE];
        if (empty($contact) || !($this->msmioServices->setAPIKey($eMIOConfig['settings']['apikey']))) {
            throw new RuntimeException('Invalid Contact or eMIO APIKey provided');
        }
        $MIOCustomFields = !empty($eMIOConfig[ 'settings' ][ 'mapping' ]) ?
            $this->integrationServices->simpleDecrypt($eMIOConfig[ 'settings' ][ 'mapping' ]) : [];
        $this->msmioServices->syncContactFieldOfDataTypeListForSingleContact($contact);
        $eMIOContact = $this->integrationServices->mapToSystemFields($contact, UniqueObjectTypeRepository::EMAILINONE,
            $MIOCustomFields);
        $this->logger->info("This is contact for create contact", $eMIOContact);
        //add additional fields for CA or DOIORCE
        if ($typeOfIntegration === MSMIOServices::ITYPE_CE || $typeOfIntegration === MSMIOServices::ITYPE_DOI_XOR_CE) {
            $eMIOContact['contactEventFields'] = $contactEventFields;
        }
        return $this->msmioServices->createContact($eMIOContact, $typeOfIntegration, $permission, $doiMailingKey,
            $contactEventId, $marketingAutomationId);
    }

    /**
     * @param array $contact
     * @param $mappingTable
     * @return array|null
     * @author Aki
     */
    private function mapEmioandMIOFileds(array $contact, $mappingTable): ?array
    {
        if (empty($contact) || empty($mappingTable)) {
            return null;
        }
        //merge all contacts fields
        if (!empty($contact['custom'])) {
            $contactFields = array_merge($contact['standard'], $contact['custom']);
        } else {
            $contactFields = $contact['standard'];
        }
        $mappedFields = [];
        //format mio and ce fields as mio field name => CE filed name
        foreach ($mappingTable as $mapping) {
            $mappedFields[$mapping['mio_field_name']] = $mapping['ce_field_name'];
        }
        $mappedContactFields = [];
        foreach ($contactFields as $contactFiled) {
            foreach ($contactFiled as $key => $value) {
                if (array_key_exists($key, $mappedFields)) {
                    $mappedContactFields[$mappedFields[$key]] = $value;
                }
            }
        }
        return $mappedContactFields;
    }

}

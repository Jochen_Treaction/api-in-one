<?php


namespace App\Services\AIOServices;


use App\Types\ContactFieldTypes;
use App\Types\ContactPermissionTypes;
use Exception;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\AuthenticationException;
use GeoIp2\Exception\GeoIp2Exception;
use GeoIp2\Exception\HttpException;
use GeoIp2\Exception\InvalidRequestException;
use GeoIp2\Exception\OutOfQueriesException;
use GeoIp2\WebService\Client;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class ContactServices
{
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var ContainerBagInterface
     * @author Pradeep
     */
    private ContainerBagInterface $containerBag;
    /**
     * @var
     * @author Pradeep
     */
    private $geoIpClient;
    /**
     * @var ContactPermissionTypes
     * @author Pradeep
     */
    private ContactPermissionTypes $contactPermissionTypes;

    public function __construct(
        LoggerInterface        $logger,
        ContainerBagInterface  $containerBag,
        ContactPermissionTypes $contactPermissionTypes
    )
    {
        $this->logger = $logger;
        $this->containerBag = $containerBag;
        $this->contactPermissionTypes = $contactPermissionTypes;
    }

    /**
     * @return false|string
     * @author Pradeep
     */
    public function getCurrentDate(): string
    {
        return date("Y-m-d");
    }

    /**
     * @param string $eMail
     * @return string|null
     * @author Pradeep
     */
    public function getDomainName(string $eMail): ?string
    {
        $domain = null;
        try {
            if (empty($eMail)) {
                throw new RuntimeException('Invlaid Email address provided');
            }
            $parseMail = explode('@', $eMail);
            if (is_array($parseMail) && !empty($parseMail[1])) {
                $domain = $parseMail[1];
            }
            return $domain;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $ip
     * @return bool
     *      @1. true : if the IPAddress is from given list of countries.
     *      @2. false : if the IPAddress is from another countries.
     * @author Pradeep
     * @internal validates whether if the Ipaddress is from given list of countries or not.
     * Sends a request to geoIp client to fetch the country.
     */
    public function validateIpAddress(string $ip): bool
    {
        $status = false;
        $ip = trim($ip);
        $client = $this->getGeoIpClient();
        if (!filter_var($ip, FILTER_VALIDATE_IP) || !$client instanceof Client) {
            return $status;
        }
        try {
            $geo_client = $client->city($ip);
            if ($geo_client->country->isoCode === 'DE') {
                $status = true;
            }
        } catch (AddressNotFoundException|AuthenticationException|
        InvalidRequestException|HttpException|OutOfQueriesException
        |GeoIp2Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }

    /**
     * @return Client
     * @author Pradeep
     * @internal GeoIpClient is an external library which is used for validating IpAddress.
     * and block the IPAddresses of other countries.
     */
    protected function getGeoIpClient(): Client
    {
        if (!$this->geoIpClient instanceof Client) {
            $accountId = trim($this->getServiceParameter('app.geoip_account_id'));
            $licenceKey = trim($this->getServiceParameter('app.geoip_licence_key'));
            if (empty($accountId) && empty($licenceKey)) {
                throw new RuntimeException("Failed to get AccountId or Licence key of GeoIPClient");
            }
            $this->geoIpClient = new Client($accountId, $licenceKey);
        }
        return $this->geoIpClient;
    }

    /**
     * @param string $serviceParameterName
     * @return mixed|null
     * @author Pradeep
     */
    protected function getServiceParameter(string $serviceParameterName)
    {
        try {
            return $this->containerBag->get($serviceParameterName);
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * Helper function for comparing the contact data with existing contact data in DB.
     * If the field value for newContact is empty or null, then automatically gets the value from existingContact.
     * $existingContact data has to be provided as array.
     * @param array $newContact
     * @param array|null $existingContact
     * @param string $fieldName
     * @return string
     * @author Pradeep
     */
    public function checkFieldValueFromExistingContact(
        array  $newContact,
        string $fieldName,
        array  $existingContact = null
    ): string
    {
        $fieldValue = '';
        // No Contact Present
        if (empty($newContact) || empty($fieldName)) {
            return $fieldValue;
        }
        // new contact, where no other contact with same email address present in DB,
        if (empty($existingContact) && isset($newContact[$fieldName])) {
            if (is_array($newContact[ $fieldName ])) {
                return implode(',', $newContact[ $fieldName ]);
            }
            return $newContact[ $fieldName ];
        }
        // field value of newContact and existing contact is null or empty.
        if (empty($newContact[$fieldName]) && empty($existingContact[$fieldName])) {
            return $fieldValue;
        }

        if (empty($newContact[$fieldName])) {
            return $existingContact[$fieldName];
        }
        // Filter duplicate eCommerceTags and append unique Tags to the end.
        if ($fieldName === ContactFieldTypes::SmartTags) {
            return $this->filterSubShops(
                $newContact[$fieldName],
                $existingContact[$fieldName]);
        }
        // Filter duplicate eCommerceTags and append unique Tags to the end.
        if ($fieldName === ContactFieldTypes::SubShops) {
            return $this->filterECommerceTags(
                $newContact[$fieldName],
                $existingContact[$fieldName]);
        }
        // Compare the Permission of the Contact
        // Permission is only upgraded and never downgraded.
        if ($fieldName === ContactFieldTypes::Permission) {
            if ((int)$newContact[$fieldName] < (int)$existingContact[$fieldName]) {
                return (int)$newContact[$fieldName];
            }
            return (int)$existingContact[$fieldName];
        }

        // consider existing contact field value when there is no field value for new contact is present.
        return empty($newContact[$fieldName]) ? $existingContact[$fieldName] : $newContact[$fieldName];
    }

    /**
     * @param $newContactShops
     * @param $existingContactShops
     * @return string
     * @author Pradeep
     */
    public function filterSubShops($newContactShops, $existingContactShops): string
    {
        return $this->filterDuplicateTags($newContactShops, $existingContactShops);
    }

    /**
     * - filterDuplicateTags filters duplicate tags for newContact version and existingContact version.
     * - Used for filtering eCommerceTags and ProductAttributes.
     * - returns unique set tags as a string.
     * @param string $newTags
     * @param string $existingTags
     * @return string
     * @author Pradeep
     */
    private function filterDuplicateTags(string $newTags, string $existingTags): string
    {
        if (empty($existingTags)) {
            return $newTags;
        }

        $existingTagsAsArr = $this->trimArrayValues(explode(',', $existingTags));
        $newTagsAsArr = $this->trimArrayValues(explode(',', $newTags));
        $diffTagsAsArr = array_unique(array_merge($newTagsAsArr, $existingTagsAsArr), SORT_REGULAR);
        return implode(',', $diffTagsAsArr);
    }

    /**
     * - trimArrayValues trims the white spaces present in the array values.
     * - uses array_map function for trimming the spaces.
     * @param array $array
     * @return array
     * @author Pradeep
     */
    private function trimArrayValues(array $array)
    {
        if (empty($array)) {
            return [];
        }
        return array_map('trim', $array);
    }

    /**
     * @param $newContactTags
     * @param $existingContactTags
     * @return string
     * @author Pradeep
     */
    public function filterECommerceTags($newContactTags, $existingContactTags): string
    {
        return $this->filterDuplicateTags($newContactTags, $existingContactTags);
    }


    public function updateOptinPermission(array $parsedContact): array
    {
        if (empty($parsedContact)) {
            return [];
        }
        if (!isset($parsedContact[ContactFieldTypes::OptinIp]) || empty($parsedContact[ContactFieldTypes::OptinIp])) {
            $parsedContact[ContactFieldTypes::OptinIp] = $parsedContact[ContactFieldTypes::IP];
            $parsedContact[ContactFieldTypes::OptinTimeStamp] = $this->getCurrentDateTime();
        }

        // If the Contact has DoubleOptIn Permission , then DOIIp and DOITimestamp will be same as OptinIp &
        // OptinTimeStamp
        if ($this->contactPermissionTypes->hasDoubleOptInPermision((int)$parsedContact[ContactFieldTypes::Permission])) {
            $parsedContact[ContactFieldTypes::DOIIp] = $parsedContact[ContactFieldTypes::OptinIp];
            $parsedContact[ContactFieldTypes::DOITimestamp] = $parsedContact[ContactFieldTypes::OptinTimeStamp];
        } else {
            $parsedContact[ContactFieldTypes::DOIIp] = "";
            $parsedContact[ContactFieldTypes::DOITimestamp] = "";
        }
        return $parsedContact;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getCurrentDateTime(): string
    {
        return (string)date('Y-m-d H:i:s');
    }

    /**
     * @param int $newContactPermission
     * @param int $existingContactPermission
     * @param int $defaultCampaignPermission
     * @return int
     * @author Pradeep
     */
    public function assignPermission(
        int $newContactPermission = 0,
        int $existingContactPermission = 0,
        int $defaultCampaignPermission = 0
    ): int
    {

        // None of the contacts have permission.
        if ($newContactPermission === 0 && $existingContactPermission === 0) {
            return $defaultCampaignPermission;
        }

        // new version of contact has higher permission
        if ($newContactPermission > $existingContactPermission) {
            return $newContactPermission;
        }

        // new version of contact has lower permission than Campaign Permission
        // (permission defined in page/webhook wizard).
        if ($newContactPermission < $defaultCampaignPermission) {
            return $defaultCampaignPermission;
        }

        return $newContactPermission;
    }

    /**
     * evaluatePermissionLevel fetches the compares the permission level of the
     * contact and permission level offered by campaign (in wizard)
     * @param int $permissionOfContact
     * @param int $permissionFromCampaign
     * @return int
     * @author Pradeep
     */
    public function evaluatePermissionLevel(int $permissionFromCampaign, int $permissionOfContact = null): int
    {
        $permission = 0;
        if ($permissionOfContact === null || (int)$permissionOfContact < 0) {
            // when no permission from contact is present
            // simple consider the permission from campaign.
            $permission = (int)$permissionFromCampaign;
        } elseif ((int)$permissionOfContact < $permissionFromCampaign) {
            // when permission of the contact has lower permission than
            // permission offered by campaign, then consider the permission from campaign.
            $permission = (int)$permissionFromCampaign;
        } elseif ((int)$permissionOfContact > $permissionFromCampaign) {
            // when permission of the contact has higger permission level than
            // permission offered by the campaign, then consider the permission from contact.
            $permission = (int)$permissionOfContact;
        }
        return $permission;
    }

    /**
     * @param array $data
     * @return array
     * @internal This adds optional parameter field_name for every contact field
     */
    public function addOptionalParametersForContactData(array $data): array
    {
        try{
            //add field name if no field name is provided (v1)
            if (!isset($data['contact']['standard'][0]['field_name'])) {
                //for each standard field
                foreach ($data['contact']['standard'] as $key => $value) {
                    $data['contact']['standard'][$key]['field_name'] = array_key_first($value);
                    $data['contact']['standard'][$key]['field_value'] = $value[0];
                }
                //for each custom field
                foreach ($data['contact']['custom'] as $key => $value) {
                    $data['contact']['custom'][$key]['field_name'] = array_key_first($value);
                    $data['contact']['custom'][$key]['field_value'] = $value[0];
                }
                return $data;
            }
        }catch(Exception $e){
            $this->logger->error( $e->getMessage());
         }

        try{
            //if field name exists and field value might exist (v2) or might not exist (v1.5)
            //for each standard field
            foreach ($data['contact']['standard'] as $key => $value) {
                $fieldName = $value['field_name'];
                $fieldValue = $value['field_value'] ?? $value[$fieldName];
                if (!isset($data['contact']['standard'][$key][$fieldName])) {
                    $data['contact']['standard'][$key][$fieldName] = $fieldValue;
                }
            }
            if (!empty($data['contact']['custom'])) {
                //for each custom field
                foreach ($data['contact']['custom'] as $key => $value) {
                    $fieldName = $value['field_name'];
                    $fieldValue = $value['field_value'] ?? $value[$fieldName];
                    if (!isset($data['contact']['custom'][$key][$fieldName])) {
                        $data['contact']['custom'][$key][$fieldName] = $fieldValue;
                    }
                }
            }
        }catch(Exception $e){
            $this->logger->error( $e->getMessage());
        }

        return $data;

    }
}
<?php

namespace App\Services\AIOServices;

use App\Repository\LanguageLocalizationRepository;
use App\Repository\ObjectRegisterMetaRepository;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LanguageLocalizationService
{

    public const OBJ_REG_META_KEY = 'localized-lang-optin-text';
    /**
     * @var LanguageLocalizationRepository
     * @author Pradeep
     */
    private LanguageLocalizationRepository $languageLocalizationRepository;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;

    public function __construct(
        LanguageLocalizationRepository $languageLocalizationRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger
    ) {
        $this->languageLocalizationRepository = $languageLocalizationRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->logger = $logger;
    }

    /**
     * Fetches the OptinText for the specified CompanyId and IsoCode.
     * If the isoCode is not specified, then the default language is fetched.
     * @param int $companyId
     * @param string $isoCode
     * @return array
     * @author Pradeep
     */
    public function getTranslatedOptinText(int $companyId, string $isoCode = ''): array
    {
        try {
            $isIsoCodeGiven = !empty($isoCode);
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid companyId of language_localization table provided');
            }

            // OptinText for specified IsoCode.
            if ($isIsoCodeGiven) {
                $details = $this->languageLocalizationRepository->getOptinTextForIsoCode($companyId, $isoCode);
            }
            if (empty($details)) {
                // Something went wrong to fetch the OptinText for isoCode,
                // fetch the OptinText for Default language instead.
                $details = $this->languageLocalizationRepository->getDefaultLanguageDetails($companyId);
            }

            if (empty($details) || !isset($details[ 'id' ], $details[ 'objectregister_id' ])) {
                throw new RuntimeException('Unable to find default language details.');
            }
            $localLangConfig = $this->objectRegisterMetaRepository->getMetaValue(
                $details[ 'objectregister_id' ], self::OBJ_REG_META_KEY);
            if (empty($localLangConfig) || !isset($localLangConfig[ 'translated_optn_txt' ])) {
                throw new RuntimeException('Unable to fetch optinText from objectRegMeta.');
            }
            return $localLangConfig[ 'translated_optn_txt' ];
        } catch (RuntimeException|\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return [];
        }
    }
}
<?php

namespace App\Services\AIOServices;

use App\Repository\ContactRepository;
use Psr\Log\LoggerInterface;
use RuntimeException;

class ContactLastSyncService
{

    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactServices;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private ContactRepository $contactRepository;

    /**
     * @param ContactServices $contactServices
     * @param ContactRepository $contactRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ContactServices $contactServices,
        ContactRepository $contactRepository,
        LoggerInterface $logger
    ) {
        $this->contactServices = $contactServices;
        $this->contactRepository = $contactRepository;
        $this->logger = $logger;
    }

    /**
     * @param int $contactId
     * @param string $timeStamp
     * @return bool
     * @author Pradeep
     */
    public function updateById(int $contactId, string $timeStamp = ''): bool
    {
        try {
            if ($contactId <= 0) {
                throw new RuntimeException("invalid contact id provided to update leadSync");
            }// Fetch TimeStamp when not provided.
            if (empty($timeStamp)) {
                $timeStamp = $this->getTimestamp();
            }
            $set = "last_sync='" . $timeStamp . "'";
            $where = "id=" . $contactId;
            return $this->contactRepository->updateLead($set, $where);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getTimestamp()
    {
        return $this->contactServices->getCurrentDateTime();
    }

    /**
     * updateForChunk updates the last synchronized time stamp for the contacts.
     * - last synchronized is updated only when contact is synchronized with eMIO.
     * - updates the timeStamp for the chunk of contacts in one query (not in looping the contacts.)
     * @param array $contactIds
     * @param string $timeStamp
     * @return bool
     * @author Pradeep
     */
    public function updateForChunk(array $contactIds, string $timeStamp = ''): bool
    {
        $this->logger->info('updateForChunk');
        try {
            if (empty($contactIds)) {
                throw new RuntimeException("invalid contact id provided to update leadSync");
            }
            $this->logger->info('Not empty ContactIds');
            // Fetch TimeStamp when not provided.
            if (empty($timeStamp)) {
                $timeStamp = $this->getTimestamp();
            }
            $this->logger->info('updated timestamp ' . $timeStamp);
            $set = "last_sync='" . $timeStamp . "'";
            $contactIdsAsString = $this->asString($contactIds);
            $this->logger->info('updated timestamp ' . $contactIdsAsString);
            if (empty($contactIdsAsString) || strlen($contactIdsAsString) <= 0) {
                throw new RuntimeException('invalid contact Ids as string provided');
            }
            $where = 'id IN (' . $contactIdsAsString . ')';
            $this->logger->info('Final Query ', [$set, $where]);
            return $this->contactRepository->updateLead($set, $where);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param array $contactIds
     * @return string
     * @author Pradeep
     */
    private function asString(array $contactIds): string
    {
        if (empty($contactIds)) {
            return '';
        }
        return implode(",", $contactIds);
    }

}
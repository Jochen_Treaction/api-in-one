<?php

namespace App\Services\AIOServices;

use Psr\Log\LoggerInterface;

class FieldTypeStringServices
{
    public const MAX_STRING_LENGTH = 255;
    public const TYPE_STRING = 'string';

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * truncateString truncates the string by provided max length.
     * - By default maxLenght is 255.
     * @param string $string
     * @param int $maxLength
     * @return false|string
     * @author Pradeep
     */
    public function truncateString(string $string, int $maxLength = self::MAX_STRING_LENGTH)
    {
        $truncatedString = '';
        if (empty($string)) {
            return $truncatedString;
        }
        return strlen($string) >= $maxLength ? substr($string, 0, $maxLength) : $string;
    }

    /**
     * Sanitizes string by removing German other non-supported alpha-numeric characters.
     * @param string $string
     * @return string
     * @author Pradeep
     */
    public function sanitizeString(string $string): string
    {
        // Sanitize string
        $sanitized_string = filter_var($string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        // Replace German Characters
        $sanitized_string = preg_replace(['/Ä/', '/ä/', '/Ö/', '/ö/', '/ß/', '/Ü/', '/ü/', '/\s+/'],
            ['AE', 'ae', 'OE', 'oe', 'ss', 'UE', 'ue', '_'], $sanitized_string);
        // Replace '&' character
        return str_replace(array(
            '&',
            '/',
            '!',
            '"',
            '§',
            '$',
            '%',
            '#',
            '*',
            '+',
            ';',
            ',',
            '>',
            '<',
            '|',
        ),
            array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', ''), $sanitized_string);
    }
}
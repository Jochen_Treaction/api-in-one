<?php

namespace App\Services\AIOServices;

use App\Repository\MIOStandardFieldRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Types\ContactFieldDataTypes;
use Psr\Log\LoggerInterface;

class FieldTypeListServices
{

    public const MIO_TAG = 'MIO_';

    public function __construct(
        MIOStandardFieldRepository $mioStandardFieldRepository,
        FieldTypeStringServices $fieldTypeStringServices,
        ContactFieldServices $contactFieldServices,
        LoggerInterface $logger
    ) {
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
        $this->fieldTypeStringServices = $fieldTypeStringServices;
        $this->contactFieldServices = $contactFieldServices;
        $this->logger = $logger;
    }

    public function convertListToBoolean(string $list, string $mioTag = self::MIO_TAG)
    {
        $booleanList = [];
        $tags = explode(',', $list);
        foreach ($tags as $tag) {
            $tag = $mioTag . $this->fieldTypeStringServices->sanitizeString($tag);
            if (!empty($tag)) {
                $booleanList[] = [
                    'name' => $tag,
                    'value' => 'true',
                ];
            }
        }
        return $booleanList;
    }


    /**
     * if param $list occurs in form of an array within provided string, brackets and newlines are removed
     * @param string $list
     * @param string $mioTag
     * @return array
     */
    public function listToMIOBoolean(string $list, string $mioTag = self::MIO_TAG):array // TODO: @jsr treat $list = '[]' or '[aaaa,bbbb,ccc]'
    {

        $booleanList = [];
        $pattern = '/[\[\]\n]{1,}/';

        // if $list has square brackets, then remove '[' and ']' and return empty array [], if empty string is left, else continue with $list string content
        if( 1 === preg_match($pattern, $list)) { // if $list has brackets
            $list = preg_replace($pattern, '', $list);
            if(empty($list)) {
                return $booleanList; // []
            }
        }

        $tags = explode(',', $list);
        foreach ($tags as $tag) {
            $tag = $mioTag . $this->fieldTypeStringServices->sanitizeString($tag);
            if (!empty($tag)) {
                $booleanList[ $tag ] = 'true';
            }
        }
        return $booleanList;
    }

    /**
     * Filters the contact payload for datatype === list,
     * - fetches the list and explode the list with ','
     * - append each and every field of list with 'MIO_'.
     * - return all the appended fields as array.
     * @param array $contact
     * @return array
     * @author Pradeep
     */
    public function fetchFieldsOfTypeList(array $contact): array
    {
        $list = [];
        // ContactFieldType === 'Standard or CustomFields'
        foreach ($contact as $contactFieldType) {
            if (empty($contactFieldType)) {
                continue;
            }
            $tagsForEachFieldType = [];
            foreach ($contactFieldType as $contactField) {
                if (!isset($contactField[ 'datatype' ])) {
                    continue;
                }
                if ($contactField[ 'datatype' ] !== ContactFieldDataTypes::FIELD_TYPE_LIST) {
                    continue;
                }
                $fieldName = $contactField[ 'field_name' ];
                $fieldValue = $contactField[ $fieldName ];
                if (empty($fieldValue)) {
                    continue;
                }
                $tags = $this->appendListWithMIO($fieldValue);
                $tagsForEachFieldType = array_unique(array_merge($tagsForEachFieldType, $tags), SORT_REGULAR);
            }
            $list = array_unique(array_merge($list, $tagsForEachFieldType), SORT_REGULAR);
        }

        return $list;
    }

    public function appendListWithMIO(string $list): array
    {
        $appendList = [];
        $tags = explode(',', $list);
        foreach ($tags as $tag) {
            $tag = self::MIO_TAG . $this->fieldTypeStringServices->sanitizeString($tag);
            $appendList[] = $tag;
        }
        return $appendList;
    }

    private function appendMIOTag(string $string)
    {
        return strlen($string) > 0 ? self::MIO_TAG . $string : '';
    }
}

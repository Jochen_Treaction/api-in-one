<?php


namespace App\Services\AIOServices;


use App\Repository\BlackListDomainRepository;
use App\Repository\CampaignParamsRepository;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\StatusdefRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Repository\WhiteListDomainRepository;
use App\Services\AIOServices\MSCIOBlackListDomains;
use App\Services\AIOServices\MSCIOCampaignServices;
use App\Services\AIOServices\MSCIOConfigurationTargets;
use App\Services\AIOServices\MSCIOConstants;
use App\Services\AIOServices\MSCIOFraud;
use App\Services\AIOServices\MSCIOLeads;
use App\Services\AIOServices\MSCIOWhiteListedDomains;
use App\Services\PrivateServices\MIOEmailValidation;
use Exception;
use JSONException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use WhichBrowser\Parser;

class AIOFraudDetectionService
{
    /**
     * @var CampaignRepository
     * @author Pradeep
     */
    private $campaignRepository;
    /**
     * @var CampaignParamsRepository
     * @author Pradeep
     */
    private $campaignParamsRepository;
    /**
     * @var BlackListDomainRepository
     * @author Pradeep
     */
    private $blackListDomainRepository;
    /**
     * @var WhiteListDomainRepository
     * @author Pradeep
     */
    private $whiteListDomainRepository;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private $objectRegisterMetaRepository;
    private $fraudType;
    private $contactStatus;
    /**
     * @var int
     * @author Pradeep
     */
    private $companyId;

    /**
     * AIOFraudDetectionService constructor.
     * @param CampaignParamsRepository $campaignParamsRepository
     * @param BlackListDomainRepository $blackListDomains
     * @param ContactRepository $contactRepository
     * @param WhiteListDomainRepository $whiteListDomains
     * @param LoggerInterface $logger
     */
    public function __construct(
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        BlackListDomainRepository $blackListDomains,
        WhiteListDomainRepository $whiteListDomains,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger
    ) {
        $this->campaignRepository = $campaignRepository;
        $this->companyRepository = $companyRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->blackListDomainRepository = $blackListDomains;
        $this->whiteListDomainRepository = $whiteListDomains;
        $this->logger = $logger;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getFraudType(): string
    {
        return $this->fraudType;
    }

    /**
     * @param string $fraudType
     * @author Pradeep
     */
    protected function setFraudType(string $fraudType): void
    {
        $this->fraudType = $fraudType;
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    public function getContactStatus(): ?string
    {
        return $this->contactStatus;
    }

    /**
     * @param string $status
     * @author Pradeep
     */
    protected function setContactStatus(string $status): void
    {
        $this->contactStatus = $status;
    }

    /**
     * @param int $campaignId
     * @param int $companyId
     * @param string $eMail
     * @param int|null $countOfContactsWithSameIpAddr
     * @param int|null $countOfContactsWithSameDomain
     * @return bool
     * @author Pradeep
     */
    public function isFraudContact(
        int $companyId,
        int $campaignId,
        string $eMail,
        int $countOfContactsWithSameIpAddr = null,
        int $countOfContactsWithSameDomain = null
    ): bool {
        $statusFraud = StatusdefRepository::FRAUD;
        $statusActive = StatusdefRepository::ACTIVE;
        $status = false;

        try {
            if (!$this->initializeFraudService($companyId, $campaignId, $eMail)) {
                throw new RuntimeException('Invalid configuration to initialize fraud service');
            }
            $domainName = $this->getDomainName($eMail);
            if (empty($domainName)) {
                throw new RuntimeException('Invalid domain name');
            }
            // White listed domains are always 'active'.
            if ($this->isDomainWhiteListed($domainName)) {
                $this->setContactStatus($statusActive);
                return true;
            }
            // TradeShow
            if ($this->isTradeShowEnabled()) {
                $this->setContactStatus($statusActive);
                return true;
            }
            // Check if the emails for the same domain is exceeded,
            if ($this->isMaxLeadsPerDomainExceeded($countOfContactsWithSameDomain)) {
                $this->setContactStatus($statusFraud);
                $this->setFraudType('Max Domain Limit Exceeded');
                return true;
            }
            // Checks if the EMail Address is valid or not.
            if (!($this->isEmailAddrValid($eMail)) || !$this->eMailAddrHasNoPlusSign($eMail)) {
                $this->setContactStatus($statusFraud);
                $this->setFraudType('Invalid Email Address');
                return true;
            }
            // Max ipAddress for Page in a day.
            if ($this->isMaxIpAddressExceeded($countOfContactsWithSameIpAddr)) {
                $this->setContactStatus($statusFraud);
                $this->setFraudType('Max IpAddress Limit Exceeded');
                return true;
            }
            return false;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * getDomainName fetches the domain name from an EMAIL.
     * @param string $eMail
     * @return string|null
     * @author Pradeep
     */
    protected function getDomainName(string $eMail): ?string
    {
        $domain = null;
        try {
            if (empty($eMail)) {
                throw new RuntimeException('invalid Email address provided');
            }
            $parseMail = explode('@', $eMail);
            if (is_array($parseMail) && !empty($parseMail[ 1 ])) {
                $domain = $parseMail[ 1 ];
            }
            return $domain;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * isDomainWhiteListed helper function checks whether the domain is whitelisted or not.
     * true - if domain is white listed.
     * false - if domain is not white listed.
     * @param string $domainName
     * @return bool
     * @throws Exception
     * @author Pradeep
     */
    private function isDomainWhiteListed(string $domainName): bool
    {
        try {
            if (empty($domainName)) {
                throw new RuntimeException('Invalid Domain Name');
            }
            return $this->whiteListDomainRepository->isWhitelisted($domainName);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $companyId
     * @param int $campaignId
     * @param string $eMail
     * @return bool
     * @author Pradeep
     */
    protected function initializeFraudService(int $companyId, int $campaignId, string $eMail): bool
    {
        try {
            if (!$this->setCampaignId($campaignId) || !$this->setCompanyId($companyId) || empty($eMail)) {
                throw new RuntimeException('Invalid FraudConfiguration or Email address provided');
            }
            // get fraud settings of the company.
            $fraudSettings = $this->getFraudSettingsForCompany($companyId);

            if ($fraudSettings === null) {
                throw new RuntimeException('Failed to get FraudSetting for Campaign and Company');
            }
            // set the fraud settings.
            $this->setFraudConfig($fraudSettings);
            return true;
        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $campaignId
     * @author Pradeep
     */
    protected function setCampaignId(int $campaignId):bool
    {
        $status = false;
        if($campaignId > 0) {
            $this->campaignId = $campaignId;
            $status = true;
        }
        return $status;
    }

    private function setCompanyId(int $companyId) :bool
    {
        $status = false;
        if($companyId > 0) {
            $this->companyId = $companyId;
            $status = true;
        }
        return $status;
    }

    /**
     * @param int $campaignId
     * @return array|null
     * @author Pradeep
     */
    private function getFraudSettingsForCampaign(int $campaignId): ?array
    {
        try {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
            $campaignDetails = $this->campaignRepository->getCampaignDetailsById($campaignId);
            if (empty($campaignDetails) || !isset($campaignDetails['objectregister_id']) || (int)$campaignDetails['objectregister_id'] <= 0) {
                throw new RuntimeException('Failed to getMetaValue CampaignDetails');
            }
            //$campaignFraudSettings = $this->objectRegisterMetaRepository->getMetaValue((int)$campaignDetails['objectregister_id'], $metaKey);
            return $this->objectRegisterMetaRepository->getMetaValue($campaignDetails[ 'objectregister_id' ], $metaKey);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param array $fraudConfigForCampaign
     * @author Pradeep
     */
    protected function setFraudConfig(array $fraudConfigForCampaign): void
    {
        $this->fraud = $fraudConfigForCampaign;
    }

    /**
     * @return bool
     * @author Pradeep
     */
    private function isTradeShowEnabled(): bool
    {
        try {
            $fraudConfig = $this->getFraudConfig();
            if ($fraudConfig === null || !isset($fraudConfig[ 'trade_show' ])) {
                throw new RuntimeException('Failed to fetch TradeShow setting');
            }
            return $fraudConfig[ 'trade_show' ];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getFraudConfig(): ?array
    {
        if (isset($this->fraud) && !empty($this->fraud)) {
            return $this->fraud;
        }
        return null;
    }

    /**
     * @param int $countOfContactsWithSameDomain
     * @return bool
     * @author Pradeep
     */
    private function isMaxLeadsPerDomainExceeded(int $countOfContactsWithSameDomain): bool
    {
        $status = false;
        try {
            $fraudConfig = $this->getFraudConfig();
            if ($countOfContactsWithSameDomain <= 0 || empty($fraudConfig[ 'maxLeadsPerDomain' ])) {
                throw new RuntimeException('Invalid FraudConfigurations provided');
            }
            if ($countOfContactsWithSameDomain > (int)$fraudConfig[ 'maxLeadsPerDomain' ]) {
                $status = true;
            }
            return $status;
        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    private function isEmailAddrValid(string $email): bool
    {
        $em_check = new MIOEmailValidation($email);
        return $em_check->isEmailAddressValid();
    }

    /**
     * @param string $email
     * @return bool
     * Currently no email address with '+' are accepted.
     */
    private function eMailAddrHasNoPlusSign(string $email): bool
    {
        $isValidEmail = false;
        try {
            if (empty($email)) {
                return $isValidEmail;
            }
            $parse_email = explode('@', $email);
            //$parse_email = explode('@', 'test+12@gmail.com');
            if (!isset($parse_email[ 0 ], $parse_email[ 1 ]) || empty($parse_email)) {
                return $isValidEmail;
            }
            $check_plus_sign = explode('+', $parse_email[ 0 ]);
            if (count($check_plus_sign) === 1) {
                $isValidEmail = true;
            }
            return $isValidEmail;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $isValidEmail;
        }
    }

    /**
     * @param int $countOfContactsWithSameIpAddr
     * @return bool
     * @author Pradeep
     */
    private function isMaxIpAddressExceeded(int $countOfContactsWithSameIpAddr): bool
    {
        $status = false;
        try {
            $fraudConfig = $this->getFraudConfig();
            if (empty($fraudConfig[ 'maxLeadsPerIp' ])) {
                throw new RuntimeException('Invalid ipAddress or FraudConfigurations provided');
            }
            if ($countOfContactsWithSameIpAddr > (int)$fraudConfig[ 'maxLeadsPerIp' ]) {
                $status = true;
            }
            return $status;
        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @return int|null
     * @author Pradeep
     */
    public function getCampaignId(): ?int
    {
        if (!isset($this->campaignId) || $this->campaignId <= 0) {
            return null;
        }
        return $this->campaignId;
    }

    /**
     * @param int $campaign_id
     * @param int $project_id
     * @param string $email
     * @param string $ipAddress
     * @return string
     * @author Pradeep
     */
    public function fetchLeadStatusType(
        int $campaign_id,
        int $project_id,
        string $email = '',
        string $ipAddress = ''
    ): string {
        $fraud_type = MSCIOConstants::LD_STATUS_TYP_NONE;
        if ($campaign_id <= 0 || empty($email)) {
            return $fraud_type;
        }
        // getFraudSettings for the campaign
        $fraud_prevention_level = $this->getFraudPreventionLevel($project_id);
        if ($fraud_prevention_level === null) {
            throw new RuntimeException('Invalid Fraud Prevention Level.');
        }
        $this->fetchFraudType($email, $ipAddress, $campaign_id, $fraud_prevention_level);
        /*        $campaign_fraud_settings = $this->fetchFraudConfigForCampaign($campaign_id);
                $campaign_lead[ MSCIOConstants::SD_PRM_LEAD_IP_ADDR ] = $campaign_lead[ MSCIOConstants::SD_PRM_LEAD_IP_ADDR ] ?? '';
                // Fraud Detection is active only with tradeShowMode is inactive.
                if (!isset($campaign_fraud_settings[ MSCIOConstants::INPT_FLD_TRADESHOWMODE ]) ||
                    ($campaign_fraud_settings[ MSCIOConstants::INPT_FLD_TRADESHOWMODE ] !== 'on')) {
                    $fraud_type = $this->fetchFraudType(
                        $campaign_lead[ MSCIOConstants::SD_PRM_EMAIL ],
                        $campaign_lead[ MSCIOConstants::SD_PRM_LEAD_IP_ADDR ],
                        $campaign_id, $campaign_fraud_settings
                    );
                }*/

        return $fraud_type;
    }

    /**
     * @param int $project_id
     * @return string|null
     * @author Pradeep
     */
    public function getFraudPreventionLevel(int $project_id): ?string
    {
        $fraud_prevention_level = null;
        if ($project_id <= 0) {
            throw new RuntimeException('Invalid project id');
        }
        $configurationTargetProject = UniqueObjectTypeRepository::PROJECT;
        $campaignParams = $this->campaignParamsRepository->getCampaignParams($project_id, $configurationTargetProject);
        if ($campaignParams === null || !isset($campaignParams[ 'advancedsettings' ][ 'fraud_prevention_level' ])) {
            return $fraud_prevention_level;
        }
        return $campaignParams[ 'advancedsettings' ][ 'fraud_prevention_level' ];
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param string $email
     * @param string $ip_addr
     * @param int $campaign_id
     * @param string $fraud_configuration
     * @return string
     * @author Pradeep
     */
    private function fetchFraudType(
        string $email,
        string $ip_addr,
        int $campaign_id,
        string $fraud_configuration
    ): string {
        $fraud_type = MSCIOConstants::LD_STATUS_TYP_NONE;

        try {
            if (empty($email) || $campaign_id <= 0 || empty($fraud_configuration)) {
                return $fraud_type;
            }
            $email_domain = explode('@', $email)[ 1 ];
            if ($this->blackListDomainRepository->isDomainBlacklisted($email_domain)) {
                $fraud_type = MSCIOConstants::LD_STATUS_TYP_BLACKLISTED;
            } elseif (!$this->whiteListDomainRepository->isWhitelisted($email_domain) &&
                      ($this->eMailAddrHasNoPlusSign($email) ||
                       !$this->isEmailAddrValid($email))) {
                $fraud_type = MSCIOConstants::LD_STATUS_TYP_NON_EXISTING;
                //Todo FraudLimit for Max limit has to be getMetaValue from campaignsettings.
            } elseif ($this->isFraudLimitExceeded($campaign_id, $email,
                5)) {
                $fraud_type = MSCIOConstants::LD_STATUS_TYP_DM_LMT_EXCEED;

            } elseif (isset($ip_addr) && !$this->whiteListDomainRepository->isWhitelisted($email_domain) &&
                      $this->isIpAddrFraud($campaign_id, $ip_addr,
                          5)) {
                $fraud_type = MSCIOConstants::LD_STATUS_TYP_IP_LMT_EXCEED;

            } else {
                $fraud_type = MSCIOConstants::LD_STATUS_TYP_NONE;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $fraud_type;
    }

    /**
     * @param int $campaign_id
     * @param string $email
     * @param int $fraud_limit_email
     * @return bool
     * @author Pradeep
     */
    private function isFraudLimitExceeded(int $campaign_id, string $email, int $fraud_limit_email): bool
    {
        $is_exceeded = false;
        $leads = new MSCIOLeads();

        try {
            if ($campaign_id <= 0 || empty($email) || $fraud_limit_email < 0) {
                return $is_exceeded;
            }
            $today = date('Y-m-d');
            $domain = explode('@', $email)[ 1 ];
            if ($leads->getDuplicateDomainCount($campaign_id, $domain, $today) >= $fraud_limit_email) {
                $is_exceeded = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $is_exceeded;
    }

    /**
     * @param int $campaign_id
     * @param string $ip_addr
     * @param int $fraud_limit_ipaddr
     * @return bool
     */
    private function isIpAddrFraud(int $campaign_id, string $ip_addr, int $fraud_limit_ipaddr): bool
    {
        $is_fraud = false;

        try {
            if ($campaign_id <= 0 || !filter_var($ip_addr, FILTER_VALIDATE_IP) || $fraud_limit_ipaddr < 0) {
                return $is_fraud;
            }
            $today = date('Y-m-d');
            $leads = new MSCIOLeads();
            $count = $leads->checkForDuplicateIpAddr($campaign_id, $ip_addr, $today);
            if ($count >= $fraud_limit_ipaddr) {
                $is_fraud = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $is_fraud;
    }

    /**
     * @param int $campaign_id
     * @return array
     */
    public function fetchFraudConfigForCampaign(int $campaign_id): array
    {
        $cio_fraud_settings = [];
        $cnf_trgt = null; //MSCIOConfigurationTargets::CNF_TRGT_FRAUD;
        try {
            // removed WebSpider Microservice.
            $ms_cio_camp = null;//new MSCIOCampaignServices();
            $cio_fraud = $ms_cio_camp->getCampaignParmsForTarget($campaign_id, $cnf_trgt);
            if (!is_array($cio_fraud) || !isset($cio_fraud[ 'id' ], $cio_fraud[ 'value' ]) || empty($cio_fraud[ 'value' ])) {
                return $cio_fraud_settings;
            }
            $cio_fraud_settings = json_decode($cio_fraud[ 'value' ], true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $cio_fraud_settings;
    }

    public function setLogger(LoggerInterface $logger)
    {
        // TODO: Implement setLogger() method.
    }



    /**
     * @return false|string
     * @author Pradeep
     */
    private function getCurrentDate()
    {
        return date("Y-m-d");
    }


    /**
     * @param int $campaign_id
     * @param array $campaign_lead
     * @param string $fraud_type
     * @return bool
     * @author Pradeep
     */
    private function postLeadDetailsAsFraud(int $campaign_id, array $campaign_lead, string $fraud_type): bool
    {
        $status = false;
        $browser_details = [];
        $fraud = new MSCIOFraud();
        if ($campaign_id <= 0 || empty($campaign_lead) || empty($fraud_type)) {
            return $status;
        }
        // Collect Browser details
        if (!empty($campaign_lead[ 'http_user_agent' ])) {
            $b_device = new Parser($campaign_lead[ 'http_user_agent' ]);
            $browser_details[ 'deviceBrowser' ] = $b_device->browser->name;
            $browser_details[ 'deviceOs' ] = $b_device->os->name;
        }

        return $fraud->insertFraudDetails($fraud_type, $campaign_lead[ 'email' ], $campaign_lead[ 'ip' ], $campaign_id,
            $browser_details);
    }

    /**
     * getFraudSettingsForCompany Gets Fraud settings from DB for a company.
     * @param int $companyId
     * @return array|null
     * @author Pradeep
     */
    private function getFraudSettingsForCompany(int $companyId):?array
    {
        try {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyFraudSettings;
            $companyDetails = $this->companyRepository->getCompanyDetails($companyId);
            if ($companyDetails === null || (int)$companyDetails[ 'objectregister_id' ] <= 0) {
                throw new RuntimeException('Failed to getMetaValue CompanyDetails ');
            }
            return $this->objectRegisterMetaRepository->getMetaValue($companyDetails[ 'objectregister_id' ], $metaKey);
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


}
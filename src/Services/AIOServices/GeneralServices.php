<?php


namespace App\Services\AIOServices;


use App\Repository\ObjectRegisterMetaRepository;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class GeneralServices
{

    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private $objectRegisterMetaRepository;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(ObjectRegisterMetaRepository $objectRegisterMetaRepository, LoggerInterface $logger)
    {
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->logger = $logger;
    }

    /**
     * @param string $api_key
     * @param int $companyId
     * @return bool
     * @author Pradeep
     */
    public function validateCIOAPIKeyWithCompanyId(string $api_key, int $companyId): bool
    {
        $status = false;
        try {
            if (empty($api_key) || $companyId <= 0) {
                throw new RuntimeException('Invalid APIKey or Company Id provided');
            }
            $apikeyFromDB = $this->objectRegisterMetaRepository->getAccountAPIKey($companyId);
            if ($apikeyFromDB === $api_key) {
                $status = true;
            }
            return $status;
        } catch (RuntimeException |Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }
}
<?php


namespace App\Services\AIOServices;


use App\Services\AIOServices\MSCIOCampaignServices;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class AIOService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected $campaign_id;
    private $cio_mp;
    private $acc_id;

    /**
     * @param array $campaign
     * @return bool
     */
    public function isCampaignActive(array $campaign): bool
    {
        if (empty($campaign) || !isset($campaign[ 'status' ]) || $campaign[ 'status' ] !== 'active') {
            return false;
        }
        return true;
    }

    /**
     * @param string $encrypted_data
     * @return array
     * @author Pradeep
     */
    public function decryptRequestData(string $encrypted_data): array
    {
        $data = [];
        if (empty($encrypted_data)) {
            return $data;
        }
        $data = $this->formatRequestData($encrypted_data);
        if (empty($data) || empty($data[ 'campaign_token' ])) {
            return $data;
        }
        $ids = $this->decryptAndFetchIds($data[ 'campaign_token' ]);
        if (!empty($ids) && (int)$ids[ 'campaign_id' ] && (int)$ids[ 'company_id' ]) {
            $data[ 'campaign_id' ]  =    (int)  $ids[ 'campaign_id' ];
            $data[ 'company_id' ]   =    (int)  $ids[ 'company_id' ];
        }

        return $data;
    }

    /**
     * @param string $req_data
     * @return array
     */
    public function formatRequestData(string $req_data): array
    {
        $data = [];
        if (empty($req_data)) {
            return $data;
        }
        try {
            $de_data = base64_decode($req_data);// Decode data
            $de_arr = json_decode($de_data, true, 512, JSON_OBJECT_AS_ARRAY);// Convert Json to Arr
            if (!empty($de_arr)) {
                foreach ($de_arr as $arr) {
                    $data[ $arr[ 'name' ] ] = $arr[ 'value' ];
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $data;
    }

    /**
     * @param string $campaign_token
     * @return array
     * @author Pradeep
     */
    public function decryptAndFetchIds(string $campaign_token): array
    {
        $ids = [];
        $token_arr = explode('|', ($campaign_token));
        $md5_str = md5($token_arr[ 0 ] . '|' . $token_arr[ 1 ] . '|' . $token_arr[ 2 ] . '|' . $token_arr[ 3 ]);
        // last part of the array is always an md5 string.
        if ($md5_str !== $token_arr[ 4 ]) {
            return $ids;
        }
        foreach ($token_arr as $str) {
            // Ignore md5 string
            if ($str === $md5_str) {
                continue;
            }
            $str_decoded = base64_decode(($str));
            // Ignore salt and pepper.
            if ($str_decoded === 'cio' || $str_decoded === 'treaction') {
                continue;
            }
            $value = explode('=', $str_decoded);
            // if ($value[ 0 ] === 'campaign_id' || $value[ 0 ] === 'company_id') { // error 2020-03-20

            $this->logger->info('DECRYPT AND FETCH IDS: ', [$value[ 0 ] => $value[ 1 ], __METHOD__, __LINE__]);

            if ($value[ 0 ] === 'campaign_id' || $value[ 0 ] === 'company_id') {
                $ids[ $value[ 0 ] ] = octdec($value[ 1 ]); // error
                $this->logger->info('DECRYPT AND FETCH IDS: ',
                    [$value[ 0 ] => octdec($value[ 1 ]), __METHOD__, __LINE__]);
            }
        }
        return $ids;
    }

    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    public function getMappingForCampaign(int $campaign_id): array
    {
        $cio_mp_arr = [];
        // removed WebSpider microservice.
        $ms_cio_camp =null; new MSCIOCampaignServices();

        $cio_mp = $ms_cio_camp->getCampaignParmsForTarget($campaign_id, 'Mapping');

        if (!is_array($cio_mp) || empty($cio_mp)) {
            return $cio_mp_arr;
        }

        $cio_mp[ 'value' ] = json_decode($cio_mp[ 'value' ], true, 512, JSON_OBJECT_AS_ARRAY);

        if (isset($cio_mp[ 'value' ][ 'mappingTable' ])) {
            $cio_mp_tbl_js = base64_decode($cio_mp[ 'value' ][ 'mappingTable' ]);
            $cio_mp_tbl_arr = json_decode($cio_mp_tbl_js, true, 512, JSON_OBJECT_AS_ARRAY);
            foreach ($cio_mp_tbl_arr as $k => $v) {
                if (!empty($v[ 'html form' ]) && !empty($v[ 'campaign-in-one' ])) {
                    $cio_mp_arr[ $v[ 'campaign-in-one' ] ] = $v;
                }
            }
        }

        // $this->logger->info('$cio_mp_arr = '.json_encode(['$cio_mp_arr' =>$cio_mp_arr]),[__METHOD__,__LINE__]);
        return $cio_mp_arr;
    }
}

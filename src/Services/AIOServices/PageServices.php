<?php


namespace App\Services\AIOServices;

use Psr\Log\LoggerInterface;

class PageServices
{

    private const PRIVACY_POLICY_URL = "url_privacy_policy";
    private const PRIVACY_IMPRINT = "url_imprint";
    private const PRIVACY_GTC = "url_gtc";
    private const PRIVACY_WEBSITE = "website";

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * @param array $companyDetails
     * @return array
     * @author aki
     */
    public function formatRequiredPolicyDetailsForClient(array $companyDetails): array
    {
        $policyInfo = [];
        //basic check
        if (empty($companyDetails)) {
            return $policyInfo;
        }
        //set the array
        $policyInfo[self::PRIVACY_POLICY_URL] = $companyDetails[self::PRIVACY_POLICY_URL];
        $policyInfo[self::PRIVACY_IMPRINT] = $companyDetails[self::PRIVACY_IMPRINT];
        $policyInfo[self::PRIVACY_GTC] = $companyDetails[self::PRIVACY_GTC];
        $policyInfo[self::PRIVACY_WEBSITE] = $companyDetails[self::PRIVACY_WEBSITE];
        //return the array
        $this->logger->info("policy info for client", [$policyInfo, __FUNCTION__, __LINE__]);
        return $policyInfo;

    }

}
<?php


namespace App\Services\AIOServices;


use App\Repository\CampaignRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\StatusdefRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Repository\Workflow\WorkflowRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\MSHubspotServices\MSHubspotServices;
use App\Services\MSMailServices\MSMailServices;
use App\Services\MSMIOServices\MSMIOServices;
use App\Types\ContactFieldDataTypes;
use App\Types\ContactPermissionTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;

class IntegrationServices
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var IntegrationsRepository
     * @author Pradeep
     */
    private $integrationsRepository;
    /**
     * @var MSHubspotServices
     * @author Pradeep
     */
    private MSHubspotServices $hubspotServices;
    /**
     * @var StandardFieldMappingRepository
     * @author Pradeep
     */
    private StandardFieldMappingRepository $standardFieldMappingRepository;
    /**
     * @var MSMIOServices
     * @author Pradeep
     */
    private MSMIOServices $msmioServices;
    /**
     * @var CampaignRepository
     * @author Pradeep
     */
    private CampaignRepository $campaignRepository;
    /**
     * @var MSMailServices
     * @author Pradeep
     */
    private MSMailServices $mailServices;
    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactServices;
    /**
     * @var FieldTypeStringServices
     * @author Pradeep
     */
    private FieldTypeStringServices $fieldTypeStringServices;
    /**
     * @var FieldTypeListServices
     * @author Pradeep
     */
    private FieldTypeListServices $fieldTypeListServices;

    public function __construct(
        LoggerInterface $logger,
        IntegrationsRepository $integrationsRepository,
        MSMailServices $mailServices,
        MSMIOServices $msmioServices,
        ContactServices $contactServices,
        CampaignRepository $campaignRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        MSHubspotServices $hubspotServices,
        WorkflowRepository $workflowRepository,
        FieldTypeStringServices $fieldTypeStringServices,
        ContactFieldServices $contactFieldServices,
        FieldTypeListServices $fieldTypeListServices,
        ContactPermissionTypes $contactPermissionTypes
    ) {
        $this->logger = $logger;
        $this->integrationsRepository = $integrationsRepository;
        $this->mailServices = $mailServices;
        $this->campaignRepository = $campaignRepository;
        $this->contactServices = $contactServices;
        $this->msmioServices = $msmioServices;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
        $this->hubspotServices = $hubspotServices;
        $this->workflowRepository = $workflowRepository;
        $this->fieldTypeStringServices = $fieldTypeStringServices;
        $this->contactFieldServices = $contactFieldServices;
        $this->fieldTypeListServices = $fieldTypeListServices;
        $this->contactPermissionTypes = $contactPermissionTypes;
    }

    /**
     * integrates Contact Information to different Integration Services(i,e eMIO, Hubspot)
     *  - In case if the workflow has already the processHook for the related integration services
     *    then the specific integration is ignored.
     *  - In case if no workflow is defined then all the integrations which are configured for the
     *    account will be executed.
     *  - eMIO : Contact is created with Default DOI
     *  - Hubspot : Contact and Deal is created.
     *
     * @param int $companyId
     * @param int $campaignId
     * @param array $contact
     * @param int $workflowId
     * @return bool|null
     * @author Pradeep
     * @internal
     */
    public function integrate(int $companyId, int $campaignId, array $contact, int $workflowId): ?bool
    {
        $status = true;
        try {
            if ($companyId <= 0 || $campaignId <= 0 || empty($contact)) {
                throw new RuntimeException("Invalid Company Id or CampaignId or ContactInformation provided");
            }
            $integrations = $this->integrationsRepository->getIntegrationsForCompany($companyId);
            $this->logger->info('integrations ' . json_encode($integrations, JSON_THROW_ON_ERROR),
                [__METHOD__, __LINE__, __FUNCTION__]);

            if (!empty($integrations[ UniqueObjectTypeRepository::EMAILINONE ]) &&
                !$this->iseMIOProcessHookDefined($campaignId, $workflowId)
            ) {
                // Call service to integrate to eMIO
                $status = $this->emailInOne($companyId, $campaignId, $contact,
                    $integrations[ UniqueObjectTypeRepository::EMAILINONE ]);
            }
            if (isset($integrations[ UniqueObjectTypeRepository::HUBSPOT ])) {
                // call service to create contact in hubspot
                $status = $this->hubspot($companyId, $campaignId, $contact,
                    $integrations[ UniqueObjectTypeRepository::HUBSPOT ]);
            }
            return $status;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * Helper function to check whether eMIO ProcessHook is defined or not.
     * @param int $campaignId
     * @param int $workflowId
     * @return bool
     * @author Pradeep
     */
    private function iseMIOProcessHookDefined(int $campaignId, int $workflowId): bool
    {
        return $this->workflowRepository->hasEMIOProcessHook($campaignId, $workflowId);
    }

    /**
     * @param int $companyId
     * @param int $campaignId
     * @param array $contact
     * @param array $eMIOConfig
     * @return bool|null
     * @author Pradeep
     * @internal Integrates Contact information to eMIO.
     */
    public function eMailInOne(int $companyId, int $campaignId, array $contact, array $eMIOConfig): ?bool
    {
        $typeOfIntegration = MSMIOServices::ITYPE_DOI;
        $permission = 0;

        // check if the permission is present in the payload of the contact.
        $perOfContact = $this->contactFieldServices->getValue($contact, 'permission');
        // fetch the permission from the campaign (page or webhook)
        $perFromCampaign = $this->campaignRepository->getPermission($campaignId);

        // evaluate both the permission levels.
        $permission = $this->contactServices->evaluatePermissionLevel($perFromCampaign, $perOfContact);

        if ($this->contactPermissionTypes->hasDoubleOptInPermision($permission)) {
            $doiMailingKey = true;
        } else {
            $doiMailingKey = $eMIOConfig[ 'settings' ][ 'doi_mailing' ] ?? '';
        }

        $contactEventId = '';
        $marketingAutomationId = '';

        if (empty($contact) || !($this->msmioServices->setAPIKey($eMIOConfig[ 'settings' ][ 'apikey' ]))) {
            throw new RuntimeException('Invalid Contact or eMIO APIKey provided');
        }
        $MIOCustomFields = !empty($eMIOConfig[ 'settings' ][ 'mapping' ]) ? $this->simpleDecrypt($eMIOConfig[ 'settings' ][ 'mapping' ]) : [];
        // create custom field for each value of the list type(eg : smart_tags, product_attributes)
        $this->msmioServices->syncContactFieldOfDataTypeListForSingleContact($contact);
        $this->logger->info('MIOCustomFields at eMailInOne : ' . json_encode($MIOCustomFields));
        $eMIOContact = $this->mapToSystemFields($contact, UniqueObjectTypeRepository::EMAILINONE, $MIOCustomFields);

        $this->logger->info('eMIO before sending ' . json_encode([
                'eMIOContact' => $eMIOContact,
                'typeOfIntegration' => $typeOfIntegration,
                'permission' => $permission,
                'doiMailingKey' => $doiMailingKey,
                'contactEventId' => $contactEventId,
                'marketingAutomationId' => $marketingAutomationId,
            ], JSON_THROW_ON_ERROR));

        //return true;
        return $this->msmioServices->createContact($eMIOContact, $typeOfIntegration, $permission, $doiMailingKey,
            $contactEventId, $marketingAutomationId);
    }

    /**
     * evaluatePermissionLevel fetches the compares the permission level of the
     * contact and permission level offered by campaign (in wizard)
     * @param int $permissionFromCampaign
     * @param int|null $permissionOfContact
     * @return int
     * @author Pradeep
     * @depreacted moved the function to contactServices.
     */
    private function evaluatePermissionLevel(int $permissionFromCampaign, int $permissionOfContact = null): int
    {
        $permission = 0;
        if ($permissionOfContact === null || (int)$permissionOfContact < 0) {
            // when no permission from contact is present
            // simple consider the permission from campaign.
            $permission = (int)$permissionFromCampaign;
        } elseif ((int)$permissionOfContact < $permissionFromCampaign) {
            // when permission of the contact has lower permission than
            // permission offered by campaign, then consider the permission from campaign.
            $permission = (int)$permissionFromCampaign;
        } elseif ((int)$permissionOfContact > $permissionFromCampaign) {
            // when permission of the contact has higger permission level than
            // permission offered by the campaign, then consider the permission from contact.
            $permission = (int)$permissionOfContact;
        }
        return $permission;
    }

    /**
     * @param string $encryptedSting
     * @return array|null
     * @author Pradeep
     */
    public function simpleDecrypt(string $encryptedSting): ?array
    {
        if (empty($encryptedSting)) {
            return [];
        }
        try {
            return json_decode(base64_decode($encryptedSting), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return [];
        }
    }

    /**
     * @param array $contact
     * @param string $systemName
     * @param array $mioCustomFields
     * @return array|null
     * @author Pradeep
     * @internal Converts MIO contact fields to eMIO contact fields.i,e
     *      'EMAIL' => string 'test@treaction.de' (length=26)
     *      'SALUTATION' => string 'Frau' (length=4)
     *      'FIRSTNAME' => string 'testName' (length=7)
     *      'LASTNAME' => string 'testLastName' (length=5)
     *      'MIO_url' => string 'testUrl' (length=65)
     *      'MIO_ip' => string 'testIP' (length=12)
     *      'MIO_device_browser' => string 'Mozilla Firefox' (length=15)
     *      'MIO_device_os' => string 'Microsoft Windows 10' (length=20)
     */
    public function mapToSystemFields(array $contact, string $systemName, array $mioCustomFields = []): ?array
    {
        $mapping = [];
        $this->logger->info('mapToSystemFields ' . json_encode(['contact' => $contact]));
        try {
            if (empty($contact) || empty($contact[ 'contact' ][ 'standard' ]) || empty($systemName)) {
                throw new RuntimeException('Invalid Contact information provided ' .
                                           json_encode(['contact' => $contact, 'systemName' => $systemName]));
            }

            // eMIO or HUBSPOT mapping.
            $fields = $this->standardFieldMappingRepository->getMappedSystemFields($systemName);
            if (empty($fields) || empty($fields[ 'mapping' ])) {
                throw new RuntimeException('Invalid System Mapping provided');
            }

            // MAP STANDARD FIELDS TO EMIO OR HUBSPOT FIELDS.
            $fields = $fields[ 'mapping' ];
            foreach ($fields as $field) {

                if (empty($field[ 'name' ]) || empty($field[ 'fieldname' ])) {
                    continue;
                }
                foreach ($contact[ 'contact' ][ 'standard' ] as $contactField) {
                    if (empty($contactField) || !isset($contactField[ $field[ 'fieldname' ] ])) {
                        continue;
                    }

                    $mapping[ $field[ 'name' ] ] = $contactField[ $field[ 'fieldname' ] ];
                }
            }

            // MAP MIO CUSTOM FIELDS TO HUBSPOT FIELDS.
            if ($systemName === UniqueObjectTypeRepository::HUBSPOT) {
                foreach ($mioCustomFields as $mioCusField) {
                    foreach ($contact[ 'contact' ][ 'custom' ] as $contactField) {
                        if (!isset($contactField[ $mioCusField[ 'mio-custom-field' ] ])) {
                            continue;
                        }
                        $mapping[ $mioCusField[ 'hb-custom-field' ] ] = $contactField[ $mioCusField[ 'mio-custom-field' ] ];
                    }
                }
            }

            // MAP MIO CUSTOM FIELDS TO EMIO.
            if ($systemName === UniqueObjectTypeRepository::EMAILINONE) {
                $this->logger->info('eMIO mapping Customfields ' . json_encode($mioCustomFields));
                // since custom Standard fields are stored in Standard Object of Contact.
                // custom fields are normal custom fields defined in project.
                $contactFlags = ['standard', 'custom'];

                foreach ($contactFlags as $flag) {
                    if (!isset($contact[ 'contact' ][ $flag ]) || empty($contact[ 'contact' ][ $flag ])) {
                        $this->logger->info('No contactCustom isset is false');
                        continue;
                    }
                    // assign each value of the datatype list with value of 'true'
                    // eg MIO_Suesses = true,MIO_Lebensmittel = true, MIO_Deutsch=true
                    // This does for both custom and standard fields of MIO.
                    foreach ($contact[ 'contact' ][ $flag ] as $fieldType) {
                        if (isset($fieldType[ 'datatype' ]) && !empty($fieldType[ 'field_name' ]) &&
                            ($fieldType[ 'datatype' ] === ContactFieldDataTypes::FIELD_TYPE_LIST)) {
                            $list = $this->fieldTypeListServices->listToMIOBoolean($fieldType[ $fieldType[ 'field_name' ] ]);
                            $mapping = array_merge($mapping, $list);
                        }
                    }

                    foreach ($mioCustomFields as $mioCusField) {

                        // fetch custom field value.
                        // value could be empty or non-empty string.
                        // when $customFieldValue is null, then $mioCusField[ 'marketing-in-one-custom-field' ] is not found in array.
                        $customFieldValue = $this->contactFieldServices->getValue(
                            $contact[ 'contact' ],
                            $mioCusField[ 'marketing-in-one-custom-field' ]
                        );
                        if (is_null($customFieldValue)) {
                            continue;
                        }

                        // eMIO only accepts when string is less than 255 characters.
                        if (is_string($customFieldValue) &&
                            strlen($customFieldValue) > $this->fieldTypeStringServices::MAX_STRING_LENGTH) {
                            $customFieldValue = $this->fieldTypeStringServices->truncateString($customFieldValue);
                        }
                        $mapping[ $mioCusField[ 'email-in-one-custom-field' ] ] = $customFieldValue;
                    }
                }
            }

            $this->logger->info('Final Mapping ' . json_encode($mapping, JSON_THROW_ON_ERROR));
            return $mapping;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $mapping;
        }
    }

    /**
     * @param int $companyId
     * @param int $campaignId
     * @param array $contact
     * @param array $hbConfig
     * @return bool|null
     * @author Pradeep
     * @internal Integration Contact Information to Hubspot.
     */
    public function hubspot(int $companyId, int $campaignId, array $contact, array $hbConfig): ?bool
    {
        $status = false;
        try {
            if (empty($contact) || !($this->hubspotServices->setAPIKey($hbConfig[ 'settings' ][ 'apikey' ]))) {
                throw new RuntimeException('Invalid Contact or eMIO APIKey provided');
            }
            $MIOCustomFields = !empty($hbConfig[ 'settings' ][ 'mapping' ]) ? $this->simpleDecrypt($hbConfig[ 'settings' ][ 'mapping' ]) : [];
            $hbContact = $this->mapToSystemFields($contact, UniqueObjectTypeRepository::HUBSPOT, $MIOCustomFields);
            $hbContactId = $this->hubspotServices->createContact($hbContact);
            if ($hbContactId > 0) {
                $status = true;
            }
            return $status;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $companyId
     * @param int $campaignId
     * @param array $contactInformation
     * @param array $eMailIntegrationConfig
     * @return false
     * @author Pradeep
     * @internal send an eMail Notification using eMailServer configuration.
     * @deprecated
     */
    private function emailServer(
        int $companyId,
        int $campaignId,
        array $contactInformation,
        array $eMailIntegrationConfig
    ): ?bool {
        try {
            $projectConfiguration = $this->campaignRepository->getProjectConfigurationForCompany($companyId);
            //$projectConfiguration[ 'advancedsettings' ][ 'forward_lead_by_email' ] = 'pradeep.veera@treaction.de';
            // Forward Email is not mentioned in Project settings.
            if ($projectConfiguration === null ||
                !isset($projectConfiguration[ 'advancedsettings' ][ 'forward_lead_by_email' ])) {
                throw new RuntimeException('Forward Email is not mentioned in Project settings.');
            }
            $recipientEmailList = $projectConfiguration[ 'advancedsettings' ][ 'forward_lead_by_email' ]; // returns string
            // There is no Email Recipient List configured.
            if (empty($recipientEmailList)) {
                return true; // TODO: really false?
            }
            if ($eMailIntegrationConfig === null || !isset($eMailIntegrationConfig[ 'settings' ]) ||
                (empty($eMailIntegrationConfig[ 'settings' ]))) {
                throw new RuntimeException('Currupted EmailIntegeration configuration, Failed to fetch setting field.');
            }
            $eMailServerConfiguration = $eMailIntegrationConfig[ 'settings' ];
            if ($eMailServerConfiguration[ 'server_type' ] === 'custom_server') {
                $eMailServerConfiguration = $eMailServerConfiguration[ 'custom_server' ];
            } elseif ($eMailServerConfiguration[ 'server_type' ] === 'default_server') {
                // get the Super Admin EmailServer Configuration (From Adminstration Menu)
                $defaulteMailServerConfig = $this->integrationsRepository->getDefaultEmailServerConfiguration();
                if (empty($defaulteMailServerConfig) || !isset($defaulteMailServerConfig[ 'settings' ][ 'default_server' ])) {
                    throw new RuntimeException('Invalid Default eMailServer.');
                }
                $eMailServerConfiguration = $defaulteMailServerConfig[ 'settings' ][ 'default_server' ];
            } else {
                throw new RuntimeException('Failed to get Server Type from Installed EmailServer.');
            }
            return $this->mailServices->sendForwardEmail($recipientEmailList, $eMailServerConfiguration,
                $contactInformation, "You got a new Contact");
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * isEMIOIntegrationActiveForCompany checks weather eMIO integration is active for the company or not.
     * returns bool.
     * true  - when eMIO integration is active for the company.
     * false - when eMIO integration is inactive / archived / not configured.
     * @param int $companyId
     * @return bool
     * @author Pradeep
     */
    public function isEMIOIntegrationActiveForCompany(int $companyId): bool
    {
        $config = $this->integrationsRepository->getIntegrationConfigForCompany($companyId,
            UniqueObjectTypeRepository::EMAILINONE, StatusdefRepository::ACTIVE);
        if (empty($config)) {
            return false;
        }
        return true;
    }
}

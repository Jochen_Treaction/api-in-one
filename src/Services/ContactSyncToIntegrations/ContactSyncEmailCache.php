<?php

namespace App\Services\ContactSyncToIntegrations;

use App\Services\RedisService;
use Psr\Log\LoggerInterface;

class ContactSyncEmailCache
{
    public const CONTACT_SYNC_EMAIL_CACHE_KEY = 'aio-contact-sync-integration-email-cache-';

    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var string
     * @author Pradeep
     */
    private string $key;

    public function __construct(
        RedisService $redisService,
        LoggerInterface $logger
    ) {
        $this->redisService = $redisService;
        $this->logger = $logger;
    }

    /**
     * set update the emails to rediscache.
     * - emailcache is the set of emails saperated by comma.
     * @param string $emailCache
     * @return bool
     * @author Pradeep
     */
    public function set(string $emailCache)
    {
        $key = $this->getKey();
        return $this->redisService->set($key, $emailCache, $this->redisService::ONE_HOUR_IN_SECONDS);
    }

    /**
     * get fetches the emailcache information from redis.
     * @return array
     * @author Pradeep
     */
    public function get(): array
    {
        $key = $this->getKey();
        $cache = $this->redisService->get($key);
        return $this->asArray($cache);
    }

    /**
     * asArray converts the emails(saperated by comma) to array.
     * @param string $cacheAsString
     * @return array|mixed
     * @author Pradeep
     */
    private function asArray(string $cacheAsString)
    {
        $cacheAsArr = [];
        if (!empty($cacheAsString)) {
            try {
                $cacheAsArr = json_decode($cacheAsString, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
                return $cacheAsArr;
            }
        }
        return $cacheAsArr;
    }

    /**
     * updates the emailcache(set of emails saperated by comma.) with email.
     *
     * @param string $email
     * @return bool
     * @throws \JsonException
     * @author Pradeep
     */
    public function update(string $email): bool
    {
        $emails = $this->get();
        if (empty($emails)) {
            $emails[] = $email;
        } else {
            if (!in_array($email, $emails, true)) {
                $emails[] = $email;
            }
        }

        if (!empty($emails)) {
            $emailCache = $this->toString($emails);
            return $this->set($emailCache);
        }
        return true;

    }

    /**
     * toString converts the array to string.
     * @param array $emails
     * @return false|string
     * @throws \JsonException
     * @author Pradeep
     */
    private function toString(array $emails)
    {
        if (empty($emails)) {
            return '';
        }
        return json_encode($emails, JSON_THROW_ON_ERROR);
    }

    /**
     * isPresent checks whether the given email (in parameter) present in the
     * emailcache or not.
     * @param string $email
     * @return bool
     * @author Pradeep
     */
    public function isPresent(string $email): bool
    {
        $emailCache = $this->get();
        if (is_bool($emailCache) && !$emailCache) {
            return false;
        }
        $emails = json_decode($emailCache);
        if (in_array($email, $emails, true)) {
            return true;
        }
        return false;
    }

    /**
     * reset the emailcache information in redis.
     * @return bool
     * @author Pradeep
     */
    public function reset()
    {
        $key = $this->getKey();
        return $this->redisService->set($key, "");
    }


    /**
     * createOrGetKey create a new Key with given integrationId.
     * @param int $integrationLogId
     * @return string
     * @author Pradeep
     */
    public function createOrGetKey(int $integrationLogId): string
    {
        if ($integrationLogId <= 0) {
            return false;
        }
        $this->key = self::CONTACT_SYNC_EMAIL_CACHE_KEY . $integrationLogId;
        return true;
    }

    /**
     * getKey returns the key.
     * @return string
     * @author Pradeep
     */
    private function getKey()
    {
        return $this->key;
    }
}
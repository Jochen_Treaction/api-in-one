<?php

namespace App\Services\ContactSyncToIntegrations;

use App\Repository\ContactRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactServices;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;

class ContactSyncToIntegrationService
{
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private ContactRepository $contactRepository;
    /**
     * @var ContactFieldServices
     * @author Pradeep
     */
    private ContactFieldServices $contactFieldServices;
    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactServices;
    /**
     * @var ContactSyncEmailCache
     * @author Pradeep
     */
    private ContactSyncEmailCache $contactSyncEmailCache;
    /**
     * @var MIOStandardFieldRepository
     * @author Pradeep
     */
    private MIOStandardFieldRepository $mioStandardFieldRepository;

    /**
     * @param LoggerInterface $logger
     * @param ContactRepository $contactRepository
     * @param ContactServices $contactServices
     * @param ContactFieldServices $contactFieldServices
     * @param ContactSyncEmailCache $contactSyncEmailCache
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     */
    public function __construct(
        LoggerInterface $logger,
        ContactRepository $contactRepository,
        ContactServices $contactServices,
        ContactFieldServices $contactFieldServices,
        ContactSyncEmailCache $contactSyncEmailCache,
        MIOStandardFieldRepository $mioStandardFieldRepository
    ) {
        $this->logger = $logger;
        $this->contactServices = $contactServices;
        $this->contactRepository = $contactRepository;
        $this->contactFieldServices = $contactFieldServices;
        $this->contactSyncEmailCache = $contactSyncEmailCache;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
    }


    /**
     * getContactsForSync fetches the contacts based on the min and max contactId and limit specified in the arguments.
     * - This sync is called by the cron job 'integrationSyncAction'
     * - result is the contact history with duplicate emails.
     * @param int $companyId
     * @param int $startContactId
     * @param int $endContactId
     * @param int $limit
     * @return array
     * @author Pradeep
     */
    public function getContactsForSync(int $companyId, int $startContactId, int $endContactId, int $limit): array
    {
        $alreadySyncedEmails = $this->getCache();

        $this->logger->info('$alreadySyncedEmails ' . json_encode($alreadySyncedEmails));

        $isFirstElement = true;
        $emailAsString = "";
        foreach ($alreadySyncedEmails as $email) {
            if ($isFirstElement) {
                $emailAsString .= "'" . $email . "'";
                $isFirstElement = false;
            } else {
                $emailAsString .= ",'" . $email . "'";
            }
        }

        return $this->contactRepository->getContactsSync($companyId, $startContactId, $endContactId, $emailAsString,
            $limit);
    }

    /**
     * getCache fetches the redis cache containing email address.
     * @return array
     * @author Pradeep
     */
    private function getCache()
    {
        return $this->contactSyncEmailCache->get();
    }

    /**
     * updateEmailCache collects the email form the filteredContacts.
     *
     * @param array $filteredContacts
     * @return array
     * @author Pradeep
     */
    public function updateEmailCache(array $filteredContacts): bool
    {
        if (empty($filteredContacts)) {
            return false;
        }
        foreach ($filteredContacts as $contact) {
            if (!isset($contact[ 'contact' ])) {
                continue;
            }
            $email = $this->contactFieldServices->getValue($contact[ 'contact' ], 'email');
            $this->updateCache($email);
        }
        return true;
    }

    /**
     * updateCache - update the email cache for synchronizing contacts with eMIO.
     * @param string $email
     * @return bool
     * @author Pradeep
     */
    public function updateCache(string $email)
    {
        return $this->contactSyncEmailCache->update($email);
    }

    /**
     * Filters duplicate records from sql query.
     * Due to the join between leads and custom field table. there are few duplicate lead records.
     * This functions removes the duplicate rows or records and merges the custom fields to contact array.
     *
     * @param array $contacts
     * @param array $standardFields
     * @param array $customFields
     * @return array
     * @author Pradeep
     */
    public function filterDuplicateRecordsForSync(array $contacts, array $standardFields, array $customFields): array
    {
        try {
            $emails = [];
            $uniqueRecords = [];
            $this->logger->info('FilterDuplicateRecords StandFields ' . json_encode($standardFields));
            $id = [];
            $leadId = 0;
            $i = 0;
            foreach ($contacts as $contact) {
                //$emails[] = $contact[ 'email' ];
                //$this->updateCache($contact[ 'email' ]);
                // Map to MIO standard and customFields
                $tempRecords = $this->filterColumns($contact, $standardFields, $customFields);
                // $this->logger->info('TempRecords ' . json_encode($tempRecords));
                if (!isset($id[ $contact[ 'email' ] ])) {
                    $i ++;
                    $id[ $contact[ 'email' ] ] = $contact[ 'lead_id' ];
                    $uniqueRecords[ $i ] = $tempRecords;
                    continue;
                }

                // Merge duplicate record
                if (isset($id[ $contact[ 'email' ] ], $tempRecords[ 'contact' ][ 'custom' ]) &&
                    $id[ $contact[ 'email' ] ] === $contact[ 'lead_id' ]) {
                    // $this->logger->info('uniqueRecords '.json_encode($uniqueRecords[ $i ][ 'custom' ]));
                    if (isset($uniqueRecords[ $i ][ 'contact' ][ 'custom' ])) {
                        $uniqueRecords[ $i ][ 'contact' ][ 'custom' ] = array_merge($uniqueRecords[ $i ][ 'contact' ][ 'custom' ],
                            $tempRecords[ 'contact' ][ 'custom' ]);
                    } else {
                        $uniqueRecords[ $i ][ 'contact' ][ 'custom' ] = $tempRecords[ 'contact' ][ 'custom' ];
                    }
                } else {
                    $this->logger->info('if failed');
                }
                //$emails[] = $contact[ 'email' ];
                //$this->updateCache($contact[ 'email' ]);
            }
            // $this->logger->info('Final uniqueRecords ' . json_encode($uniqueRecords));
            return $uniqueRecords;
        } catch (JsonException|Exception $e) {
            $this->logger->info($e->getMessage(), [__METHOD__, __LINE__]);
            return $uniqueRecords;
        }
    }

    /**
     * This function filters the 'CustomFieldAnswer' and 'CustomFieldQuestion' and
     * merges the values to the contact array.
     * SQL Query fetches the custom fields as 'CustomFieldQuestion'=> custom field name
     * and 'CustomFieldAnswer' => lead value for the custom field.
     *
     * @param array $contact
     * @param array $standardFieldsKey
     * @param array $customFieldsKey
     * @return array
     * @author Pradeep
     */
    private function filterColumns(array $contact, array $standardFieldsKey, array $customFieldsKey): array
    {
        $filtered = [];
        $ids = ['campaign_id', 'lead_id'];
        foreach ($contact as $key => $value) {
            if (in_array($key, $ids, true)) {
                $filtered[ 'base' ][ $key ] = $value;
            }
            if (in_array($key, $standardFieldsKey, true)) {
                $fieldDetails = $this->mioStandardFieldRepository->getFieldTypeByFieldName($key);
                // $filtered['contact'][ 'standard' ][ $key ] = $value;
                $filtered[ 'contact' ][ 'standard' ][] = [
                    $key => $value,
                    'field_name' => $key,
                    'required' => '',
                    'datatype' => $fieldDetails[ 'datatype' ],
                    'regex' => '',
                ];
            }
            // Just for safety, when standard fields are present as customfields as well
            if ($key === 'CustomFieldQuestion' && in_array($value, $standardFieldsKey, true)) {
                continue;
            }
            if ($key === 'CustomFieldQuestion') {
                $tempCustomFieldKey = $value;
            }
            if ($key === 'CustomFieldAnswer') {
                $tempCustomFieldValue = $value;
            }
            if (!empty($tempCustomFieldKey) && !empty($tempCustomFieldValue) &&
                in_array($tempCustomFieldKey, $customFieldsKey, true)) {
                $filtered[ 'contact' ][ 'custom' ][] = [
                    $tempCustomFieldKey => $tempCustomFieldValue,
                    'required' => '',
                    'datatype' => '',
                    'regex' => '',
                ];
                $tempCustomFieldKey = '';
                $tempCustomFieldValue = '';
            } else {
                $filtered[ 'contact' ][ 'custom' ] = [];
            }
        }
//        $this->logger->info('filterColumn Final '.json_encode($filtered));
        return $filtered;
    }

    /**
     * @param array $contacts
     * @return int|mixed
     * @author Pradeep
     */
    public function getLastId(array $contacts)
    {
        if (empty($contacts)) {
            return 0;
        }
        $count = count($contacts);
        return $contacts[ $count - 1 ][ 'id' ];
    }

    /**
     * getContactIdAsArray - gets the Contact/lead database Id for given contacts as array.
     * - special function used by the 'integrationSyncAction'.
     * - All the contactIds are sent as array .
     * @param array $contacts
     * @return array
     * @author Pradeep
     */
    public function getContactIdAsArray(array $contacts): array
    {
        $contactIds = [];

        foreach ($contacts as $contact) {
            if (!isset($contact[ 'base' ][ 'lead_id' ]) || empty($contact[ 'base' ][ 'lead_id' ])) {
                continue;
            }
            $contactIds[] = (int)$contact[ 'base' ][ 'lead_id' ];
        }
        return $contactIds;
    }
}
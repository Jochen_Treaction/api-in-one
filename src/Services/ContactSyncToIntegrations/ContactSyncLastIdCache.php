<?php

namespace App\Services\ContactSyncToIntegrations;

use App\Services\RedisService;
use Psr\Log\LoggerInterface;

class ContactSyncLastIdCache
{

    private const REDIS_KEY = 'aio-contact-sync-last-sync-id-';

    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        RedisService $redisService,
        LoggerInterface $logger
    ) {
        $this->redisService = $redisService;
        $this->logger = $logger;
    }

    /**
     * createOrGetKey creates a new key for storing the last synchonized ContactId.
     * @param int $integrationLogId
     * @return string
     * @author Pradeep
     */
    public function createOrGetKey(int $integrationLogId): string
    {
        if ($integrationLogId <= 0) {
            return false;
        }
        $this->key = self::REDIS_KEY . $integrationLogId;
        return true;
    }

    /**
     * getKey returns the key used to fetch value from redis.
     * @return string
     * @author Pradeep
     */
    private function getKey()
    {
        return $this->key;
    }

    /**
     * sets the value of contactId for the redisKey.
     * @param int $contactId
     * @author Pradeep
     */
    public function set(int $contactId)
    {
        $key = $this->getKey();
        $this->redisService->set($key, $contactId, $this->redisService::ONE_HOUR_IN_SECONDS);
    }

    /**
     * get fetches the value for the redisKey.
     * @return false|mixed|string
     * @author Pradeep
     */
    public function get()
    {
        $key = $this->getKey();
        return $this->redisService->get($key);
    }

    /**
     * reset clears the value for the redisKey.
     * @author Pradeep
     */
    public function reset()
    {
        $key = $this->getKey();
        $this->redisService->set($key, 0);
    }
}
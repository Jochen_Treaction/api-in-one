<?php


namespace App\Services\PrivateServices;


class MIOEmailValidation
{

    public $email;
    protected $user = 'livetreaction';
    protected $pass = 'HPb0oU1rESV7';
    protected $server = 'https://adc.maileon.com';
    protected $url = 'https://adc.maileon.com/svc/2.0/address/quality';
    public $response = array();
    public $responceSize = 0;

    /**
     * MIOEmailValidation constructor.
     * @param $mail
     */
    public function __construct($mail){
        $this->email = $mail;
    }

    private function getEmail() : string
    {
        return $this->email;
    }

    private function getURL() :string
    {
        return $this->url;
    }

    protected function getResponse(): array
    {
        if($this->response !== null){
            return $this->response;
        }
        return [];
    }

    protected function setResponseSize(int $size) : void
    {
        $this->responceSize = $size;
    }

    protected function getResponseSize() : int
    {
        $size = 0;
        if(isset($this->responceSize) && (int) $this->responceSize > 0){
            $size = $this->responceSize;
        }
        return $size;
    }

    /**
     * @return bool
     */
    public function isEmailAddressValid() : bool
    {
        $email = $this->getEmail();
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return false;
        }
        if($this->curlRequest()) {
            return $this->curlResponse();
        }
        return false;
    }

    protected function curlRequest() :bool
    {
        $email = $this->getEmail();
        $url = $this->getURL();
        if(!isset($email, $url)){
            return false;
        }
        $url .= '/' . $email;
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Accept: application/json'),
            CURLOPT_USERPWD => "$this->user:$this->pass"
        );
        curl_setopt_array($ch, $optArray);
        $curl_resp = curl_exec($ch);
        $close = curl_close($ch);
        $this->response = json_decode($curl_resp, true);
        if($this->response === null){
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function curlResponse(): bool
    {
        $resp = $this->getResponse();
        if(!empty($resp)) {
            $this->setResponseSize(sizeof($resp));
        }
        $result = $this->parseResponse();
        return $result;
    }

    /**
     * @return bool
     */
    protected function parseResponse(): bool
    {
        $resp = $this->getResponse();
        $result = true;
        foreach( $resp as $key => $val){
            switch ($key){
                case 'mailserver':
                case 'address':
                    if($val === 0){
                        $result = false;
                    }
                    break;
            }
        }
        return $result;
    }
}
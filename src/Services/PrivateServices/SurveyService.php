<?php


namespace App\Services\PrivateServices;


use App\Services\AIOServices\MSCIOCampaignServices;
use App\Services\AIOServices\MSMIOContact;
use App\Services\AIOServices\MSCIOSurveyService;
use JsonException;
use Psr\Log\LoggerInterface;

class SurveyService
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param int $campaign_id
     * @return bool
     * @author Pradeep
     */
    public function isSurveyConfiguredForCampaign(int $campaign_id): bool
    {
        $is_configured = false;
        if ($campaign_id <= 0) {
            return $is_configured;
        }
        // removed Webspider Microservice
        // Todo update the values with DB repository.
        $campaignServices = null;// new MSCIOCampaignServices();
        $campaign_details = $campaignServices->fetchCampaignDetails($campaign_id);
        if (!empty($campaign_details) && isset($campaign_details[ 'surveyId' ]) && (int)$campaign_details[ 'surveyId' ] > 0) {
            $is_configured = true;
        }
        return $is_configured;
    }

    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    public function fetchSurveyForCampaign(int $campaign_id): array
    {
        $survey = [];
        if ($campaign_id <= 0) {
            return $survey;
        }
        // removed Webspider Microservice
        $campaignServices = null;// new MSCIOCampaignServices();
        $campaign_details = $campaignServices->fetchCampaignDetails($campaign_id);
        if (empty($campaign_details) || !isset($campaign_details[ 'id' ], $campaign_details[ 'surveyId' ])) {
            return $survey;
        }
        // Todo update the values from DB repository
        $surveyService = null ;//new MSCIOSurveyService();
        $survey[ 'survey' ] = $surveyService->fetchSurveyDetails($campaign_details[ 'surveyId' ]);
        $survey[ 'questions' ] = $surveyService->fetchSurveyQuestions($campaign_details[ 'surveyId' ]);
        return $survey;
    }

    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    public function getSurveySettingsForCampaign(int $campaign_id) :array
    {
        $survey_settings = [];
        if($campaign_id <= 0) {
            return $survey_settings;
        }
        try {
            // removed Webspider Microservice
            $campaignServices = null;// new MSCIOCampaignServices();
            $response = $campaignServices->getCampaignParmsForTarget($campaign_id, 'General');
            if (empty($response) || empty($response[ 'value' ])) {
                return $survey_settings;
            }
            $value = json_decode($response[ 'value' ], true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
            if (isset($value[ 'cal_survy_score' ], $value[ 'survy_score_calc_format' ])) {
                $survey_settings[ 'cal_survy_score' ] = $value[ 'cal_survy_score' ];
                $survey_settings[ 'survy_score_calc_format' ] = $value[ 'survy_score_calc_format' ];
            }
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $survey_settings;
    }
    /**
     * @param int $campaign_id
     * @param int $lead_id
     * @param array $lead_survey_answers
     * @return bool
     * @author Pradeep
     */
    public function storeSurveyFromLead(int $campaign_id, int $lead_id, array $lead_survey_answers): bool
    {
        $status = false;
        if ($campaign_id <= 0 || $lead_id <= 0 || empty($lead_survey_answers)) {
            return false;
        }
        // removed webSpider microservice
        $leads = null;// new MSMIOContact();
        try {
            foreach ($lead_survey_answers as $question_id => $lead_answer) {
                if ($question_id <= 0) {
                    continue;
                }
                $lead_answer_score = $this->calculateScoreForQuestion($question_id, json_encode($lead_answer, JSON_THROW_ON_ERROR));
                $lead_answer_content = [
                    'leadsId' => (string)$lead_id,
                    'questionsId' => (string)$question_id,
                    'leadAnswerContent' => json_encode($lead_answer),
                    'leadAnswerScore' => (string)$lead_answer_score
                ];

                $status = $leads->insertLeadAnswerContent($lead_answer_content);
                if(!$status) {
                    $this->logger->critical('Failed to insert Lead answer', [__METHOD__, __LINE__]);
                }
            }
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }

    /**
     * @param int $question_id
     * @param string $lead_answer_for_question
     * @return float
     * @author Pradeep
     */
    public function calculateScoreForQuestion(int $question_id, string $lead_answer_for_question): float
    {
        $lead_score_for_question = 0;
        $score = 0;
        $ques_dd_score = 0;
        if ($question_id <= 0 || empty($lead_answer_for_question)) {
            return $lead_score_for_question;
        }
        try {
            $surveyService = new MSCIOSurveyService();
            $ques_details = $surveyService->fetchQuestionDetails($question_id);
            if (empty($ques_details) || !isset($ques_details[ 'questionJsonRepresentation' ])) {
                $this->logger->error('Failed to fetch question details from question table', [__METHOD__, __LINE__]);
                return $lead_score_for_question;
            }
            $question_details = json_decode($ques_details[ 'questionJsonRepresentation' ], true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
            $question_type = $question_details[ 'type' ];
            $valid_options = $question_details[ $question_type ];
            $lead_selected_options = json_decode($lead_answer_for_question, true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
            foreach ($valid_options['options'] as $option) {
                // Currently score is calculated for select and multiple choice questions.
                if ($question_type === 'checkbox') {

                    foreach ($lead_selected_options as $selected_option) {
                        if ($selected_option === $option[ 'value' ]) {
                            $lead_score_for_question += (float)$option[ 'score' ];
                        }
                    }
                } else {
                    if ($question_type === 'dropdown') {

                        if ($lead_selected_options === $option[ 'value' ]) {
                            $lead_score_for_question = (float)$option[ 'score' ];
                        }
                    }
                }
            }
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $lead_score_for_question;
    }

    /**
     * @param int $lead_id
     * @param bool $score_calc
     * @param string $score_calc_format
     * @return int
     * @author Pradeep
     */
    public function getSurveyScoreForLead(int $lead_id, bool $score_calc, string $score_calc_format) :int
    {

    }
}

<?php


namespace App\Services\PrivateServices;

use App\Repository\CampaignRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Services\ToolServices;
use App\Types\ProjectWizardTypes;
use Psr\Log\LoggerInterface;
use App\Repository\ObjectRegisterRepository;
use App\Services\PrivateServices\DataValidationService;

class ProjectService
{
    private const CommercialRelastionships = 'commercial_relationships';
    private const LeadRegistrationType = 'register_multiple_times';
    private const Country = 'country';
    private const FraudPreventionLevel = 'fraud_prevention_level';
    private const RedirectAfterRegistration = 'redirect_after_registration';
    private const AccountNumber = 'accountNo';
    private const ObjectType = "project";
    private $CampaignRepository;
    private $LoggerInterface;
    private $ObjectRegisterRepository;
    private $DataValidationService;
    /**
     * @var ProjectWizardTypes
     * @author Pradeep
     */
    private $projectWizardTypes;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private $objectRegisterMetaRepository;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;


    /**
     * ProjectController constructor.
     * @param CampaignRepository $CampaignRepository
     * @param LoggerInterface $logger
     * @param ObjectRegisterRepository $ObjectRegisterRepository
     * @param \App\Services\PrivateServices\DataValidationService $DataValidationService
     * @author aki
     */
    public function __construct(
        CampaignRepository $CampaignRepository,
        LoggerInterface $logger,
        ObjectRegisterRepository $ObjectRegisterRepository,
        ObjectRegisterMetaRepository $ObjectRegisterMetaRepository,
        DataValidationService $DataValidationService,
        ProjectWizardTypes $ProjectWizardTypes,
        LoggerInterface $Logger
    ) {
        $this->CampaignRepository = $CampaignRepository;
        $this->LoggerInterface = $logger;
        $this->ObjectRegisterRepository = $ObjectRegisterRepository;
        $this->objectRegisterMetaRepository = $ObjectRegisterMetaRepository;
        $this->DataValidationService = $DataValidationService;
        $this->projectWizardTypes = $ProjectWizardTypes;
        $this->logger = $logger;
    }

    /**
     * @param int $accountNumber
     * @return bool
     * @author aki
     */
    public function checkForValidAccount(int $accountNumber): bool
    {
        $return = false;
        $companyDetails = "";
        if ($accountNumber > 0) {
            $companyDetails = $this->CampaignRepository->getAccountidAndAccountName($accountNumber);
        }
        if ($companyDetails[ 'id' ] || $companyDetails[ 'name' ]) {
            $return = true;
        }
        return $return;
    }

    /**
     * @param int $accountNumber
     * @return int|null
     * @author Pradeep
     */
    public function createProject(int $accountNumber): ?int
    {
        //todo: always make entry for created by and updated by !!!
        $objectRegisterId = $this->makeObjectRegisterLayerEntry(); //objectregister layer entry
        $this->LoggerInterface->info("objectRegister Entry made for project with id" . $objectRegisterId);
        //entry in campaign table
        return $this->makeCampaignLayerEntry($accountNumber, $objectRegisterId); //campaign and campaign params entry
    }

    /**
     * @return int|null
     * @author aki
     */
    public function makeObjectRegisterLayerEntry(): ?int
    {
        //object register entry
        $objectRegisterId = $this->ObjectRegisterRepository->insertObjectRegisterEntry(self::ObjectType);
        //status model entry
        $statusdefId = $this->ObjectRegisterRepository->getStatusdefIdByName('active');
        $uniqueObjectTypeId = $this->ObjectRegisterRepository->getUniqueObjectTypeIdFromName(self::ObjectType);
        // Todo :  discuss with Aravind -> NO need of entry the projectId to statusModel table.
/*        $statusModelId = $this->ObjectRegisterRepository->insertStatusModelEntry($statusdefId, self::ObjectType,
            $uniqueObjectTypeId);
        if (empty($statusModelId)) {
            var_dump('problem in status model entry');
            return $objectRegisterId;
        }*/
        return $objectRegisterId;
    }

    /**
     * @param int $acconutNumber
     * @param int $projectObjectRegisterId
     * @return int|null
     * @author aki
     */
    public function makeCampaignLayerEntry(int $acconutNumber, int $projectObjectRegisterId): ?int
    {
        //campaign table entry
        $campaignId = null;
        $companyDetails = $this->CampaignRepository->getAccountidAndAccountName($acconutNumber);
        // If the companyName is missing
        if(empty($companyDetails[ 'name' ])) {
            $companyDetails[ 'name' ] = 'Project_For_account_no_'.$acconutNumber;
        }
        $useCaseType = self::ObjectType;
        //hard coded parameters Todo: remove them or find some solution !
        $url = "https://dummy-project-domain.net"; // hard coded dummy URL => serves as dummy for campaign.url, which has not null constraint
        $comment = "This project is created by AIO";
        $version = 1;
        $activatedBy = 0;
        $status = "Active";
        $campaignId = $this->CampaignRepository->createCampaignEntry(
            $companyDetails[ 'id' ],
            $companyDetails[ 'name' ],
            (string)$acconutNumber,
            $url,
            $projectObjectRegisterId,
            $comment,
            99999,
            99999
        );

        if ($campaignId === null) {
            return null;
        }
        $projectDefaultConfig = $this->projectWizardTypes->getArrayWithDefaultValues();
        $projectDefaultConfig['relation']['company_name'] = $companyDetails[ 'name' ];
        $projectDefaultConfig['relation']['company_id'] = (int)$companyDetails[ 'id' ];
        $projectDefaultConfig['relation']['project_objectregister_id'] = $projectObjectRegisterId;
        $projectDefaultConfig['relation']['project_object_unique_id'] =$projectObjectRegisterId;
        $metaValue = json_encode($projectDefaultConfig, JSON_THROW_ON_ERROR);
        $objRegisterMetaId = $this->objectRegisterMetaRepository->insert($projectObjectRegisterId,'project', $metaValue);
        if($objRegisterMetaId === null || $objRegisterMetaId <= 0 ) {
            $this->logger->error('Failed to insert project default settings to object register meta ');
            return null;
        }

        return $campaignId;
    }

    /*
     * @return array
     * @author aki
     */
    public function getBaseProjectDataStructure(): array
    {
        //todo:get all basic settings and add them
        $projectStructure = [
            self::CommercialRelastionships => 'both',
            self::LeadRegistrationType => 'yes',
            self::Country => 'Germany',
            self::FraudPreventionLevel => 'low',
            self::RedirectAfterRegistration => 'no',
        ];

        return $projectStructure;
    }

    /**
     * @param array $data
     * @return bool
     * @author aki
     */
    public function checkForValidData(array $data): bool
    {
        $datavalid = true;


        $projectDataStructure = [
            // https://www.php.net/manual/en/function.filter-list.php
            [
                'fieldname' => self::CommercialRelastionships,
                'phpdatatype' => 'string',
                'pattern' => '/^(B2B)|(B2C)|(both){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::CommercialRelastionships,
                'phpdatatype' => 'string',
                'pattern' => '/^(yes)|(no){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::Country,
                'phpdatatype' => 'string',
                'pattern' => '/^[äöüßa-z\.\- ]{2,80}$/i',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::FraudPreventionLevel,
                'phpdatatype' => 'string',
                'pattern' => '/^(low)|(medium)|(high){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::RedirectAfterRegistration,
                'phpdatatype' => 'int',
                'pattern' => '/^(yes)|(no){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
        ];

        //todo:validate the additional data sent i.e check if has additional params then required
        return $datavalid;
    }

    /**
     * @param array $data
     * @return bool|null
     * @author aki
     */
    public function updateProject(array $data): ?bool
    {
        $isUpdated = null;
        $accountNumber = $data[ self::AccountNumber ];//get account number
        $companyDetails = $this->CampaignRepository->getAccountidAndAccountName($accountNumber); //get company details
        $campaignId = $this->CampaignRepository->getCampaignId($companyDetails[ 'id' ]); //get campaign id
        if (empty($campaignId)) {
            $this->LoggerInterface->error('campaign id not valid or did not fetched', [__METHOD__, __LINE__]);
            return null;
        }
        unset($data[ self::AccountNumber ]); //remove account number from data
        $isUpdated = $this->CampaignRepository->updateProject($campaignId, $data);

        return $isUpdated;
    }

    public function getDescription(): array
    {
        $projectDataStructure = [
            [
                'fieldname' => self::CommercialRelastionships,
                'phpdatatype' => 'string',
                'pattern' => '/^(B2B)|(B2C)|(both){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::CommercialRelastionships,
                'phpdatatype' => 'string',
                'pattern' => '/^(yes)|(no){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::Country,
                'phpdatatype' => 'string',
                'pattern' => '/^[äöüßa-z\.\- ]{2,80}$/i',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::FraudPreventionLevel,
                'phpdatatype' => 'string',
                'pattern' => '/^(low)|(medium)|(high){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
            [
                'fieldname' => self::RedirectAfterRegistration,
                'phpdatatype' => 'int',
                'pattern' => '/^(yes)|(no){1,1}$/',
                'filterValidate' => null,
                'filterSanitize' => 'string',
            ],
        ];

        return $projectDataStructure;

    }

    /**
     * @param int $accountNumber
     * @return string
     * @author Aki
     */
    public function getProjectData(int $accountNumber): string
    {
        $projectData = [];
        if (empty($accountNumber)) {
            return $projectData;
        }
        $companyDetails = $this->CampaignRepository->getAccountidAndAccountName($accountNumber); //get company details
        $campaignId = $this->CampaignRepository->getCampaignId($companyDetails[ 'id' ]); //get campaign id
        if (empty($campaignId)) {
            $this->LoggerInterface->error('campaign id not valid or did not fetched', [__METHOD__, __LINE__]);
            var_dump('campaign id not valid or did not fetched', [__METHOD__, __LINE__]);
            return $projectData;
        }
        $projectData = $this->CampaignRepository->getProjectdata($campaignId);
        return $projectData;
    }

}

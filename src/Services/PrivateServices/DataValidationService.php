<?php


namespace App\Services\PrivateServices;
use Psr\Log\LoggerInterface;

class DataValidationService
{
    /**
     * @var LoggerInterface
     * @author aki
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $data
     * @return bool
     * @author aki
     */
    public function validateData(array $data): bool
    {


    }

    /**
     * @param array $data
     * @return array
     * @author aki
     */
    public function getDefaultProjectSettings(): array
    {
        $defaultSettings['project'] = json_decode('{
        "ProjectWizard":{"relation":{"company_name":"","company_id":"","project_object_unique_id":""},
        "settings":{"description":"","beginDate":"","expireDate":""},
        "advancedsettings":{"commercial_relationships":"both","register_multiple_times":"yes","country":"Germany","fraud_prevention_level":"low","forward_lead_by_email":"","bool_redirect_after_registration":"","redirect_after_registration":""},
        "customfields":"",
        "tracker":{"goolge_apikey":"","facebook_pixel":"","webgains_id":"","outbrain_id":"","bool_outbrain_track_after_30_seconds":""}}
        }');

        return $defaultSettings;

    }


}
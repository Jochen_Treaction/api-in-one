<?php


namespace App\Services\PrivateServices;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;
use Psr\Log\LoggerInterface;

class DeviceDetectionService
{

    /**
     * @var DeviceDetector
     * @author Pradeep
     */
    private $deviceDetector;

    /**
     * DeviceDetectionService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $http_user_agent
     * @return bool
     * @author Pradeep
     */
    public function setDeviceDetector(string $http_user_agent) :bool
    {
        if(empty($http_user_agent)) {
            $this->logger->error('Invalid http_user_agent passed', [__METHOD__, __LINE__]);
            return false;
        }
        $this->deviceDetector = new DeviceDetector($http_user_agent);
        $this->deviceDetector->parse();
        return true;
    }

    /**
     * @return DeviceDetector
     * @author Pradeep
     */
    private function getDeviceDetector():DeviceDetector {
        return $this->deviceDetector;
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function isBotDetected() : bool
    {
        $dd = $this->getDeviceDetector();
        return $dd->isBot();
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getBotInfo() :array
    {
        $dd = $this->getDeviceDetector();
        return $dd->getBot();
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getDeviceDetails() :array
    {
        $dd = $this->getDeviceDetector();
        return $dd->getClient();
    }


}
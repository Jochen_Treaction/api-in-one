<?php


namespace App\Services\PrivateServices;


class APCuService
{
    const SECRET = 'f#`R5oWw1\e>Lt\20RcN&>jC%a>5QZ';
    const FIFTEEN_MINUTES = 900;
    const TWO_HOURS = 7200;
    const FOUR_HOURS = 14400;
    const ONE_DAY = 86400;


    /**
     * @var bool
     * @author Pradeep
     */
    private $isApcu;

    public function __construct()
    {
        $this->isApcu = (empty(apcu_cache_info())) ? false : true;
    }

    /**
     * checks if Apcu is available
     * @return bool
     * @author jsr
     */
    public function hasApcu(): bool
    {
        return $this->isApcu;
    }

    /**
     * Caches a variable in the data store, only if it's not already stored.
     * @param string $key
     * @param mixed $value
     * @param $ttl
     * @return bool
     * @author jsr
     */
    public function addIfNotExists(string $key, $value, $ttl = self::ONE_DAY): bool
    {
        $ret = false;

        if ($this->isApcu) {
            $ret = apcu_add($key, serialize($value), $ttl);
            apcu_add(
                hash_hmac('sha3-512', $key, self::SECRET),
                hash_hmac('sha3-512', serialize($value), self::SECRET),
                $ttl
            );
        }

        return $ret;
    }

    /**
     * Get a value by key from the cache
     * @param string $key
     * @return bool|false if key not found, else mixed => value
     * @author jsr
     */
    public function get(string $key)
    {
        $ret = false;

        if ($this->isApcu) {
            $temp = apcu_fetch($key);
            $ret = unserialize($temp, ['']);

            $check = apcu_fetch(hash_hmac('sha3-512', $key, self::SECRET));

            if ($check === hash_hmac('sha3-512', serialize($ret), self::SECRET)) {
                return $ret;
            }
        }
        return $ret;
    }

    /**
     * check if an entry for key exists in cache
     * @param string $key
     * @return bool
     * @author jsr
     */
    public function entryExists(string $key): bool
    {
        $ret = false;
        if ($this->isApcu) {
            $ret = apcu_exists($key);
        }
        return $ret;
    }

    /**
     * Caches a variable in the data store. CAUTION: If it already exists, it is overwritten!!!
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     * @author jsr
     */
    public function addOrOverwrite(string $key, $value, $ttl = self::ONE_DAY): bool
    {
        $ret = false;

        if ($this->isApcu) {
            $ret = apcu_store($key, serialize($value), $ttl);
            apcu_store(
                hash_hmac('sha3-512', $key, self::SECRET),
                hash_hmac('sha3-512', serialize($value), self::SECRET),
                $ttl
            );
        }
        return $ret;
    }

    /**
     * delete an entry from apcu cache by key
     * @param string $key
     * @author jsr
     */
    public function del(string $key): void
    {
        if ($this->isApcu) {
            apcu_delete($key);
            apcu_delete(hash_hmac('sha3-512', $key, self::SECRET));
        }
    }
}
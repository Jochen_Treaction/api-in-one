<?php


namespace App\Services\PrivateServices;


use App\Repository\CampaignRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Services\AIOServices\MSCIOCampaignServices;
use App\Services\AIOServices\MSMIOContact;
use JsonException;

/**
 * @property CampaignRepository campaignRepository
 * @property ObjectRegisterMetaRepository objectRegisterMetaRepository
 */
class SplitTestService
{

    /**
     * @var string
     * @author Pradeep
     */
    private $cnf_trgt;
    /**
     * @var APCuService
     * @author Pradeep
     */
    private $apcuService;

    /**
     * SplitTestService constructor.
     */
    public function __construct(
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        CampaignRepository $campaignRepository
    )
    {
        $this->apcuService = new APCuService();
        $this->campaignRepository = $campaignRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;

    }


    /**
     * @param int $campaign_id
     * @return bool
     * @author Pradeep
     * @Internal update the changes to Database
     */
    public function isSplitTestActive(int $campaign_id): bool
    {

        $is_active = false;
        $cnf_trgt = 'Splittest';
        if ($campaign_id <= 0) {
            return $is_active;
        }
        // removed Webspider Microservice
        // Todo update the changes to database repository.
        $campaignServices = null;// new MSCIOCampaignServices();
        $split_test_params = $campaignServices->getCampaignParmsForTarget($campaign_id, $cnf_trgt);
        if (empty($split_test_params) || empty($split_test_params[ 'value' ])) {
            return $is_active;
        }
        $split_test_value = json_decode($split_test_params[ 'value' ], true, 512, JSON_UNESCAPED_SLASHES);
        if (isset($split_test_value[ 'status' ]) && $split_test_value[ 'status' ] === 'on') {
            $is_active = true;
        }
        return $is_active;
    }

    /**
     * @param int $campaign_id
     * @param string|null $sticky_ip
     * @param string|null $sticky_cookie
     * @return string
     * @throws JsonException
     * @author Pradeep
     */
    public function fetchNextSplitTestVariant(
        int $campaign_id,
        string $sticky_ip = null,
        string $sticky_cookie = null
    ): string {

        $variant_url = '';
        $variant_id = 0;
        $campaign_details = $this->campaignRepository->getCampaignDetailsById($campaign_id);
        $pageSettings = $this->objectRegisterMetaRepository->getMetaValue($campaign_details['objectregister_id'],ObjectRegisterMetaRepository::MetaKeyPage);
        $split_test_value = $pageSettings['splittest'];
        if (empty($split_test_value) || empty($campaign_details) || empty($campaign_details[ 'url' ])) {
            return $variant_url;
        }
        // fetch variant_id from sticky configuration
        if ($split_test_value[ 'sticky_config' ] !== 'None' && (!empty($sticky_ip) || !empty($sticky_cookie))) {
            $variant_id = $this->fetchStickyConfiguration($campaign_details[ 'url' ],
                $split_test_value[ 'sticky_config' ],
                $split_test_value[ 'variants' ], $sticky_ip, $sticky_cookie);
        }
        // fetch variant id from apcu
        if ($variant_id <= 0) {
            $variant_id = $this->fetchVariantIdFromApcu($campaign_id, $campaign_details[ 'url' ], $split_test_value);
        }
        // build campaign url
        if ($variant_id > 0) {
            $variant_url = $campaign_details[ 'url' ] . '/' . $split_test_value[ 'name' ] . '/' . $variant_id;
        }

        return $variant_url;
    }

    /**
     * @param string $campaign_url
     * @param string $sticky_configuration
     * @param array $variants
     * @param string|null $stick_ip
     * @param string|null $sticky_email
     * @return int
     * @author Pradeep
     */
    private function fetchStickyConfiguration(
        string $campaign_url,
        string $sticky_configuration,
        array $variants,
        string $stick_ip = null,
        string $sticky_email = null
    ): int {
        $variant_id = 0;
        // removed Webspider microservice
        $leads = null;// new MSMIOContact();
        if (empty($sticky_configuration) || empty($variants) || empty($campaign_url) || $leads === null) {
            return $variant_id;
        }

        if ($sticky_configuration === 'IP' && !empty($stick_ip)) {
            $leads = $leads->getAllLeadsByIpForCampaign($stick_ip, $campaign_url);
        } elseif ($sticky_configuration === 'Cookie' && !empty($sticky_email)) {
            $leads = $leads->getAllLeadsByEmailForCampaign($sticky_email, $campaign_url);
        } else {
            $leads = '';
        }
        if (!empty($leads) && !empty($leads[ 'splitVersion' ])) {
            $variant_id = $leads[ 'splitVersion' ];
        }

        return $variant_id;
    }

    /**
     * @param int $campaign_id
     * @param string $campaign_url
     * @param array $splittest_params
     * @return int
     * @author Pradeep
     */
    private function fetchVariantIdFromApcu(int $campaign_id, string $campaign_url, array $splittest_params): int
    {
        $variant_id = 1;
        if ($campaign_id <= 0 || empty($campaign_url) || empty($splittest_params)) {
            return $variant_id;
        }

        $sp_apcu_key = implode('_', [$campaign_id, $splittest_params[ 'name' ]]);
        $campaign_apcu_record = $this->apcuService->get($sp_apcu_key);

        if ($splittest_params[ 'expiration_criteria' ] === 'visitors') {
            $variant_id = $this->checkExpCriteriaForVisitors($sp_apcu_key, $splittest_params[ 'variants' ],
                $splittest_params[ 'expiration_value_visitors' ]);

        } elseif ($splittest_params[ 'expiration_criteria' ] === 'conversions') {
            $variant_id = $this->checkExpCriteriaForConversions($sp_apcu_key, $splittest_params[ 'variants' ],
                $splittest_params[ 'expiration_value_conversions' ], $campaign_url);

        } elseif ($splittest_params[ 'expiration_criteria' ] === 'date') {
            $variant_id = $this->checkExpCriteriaForDate($sp_apcu_key, $splittest_params[ 'variants' ],
                $splittest_params[ 'expiration_value_date' ], $campaign_url);

        } else {
            $variant_id = $this->fetchNextVariantId($sp_apcu_key, $splittest_params[ 'variants' ]);

        }

 /*       // get the winner variant for splittest
        if (!empty($campaign_apcu_record) &&
            isset($campaign_apcu_record[ 'active_variant_id' ], $campaign_apcu_record[ 'sp_variants' ])) {
            $variant_id = $this->fetchNextVariantId($sp_apcu_key, $splittest_params[ 'variants' ]);
        }*/

        $campaign_apcu_record[ 'campaign_url' ] = $campaign_url;
        $campaign_apcu_record[ 'campaign_id' ] = $campaign_id;
        $campaign_apcu_record[ 'sp_status' ] = $splittest_params[ 'status' ];
        $campaign_apcu_record[ 'sp_variants' ] = $splittest_params[ 'total_variants' ];
        $campaign_apcu_record[ 'active_variant_id' ] = $variant_id;
        $this->apcuService->addOrOverwrite($sp_apcu_key, $campaign_apcu_record);

        return $variant_id;
    }

    /**
     * @param string $apcu_key
     * @param array $variants
     * @param int $expiration_visitors
     * @return int
     * @author Pradeep
     */
    private function checkExpCriteriaForVisitors(string $apcu_key, array $variants, int $expiration_visitors): int
    {
        /*
         * There were no visitors information as of now
         * Number of visitors for each variant has to be given by Matomo.
         *
         * Currently it fetches next variant_id.
         * */
        return $this->fetchNextVariantId($apcu_key, $variants);

    }

    /**
     * @param string $sp_apcu_key
     * @param array $variants
     * @return int
     * @author Pradeep
     */
    private function fetchNextVariantId(
        string $sp_apcu_key,
        array $variants
    ): int {
        $variant_id = 0;

        // Validates variants and active variants.
        if (empty($variants) || empty($sp_apcu_key)) {
            return $variant_id;
        }
        // Get Splittest from APCu
        $variant_apcu = $this->apcuService->get($sp_apcu_key);
        // fetch Splittest variant Id.
        if (($variant_apcu[ 'active_variant_id' ] + 1) <= $variant_apcu[ 'sp_variants' ]
            && $variant_apcu[ 'active_variant_id' ] + 1 <= count($variants)) {
            $next_variant_id = $variant_apcu[ 'active_variant_id' ] + 1;
        } else {
            $next_variant_id = 1;
        }

        $variant_id = $next_variant_id;

        return $variant_id;
    }

    /**
     * @param string $apcu_key
     * @param array $variants
     * @param int $expiration_conversions
     * @param string $campaign_url
     * @return int
     * @author Pradeep
     */
    private function checkExpCriteriaForConversions(
        string $apcu_key,
        array $variants,
        int $expiration_conversions,
        string $campaign_url
    ): int {

        $variant = [];
        $variant_id = 0;

        if (empty($campaign_url) || empty($expiration_conversions)) {
            return $variant_id;
        }

        $MSCIOleads = null ;//new MSMIOContact();
        $leads = $MSCIOleads->getLeadConversionsBasedOnCampaignURL($campaign_url);
        foreach ($leads as $lead) {
            foreach ($variants as &$variant) {
                if ($lead[ 'splitVersion' ] !== $variant[ 'variant_id' ]) {
                    continue;
                }

                if (!isset($variant[ 'conversions' ]) || $variant[ 'conversions' ] === 0) {
                    $variant[ 'conversions' ] = 1;
                } else {
                    ++$variant[ 'conversions' ];
                }

                if (isset($variant[ 'conversions' ]) && $variant[ 'conversions' ] >= $expiration_conversions) {
                    $variant_id = $variant[ 'variant_id' ];
                    break;
                }
            }
        }
        // unset value by reference
        unset($variant);
        if ($variant_id <= 0) {
            $variant_id = $this->fetchNextVariantId($apcu_key, $variants);
        }

        return $variant_id;
    }

    /**
     * @param string $apcu_key
     * @param array $variants
     * @param string $expiration_date
     * @param string $campaign_url
     * @return int
     * @author Pradeep
     */
    private function checkExpCriteriaForDate(
        string $apcu_key,
        array $variants,
        string $expiration_date,
        string $campaign_url
    ): int {

        $variant_id = 0;
        $current_date = date('Y-m-d');
        // removed Webspider microservice
        $MSCIOleads = null;// new MSMIOContact();
        if (empty($expiration_date)) {
            return $variant_id;
        }

        if (strtotime($expiration_date) < strtotime($current_date)) {
            $leads = $MSCIOleads->getLeadConversionsBasedOnCampaignURL($campaign_url);
            foreach($leads as $lead) {
                foreach($variants as &$variant) {
                    if($lead['splitVersion'] !== $variant['variant_id']) {
                        continue;
                    }
                    if (!isset($variant[ 'conversions' ]) || $variant[ 'conversions' ] === 0) {
                        $variant[ 'conversions' ] = 1;
                    } else {
                        ++$variant[ 'conversions' ];
                    }
                }
            }
            unset($variant);
            $conversions = array_column($variants, 'conversions');
            $variant_id = array_keys($conversions, max($conversions))[0] + 1;
        } else {
            $variant_id = $this->fetchNextVariantId($apcu_key, $variants);
        }

        return $variant_id;
    }
}

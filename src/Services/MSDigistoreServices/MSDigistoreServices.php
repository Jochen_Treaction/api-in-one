<?php


namespace App\Services\MSDigistoreServices;


use App\Services\HttpMicroServices\HttpMicroService;
use App\Services\AIOServices\MSCIOCampaignServices;

class MSDigistoreServices
{

    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var MSCIOCampaignServices
     * @author Pradeep
     */
    private $campaignServices;
    /**
     * @var string
     * @author Pradeep
     */
    private $apikey;

    public function __construct()
    {
        $this->http_client = new HttpMicroService();
        // removed WebSpider Microservice.
        $this->campaignServices = null;//new MSCIOCampaignServices();
    }
    
    /**
     * @param string $apikey
     * @param int $campaign_id
     * @return bool
     * @author Pradeep
     */
    public function isDigistoreEnabledForCampaign(string $apikey, int $campaign_id): bool
    {
        $status = false;
        if ($this->http_client === null || $this->campaignServices === null || empty($apikey)) {
            return $status;
        }
        if ($this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Digistore') !== null) {
            $status = true;
            $this->apikey = $apikey;
        }

        return $status;
    }

    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    private function fetchDigistoreConfiguration(int $campaign_id): array
    {
        $digistore_config = [];
        if ($this->http_client === null || $this->campaignServices === null) {
            return $digistore_config;
        }
        $result = $this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Digistore');
        if (!empty($result) && !empty($result[ 'value' ])) {
            $digistore_config = json_decode($result[ 'value' ], true, 512, JSON_UNESCAPED_SLASHES);
        }

        return $digistore_config;
    }


    /**
     * @param int $campaign_id
     * @param array $campaign_lead
     * @return string
     * @author Pradeep
     */
    public function fetchProductBuyURL(int $campaign_id, array $campaign_lead):string
    {
        $buy_url = '';
        if($campaign_id <= 0 || empty($campaign_lead) || !filter_var($campaign_lead['email'], FILTER_VALIDATE_EMAIL)) {
            return $buy_url;
        }
        if ($this->http_client === null || $this->campaignServices === null) {
            return $buy_url;
        }
        $digistore_properties = $this->fetchDigistoreConfiguration($campaign_id);
        $digistore_properties['email'] = $campaign_lead['email'] ?? '';
        $digistore_properties['first_name'] = $campaign_lead['first_name'] ?? '';
        $digistore_properties['last_name'] = $campaign_lead['last_name'] ?? '';
        $base64_text = base64_encode(json_encode($digistore_properties));
        if ($this->http_client->postRequest('POST', $_ENV[ 'MSDIGISTORE_URL' ] . '/get_product_buyurl', $base64_text)) {
            $resp = json_decode($this->http_client->getContent(), true, 512, JSON_UNESCAPED_SLASHES);
            if (!empty($resp) && !isset($resp['error'])) {
                $buy_url = $resp;
            }
        }

        return $buy_url;
    }
}
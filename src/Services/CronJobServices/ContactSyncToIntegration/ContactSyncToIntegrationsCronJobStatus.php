<?php

namespace App\Services\CronJobServices\ContactSyncToIntegration;

use App\Repository\ContactSyncToIntegrationRepository;
use App\Services\SemaphoreService;

class ContactSyncToIntegrationsCronJobStatus
{

    /**
     * Supported status for the table `lead_sync_log`.
     */
    public const SYNC_STATUS_TYP_NEW = 'new';
    public const SYNC_STATUS_TYP_WAITING = 'waiting';
    public const SYNC_STATUS_TYP_WORKING = 'working';
    public const SYNC_STATUS_TYP_DONE = 'done';
    public const SYNC_STATUS_TYP_ERROR = 'failed';
    public const SYNC_STATUS_TYP_RETRY = 'retry';

    public const SEMAPHORE_KEY_CNTCT_SYNC_TO_INTEGRATION = 'semaphore-contact-sync-to-integration';
    /**
     * @var SemaphoreService
     * @author Pradeep
     */
    private SemaphoreService $semaphoreService;

    public function __construct(
        SemaphoreService $semaphoreService,
        ContactSyncToIntegrationRepository $contactSyncToIntegrationRepository
    ) {
        $this->semaphoreService = $semaphoreService;
        $this->contactSyncToIntegrationRepository = $contactSyncToIntegrationRepository;
    }

    /**
     * @author Pradeep
     */
    public function start(int $id)
    {
        $message = "Integration under Process.";
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_WORKING, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), true);
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getSemaphoreKey()
    {
        return self::SEMAPHORE_KEY_CNTCT_SYNC_TO_INTEGRATION;
    }

    /**
     * @author Pradeep
     */
    public function stop(int $id, string $message)
    {
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_ERROR, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), false);
        // set the status to error.
        // removes the semaphore
    }

    public function completed(int $id)
    {
        $message = "All contacts are synchronized successfully.";
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_DONE, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), false);
    }

    public function Lock(int $id)
    {
        $message = "Synchronization is under process.";
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_WORKING, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), true);
    }

    public function unLock(int $id)
    {
        $message = "Synchronization is under process, waiting for the CronJob.";
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_WAITING, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), false);
    }

    public function retry(int $id)
    {
        $message = "Synchronization is under process, retrying the last chunk to sync.";
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_RETRY, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), false);
    }

    /**
     * @return bool
     * @author Pradeep
     */
    public function isRunning(): bool
    {
        return $this->semaphoreService->isLocked($this->getSemaphoreKey());
    }

    /**
     * @author Pradeep
     */
    public function error(int $id, string $message = "")
    {
        $this->contactSyncToIntegrationRepository->updateStatus($id, self::SYNC_STATUS_TYP_WAITING, $message);
        $this->semaphoreService->set($this->getSemaphoreKey(), false);
    }
}
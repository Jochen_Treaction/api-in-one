<?php

namespace App\Services\CronJobServices\ContactSyncToIntegration;

use App\Repository\ContactSyncToIntegrationRepository;
use Psr\Log\LoggerInterface;

class ContactSyncToIntegrationsCronJobProcessor
{

    /**
     * @var ContactSyncToIntegrationRepository
     * @author Pradeep
     */
    private ContactSyncToIntegrationRepository $contactSyncToIntegrationRepository;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        ContactSyncToIntegrationRepository $contactSyncToIntegrationRepository,
        LoggerInterface $logger
    ) {
        $this->contactSyncToIntegrationRepository = $contactSyncToIntegrationRepository;
        $this->logger = $logger;
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getNext(): ?array
    {
        try {

            // get the next integration_log with status 'retry'
            $statusRetry = $this->contactSyncToIntegrationRepository::SYNC_STATUS_TYP_RETRY;
            $jobWithStatusRetry = $this->contactSyncToIntegrationRepository->getByStatus($statusRetry);// First complete all the working jobs to Done.
            if (!empty($jobWithStatusRetry)) {
                return $jobWithStatusRetry;
            }

            // get the next integration_log with status 'waiting'
            $statusWaiting = $this->contactSyncToIntegrationRepository::SYNC_STATUS_TYP_WAITING;
            $jobWithStatusWaiting = $this->contactSyncToIntegrationRepository->getByStatus($statusWaiting);// First complete all the working jobs to Done.
            if (!empty($jobWithStatusWaiting)) {
                return $jobWithStatusWaiting;
            }

            // Take up the new Job only after all the 'working' jobs are moved to 'done'
            // then take up the job with status 'new'
            $statusNew = $this->contactSyncToIntegrationRepository::SYNC_STATUS_TYP_NEW;
            return $this->contactSyncToIntegrationRepository->getByStatus($statusNew);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    /**
     * getCurrent fetches the job which has status 'working.'
     * @return array|void
     * @author Pradeep
     */
    public function getCurrent()
    {
        // get the next integration_log with status 'working'
        $statusWorking = $this->contactSyncToIntegrationRepository::SYNC_STATUS_TYP_WORKING;
        $jobWithStatusWorking = $this->contactSyncToIntegrationRepository->getByStatus($statusWorking);// First complete all the working jobs to Done.
        if (!empty($jobWithStatusWorking)) {
            return $jobWithStatusWorking;
        }
    }

    /**
     * isJobPresent fetches number of jobs with status 'new'
     * @return bool
     * @author Pradeep
     */
    public function isJobPresent(): bool
    {
        $numberOfOpenJobs = $this->contactSyncToIntegrationRepository->getCountOfOpenJobs();
        if ($numberOfOpenJobs <= 0) {
            return false;
        }
        return true;
    }

    public function getPending()
    {

    }
}
<?php

namespace App\Services\CronJobServices\CronJobRetryService;

use App\Services\CronJobServices\ContactSyncToIntegration\ContactSyncToIntegrationsCronJobStatus;
use App\Services\SemaphoreService;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class ContactSyncToIntegrationCronJobRetry extends CronJobRetryService
{

    public const BUFFER_TIME_IN_SECONDS_TO_RETRY = 120;
    public const BUFFER_TIME_IN_SECONDS_TO_ERROR = 240;

    public function __construct(
        ContactSyncToIntegrationsCronJobStatus $contactSyncToIntegrationsCronJobStatus,
        LoggerInterface $logger
    ) {
        $this->contactSyncToIntegrationsCronJobStatus = $contactSyncToIntegrationsCronJobStatus;
        $this->logger = $logger;
    }

    /**
     * @author Pradeep
     */
    public function setToRetry(int $dbRowId)
    {
        $this->contactSyncToIntegrationsCronJobStatus->retry($dbRowId);
    }

    /**
     * @param string $lastUpdateTimeStamp
     * @return bool
     * @author Pradeep
     */
    public function isLastChunkProcessed(string $lastUpdateTimeStamp): bool
    {
        $status = false;
        try {
            if (empty($lastUpdateTimeStamp)) {
                throw new RuntimeException('failed to get updated timestamp from table.');
            }
            $currentTimeStamp = $this->currentTimeStamp();
            $timeDiff = $this->calTimeDiff($currentTimeStamp, $lastUpdateTimeStamp);
            if (is_null($timeDiff)) {
                throw new RuntimeException('failed to calculate the time difference.');
            }
            return $timeDiff < self::BUFFER_TIME_IN_SECONDS_TO_RETRY;

        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    public function updateRedisValue()
    {

    }
}
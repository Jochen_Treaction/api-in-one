<?php

namespace App\Services\CronJobServices\CronJobRetryService;


use App\Services\SemaphoreService;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

class CronJobRetryService
{

    public const SYNC_STATUS_TYP_RETRY = 'retry';


    public function currentTimeStamp(): string
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * calTimeDiff calculates the time difference between two differnce time stamp.
     * - return difference is always in seconds.
     * - null on failure
     * @param string $timeStamp1
     * @param string $timeStamp2
     * @return false|int|null
     * @author Pradeep
     */
    protected function calTimeDiff(string $timeStamp1, string $timeStamp2)
    {

        try {
            if (empty($timeStamp1) || empty($timeStamp2)) {
                throw new RuntimeException('Invalid timestamp provided');
            }
            $ts1 = strtotime($timeStamp1);
            $ts2 = strtotime($timeStamp2);
            if ((is_bool($ts1) && !$ts1) || (is_bool($ts2) && !$ts2)) {
                throw new RuntimeException('failed strtotime() : '
                                           . json_encode(['ts1' => $ts1, 'ts2' => $ts2], JSON_THROW_ON_ERROR));
            }
            return $ts2 - $ts1;
        } catch (RuntimeException|Exception  $e) {
            $this->logger->error('calTimeDiff :' . $e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }
}
<?php


namespace App\Services\MSWebinarisServices;


use App\Services\HttpMicroServices\HttpMicroService;
use App\Services\AIOServices\MSCIOCampaignServices;

class MSWebinarisServices
{

    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var MSCIOCampaignServices
     * @author Pradeep
     */
    private $campaignServices;

    /**
     * MSWebinarisServices constructor.
     */
    public function __construct()
    {
        $this->http_client = new HttpMicroService();
        // removed Webspider Microservice.
        $this->campaignServices = null;new MSCIOCampaignServices();
    }

    /**
     * @param string $apikey
     * @param int $campaign_id
     * @return bool
     * @author Pradeep
     */
    public function isWebinarisEnabledForCampaign(string $apikey, int $campaign_id):bool
    {
        $is_webinaris_active = false;
        if ($this->http_client === null || $this->campaignServices === null || empty($apikey)) {
            return $is_webinaris_active;
        }
        if ($this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Webinaris') !== null) {
            $is_webinaris_active = true;
            $this->apikey = $apikey;
        }

        return $is_webinaris_active;
    }

    /**
     * @param int $campaign_id
     * @param array $campaign_lead
     * @return bool
     * @author Pradeep
     */
    public function integrateLeadToWebinaris(int $campaign_id, array $campaign_lead):bool
    {
        $status = false;
        if($campaign_id <= 0 || empty($campaign_lead)) {
            return $status;
        }
        $webinaris_parameters['participant'] = $this->fetchWebinarisParticipantParms($campaign_lead);
        $webinaris_config = $this->fetchWebinarisConfiguration($campaign_id);
        $webinaris_parameters['webinarId'] = $webinaris_config['webinarId'] ?? '';
        $webinaris_parameters['apiPassword'] = $this->apikey ?? '';
        $base64_text = base64_encode(json_encode($webinaris_parameters));
        if ($this->http_client->postRequest('POST', $_ENV[ 'MSWEBINARIS_URL' ] . '/create_participant', $base64_text)) {
            $status = json_decode($this->http_client->getContent(), true, 512, JSON_UNESCAPED_SLASHES);
        }
        return $status;
    }


    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    private function fetchWebinarisConfiguration(int $campaign_id) :array
    {
        $webinaris_configuration=[];
        if($campaign_id <= 0) {
            return $webinaris_configuration;
        }
        $result = $this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Webinaris');
        if (!empty($result) && !empty($result[ 'value' ])) {
            $webinaris_configuration = json_decode($result[ 'value' ], true, 512, JSON_UNESCAPED_SLASHES);
        }
        return $webinaris_configuration;
    }

    /**
     * @param array $campaign_lead
     * @return array
     * @author Pradeep
     */
    private function fetchWebinarisParticipantParms(array $campaign_lead) :array
    {
        $participant = [];
        if(empty($campaign_lead) || !isset($campaign_lead['email'], $campaign_lead['first_name'] ,$campaign_lead['last_name'])) {
            return $participant;
        }
        $participant['email'] = $campaign_lead['email'];
        $participant['firstname'] = $campaign_lead['first_name'];
        $participant['lastname'] = $campaign_lead['first_name'];
        $participant['ip_address'] = $campaign_lead['ip'];
        $participant['time'] = '';

        return $participant;
    }
}
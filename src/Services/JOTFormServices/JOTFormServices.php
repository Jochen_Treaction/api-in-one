<?php


namespace App\Services\JOTFormServices;


use App\Repository\CampaignRepository;
use App\Repository\CustomFieldRepository;
use App\Repository\DataTypesRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterRepository;
use App\Services\HttpMicroServices\HttpMicroService;
use App\Services\ToolServices;
use App\Types\APITypes\SourceApiTypes;
use App\Types\MioStandardFieldTypes;
use Psr\Log\LoggerInterface;

class JOTFormServices
{

    private $logger;
    private ToolServices $toolServices;
    private ObjectRegisterRepository $objectRegisterRepository;
    private MIOStandardFieldRepository $standardFieldRepository;
    private MioStandardFieldTypes $mioStandardFieldTypes;


    public function __construct(
        LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        MioStandardFieldTypes $mioStandardFieldTypes,
        MIOStandardFieldRepository $standardFieldRepository,
        CampaignRepository $campaignRepository,
        CustomFieldRepository $customFieldRepository,
        DataTypesRepository $dataTypesRepository,
        ToolServices $toolServices
    ) {
        $this->logger = $logger;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->mioStandardFieldTypes = $mioStandardFieldTypes;
        $this->standardFieldRepository = $standardFieldRepository;
        $this->campaignRepository = $campaignRepository;
        $this->customFieldRepository = $customFieldRepository;
        $this->dataTypesRepository = $dataTypesRepository;
        $this->toolServices = $toolServices;
    }


    private function setKeyValuePair(string $field_name_value, string $field_value_value, array $standardFieldRecord = []): array
    {
        return [
            'field_name' => $field_name_value,
            $field_name_value => $field_value_value,
            'regex' => (empty($standardFieldRecord)) ? '.*' : $standardFieldRecord['regex'], // this must be defined in table mio.miostandardfield
            'required' => 0, // information does not come from JOTForms
            'datatype' => (empty($standardFieldRecord)) ? 'Text' : $standardFieldRecord['datatype'], // neither provided by JOTForms, nor defined in table mio.miostandardfield
        ];
    }


    /**
     * @param string $encodedData
     * @return array|null
     * @author Aki // Jsr // Pradeep
     * @internal structures payload form jotform to MIO acceptable formate
     */
    public function structureJotFormData(string $encodedData): ?string
    {
        $data = base64_decode($encodedData);
        $this->logger->info('$data', [$data, __METHOD__, __LINE__]);
        $postArray = json_decode($data, true, 20, JSON_OBJECT_AS_ARRAY);
        $this->logger->info('$postArray', [$postArray, __METHOD__, __LINE__]);


        // TODO: this stuff goes to JotFormsService >>>> START
        $receiveContact = [];
        $receiveContact[ 'base' ][ 'apikey' ] = $postArray[ 'apikey' ];
        $receiveContact[ 'base' ][ 'account_number' ] = $postArray[ 'account_number' ];
        $receiveContact[ 'base' ][ 'uuid' ] = $postArray[ 'uuid' ];

        $objRegIdOfCampaign = $this->objectRegisterRepository->getObjectRegisterIdWithUUID($postArray[ 'uuid' ]);
        $campaign = $this->campaignRepository->getCampaignDetailsByObjectRegisterId($objRegIdOfCampaign);
        $projectId = $this->campaignRepository->getProjectId($postArray[ 'account_number' ]);
        //remove base elements from the post array
        unset($postArray[ 'apikey' ], $postArray[ 'account_number' ], $postArray[ 'uuid' ], $postArray[ 'app_env' ]);
        //store the keys
        $postArrayKeys = array_keys($postArray);
        //loop the post array data
        foreach ($postArrayKeys as $jotFormKey) {
            //if the key has no brackets in jot forms
            if (array_key_exists($jotFormKey, $this->mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS)) {
                $mappedKey = $this->mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITHOUT_BRACKETS[ $jotFormKey ];
                $this->logger->info('$mappedKey', [$mappedKey, __METHOD__, __LINE__]);

                $mappedKeyParts = explode('_', $jotFormKey); //  contact_salutation => 0:contact, 1:salutation

                // get mio.miostandardfield.fieldtype
                $standardFieldRecord = $this->standardFieldRepository->getFieldTypeByFieldName($mappedKey);
                $this->logger->info('$standardFieldRecord', [$standardFieldRecord, __METHOD__, __LINE__]);
                $this->logger->info('setKeyValuePair PARAMS',
                    [$mappedKeyParts[ 1 ], $postArray[ $jotFormKey ], $standardFieldRecord, __METHOD__, __LINE__]);

                // TODO: phone value is an array  setKeyValuePair PARAMS ["phone",["(111) 111-1111"],{"fieldtype":"Standard","datatype":"Phone","regex":"[0-9-/ ()+]*"},"App\\Controller\\JotFormsController::v1JotFormsContactCreate",103] []
                $value = (is_array($postArray[ $jotFormKey ])) ? implode(',',
                    $postArray[ $jotFormKey ]) : $postArray[ $jotFormKey ];

                if ($standardFieldRecord[ 'datatype' ] === 'Date') {
                    $tempDate = $postArray[ $jotFormKey ][ 2 ] . '-' . $postArray[ $jotFormKey ][ 1 ] . '-' . $postArray[ $jotFormKey ][ 1 ];
                    $value = date("d-m-Y", strtotime($tempDate));
                }
                if (!empty($value)) {
                    $receiveContact[ $mappedKeyParts[ 0 ] ][ $standardFieldRecord[ 'fieldtype' ] ][] = $this->setKeyValuePair($mappedKey,
                        $value, $standardFieldRecord);
                }
            } elseif (array_key_exists($jotFormKey, $this->mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS)) {
                //if the key has brackets
                $mappedKey = $this->mioStandardFieldTypes::JOTFORMS_MAPPING_BY_WITH_BRACKETS[$jotFormKey];
                $mappedKeyParts = explode('_', $jotFormKey); //  contact_salutation => 0:contact, 1:salutation
                $standardFieldRecord = $this->standardFieldRepository->getFieldTypeByFieldName($mappedKey);
                $this->logger->info('$standardFieldRecord', [$standardFieldRecord, __METHOD__, __LINE__]);

                if (is_array($postArray[$jotFormKey])) {
                    $tempArray = [];
                    foreach ($postArray[$jotFormKey] as $value) {
                        $tempArray[] = explode('|', $value)[0];
                    }
                    $value = implode(',', $tempArray); // get as list
                    $receiveContact[$mappedKeyParts[0]][$standardFieldRecord['fieldtype']][] = $this->setKeyValuePair($mappedKeyParts[1], $value, $standardFieldRecord);
                } else {
                    $value = (is_array($postArray[$jotFormKey])) ? implode(',', $postArray[$jotFormKey]) : $postArray[$jotFormKey];

                    $receiveContact[$mappedKeyParts[0]][$standardFieldRecord['fieldtype']][] = $this->setKeyValuePair($mappedKey, $value, $standardFieldRecord);
                }
            } else { // handle customfields
                $dataTypeOfField = 'Text';
                $value = (is_array($postArray[ $jotFormKey ])) ? implode(',',
                    $postArray[ $jotFormKey ]) : $postArray[ $jotFormKey ];
                $jotFormKey = ($jotFormKey === 'submission_id') ? 'submissionId' : $jotFormKey;
                $jotFormKey = ($jotFormKey === 'formID') ? 'formId' : $jotFormKey;

                if (str_contains($jotFormKey, '_')) {
                    $jotFormKey = substr($jotFormKey, strpos($jotFormKey, "_") + 1);
                }

                // Fetch Customfield details from DB.
                $customFieldDetails = $this->customFieldRepository->getCustomFieldDetailsForCampaign($jotFormKey,
                    $projectId);
                // Accordingly fetch the datatype details about customfields.
                if (!empty($customFieldDetails) && isset($customFieldDetails[ 'datatypes_id' ])) {
                    $dataType = $this->dataTypesRepository->get($customFieldDetails[ 'datatypes_id' ]);
                    if (!empty($dataType) && isset($dataType[ 'name' ])) {
                        $dataTypeOfField = $dataType[ 'name' ];
                    }
                }

                $receiveContact[ 'contact' ][ 'custom' ][] = $this->setKeyValuePair($jotFormKey, $value,
                    ['regex' => '', 'datatype' => $dataTypeOfField]);
            }
        }
        // TODO: <<<< END this stuff goes to JotFormsService
        $this->logger->info('$receiveContact content', [$receiveContact, __METHOD__, __LINE__]);

        try {
            if ($_ENV['APP_ENV'] === 'dev') {
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'receiveContact.json', json_encode($receiveContact, JSON_PRETTY_PRINT));
            }
        } catch (\Exception $e) {
            $logger->info('file_put_contents failed', [$e->getMessage(), __METHOD__, __LINE__]);
        }
        try{
            return base64_encode(json_encode($receiveContact));
        }catch(Exception $e){
            $logger->info('file_put_contents failed', [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        }
    }

}
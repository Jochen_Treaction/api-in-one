<?php

namespace App\Services\JOTFormServices;

use App\Repository\ObjectRegisterMetaRepository;
use App\Services\RedisService;
use Doctrine\DBAL\ConnectionException;
use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Connection;
use RuntimeException;

class JotFormPluginService
{

    public const META_KEY_JOTFORM_ID = 'jotform_form_id';
    public const META_KEY_JOTFORM_USERNAME = 'jotform_user_name';
    public const META_KEY_JOTFORM_APIKEY = 'jotform_apikey';
    public const META_KEY_JOTFORM_REQUEST_URI = 'jotform_request_uri';
    public const META_KEY_JOTFORM_CONFIG = 'jotform_config';

    public const REDIS_KEY_JOTFORM = 'jotform_id_';
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    /**
     * @var RedisService
     * @author Pradeep
     */
    private RedisService $redisService;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $dbConnection;

    public function __construct(
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        RedisService $redisService,
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->redisService = $redisService;
        $this->dbConnection = $connection;
        $this->logger = $logger;
    }

    /**
     * Searches ObjectRegisterMeta of the Webhook or Page with JotFormId.
     * @param string $jtFormId
     * @param string $jtUserName
     * @param string $jtApiKey
     * @return array
     * @author Pradeep
     */
    public function search(string $jtFormId, string $jtUserName = '', string $jtApiKey = ''): array
    {
        return $this->getFieldMapping($jtFormId);
        /*        $searchRes = $this->objectRegisterMetaRepository->search(self::META_KEY_JOTFORM_ID, $jtFormId);
                return $searchRes;*/
    }

    /**
     * Fetches the mapping config of Webhook and JotForm fields.
     * - jotform fields are stored under self::META_KEY_JOTFORM_CONFIG inside objectRegMeta Table.
     * @param int $jtFormId
     * @return array|mixed
     * @author Pradeep
     */
    public function getFieldMapping(string $jtFormId): array
    {
        $config = [];
        try {
            $metaValue = json_encode($jtFormId, JSON_THROW_ON_ERROR);
            $objRegMetaResult = $this->objectRegisterMetaRepository->search(self::META_KEY_JOTFORM_ID, $metaValue);
            $this->logger->info("GETFIELDMAPPING : ObjREGRESULT",
                [$objRegMetaResult[ 'object_register_id' ], __METHOD__, __LINE__]);
            $objRegId = (int)$objRegMetaResult[ 'object_register_id' ];
            $objRegMetaRows = $this->objectRegisterMetaRepository->getMultipleMetaValues($objRegId);
            $this->logger->info("GETFIELDMAPPING :", [$objRegMetaRows, __METHOD__, __LINE__]);
            foreach ($objRegMetaRows as $row) {
                if ($row[ 'meta_key' ] !== self::META_KEY_JOTFORM_CONFIG && !empty($row[ 'meta_value' ])) {
                    continue;
                }
                $config = json_decode($row[ 'meta_value' ], true, 512, JSON_THROW_ON_ERROR);
                break;
            }
            return $config;
        } catch (\JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $config;
        }
    }

    /**
     * Insert jotform data inside objectRegMeta Table.
     * - data related to
     *          self::META_KEY_JOTFORM_CONFIG,
     *          self::REDIS_KEY_JOTFORM,
     *          self::META_KEY_JOTFORM_ID,
     *          self::META_KEY_JOTFORM_APIKEY,
     *          self::META_KEY_JOTFORM_REQUEST_URI,
     *          self::META_KEY_JOTFORM_USERNAME
     * - insert is performed after the jotform is connected with Webhook.
     * - Rollback the transaction when atleast one of the key is failed to insert.
     * @param int $objRegId
     * @param array $jotFormPluginConfig
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function insert(int $objRegId, array $jotFormPluginConfig): bool
    {
        $supportedJTMetaKeys = $this->registeredJTMetaKeys();
        if (empty($jotFormPluginConfig) || $objRegId <= 0) {
            return false;
        }

        try {
            $this->dbConnection->beginTransaction();
            foreach ($jotFormPluginConfig as $jtFormKey => $jtFormValue) {
                if (empty($jtFormKey) || !in_array($jtFormKey, $supportedJTMetaKeys, true)) {
                    continue;
                }
                $jtFormValueToStr = json_encode($jtFormValue, JSON_THROW_ON_ERROR);
                // Check if the metakey for given objectRegId is present or not.
                // Update the metaValue if the entry is already present.
                // Insert the metaValue if the entry is not present.
                $existingMetaValue = $this->objectRegisterMetaRepository->getMetaValue($objRegId, $jtFormKey);
                if ($existingMetaValue === null) {
                    $status = !is_null($this->objectRegisterMetaRepository->insert($objRegId, $jtFormKey,
                        $jtFormValueToStr));
                } else {
                    $status = $this->objectRegisterMetaRepository->update($objRegId, $jtFormKey, $jtFormValueToStr);
                }
                if (!$status) {
                    throw new RuntimeException("failed to insert metakey: " . $jtFormKey . " with metavalue: " . $jtFormValue);
                }
            }
            $this->dbConnection->commit();
            return true;
        } catch (ConnectionException | RuntimeException | Exception  $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->dbConnection->rollback();
            return false;
        }
    }

    /**
     * Helper function to validate key inserted inside the objectRegMeta table.
     * - each key is uniquely defined to specific objectRegId of webhook or Page.
     * @return string[]
     * @author Pradeep
     */
    private function registeredJTMetaKeys()
    {
        return [
            self::META_KEY_JOTFORM_CONFIG,
            self::META_KEY_JOTFORM_ID,
            self::META_KEY_JOTFORM_APIKEY,
            self::META_KEY_JOTFORM_REQUEST_URI,
            self::META_KEY_JOTFORM_USERNAME,
        ];
    }

    /**
     * remove deletes the JotForm configuration from objectRegMeta table for
     * a specific objRegId.
     * @param int $objRegId
     * @param string $jotformId
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function remove(int $objRegId, string $jotformId)
    {
        $debug[ 'objRegId' ] = $objRegId;
        $debug[ 'jotformId' ] = $jotformId;
        $supportedJTMetaKeys = $this->registeredJTMetaKeys();
        $debug[ '$supportedJTMetaKeys' ] = $supportedJTMetaKeys;
        if ($objRegId <= 0 || empty($jotformId)) {
            return false;
        }

        try {
            $this->dbConnection->beginTransaction();
            $debug[ 'transaction_started' ] = true;
            foreach ($supportedJTMetaKeys as $jtMetaKey) {
                $debug[ $jtMetaKey ] = $objRegId;
                if (!$this->objectRegisterMetaRepository->delete($objRegId, $jtMetaKey)) {
                    $debug[ 'delete_failed_obj' ] = $objRegId;
                    $debug[ 'delete_failed_jtmtaky' ] = $jtMetaKey;
                    throw new RuntimeException("failed to delete metakey: " . $jtMetaKey . " with ObjRegid: " . $objRegId);
                }
            }

            $this->dbConnection->commit();
            $debug[ 'transaction_commited' ] = true;
            $this->logger->error("REMOVE:DEBUG", [$debug, __METHOD__, __LINE__]);
            return true;
        } catch (ConnectionException | RuntimeException | Exception  $e) {
            $debug[ 'found_exception' ] = true;
            $this->logger->error($e->getMessage(), [$debug, __METHOD__, __LINE__]);
            $this->dbConnection->rollback();
            $debug[ 'transaction_rollback' ] = true;
            return false;
        }
    }

    /**
     * Helper function to map the jotForm fields to JotFormMetaKey for objectRegMeta Table.
     * @param string $jotformId
     * @param string $jotFormUserName
     * @param string $jotFormApiKey
     * @param string $jotFormRequestUri
     * @param string $mapping
     * @return string[]
     * @author Pradeep
     */
    public function mapToJotFormMetaKeys(
        string $jotformId,
        string $jotFormUserName = "",
        string $jotFormApiKey = "",
        string $jotFormRequestUri = "",
        string $mapping = "",
        string $apikey = "",
        int $accountNumber = 0,
        string $uuid = ""
    ): array {
        $config = [
            'base' => [
                'apikey' => $apikey,
                'account_no' => $accountNumber,
                'uuid' => $uuid,
            ],
            'config' => [
                'form_id' => $jotformId,
                'mapping' => base64_encode($mapping),
            ],
        ];

        return [
            self::META_KEY_JOTFORM_USERNAME => $jotFormUserName ?? 'not-provided',
            self::META_KEY_JOTFORM_APIKEY => $jotFormApiKey ?? 'not-provided',
            self::META_KEY_JOTFORM_REQUEST_URI => $jotFormRequestUri ?? 'not-provided',
            self::META_KEY_JOTFORM_ID => $jotformId,
            self::META_KEY_JOTFORM_CONFIG => $config,
        ];
    }

    public function getWebhooksNotInstalledWithJotForm(string $apikey, string $accountNumber): ?array
    {
        //$sql =
    }

    /*    private function getObjRegId(string $jtFormId)
        {
            $objRegId = 0;
            if(empty($jtFormId)) {
                return $objRegId;
            }
            $objRegMetaDetails = $this->search($jtFormId);
            if(!empty($objRegMetaDetails) && isset($objRegMetaDetails['object_register_id'])) {
                $objRegId = $objRegMetaDetails['object_register_id'];
            }
            return $objRegId;
        }

        private function prepareRedisKey(string $formId)
        {
            if(empty($formId)) {
                return false;
            }
            $this->setKey(self::REDIS_KEY_JOTFORM.$formId);
            return true;
        }

        private function setKey(string $redisKey)
        {
            $this->redisKey = $redisKey;
        }

        private function getRedisKey()
        {
            return $this->redisKey;
        }*/
}
<?php

namespace App\Services;

use Redis;
use Psr\Log\LoggerInterface;

class RedisService
{

    public const ONE_HOUR_IN_SECONDS = 3600;
    public const FIFTEEN_MINUTES = 900;

    public function __construct(
        LoggerInterface $logger,
        string $redisHost,
        string $redisPort,
        string $redisUser,
        string $redisPw
    )
    {
        $this->redis = new Redis();
        // $this->redis->connect('127.0.0.1', 6379);
        $this->redis->connect($redisHost, $redisPort);
        $redisAuth = $this->redis->auth([$redisUser, $redisPw]);
        $logger->info('$redisAuth', [$redisAuth, __METHOD__, __LINE__]);
        $this->redisUser = $redisUser;
        $this->redisPw = $redisPw;
        $this->redisHost = $redisHost;
    }

    public function delete(string $key): int
    {
        return $this->redis->del($key);
    }

    public function get(string $key)
    {
        if (!empty($key)) {
            return $this->redis->get($key);
        }
        return false;
    }

    /**
     * set stores the cache into redis server,
     * automatically expires the cache in 15 minutes.
     * @param string $key
     * @param string $value
     * @return bool
     * @author Pradeep
     */
    public function set(string $key, string $value, $expireTime = self::FIFTEEN_MINUTES): bool
    {
        return !empty($key) && $this->redis->set($key, $value, $expireTime);
    }

    /**
     * hashesObject is stores into the array into redis server.
     * - hashesObject could be -> campaign or company or Contact details.
     * - by default all the hashesObject expires in one hour.
     * @param string $key
     * @param array $keyValuePair -> has to be key value pairs
     * @author Pradeep
     */
    public function hashesObject(string $key, array $keyValuePair)
    {
        if (empty($key) || empty($keyValuePair)) {
            return false;
        }

        foreach ($keyValuePair as $field => $value) {
            if (!is_string($value)) {
                $value = json_encode($value, JSON_THROW_ON_ERROR);
            }
            $this->hashesSet($key, $field, $value);
            $this->redis->expire($key, self::ONE_HOUR_IN_SECONDS);
        }
    }

    /**
     * @param string $key
     * @param array $contactDetails -> has to be key value pairs
     * @author Pradeep
     */
    public function hashesContact(string $key, array $contactDetails)
    {
        if (empty($key) || empty($contactDetails)) {
            return false;
        }

        foreach ($contactDetails as $field => $value) {
            if (!is_string($value)) {
                $value = json_encode($value, JSON_THROW_ON_ERROR);
            }
            $this->hashesSet($key, $field, $value);
        }
    }

    public function hashesAccount()
    {

    }

    public function hashesExists(string $key, string $hashKey)
    {
        $this->redis->hExists($key, $hashKey);
    }

    public function hashesSet(string $key, string $hashKey, string $value)
    {
        $this->redis->hSet($key, $hashKey, $value);
    }

    public function hashesGet(string $key, string $hashKey)
    {
        return $this->redis->hGet($key, $hashKey);
    }

    public function hashesAll(string $key)
    {
        return $this->redis->hGetAll($key);
    }

    public function flushDB()
    {
        return $this->redis->flushDB();
    }

    public function getUserDetails()
    {
        return [
            "user" => $this->redisUser,
            "password" => $this->redisPw,
            "host" => $this->redisHost
        ];
    }

}

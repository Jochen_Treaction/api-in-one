<?php
/**
 * ProcessIntegrationPointService Is used to create and managing process at the Server.
 * The word process could be 'Page' or 'webhook'
 */
namespace App\Services\PIPServices;


use Exception;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProcessIntegrationPointService
{
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
    }


    /**
     * @param string $microService
     * @param string $campaignName
     * @param int $campaignId
     * @param int $companyId
     * @param string $useCaseType
     * @param string $companyName
     * @return bool
     * @author pva
     */
    public function createProcessInServer(
        string $microService,
        string $campaignName,
        int $campaignId,
        int $companyId,
        string $useCaseType,
        string $companyName

    ): bool {
        $status = false;

        $token = $this->createProcessToken($campaignId, $companyId);
        $this->logger->info("createProcessInServer ", [
            '$campaign_name' => $campaignName,
            '$campaign_id' => $campaignId,
            '$company_id' => $companyId,
            '$company_name' => $companyName,
            '$token' => $token,
        ]);
        if (empty($campaignName) || $companyId <= 0 || empty($token)) {
            throw new Exception('Invalid CampaignName or Company Id, or Token passed while create process in Server');
        }

        try {
            $http_client_resp = $this->httpClient->request('POST', $microService . '/create', [
                'body' => [
                    'campaign_name' => $campaignName,
                    'company' => $companyName,
                    'token' => $token,
                    'useCaseType' => $useCaseType,
                ],
            ]);
            $this->logger->info('http status code ' . $http_client_resp->getStatusCode());
            $this->logger->info($http_client_resp->getContent(), [__METHOD__, __LINE__]);
            $status = $http_client_resp->getContent();
            $this->logger->info('crate campaign status ' . $status, [__METHOD__, __LINE__]);
        } catch (TransportExceptionInterface | ClientExceptionInterface |
        RedirectionExceptionInterface | ServerExceptionInterface | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
        return $status;
    }


    /**
     * @param string $microService
     * @param string $campaignName
     * @param int    $version
     * @param string $company
     * @param string $campaignSettings
     * @return bool
     */
    public function activateProcessInServer(
        string $microService,
        string $campaignName,
        int $version,
        string $company,
        string $campaignSettings
    ): bool {
        $status = true;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($campaignName) || empty($company) || $version <= 0) {
            return $status;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/activate', [
                'body' => [
                    'campaign_name' => $campaignName,
                    'company' => $company,
                    'version' => $version,
                    'campaignSettings' => $campaignSettings,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface | ClientExceptionInterface |
        RedirectionExceptionInterface | ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }



    /**
     * @param int $campaignId
     * @param int $companyId
     * @return string
     * @author Pradeep
     */
    private function createProcessToken(int $campaignId, int $companyId): string
    {
        $salt = str_rot13(base64_encode('cio'));
        $pepper = str_rot13(base64_encode('treaction'));
        try {
            if ($companyId <= 0 || $campaignId <= 0) {
                throw new Exception('Invalid CampaignId or Company Id passed while creating Process Token');
            }
            $encrypt_campaign_id = str_rot13(base64_encode('campaign_id=' . decoct($campaignId)));
            $encrypt_company_id = str_rot13(base64_encode('company_id=' . decoct($companyId)));
            $encrypt_str = str_rot13($salt . '|' . $encrypt_campaign_id . '|' . $encrypt_company_id . '|' . $pepper);
            $encrypt_md5 = md5($encrypt_str);
            return $encrypt_str . '|' . $encrypt_md5;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return '';
        }
    }
}
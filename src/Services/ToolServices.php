<?php


namespace App\Services;


use Exception;
use JsonException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ToolServices implements LoggerAwareInterface
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $data
     * @return string
     * @throws JsonException
     * @author Pradeep
     */
    public function encryptDataToNameValuePair(array $data): string
    {
        $encrypt_str = '';
        if (empty($data)) {
            return $encrypt_str;
        }
        try {
            foreach ($data as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                $encrypt_arr[] = [
                    'name' => $key,
                    'value' => $value
                ];
/*                $encrypt_arr[ 'name' ] = $key;
                $encrypt_arr[ 'value' ] = $value;*/
            }
            if (empty($encrypt_arr)) {
                return $encrypt_str;
            }
            $encrypt_json = json_encode($encrypt_arr, JSON_THROW_ON_ERROR);
            $encrypt_str = base64_encode($encrypt_json);
            return $encrypt_str;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $encrypt_str;
        }
    }

    /**
     * @param string $req_data
     * @return array
     */
    public function decryptDataToNameValuePair(string $req_data): array
    {
        $data = [];
        if (empty($req_data)) {
            return $data;
        }
        try {

            $de_data = base64_decode($req_data);// Decode data
            $de_arr = json_decode($de_data, true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);// Convert Json to Arr

            if (!empty($de_arr)) {
                foreach ($de_arr as $arr) {
                    $data[ $arr[ 'name' ] ] = $arr[ 'value' ] ?? '';
                }
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $data;
    }

    /**
     * @param string $req_data
     * @return array
     */
    public function decryptDataToPair(string $req_data): array
    {
        $data = [];
        if (empty($req_data)) {
            return $data;
        }
        try {
            $de_data = base64_decode($req_data);// Decode data
            $de_arr = json_decode($de_data, true, 512,
                JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);// Convert Json to Arr
            if (!empty($de_arr)) {
                foreach ($de_arr as $key => $value) {
                    foreach($value as $x => $y){
                        $data[ $x ] = $y ?? '';
                    }
                }
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $data;
    }





    /**
     * @param Response $response
     * @return Response
     * @author Pradeep
     */
    public function setHeadersToAvoidCORSBlocking(Response $response): Response
    {
        $response->headers->set('Content-Type', 'application/json');
        //$response->headers->set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //$response->headers->set('Content-Type', 'text/plain');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers',
            'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer');
        $response->headers->set('Access-Control-Max-Age', '86400');
        $response->headers->set('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
        return $response;
    }

    /**
     * @param Response $response
     * @param string $message
     * @param bool $status
     * @param array|null $response_body
     * @return Response
     * @author Pradeep
     */
    public function setResponseWithMessage(Response $response, string $message, bool $status, array $response_body=null): Response
    {
        $return_response = $response;
        try {
            $return_response = $response->setContent(json_encode([
                'status' => $status,
                'message' => $message,
                'response' => $response_body,
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException  | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return_response;
    }

    /**
     * @param Response $response
     * @param array $response_body
     * @param bool $status
     * @param string|null $message
     * @return Response
     * @author Pradeep
     */
    public function setJsonResponse(
        Response $response,
        array $response_body,
        bool $status = false,
        string $message = null
    ): Response {
        $return = $response;
        try {
            $return = $response->setContent(json_encode([
                'status' => $status,
                'message' => $message,
                'response' => $response_body,
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }

    /**
     * @param string $campaign_token
     * @return array
     * @author Pradeep
     */
    public function decryptCampaignToken(string $campaign_token): array
    {
        $ids = [];
        $token_arr = explode('|', ($campaign_token));
        $md5_str = md5($token_arr[ 0 ] . '|' . $token_arr[ 1 ] . '|' . $token_arr[ 2 ] . '|' . $token_arr[ 3 ]);
        // last part of the array is always an md5 string.
        if ($md5_str !== $token_arr[ 4 ]) {
            return $ids;
        }
        foreach ($token_arr as $str) {
            // Ignore md5 string
            if ($str === $md5_str) {
                continue;
            }
            $str_decoded = base64_decode(($str));
            // Ignore salt and pepper.
            if ($str_decoded === 'cio' || $str_decoded === 'treaction') {
                continue;
            }
            $value = explode('=', $str_decoded);
            // if ($value[ 0 ] === 'campaign_id' || $value[ 0 ] === 'company_id') { // error 2020-03-20

            $this->logger->info('DECRYPT AND FETCH IDS: ', [$value[ 0 ] => $value[ 1 ], __METHOD__, __LINE__]);

            if ($value[ 0 ] === 'campaign_id' || $value[ 0 ] === 'company_id') {
                $ids[ $value[ 0 ] ] = octdec($value[ 1 ]); // error
                $this->logger->info('DECRYPT AND FETCH IDS: ',
                    [$value[ 0 ] => octdec($value[ 1 ]), __METHOD__, __LINE__]);
            }
        }
        return $ids;
    }

    /**
     * @param string $string
     * @return string
     * @author Pradeep
     */
    public function simpleEncrypt(string $string):string
    {
        // return base64_encode(gzcompress(str_rot13($string)));
        return base64_encode(str_rot13($string));
    }

    /**
     * @param string $string
     * @return string
     * @author Pradeep
     */
    public function simpleDecrypt(string $string):string
    {
        //return str_rot13(gzuncompress(base64_decode($string)));
        return str_rot13(base64_decode($string));
    }

    public function simpleBase64Decrypt(string $string): string
    {
        return base64_decode($string);
    }

    public function arrToBase64Encrypt(array $array): string
    {
        if(empty($array)) {
            return '';
        }
        return base64_encode(json_encode($array, JSON_THROW_ON_ERROR));
    }

    /**
     * decryptContentData decrypts the payload
     * @param string $encodedString
     * @param bool $useGZCompress
     * @return array
     * @author Pradeep
     */
    public function decryptContentData(string $encodedString, bool $useGZCompress = false): array
    {
        $data = [];
        try {
            if (empty($encodedString)) {
                return $data;
            }
            $decodedString = base64_decode($encodedString);
            // decompress the data if 'gzcompress' is used to compress the data.
            if ($useGZCompress) {
                $decodedString = gzuncompress($decodedString);
            }
            $array = json_decode($decodedString, true, 512, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
            if (!empty($array)) {
                $data = $array;
            }
            return $data;
        } catch (JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $data;
        }
    }

    /**
     * Encrypts the array of data to base64Encoded string.
     *
     * @param array $data
     * @param bool $useGZCompress
     * @return string
     * @author Pradeep
     */
    public function encryptContentData(array $data, bool $useGZCompress = false): string
    {
        $result = '';
        if (empty($data)) {
            return $result;
        }
        try {
            $jsonString = json_encode($data, JSON_THROW_ON_ERROR);
            if ($useGZCompress) {
                $jsonString = gzcompress($jsonString);
            }
            $result = base64_encode($jsonString);
        } catch (JsonException $e) {
            $result = '';
        }
        return $result;
    }

    /**
     * @param array $data
     * @return bool
     * @author Pradeep
     */
    public function validateJsonStructureForWebhook(array $data): bool
    {
        $status = true;
        // check if the array has Apikey, accountNo, ObjectRegisterId,
        if(!isset($data['base']) || empty($data['base'])) {
            $status=false;
        }
        if(!isset($data['base']['apikey'],$data['base']['accountNo'], $data['base']['objectRegisterId'])) {
            $status = false;
        }
        if(empty($data['base']['apikey'])|| empty($data['base']['accountNo']) || empty($data['base']['objectRegisterId'])) {
            $status = false;
        }
        if(!$this->validateDataTypesForFields($data) || !$this->validateRequiredFields($data)) {
            $status = false;
        }
        return $status;
    }

    public function validateRequiredFields(array $formFields):bool
    {
        $status = true;
        if(isset($formFields['contact']['standard'])) {
            $standardFields = $formFields['contact']['standard'];
/*            foreach($standardFields as $fields) {
                if(empty($fields))
            }*/
        }
        if(isset($formFields['contact']['custom'])) {
            $customFields = $formFields['contact']['custom'];
        }
        // check if the value present for all the required field yes.
        return false;
    }
    

    public function formatContactFieldsAsKeyValuePair(array $contactFields):?array
    {
        $formatedArr = [];
        if(empty($contactFields)) {
            return $formatedArr;
        }
        foreach($contactFields  as $fields) {
            foreach($fields as $key => $value) {
                if(empty($key) || in_array($key, ['required', 'datatype', 'regex'])) {
                    continue;
                }
                $formatedArr[$key] = $fields;
            }
        }
        return $formatedArr;
    }

    /**
     * @param array $data
     * @param string $fieldName
     * @return string|null
     * @author Pradeep
     */
    public function parseFieldFromContactData(array $data, string $fieldName):?string
    {
        $fieldValue ='';
        try {
            if (empty($data) || empty($data[ 'contact' ]) || empty($fieldName)) {
                throw new RuntimeException('Invalid Contact data or Field name provided');
            }
            $standardFields = $data[ 'contact' ][ 'standard' ];
            foreach ($standardFields as $field) {
                if (empty($field) || !isset($field[ $fieldName ])) {
                    continue;
                }
                $fieldValue = $field[ $fieldName ];
            }
            return $fieldValue;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $fieldValue;
        }
    }
}
<?php


namespace App\Services\MSManagementServices;


use App\Services\HttpMicroServices\HttpMicroService;
use Exception;
use JsonException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

class MSManagementServices implements LoggerAwareInterface
{

    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    /**
     * MSManagementServices constructor.
     */
    public function __construct()
    {
        $this->http_client = new HttpMicroService();
    }

    /**
     * @param int $account_id
     * @return string
     * @author Pradeep
     */
    public function generateCIOAPIKey(int $account_id): ?string
    {
        $apikey = null;
        try {
            if ($account_id <= 0 || $this->http_client === null || !isset($_ENV[ 'MSMANAGEMENT_URL' ])) {
                throw new RuntimeException('Invalid Parameters provided');
            }
            $base64_text = base64_encode(json_encode(['account_id' => $account_id], JSON_THROW_ON_ERROR));
            if (!$this->http_client->postRequest('POST', $_ENV[ 'MSMANAGEMENT_URL' ] . '/generatecioapikey',
                $base64_text)) {
                throw new RuntimeException('Failed to post http_client request');
            }
            $resp = json_decode($this->http_client->getContent(), true, 512,
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_OBJECT_AS_ARRAY);
            if (!empty($resp) && !empty($resp[ 'api_key' ]) && $resp[ 'status' ]) {
                $apikey = $resp[ 'api_key' ];
            }

            return $apikey;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $apikey;
        }
    }

    /**
     * @param LoggerInterface $logger
     * @author Pradeep
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
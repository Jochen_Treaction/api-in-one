<?php

namespace App\Services;

use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class MessageBusServices
{
    /**
     * @var MessageBusInterface
     * @author Pradeep
     */
    private MessageBusInterface $messageBus;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(MessageBusInterface $messageBus, LoggerInterface $logger)
    {
        $this->messageBus = $messageBus;
        $this->logger = $logger;
    }

    public function dispatchMessage($message): array
    {
        try {
            $envelope = $this->messageBus->dispatch($message);
            $handledStamp = $envelope->last(HandledStamp::class);
            if ($handledStamp === null) {
                throw new RuntimeException('failed to integrate the chunk');
            }
            $result = $handledStamp->getResult();
            if (empty($result) || !isset($result[ 'status' ], $result[ 'message' ])) {
                throw new RuntimeException('failed to process the chunk');
            }
            return ['status' => $result[ 'status' ], 'message' => $result[ 'message' ]];
        } catch (Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }
    }
}
<?php


namespace App\Services\MSMIOServices;


use App\Entity\Users;
use App\Repository\ContactRepository;
use App\Repository\DataTypesRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\UniqueObjectTypeRepository;
use App\Services\AIOServices\Contact\ContactFieldServices;
use App\Services\AIOServices\ContactServices;
use App\Services\AIOServices\FieldTypeListServices;
use App\Services\AIOServices\FieldTypeStringServices;
use App\Services\HttpMicroServices\HttpMicroService;
use App\Services\ToolServices;
use App\Types\ContactFieldDataTypes;
use App\Types\ContactPermissionTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MSMIOServices implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public const ITYPE_MA = 'MarketingAutomation';
    public const ITYPE_DOI = 'DOI';
    public const ITYPE_CE = 'ContactEvent';
    public const ITYPE_DOI_XOR_CE = 'DOIXorContactEvent';

    // const ITYPE_NONE = 'None';
    public const PERMISSION_TYPE_NONE = 1;
    public const PERMISSION_TYPE_SINGLE_OPT_IN = 2;
    public const PERMISSION_TYPE_CONFIRMED_OPT_IN = 3;
    public const PERMISSION_TYPE_DOUBLE_OPT_IN = 4;
    public const PERMISSION_TYPE_DOUBLE_OPT_IN_SINGLE_USER_TRACKING = 5;
    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;

    // Permission
    /**
     * @var MSCIOCampaignServices
     * @author Pradeep
     */
    private $campaignServices;
    /**
     * @var string
     * @author Pradeep
     */
    private $apikey;
    /**
     * @var int
     * configuration target for MIO.
     * @author Pradeep
     */
    private $configuration_target_id = 8;
    /**
     * @var HttpClientInterface
     * @author Pradeep
     */
    private $httpClient;
    /**
     * @var IntegrationsRepository
     * @author Pradeep
     */
    private $integrationsRepository;
    /**
     * @var StandardFieldMappingRepository
     * @author Pradeep
     */
    private StandardFieldMappingRepository $standardFieldMappingRepository;
    /**
     * @var ContactRepository
     * @author Pradeep
     */
    private ContactRepository $contactRepository;
    /**
     * @var ContactServices
     * @author Pradeep
     */
    private ContactServices $contactServices;
    /**
     * @var FieldTypeStringServices
     * @author Pradeep
     */
    private FieldTypeStringServices $fieldTypeStringServices;
    /**
     * @var MIOStandardFieldRepository
     * @author Pradeep
     */
    private MIOStandardFieldRepository $mioStandardFieldRepository;
    /**
     * @var ToolServices
     * @author Pradeep
     */
    private ToolServices $toolServices;


    public function __construct(
        IntegrationsRepository $integrationsRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        ContactRepository $contactRepository,
        FieldTypeStringServices $fieldTypeStringServices,
        ContactFieldServices $contactFieldServices,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        FieldTypeListServices $fieldTypeListServices,
        ContactServices $contactServices,
        ToolServices $toolServices
    ) {
        $this->apikey = '';
        $this->http_client = new HttpMicroService();
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
        $this->integrationsRepository = $integrationsRepository;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
        $this->contactRepository = $contactRepository;
        $this->fieldTypeStringServices = $fieldTypeStringServices;
        $this->contactFieldServices = $contactFieldServices;
        $this->contactServices = $contactServices;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
        $this->fieldTypeListServices = $fieldTypeListServices;
        $this->toolServices = $toolServices;
        //$this->campaignServices = new MSCIOCampaignServices();
    }

    /**
     * create or Update(datatype) the customFields to eMIO.
     * @param string $apikey
     * @param array $customFields
     * @return bool|null
     * @author Pradeep
     */
    public function syncCustomFields(string $apikey, array $customFields): ?bool
    {
        $customFieldList[ 'apikey' ] = $apikey;
        $status = false;
        try {
            $eMIOCustomFields = $this->getCustomFields1($apikey);
            foreach ($customFields as $customField) {
                $eMIOCustomField = [];
                if (empty($customField[ 'fieldname' ])) {
                    continue;
                }
                $this->logger->info(json_encode(['customField' => $customField], JSON_THROW_ON_ERROR));
                if (isset($customField[ 'datatypes_name' ])) {
                    $eMIOCustomField[ 'datatype' ] = $this->mapToeMIODataType($customField[ 'datatypes_name' ]);
                }
                $eMIOCustomField[ 'fieldname' ] = $this->standardizeCustomFields($customField[ 'fieldname' ]);
                $eMIOCustomField[ 'operation' ] = $customField[ 'instruction' ] ?? 'insert';
                $eMIOCustomField[ 'mandatory' ] = $customField[ 'mandatory' ] ?? '0';
                $customFieldList[ 'customfieldlist' ][] = $eMIOCustomField;
            }
            $base64Text = base64_encode(json_encode($customFieldList, JSON_THROW_ON_ERROR));
            $this->logger->info($base64Text, [__METHOD__, __LINE__]);
            if ($this->http_client->postRequest('POST',
                $_ENV[ 'MSMIO_URL' ] . '/customfield/sync', $base64Text)) {
                $resp = json_decode($this->http_client->getContent(), true, 512,
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
                if (!empty($resp) && !isset($resp[ 'error' ])) {
                    $status = true;
                }
            }
            return $status;
        } catch (JsonException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $apikey
     * @return array|null
     * @author Pradeep
     * @deprecated
     */
    public function getCustomFields1(string $apikey): ?array
    {
        $customFields = [];
        try {
            if (!isset($apikey) || empty($apikey)) {
                throw new RuntimeException('Invalid eMIO APIKey provided');
            }// send http client request to msmio service.
            if ($this->http_client->postRequest('POST',
                $_ENV[ 'MSMIO_URL' ] . '/get_custom_fields?apikey=' . $apikey, '')) {
                $resp = json_decode($this->http_client->getContent(), true, 512,
                    JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
                if (!empty($resp) && !isset($resp[ 'error' ])) {
                    $customFields = $resp;
                }
            }
            return $customFields;
        } catch (JsonException|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * returns the equivalent eMIO datatype for the give MIO datatype.
     * @param $MIODataType
     * @return string|null
     * @author Pradeep
     * @deprecated
     */
    protected function mapToeMIODataType($MIODataType): ?string
    {
        $eMIODataType = '';
        if ($MIODataType === 'Decimal') {
            $eMIODataType = 'float';
        } elseif ($MIODataType === 'Email' || $MIODataType === 'Phone' || $MIODataType === 'Text' || $MIODataType === 'URL' || $MIODataType === 'DateTime') {
            $eMIODataType = 'string';
        } elseif ($MIODataType === 'Integer') {
            $eMIODataType = 'integer';
        } elseif ($MIODataType === 'Date') {
            $eMIODataType = 'date';
        } // Currently MIO does not have any datatype as bool i,e true or false,
        elseif ($MIODataType === 'Bool') {
            $eMIODataType = 'boolean';
        } else {
            $eMIODataType = 'string';
        }
        return $eMIODataType;
    }

    private function standardizeCustomFields(string $customFieldName): ?string
    {
        if (empty($customFieldName)) {
            throw new RuntimeException('Invalid customfield name provided');
        }
        return 'MIO_' . preg_replace(['/ä/', '/ö/', '/ü/', '/ß/', '/Ä/', '/Ö/', '/Ü/', '/\\W/'],
                ['ae', 'oe', 'ue', 'ss', 'AE', 'OE', 'UE', '_'], $customFieldName);
    }

    /**
     * @param int $blk_id
     * @param string $blk_email_list
     * @param string|null $blk_import_name
     * @param string $service
     * @return bool
     * @throws JsonException
     * @author Pradeep
     */
    public function blackListEmail(
        int $blk_id,
        string $blk_email_list,
        string $blk_import_name = null,
        string $service = "/blacklist_email"
    ):
    bool {
        $status = false;
        if ($blk_id <= 0 || empty($blk_email_list)) {
            return $status;
        }
        $blk_content[ 'blk_id' ] = $blk_id;
        $blk_content[ 'blk_email_list' ] = $blk_email_list;
        $blk_content[ 'blk_imp_name' ] = $blk_import_name;
        $blk_content[ 'api_key' ] = $this->apikey;
        // convert array to base64text.
        $base64_text = base64_encode(json_encode($blk_content, JSON_THROW_ON_ERROR));
        $this->logger->info('before sending to eMIO Service ' . json_encode($base64_text));
        // send http client request to msmio service.i
        //return $this->http_client->postRequest('POST', $_ENV[ 'MSMIO_URL' ] . '/blacklist_email', $base64_text);
        return $this->http_client->postRequest('POST', $_ENV[ 'MSMIO_URL' ] . $service, $base64_text);
    }

    /**
     * @param array $user
     * @param array $company
     * @return bool
     * @author Pradeep
     */
    public function subscribeUserToeMIONewsLetter(array $user, array $company): bool
    {
        $this->logger->info('subscribeUserToeMIONewsLetter_1 :' . json_encode([
                'User' => $user,
                'company' => $company,
            ], JSON_THROW_ON_ERROR));

        $status = false;
        $apikey = $this->integrationsRepository->getEMailInOneAPIkeySyncUserForNewsletter();
        if ($user[ 'email' ] === null || empty($company) || empty($apikey)) {
            return $status;
        }

        $this->logger->info('subscribeUserToeMIONewsLetter_2 :' . json_encode([
                'User' => $user,
                'company' => $company,
                'apikey' => $apikey,
            ], JSON_THROW_ON_ERROR));

        $method = 'POST';
        $endPoint = $_ENV[ 'MSMIO_URL' ] . '/integratetomio';
        try {
            $mio_body[ 'instructions' ] = [
                'apikey' => $apikey,
                'doiPermission' => ContactPermissionTypes::PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING,
                'doiMailingKey' => true,
                'contactEventId' => '',
                'marketingAutomationId' => '',
                'campaignName' => '',
                'typeOfIntegration' => 'DOI',
                'unsubscriberStatus' => true,
            ];
            $mio_body[ 'customFields' ] = [];
            $mio_body[ 'contactEventFields' ] = [];
            $mio_body[ 'standardFields' ] = [
                'FIRSTNAME' => $user[ 'first_name' ],
                'LASTNAME' => $user[ 'last_name' ],
                'SALUTATION' => $user[ 'salutation' ],
                'EMAIL' => $user[ 'email' ],
                'ADDRESS' => $company[ 'street' ] ?? '',
                'CITY' => $company[ 'city' ] ?? '',
                'COUNTRY' => $company[ 'country' ] ?? '',
                'ZIP' => $company[ 'postal_code' ] ?? '',
                'ORGANIZATION' => $company[ 'name' ] ?? '',
            ];

            $mio_body[ 'checksum' ] = hash('sha512', str_rot13(json_encode($mio_body, JSON_THROW_ON_ERROR)));
            $body = base64_encode(json_encode($mio_body, JSON_THROW_ON_ERROR));
            $this->logger->info(json_encode(['MIOBody' => $body], JSON_THROW_ON_ERROR));
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if ($response->getStatusCode() === 200) {
                $status = $response->toArray()[ 'status' ];
            }
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|
        ServerExceptionInterface|TransportExceptionInterface|JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
        // validate the response and return the status;
        return $status;
    }

    /**
     * @param array $contact
     * @param string $typeOfIntegration
     * @param int $permission
     * @param string $doiMailingKey
     * @param string $contactEventId
     * @param string $marketingAutomationId
     * @return bool|null
     * @author Pradeep
     */
    public function createContact(
        array $contact,
        string $typeOfIntegration,
        int $permission,
        string $doiMailingKey,
        string $contactEventId,
        string $marketingAutomationId
    ): bool {
        $status = false;
        try {
            if (empty($_ENV[ 'MSMIO_URL' ])) {
                throw new RuntimeException('Failed to get endpoint for MSMIO');
            }
            if (!$this->validateTypeOfIntegration($typeOfIntegration, $contactEventId, $marketingAutomationId)) {
                $this->logger->info("type of integration is " . $typeOfIntegration);
                throw new RuntimeException('Invalid Type of Integration');
            }

            $method = 'POST';
            $endPoint = $_ENV[ 'MSMIO_URL' ] . '/integratetomio';
            $mio_body[ 'instructions' ] = [
                'apikey' => $this->getAPIKey(),
                'doiPermission' => $permission,
                'doiMailingKey' => $doiMailingKey,
                'contactEventId' => $contactEventId,
                'marketingAutomationId' => $marketingAutomationId,
                'campaignName' => '',
                'typeOfIntegration' => $typeOfIntegration,
                // The status of UnsubscriberStatus is always true,
                // when a new contact request arrives from Page or Webhook
                // i,e Contact in eMIO is again set back to active.
                'unsubscriberStatus' => true,
            ];
            $standardFields = $this->getStandardFields();
            $customFields = $this->getCustomFields();
            $mio_body[ 'standardFields' ] = $this->mapStandardFields($contact, $standardFields);
            $mio_body[ 'customFields' ] = $this->mapCustomFields($contact, $customFields);

            $this->logger->info('contact createContact ' . json_encode([
                    'contact' => $contact,
                    '$customFields' => $customFields,
                ], JSON_THROW_ON_ERROR));

            if (!empty($contact[ 'contactEventFields' ])) {
                $mio_body[ 'contactEventFields' ] = $contact[ 'contactEventFields' ];
            }
            $this->logger->info("This is mio body for processhook type" . $typeOfIntegration, $mio_body);
            $mio_body[ 'checksum' ] = hash('sha512', str_rot13(json_encode($mio_body, JSON_THROW_ON_ERROR)));
            $body = base64_encode(json_encode($mio_body, JSON_THROW_ON_ERROR));
            $this->logger->info(json_encode(['MIOBody' => $body], JSON_THROW_ON_ERROR));
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            $this->logger->info('response Info' . json_encode($response->getInfo(), JSON_THROW_ON_ERROR));
            $this->logger->info('response Content' . json_encode($response->getContent(), JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $this->logger->info("Response form MSMIO mircroservice for" . $typeOfIntegration, $response->toArray());
                $status = $response->toArray()[ 'status' ];
            }
            return $status;
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|
        ServerExceptionInterface|TransportExceptionInterface|JsonException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $typeOfIntegration
     * @param string $contactEventId
     * @param string $marketingAutomationId
     * @return bool
     * @author Pradeep
     */
    private function validateTypeOfIntegration(
        string $typeOfIntegration,
        string $contactEventId,
        string $marketingAutomationId
    ): bool {
        if ($typeOfIntegration === self::ITYPE_DOI) {
            return true;
        }
        if ($typeOfIntegration === self::ITYPE_CE && !empty($contactEventId)) {
            return true;
        }
        if (($typeOfIntegration === self::ITYPE_MA || $typeOfIntegration === self::ITYPE_DOI_XOR_CE)) {
            return true;
        }
        return false;
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    public function getAPIKey(): ?string
    {
        return $this->apikey ?? null;
    }

    /**
     * @param $apikey
     * @return bool
     * @author Pradeep
     */
    public function setAPIKey($apikey): bool
    {
        $status = false;
        if (!empty($apikey)) {
            $this->apikey = $apikey;
            $status = true;
        }
        return $status;
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getStandardFields(): ?array
    {
        $sdFields = [];
        $method = 'POST';
        $endPoint = $_ENV[ 'MSMIO_URL' ] . '/standardfields/get';
        try {
            $response = $this->httpClient->request($method, $endPoint);
            if ($response->getStatusCode() === 200) {
                $sdFields = $response->toArray();
            }
            return $sdFields;
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface
        |ServerExceptionInterface|TransportExceptionInterface $e) {
            print_r($e->getMessage());
            return null;
        }
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getCustomFields(): ?array
    {
        $customFields = [];
        $method = 'POST';
        $endPoint = $_ENV[ 'MSMIO_URL' ] . '/customfields/get';
        try {
            $body = ['apikey' => $this->getAPIKey()];
            $this->logger->info('getCustomFields' . json_encode($body));
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            $this->logger->info('resposne: getCustomFields' . json_encode(['code' => $response->getStatusCode()],
                    JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $customFields = $response->toArray();
            }
            return $customFields;
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface
        |ServerExceptionInterface|TransportExceptionInterface $e) {
            print_r($e->getMessage());
            return null;
        }
    }

    /**
     * @param array $contact
     * @param array $standardFields
     * @return array
     * @author Pradeep
     */
    private function mapStandardFields(array $contact, array $standardFields): array
    {
        $mappedSdFields = [];
        if (empty($contact) || empty($standardFields)) {
            return $mappedSdFields;
        }
        foreach ($standardFields as $key => $value) {
            if (empty($key) || !isset($contact[ $key ])) {
                continue;
            }
            $mappedSdFields[ $key ] = $contact[ $key ];
        }
        return $mappedSdFields;
    }

    /**
     * @param array $contact
     * @param array $customFields
     * @return array|null
     * @author Pradeep
     */
    private function mapCustomFields(array $contact, array $customFields): array
    {
        $mappingCusFields = [];
        if (empty($contact) || empty($customFields)) {
            return $mappingCusFields;
        }
        foreach ($contact as $key => $value) {
            if (empty($key) || !isset($customFields[ $key ])) {
                continue;
            }
            $mappingCusFields[ $key ] = $value;
        }

        return $mappingCusFields;
    }

    /**
     * @param array $contacts
     * @param array $eMIOIntegrationConfig
     * @param int|null $companyId
     * @return bool
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function syncContacts(array $contacts, array $eMIOIntegrationConfig, int $companyId = null): bool
    {
        $method = 'POST';
        $endPoint = $_ENV[ 'MSMIO_URL' ] . '/contacts/sync';

        $eMIOMappedFields = $this->standardFieldMappingRepository->getMappedSystemFieldsWithMIO(UniqueObjectTypeRepository::EMAILINONE);
        $eMIOCustomFieldsMapping = json_decode(base64_decode($eMIOIntegrationConfig[ 'mapping' ]), true, 512,
            JSON_THROW_ON_ERROR);
        if ($companyId !== null) {
            $eMIOContacts = $this->map($contacts, $eMIOMappedFields, $eMIOCustomFieldsMapping, $companyId, false);
        } else {
            $eMIOContacts = $this->map($contacts, $eMIOMappedFields, $eMIOCustomFieldsMapping, null, true);
        }

        $data[ 'contacts' ] = $eMIOContacts;
        $data[ 'apikey' ] = $eMIOIntegrationConfig[ 'apikey' ];
        $payload1 = base64_encode(json_encode($data));

        // Large data is being transfered between AIO and eMIO.
        // So use GZCompress to compress the data.
        $payload = $this->toolServices->encryptContentData($data, true);

        $this->logger->info('MSMIO Payload ' . json_encode([
                'sizeOfPayload1' => strlen($payload1),
                'sizeofPayload2' => strlen($payload),
            ]));

        /*try{
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'emio_sync_payload.log',json_encode($payload));
        }catch(Exception $e){
            $this->logger->error('file_put_contents :' . $e->getMessage(), [__METHOD__, __LINE__]);
        }*/
        //  $this->logger->info('PAYLOAD SYNC :'.json_encode($payload));

        try {
            $response = $this->httpClient->request($method, $endPoint, ['body' => $payload]);
            $this->logger->info('MSMIO Response ' . json_encode($response->getStatusCode()));
            $t = [
                'status' => $response->getStatusCode(),
                'content' => $response->getContent(),
                'info' => $response->getInfo(),
                'toArray' => $response->toArray(),
                'headers ' => $response->getHeaders(),
            ];
            $this->logger->info('response Code :' . json_encode($t), [__METHOD__, __LINE__]);
            return $response->getStatusCode() === 200;
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface
        |ServerExceptionInterface|TransportExceptionInterface $e) {
            $this->logger->error('syncContacts :' . $e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param array $contacts
     * @param array $eMIOStandardFields
     * @param array $eMIOCustomFields
     * @return array
     * @author Pradeep
     * @deprecated : moved since v2.0.2.2
     */
    private function mapContactFieldsForSync(
        array $contacts,
        array $eMIOStandardFields,
        array $eMIOCustomFields = [],
        int $companyId = null,
        bool $chkLalestObjRegId = false
    ): array {
        $this->logger->info('start:mapContactFieldsForSync');
        $contactsForSync = [];
        $customFields = $this->getCustomFields();
        $standardFields = $this->getStandardFields();
        // Loop all Contacts
        foreach ($contacts as $contact) {
            $this->logger->info('contact:mapContactFieldsForSync');
            if (!isset($contact[ 'standard' ])) {
                continue;
            }
            $eMIOContact = [];
            $ctObjRegId = 0;
            $tempContObjRegId = 0;
            // Loop all MIOStandard and MIO StandardCustomFields(smart_tags, last_order_date)
            foreach ($contact[ 'standard' ] as $field) {
                $this->logger->info('cnt-field:mapContactFieldsForSync');
                foreach ($field as $key => $value) {
                    if (in_array($key, ['required', 'datatype', 'regex'])) {
                        continue;
                    }
                    // Email Mapping
                    if ($key === 'email') {
                        $ct = $this->contactRepository->getContactByEmail($value, $companyId);
                        if (!empty($ct) && isset($ct[ 'objectregister_id' ]) && (int)$ct[ 'objectregister_id' ] > 0) {
                            $tempContObjRegId = (int)$ct[ 'objectregister_id' ];
                        }
                        $eMIOContact[ $key ] = $value;
                        break;
                    }
                    if ($key === 'objectregister_id') {
                        $ctObjRegId = (int)$value;
                        break;
                    }
                    // Standard Fields Mapping
                    foreach ($eMIOStandardFields as $eMIOFields) {
                        $this->logger->info('cnt-emio-sd-field:mapContactFieldsForSync');
                        if ($eMIOFields[ 'mioFieldName' ] === $key &&
                            isset($standardFields[ $eMIOFields[ 'systemFieldName' ] ])) {
                            $eMIOContact[ 'standard_fields' ][] = [
                                'name' => $eMIOFields[ 'systemFieldName' ],
                                'value' => $value,
                            ];
                            break 2;
                        }

                        if ($eMIOFields[ 'mioFieldName' ] === $key) {
                            $this->logger->info('cnt-emio-fields:mapContactFieldsForSync');
                            if ($key === 'smart_tags') {
                                $categories = explode(',', $value);
                                foreach ($categories as $category) {
                                    $eMIOCategory = 'MIO_' . $this->fieldTypeStringServices->sanitizeString($category);
                                    if (isset($customFields[ $eMIOCategory ])) {
                                        $eMIOContact[ 'custom_fields' ][] = [
                                            'name' => $eMIOCategory,
                                            'value' => 'true',
                                        ];
                                    }
                                }
                            } else {
                                $eMIOContact[ 'custom_fields' ][] = [
                                    'name' => $eMIOFields[ 'systemFieldName' ],
                                    'value' => $value,
                                ];
                            }
                        }

                        /*if($eMIOFields['mioFieldName'] === $key &&
                           isset($customFields[$eMIOFields['systemFieldName']])) {

                            $eMIOContact['custom_fields'][] = [
                                'name'=>$eMIOFields['systemFieldName'],
                                'value' => $value
                            ];
                        }*/
                    }
                    // CustomFields Mapping
                    foreach ($eMIOCustomFields as $eMIOFields) {
                        $this->logger->info('cnt-emio-cus-field:mapContactFieldsForSync');
                        if ($eMIOFields[ 'marketing-in-one-custom-field' ] === $key) {
                            if ($key === 'smart_tags') {
                                $categories = explode(',', $value);
                                foreach ($categories as $category) {
                                    $eMIOCategory = 'MIO_' . $this->fieldTypeStringServices->sanitizeString($category);
                                    if (isset($customFields[ $eMIOCategory ])) {
                                        $eMIOContact[ 'custom_fields' ][] = [
                                            'name' => $eMIOCategory,
                                            'value' => true,
                                        ];
                                    }
                                }
                            } else {
                                if (isset($customFields[ $eMIOFields[ 'email-in-one-custom-field' ] ])) {
                                    $eMIOContact[ 'custom_fields' ][] = [
                                        'name' => $eMIOFields[ 'email-in-one-custom-field' ],
                                        'value' => $value,
                                    ];
                                }
                            }

                            break 2;
                        }
                    }
                }
            }
            if (isset($contact[ 'custom' ]) && !empty($contact[ 'custom' ])) {
                $this->logger->info('cnt-cus-field:mapContactFieldsForSync');
                foreach ($contact[ 'custom' ] as $field) {
                    foreach ($field as $key => $value) {
                        if (in_array($key, ['required', 'datatype', 'regex'])) {
                            continue;
                        }
                        foreach ($eMIOCustomFields as $eMIOFields) {
                            if ($eMIOFields[ 'marketing-in-one-custom-field' ] === $key) {
                                if ($key === 'smart_tags') {
                                    $categories = explode(',', $value);
                                    foreach ($categories as $category) {
                                        $eMIOCategory = 'MIO_' . $this->fieldTypeStringServices->sanitizeString($category);
                                        if (isset($customFields[ $eMIOCategory ])) {
                                            $eMIOContact[ 'custom_fields' ][] = [
                                                'name' => $eMIOCategory,
                                                'value' => true,
                                            ];
                                        }
                                    }
                                } else {
                                    if (isset($customFields[ $eMIOFields[ 'email-in-one-custom-field' ] ])) {
                                        $eMIOContact[ 'custom_fields' ][] = [
                                            'name' => $eMIOFields[ 'email-in-one-custom-field' ],
                                            'value' => $value,
                                        ];
                                    }
                                }

                                break 2;
                            }
                        }
                    }
                }
            }
            if (!empty($eMIOContact)) {
                if ($chkLalestObjRegId) {
                    if (isset($tempContObjRegId, $ctObjRegId) &&
                        $tempContObjRegId === $ctObjRegId) {
                        $contactsForSync[] = $eMIOContact;
                    }
                } else {
                    $contactsForSync[] = $eMIOContact;
                }
            }
            /*            if(!empty($eMIOContact) &&
                           isset($tempContObjRegId, $ctObjRegId) &&
                           $tempContObjRegId === $ctObjRegId) {
                            $contactsForSync[] = $eMIOContact;
                        }*/

        }
        $this->logger->info('end:mapContactFieldsForSync');
        return $contactsForSync;
    }


    /**
     * syncContactFieldsOfDataTypeList Synchronizes each and every value of LIST (datatype in MIO)
     * as a customfield in eMIO.
     * @param array $contactList
     * @return array
     * @author Pradeep
     */
    public function syncContactFieldsOfDataTypeList(array $contactList): array
    {
        $smartTags = [];

        try {
            $eMIOCustomFields = $this->getCustomFields();
            $this->logger->info('eMIOCustomFields: ' . json_encode($eMIOCustomFields));
            foreach ($contactList as $contact) {
                if (!isset($contact[ 'standard' ])) {
                    continue;
                }
                // fetch the fields of dataType 'list'.
                // append each tag with 'MIO_'
                $tags = $this->fieldTypeListServices->fetchFieldsOfTypeList($contact);
                $smartTags = array_unique(array_merge($smartTags, $tags), SORT_REGULAR);
            }
            foreach ($smartTags as $smartTag) {
                $this->logger->info('smartTage ' . $smartTag . ' is not present');
                if (!$this->createCustomField($smartTag, 'string', $this->getAPIKey())) {
                    $this->logger->error('Failed to create CustomField ' . $smartTag . ' in eMIO');
                    continue;
                }
            }
            $this->logger->info('SYNC EMIO SMART_TAGS: ' . json_encode($smartTags));
            return $smartTags;
        } catch (JsonException|Exception $e) {
            $this->logger->error('syncContactFieldsOfDataTypeList :' . $e->getMessage(),
                [__METHOD__, __LINE__, $e->getCode()]);
            return [];
        }
    }

    /**
     * @param array $singleContact
     * @return false|void
     * @author Pradeep
     */
    public function syncContactFieldOfDataTypeListForSingleContact(array $singleContact)
    {
        if (empty($singleContact[ 'contact' ])) {
            return false;
        }
        // fetch the fields of dataType 'list'.
        // append each tag with 'MIO_'
        $tags = $this->fieldTypeListServices->fetchFieldsOfTypeList($singleContact[ 'contact' ]);
        // remove the duplicate fields.
        $smartTags = array_unique($tags, SORT_REGULAR);

        foreach ($smartTags as $smartTag) {
            $this->logger->info('smartTage ' . $smartTag . ' is not present');
            if (!$this->createCustomField($smartTag, 'string', $this->getAPIKey())) {
                $this->logger->error('Failed to create CustomField ' . $smartTag . ' in eMIO');
                continue;
            }
        }
    }

    /**
     * @param string $customField
     * @param string $dataType
     * @param string $apikey
     * @param bool $appendMIOTag
     * @return bool
     * @author PradeepsanitizeString
     */
    public function createCustomField(
        string $customField,
        string $dataType,
        string $apikey,
        bool $appendMIOTag = false
    ): bool {
        try {
            if (empty($customField) || (empty($apikey))) {
                throw new RuntimeException('Invalid Custom Field or APIKey provided.');
            }
            $body = [
                'apikey' => $apikey,
                'customfield' => [
                    'name' => $customField,
                    'datatype' => $dataType,
                ],
                'append_mio_tag' => $appendMIOTag,
            ];
            $endPoint = $_ENV[ 'MSMIO_URL' ] . '/customfield/create';
            $base64Text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
            $this->logger->info('createCustomFieldIneMIO ' . $base64Text);
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);
            $this->logger->info('createCustomFieldIneMIO ' . json_encode($response, JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            return true;
        } catch (JsonException|TransportExceptionInterface|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param array $contacts
     * @param array $eMIOStandardFields
     * @param array $eMIOCustomFields
     * @param int|null $companyId
     * @param bool $chkLalestObjRegId
     * @return array
     * @throws JsonException
     * @author Pradeep
     */
    private function map(
        array $contacts,
        array $eMIOStandardFields,
        array $eMIOCustomFields = [],
        int $companyId = null,
        bool $chkLalestObjRegId = false
    ) {
        $contactsForSync = [];
        $customFields = $this->getCustomFields();
        $standardFields = $this->getStandardFields();
        foreach ($contacts as $contact) {
            $eMIOContact = [];
            $eMIOContact[ 'email' ] = $this->contactFieldServices->getValue($contact, 'email');
            foreach ($eMIOStandardFields as $eMIOFields) {
                if (empty($eMIOFields[ 'mioFieldName' ])) {
                    continue;
                }
                $value = $this->contactFieldServices->getValue($contact, $eMIOFields[ 'mioFieldName' ]);
                $fieldDetails = $this->mioStandardFieldRepository->getFieldTypeByFieldName($eMIOFields[ 'mioFieldName' ]);

                if (empty($fieldDetails)) {
                    continue;
                }

                // eMIO only accepts when string is less than 255 characters.
                if (is_string($value) &&
                    strlen($value) > $this->fieldTypeStringServices::MAX_STRING_LENGTH) {
                    $value = $this->fieldTypeStringServices->truncateString($value);
                }

                // eMIO Standard Fields.
                if (isset($standardFields[ $eMIOFields[ 'systemFieldName' ] ])) {
                    $eMIOContact[ 'standard_fields' ][] = [
                        'name' => $eMIOFields[ 'systemFieldName' ],
                        'value' => $value,
                    ];
                } else {
                    $eMIOContact[ 'custom_fields' ][] = [
                        'name' => $eMIOFields[ 'systemFieldName' ],
                        'value' => $value,
                    ];
                }

                if (isset($fieldDetails[ 'datatype' ]) && !empty($value) &&
                    ($fieldDetails[ 'datatype' ] === ContactFieldDataTypes::FIELD_TYPE_LIST)) {
                    $list = $this->fieldTypeListServices->convertListToBoolean($value);
                    $customFields = $eMIOContact[ 'custom_fields' ];
                    $eMIOContact[ 'custom_fields' ] = array_merge($customFields, $list);
                }
            }

            foreach ($eMIOCustomFields as $eMIOFields) {
                $this->logger->info('cnt-emio-cus-field:mapContactFieldsForSync');
                $value = $this->contactFieldServices->getValue($contact,
                    $eMIOFields[ 'marketing-in-one-custom-field' ]);
                if (isset($customFields[ $eMIOFields[ 'email-in-one-custom-field' ] ])) {
                    $eMIOContact[ 'custom_fields' ][] = [
                        'name' => $eMIOFields[ 'email-in-one-custom-field' ],
                        'value' => $value,
                    ];
                }
                // Todo: handle datatype list value hear, similar to standardfields.

            }
            $contactsForSync[] = $eMIOContact;
        }
        return $contactsForSync;
    }
}

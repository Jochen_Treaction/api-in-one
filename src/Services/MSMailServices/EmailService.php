<?php


namespace App\Services\MSMailServices;


use App\Services\HttpMicroServices\HttpMicroService;
use Psr\Log\LoggerInterface;
// --------------------------------
use App\Types\MsMailInterfaceTypes;
use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;



class EmailService
{
	private LoggerInterface $logger;
	private MsMailInterfaceTypes $msMailInterfaceTypes;

	private EmailContentTypes $emailContentTypes;

	//public function __construct(LoggerInterface $logger, MsMailInterfaceTypes $msMailInterfaceTypes)


	/**
	 * EmailService constructor.
	 * @param LoggerInterface             $logger
	 * @param \App\Services\MsMailService $msMail
	 * @param MsMailInterfaceTypes        $msMailInterfaceTypes
	 * @param EmailContentTypes           $emailContentTypes
	 */
	public function __construct(LoggerInterface $logger, MsMailInterfaceTypes $msMailInterfaceTypes, EmailContentTypes $emailContentTypes)
	{
		$this->http_client = new HttpMicroService();
		$this->myHttpClient = $this->http_client->getHttpClient();
		$this->msMailInterfaceTypes = $msMailInterfaceTypes;
		$this->emailContentTypes = $emailContentTypes;
		$this->logger = $logger;
	}


	/**
	 * @param array                     $recipientEmailList
	 * @param string                    $subject
	 * @param string                    $emailContentType
	 * @param MessageParameterTypes     $messageParameterTypes
	 * @param string|null               $returnMessage
	 * @param MsMailInterfaceTypes|null $paramMsMailInterfaceTypes
	 * @return bool
	 * @throws \Exception
	 */
	public function sendByMsMail(array $recipientEmailList, string $subject, string $emailContentType, MessageParameterTypes $messageParameterTypes, string &$returnMessage=null, MsMailInterfaceTypes $paramMsMailInterfaceTypes=null):bool
	// public function sendByMsMail(array $recipientEmailList, string $subject, string $emailContentType, MessageParameterTypes $messageParameterTypes, string &$returnMessage=null):bool
	{  // TODO: add
		// $this->logger->info('PARAMS ', [json_encode(['$recipientEmailList'=>$recipientEmailList,'$subject' => $subject, '$emailContentType' => $emailContentType, '$messageParameterTypes' => $messageParameterTypes]), __METHOD__, __LINE__ ]);
		$this->logger->info(__METHOD__, [__LINE__]);

		$checkSendByMsMailParams = $this->checkSendByMsMailParams($recipientEmailList, $subject, $emailContentType);
		$this->logger->info(__METHOD__, [__LINE__]);

		if(!empty($checkSendByMsMailParams)) {
			throw new \Exception("parameter error: {$checkSendByMsMailParams}", __LINE__);
			return false;
		}

		$this->logger->info(__METHOD__, [__LINE__]);
		$this->msMailInterfaceTypes->recipientEmailList = $recipientEmailList;
		$this->msMailInterfaceTypes->messageSubject = $subject;
		$propertyName = $this->emailContentTypes->getPropertyName($emailContentType);
		$this->logger->info(__METHOD__, [__LINE__]);

		$this->logger->info('$this->msMailInterfaceTypes->getConfigurationArray()', [$this->msMailInterfaceTypes->getConfigurationArray(), __METHOD__, __LINE__]);
		$this->logger->info('$paramMsMailInterfaceTypes->getConfigurationArray()', [$paramMsMailInterfaceTypes->getConfigurationArray(), __METHOD__, __LINE__]);
		try {
			$responseContent = $this->sendStandardMail(
				$this->emailContentTypes->{$propertyName},
				$messageParameterTypes->getMessageParameterTypesArray(),
                (null === $paramMsMailInterfaceTypes)
                    ? $this->msMailInterfaceTypes->getConfigurationArray()
                    : $paramMsMailInterfaceTypes->getConfigurationArray()
			);
		} catch (ClientExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode().", line ".__LINE__;
			return false;
		} catch (RedirectionExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode().", line ".__LINE__;
			return false;
		} catch (ServerExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode().", line ".__LINE__;
			return false;
		} catch (TransportExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode().", line ".__LINE__;
			return false;
		}

		$this->logger->info(__METHOD__, [__LINE__]);
		$this->logger->info('$responseContent', [$responseContent, __METHOD__,__LINE__]);
		$returnMessage = $responseContent;
		return true;

	}


	/**
	 * @param array  $recipientEmailList
	 * @param string $subject
	 * @param string $emailContentType
	 * @return string if empty, no error where found, else returned string contains error messages
	 */
	private function checkSendByMsMailParams(array $recipientEmailList, string $subject, string $emailContentType) :string
	{
		$msg = '';
		if(empty($subject)) {
			$msg .= "error: empty \$subject, ";
		}

		if(empty($recipientEmailList)) {
			$msg .=  "error: empty \$recipientEmailList, ";
		}

		if(null === $this->emailContentTypes->getPropertyName($emailContentType) ) {
			$msg .= "error: undefined \$emailContentType {$emailContentType}, ";
		}

		return $msg;
	}


	/**
	 * @param string $emailContentType
	 * @param array  $messageParameterTypes
	 * @param array  $mailInterface
	 * @return mixed|string
	 * @throws TransportExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
	 */
	public function sendStandardMail(string $emailContentType, array $messageParameterTypes, array $mailInterface)
	{
		/*
			MSMAIL endpoint expects body content:
			$data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);
			$data['emailContentTypes']; // one string of EmailContentTypes->{key} e.g. "account-deletion-notification", where key is "accountDeletionNotification"
			$data['messageParameterTypes']; // firstName, lastName, email, activationLink, ..., filled with values (then processed) or empty '' (not processed)
			$data['mailInterface']; // configuration settings of email => senderName, senderPw, smtpserver, port, and so on
		*/

		$bodyContent = base64_encode(json_encode([
			'emailContentTypes' => $emailContentType,
			'messageParameterTypes' => $messageParameterTypes,
			'mailInterface' => $mailInterface,
		]));

		$this->logger->info('$bodyContent', [$bodyContent, __METHOD__,__LINE__]);

		try {
			$response = $this->myHttpClient->request('POST', $_ENV['MSMAIL_URL'] . '/email/sendstandardmail', ['body' => $bodyContent] );
            $this->logger->info('RESPONSE code, info, content:', [$response->getStatusCode(), $response->getInfo(), $response->getContent(), __METHOD__, __LINE__]);
		} catch (TransportExceptionInterface $e) {
			throw new \Exception($e->getMessage(), __LINE__);
			return 'error: '.$e->getMessage() .', line: '. __LINE__;
		} catch (ServerExceptionInterface $e) {
			throw new \Exception($e->getMessage(), __LINE__);
			return 'error: '.$e->getMessage() .', line: '. __LINE__;
		}

		$this->logger->info('statuscode', [$response->getStatusCode(), __METHOD__,__LINE__]);

		$responseContent = json_decode($response->getContent(), true, 512, JSON_OBJECT_AS_ARRAY);
		return $responseContent['msg'];
	}
}

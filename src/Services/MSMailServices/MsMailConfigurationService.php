<?php


namespace App\Services\MSMailServices;


use App\Services\HttpMicroServices\HttpMicroService;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MsMailConfigurationService
{
	private $myHttpClient;

	public function __construct()
	{
		$httpClient = new HttpMicroService();
		$this->myHttpClient = $httpClient->getHttpClient();
	}

	public function getMailParameters():array
	{
		try {
			$response = $this->myHttpClient->request('GET', $_ENV['MSMAIL_URL'] . '/email/configuration/params');
		} catch (TransportExceptionInterface $e) {
			throw new \Exception($e->getMessage(), __LINE__);
			return [];
		}
		$content = json_decode($response->getContent(), true, 512, JSON_OBJECT_AS_ARRAY);

		return $content;
	}
}

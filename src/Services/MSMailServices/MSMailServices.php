<?php

namespace App\Services\MSMailServices;

use App\Types\ContactFieldTypes;
use App\Services\HttpMicroServices\HttpMicroService;
//use App\Services\MSCIOServices\MSCIOAccountIntegrations;
//use App\Services\MSCIOServices\MSCIOCampaignServices;
use App\Repository\UserRepository;
use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use App\Types\MsMailInterfaceTypes;
use Doctrine\DBAL\Connection;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class MSMailServices implements LoggerAwareInterface
{

    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;
    /**
     *
     * @author Pradeep
     */
    private $campaignServices;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     *
     * @author Pradeep
     */
    private $accountIntegrations;

    private $emailService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    private $emailContentTypes;

    private $messageParameterTypes;

    private $msMailInterfaceTypes;
    /**
     * @var ContactFieldTypes
     */
    private ContactFieldTypes $leadInterface;

    /**
     * @internal  check correct (dev, testaio, prod) setting via php bin/console debug:container --parameters
     * @var $senderEmailAddress
     * @see config => config/services_dev.yaml, config/services_prod.yaml, config/services_testmio.yaml
     */
    private $senderEmailAddress;

    /**
     * @internal  check correct (dev, testaio, prod) setting via php bin/console debug:container --parameters
     * @var $senderPassword
     * @see  config => config/services_dev.yaml, config/services_prod.yaml, config/services_testmio.yaml
     */
    private $senderPassword;

    /**
     * @internal  check correct (dev, testaio, prod) setting via php bin/console debug:container --parameters
     * @var $smtpServer
     * @see  config => config/services_dev.yaml, config/services_prod.yaml, config/services_testmio.yaml
     */
    private $smtpServer;

    /**
     * @internal  check correct (dev, testaio, prod) setting via php bin/console debug:container --parameters
     * @var $smtpPort
     * @see  config => config/services_dev.yaml, config/services_prod.yaml, config/services_testmio.yaml
     */
    private $smtpPort;

    private $containerBag;

    use LoggerAwareTrait;


    /**
     * MSMailServices constructor.
     * @param Connection $connection
     * @param EmailService $emailService
     * @param UserRepository $userRepository
     * @param EmailContentTypes $emailContentTypes
     * @param MessageParameterTypes $messageParameterTypes
     * @param MsMailInterfaceTypes $msMailInterfaceTypes
     * @param ContactFieldTypes $leadInterface
     * @param ContainerBagInterface $containerBag
     */
    public function __construct(
        Connection $connection,
        //MSCIOAccountIntegrations $accountIntegrations,
        EmailService $emailService,
        UserRepository $userRepository,
        EmailContentTypes $emailContentTypes,
        MessageParameterTypes $messageParameterTypes,
        MsMailInterfaceTypes $msMailInterfaceTypes,
        ContactFieldTypes $leadInterface,
        ContainerBagInterface $containerBag
    )
    {
        $this->containerBag = $containerBag;
        $this->smtpServer= $this->getServiceParameter('app.smtp_server');
        $this->smtpPort= $this->getServiceParameter('app.smtp_port');
        $this->senderEmailAddress= $this->getServiceParameter('app.sender_email_address');
        $this->senderPassword = $this->getServiceParameter('app.sender_password');
        $this->conn = $connection;
        $this->accountIntegrations = null; //$accountIntegrations;
        $this->http_client = new HttpMicroService();
        $this->campaignServices = null; //= new MSCIOCampaignServices();
        $this->emailService = $emailService;
        $this->messageParameterTypes = $messageParameterTypes;
        $this->msMailInterfaceTypes = $msMailInterfaceTypes;
        $this->userRepository = $userRepository;
        $this->emailContentTypes = $emailContentTypes;
        $this->leadInterface = $leadInterface;
    }


    /**
     * @internal get Parameters from service.yaml, defined in config/services_dev/services.yaml, config/services_testaio/services.yaml, config/services_prod/services.yaml, depending on APP_ENV setting in .env (dev, testaio, prod)
     * @param string $serviceParameterName
     * @return array|bool|float|int|mixed|string|null
     * @author jsr
     * @since 2021-11-03
     */
    protected function getServiceParameter( string $serviceParameterName)
    {
        $parameterString = $this->containerBag->get($serviceParameterName);

        return $parameterString;
    }


    /**
     * @param int $campaign_id
     * @return bool
     * @author Pradeep
     */
    public function isForwardEmailConfigured(int $campaign_id): bool
    {
        $status = false;
        if ($campaign_id <= 0) {
            return $status;
        }
        // TODO replace by database access, since MSCIOCampaignServices is deprecated => replace "getCampaignParmsForTarget" by DB select statement
        if ($this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Email') !== null) {
            $status = true;
        }
        return $status;
    }


    /**
     * @param array $recipientEmailList
     * @param array $eMailConfiguration
     * @param array $contactDetails
     * @return bool
     * @author Pradeep,jsr,Aki
     */
    public function sendForwardEmail(
        string $recipientEmailList,
        array $eMailConfiguration,
        array $contactDetails,
        string $subjectLine
    ): ?bool {
        $this->logger->info('PARAMS ' . __FUNCTION__, ['recipientEmailList'=>$recipientEmailList, 'eMailConfiguration'=> $eMailConfiguration, 'contactDetails' => $contactDetails, __METHOD__, __LINE__]);
        $status = false;
        if (empty($recipientEmailList) || empty($contactDetails)) {
            return $status;
        }

        if (isset($contactDetails['base'])) {
            unset($contactDetails['base']);
        }
        $contactDetails = $this->removeParamsInForwardMail($contactDetails);
        // TODO: check if $recipientEmailList can be comma separated list => if yes, explode to array
        $recipientEmailListArray = explode(',', $recipientEmailList);

        // -------------------------------------------------
        // $this->emailContentTypes->aioSendLeadNotification;
        // -------------------------------------------------
        // gen custom content
        $customContent = '<table><tbody style="display: block;margin-left: 50px;">';
        foreach ($this->generateMessageBodyFromContactArray($contactDetails) as $key => $value) {
            $customContent .= "<tr><td><b>$key</b></td><td>$value</td></tr>";
        }
        $customContent .= '</tbody></table>';

        $this->messageParameterTypes->customContent = $customContent;

        $paramMsMailInterfaceTypes = $this->setParamMsMailInterfaceTypes(
            $eMailConfiguration,
            $customContent,
            $subjectLine,
            $recipientEmailListArray
        );

        try {
            $success = $this->emailService->sendByMsMail(
                $recipientEmailListArray, // ???? TODO: check => email of lead or email of campaign-owner?
                $subjectLine,
                $this->emailContentTypes->aioSendLeadNotification,
                $this->messageParameterTypes,
                $retMsg,
                $paramMsMailInterfaceTypes
            );
        } catch (ClientExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (RedirectionExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (ServerExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (TransportExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        }
        return $success;
    }


    /**
     * Sets properties of MsMailInterfaceTypes by rest of provided params.<br>
     * Uses either default_server or custom_server configuration from MIO|integrations|email server|lead notifications definition,<br>
     * chosen in ProcesshookServices:: sendLeadNotificationProcesshook for $eMailConfiguration
     * @param MsMailInterfaceTypes $paramMsMailInterfaceTypes call by reference
     * @param array                $eMailConfiguration
     * @param string               $messageBody
     * @param string               $messageSubject
     * @param array                $recipientEmailList
     * @return MsMailInterfaceTypes
     */
    private function setParamMsMailInterfaceTypes(
        array $eMailConfiguration,
        string $messageBody,
        string $messageSubject,
        array $recipientEmailList
    ):MsMailInterfaceTypes
    {
        $paramMsMailInterfaceTypes = new MsMailInterfaceTypes(new MsMailConfigurationService());
        $paramMsMailInterfaceTypes->smtpServer = $eMailConfiguration['smtp_server_domain'];
        $paramMsMailInterfaceTypes->smtpPort = $eMailConfiguration['port'];
        $paramMsMailInterfaceTypes->senderEmailAddress = $eMailConfiguration['sender_user_email'];
        $paramMsMailInterfaceTypes->senderUsername =
            (!isset($eMailConfiguration['sender_user_name']) || empty($eMailConfiguration['sender_user_name']))
                ? $eMailConfiguration['sender_user_email']
                : $eMailConfiguration['sender_user_name'];
        $paramMsMailInterfaceTypes->senderPassword = $eMailConfiguration['sender_password'];
        $paramMsMailInterfaceTypes->messageBody = $messageBody; // $customContent;
        $paramMsMailInterfaceTypes->messageSubject = $messageSubject; // $subjectLine;
        $paramMsMailInterfaceTypes->recipientEmailList = $recipientEmailList; // $recipientEmailListArray;
        return $paramMsMailInterfaceTypes;
    }

    /**
     * @param array  $company
     * @param string $userEmail
     * @param array  $messageBody
     * @param string $activationLink
     * @return bool
     * @throws JsonException
     * @author Pradeep,jsr
     */
    // public function sendAPIKeyConformationEmail(
    public function sendAccountActivationEmail(
        array $company,
        string $userEmail,
        array $messageBody,
        string $activationLink,
        string $accountNo
    ): bool {
        /*
            public function sendByMsMail(Request $request, EmailService $emailService, EmailContentTypes $emailContentTypes, MessageParameterTypes $messageParameters) : JsonResponse
            {
                $recipientEmailList = ['jochen.schaefer@treaction.net'];
                $subject = 'MSMAIL TEST MAIL';
                $emailContentType = $emailContentTypes->accountRegistrationNotification;
                $messageParameters->firstName = 'Jochen';
                $messageParameters->lastName = 'Schäfer';
                $messageParameters->accountNo = "#4711";
                // $messageParameters->activationLink = "https://dev-campaing-in-one.net/account/activation";
                // $messageParameters->apiKey = "9999999-0000-11111-aaaaaa";
                $messageParameters->email = 'jochen.schaefer@treaction.net';

                $response = $emailService->sendByMsMail($recipientEmailList, $subject, $emailContentType, $messageParameters, $retMsg);
                   return $this->json(['eMailSendResult' => $response, 'msg' => $retMsg]);

            }
         */
        if (empty($company) || empty($userEmail)) {
            $this->logger->info('Invalid configuration ', [__CLASS__, __METHOD__, __LINE__]);
            return false;
        }

        $this->logger->info('PARAMS ' . __FUNCTION__, [$company, $userEmail, $activationLink, $accountNo, __METHOD__, __LINE__]);

        // -------------------------------------------------------
        // $this->emailContentTypes->aioAccountSendActivationlink;
        // -------------------------------------------------------
        $this->messageParameterTypes->email = $userEmail;
        $this->messageParameterTypes->accountNo = '#' . $accountNo;
        $this->messageParameterTypes->firstName = $messageBody['firstName'];
        $this->messageParameterTypes->lastName = $messageBody['lastName'];
        $this->messageParameterTypes->activationLink = $activationLink;

        try {
            $success = $this->emailService->sendByMsMail([$userEmail], 'Please activate your Marketing-In-One account',
                $this->emailContentTypes->aioAccountSendActivationlink, $this->messageParameterTypes, $retMsg);
        } catch (ClientExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode() . ", line " . __LINE__);
        } catch (RedirectionExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode() . ", line " . __LINE__);
        } catch (ServerExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode() . ", line " . __LINE__);
        } catch (TransportExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode() . ", line " . __LINE__);
        }


        /*
            $userData = '';



            $body[ 'recipientEmailList' ] = $userEmail;
            //$body[ 'messageSubject' ] = 'Request for Marketing-In-One APIKey';
            $body[ 'messageSubject' ] = 'Please activate your Marketing-In-One account';

            // message body
            foreach($messageBody as $key=>$value) {
                $userData .= "<br>   ".$key." : " .$value;
            }
            // TODO: change template: put the userinfo in place of the apikey
            // Message body ["{​​\"salutation\":\"Mr\",\"firstName\":\"Pradeep\",\"lastName\":\"Veera\",\"email\":\"vefiv50038@200cai.com\",\"permission\":true}​​"] []
            $body[ 'messageBody' ] = $this->emailService->getActivationLinkContent($activationLink, $messageBody['firstName'],$messageBody['lastName'], $accountNo); // HTML content

            // file_put_contents(__DIR__.'/mio-email-template-account-activation.html', $body[ 'messageBody' ] ); // REMOVE

    //		  TEXT content
    //        $body[ 'messageBody' ] = "\n Thanks for your interest for working with treaction AG. \n";
    //        $body[ 'messageBody' ] .= "\n Please confirm the bellow details \n";
    //        foreach($messageBody as $key=>$value) {
    //            $body[ 'messageBody' ] .="\n   ".$key." : " . json_encode($value, JSON_THROW_ON_ERROR);
    //        }
    //        $body[ 'messageBody' ] .= "\n";
    //        $body[ 'messageBody' ] .= "\n Kindly click on below URL to get the APIKey for your plugin";
    //        $body[ 'messageBody' ] .= "\n ".$activationLink."\n";
            $mailer_configuration = $this->fetchCIOMailerConfiguration($company['id']);
            $body[ 'senderEmailAddress' ] = $mailer_configuration[ 'senderEmailAddress' ];
            $body[ 'senderPassword' ] = $mailer_configuration[ 'senderPassword' ];
            $body[ 'smtpServer' ] = $mailer_configuration[ 'smtpServer' ];
            $body[ 'smtpPort' ] = $mailer_configuration[ 'smtpPort' ];

            try {
                $base64text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
                if ($this->http_client->postRequest('POST', $_ENV[ 'MSMAIL_URL' ] . '/email/send',
                    $base64text)) {
                    $resp = json_decode($this->http_client->getContent(), true, 512,
                        JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);

                    $status = $resp[ 'success' ];
                }
            } catch (JsonException | \Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
            */
        return $success;
    }


    /**
     * @param array  $company_details
     * @param array  $admin_details
     * @param string $api_key
     * @return bool
     * @author Pradeep,jsr
     */
    public function sendAPIKeyToClient(
        array $company_details,
        array $admin_details,
        string $api_key,
        string $accountNo = null
    ): bool {
        $status = false;

        if (empty($company_details) || empty($admin_details) || empty($api_key)) {
            $this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
            return false;
        }

        $user = $this->userRepository->getUserDetailsByEmail($admin_details['email']);
        $firstName = (!empty($user['first_name'])) ? $user['first_name'] : null;
        $lastName = (!empty($user['last_name'])) ? $user['last_name'] : null;

        // ----------------------------------------------
        // $this->emailContentTypes->accountVerifyApikey;
        // ----------------------------------------------
        $this->messageParameterTypes->accountNo = $accountNo;
        $this->messageParameterTypes->firstName = $firstName;
        $this->messageParameterTypes->lastName = $lastName;
        $this->messageParameterTypes->apiKey = "XXXXX-XXXX-XXXX-" . substr($api_key, -4);
        $this->messageParameterTypes->activationLink = $_ENV['MARKETINGINONE_URL'] . "/?redirect=" . base64_encode('acc.getSettingsGeneral');

        try {
            $success = $this->emailService->sendByMsMail([$admin_details['email']], 'Get your Marketing-In-One API-Key',
                $this->emailContentTypes->accountVerifyApikey, $this->messageParameterTypes, $retMsg);
        } catch (ClientExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (RedirectionExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (ServerExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        } catch (TransportExceptionInterface $e) {
            throw new Exception("error: " . $e->getMessage() . "/{$retMsg}, code: " . $e->getCode());
        }

        return $success;
        /*


                $body[ 'recipientEmailList' ] = $admin_details[ 'email' ];



                $body[ 'messageSubject' ] = 'Get your Marketing-In-One API-Key';

                $body[ 'messageBody' ] = $this->emailService->getApiKeyWithRedirectContent( // HTML content
                    $_ENV['MARKETINGINONE_URL']."/?redirect=".base64_encode('acc.getSettingsGeneral'),
                    "XXXXX-XXXX-XXXX-".substr($api_key, -4),
                    $firstName,
                    $lastName,
                    $accountNo
                );

        //		  TEXT content
        //        $body[ 'messageBody' ] = "\n Thanks for your interest for working with treaction AG. \n";
        //        $body[ 'messageBody' ] .= "\n Please check the last 4 digits of the APIKey bellow \n";
        //        $body[ 'messageBody' ] .= "\n APIKey XXXXX-XXXX-XXXX-".substr($api_key, -4)."\n";
        //        $body[ 'messageBody' ] .= "\n Please login into https://dev-campaign-in-one.net/?redirect=YWNjLmdldFNldHRpbmdzR2VuZXJhbA== to verify your APIKey \n";
        //        $body[ 'messageBody' ] .= "\n\n";
                $mailer_configuration = $this->fetchCIOMailerConfiguration($company_details['id']);
                $body[ 'senderEmailAddress' ] = $mailer_configuration[ 'senderEmailAddress' ];
                $body[ 'senderPassword' ] = $mailer_configuration[ 'senderPassword' ];
                $body[ 'smtpServer' ] = $mailer_configuration[ 'smtpServer' ];
                $body[ 'smtpPort' ] = $mailer_configuration[ 'smtpPort' ];

                try {
                    $base64text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
                    if ($this->http_client->postRequest('POST', $_ENV[ 'MSMAIL_URL' ] . '/email/send',
                        $base64text)) {
                        $resp = json_decode($this->http_client->getContent(), true, 512,
                            JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);

                        $status = $resp[ 'success' ];
                    }
                } catch (JsonException | \Exception $e) {
                    $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
                }
                return $status;*/
    }


    /**
     * @param array  $companyDetails
     * @param array  $adminDetails
     * @param string $message
     * @return bool
     * @author     Pradeep
     * @deprecated since 2020
     */
    public function sendAccountInformationToClient(array $adminDetails, int $company_id): bool
    {
        $status = false;
        if (empty($adminDetails) || !isset($adminDetails['email'])) {
            return $status;
        }
        $this->logger->info(json_encode($adminDetails, JSON_THROW_ON_ERROR));
        try {
            $password_reset_link = "https://{$_SERVER['SERVER_NAME']}/forgotpassword";
            $body['recipientEmailList'] = $adminDetails['email'];
            // $body[ 'messageSubject' ] = 'Request for Marketing-In-One APIKey';
            $body['messageSubject'] = 'Please activate your Marketing-In-One account';
            $body['messageBody'] = $this->emailService->getActivationLinkContent($password_reset_link); // HTML content

//			  TEXT content
//            $body[ 'messageBody' ] = "\n Thanks for your interest for working with treaction AG. ";
//            $body[ 'messageBody' ] .="\n We were able to find your Account details with us \n";
//            $body[ 'messageBody' ] .="\n Kindly follow bellow link to reset your password for Marketing-In-One \n";
//            $body[ 'messageBody' ] .="\n ".$password_reset_link."\n";

            $mailer_configuration = $this->fetchCIOMailerConfiguration($company_id);
            $body['senderEmailAddress'] = $mailer_configuration['senderEmailAddress'];
            $body['senderPassword'] = $mailer_configuration['senderPassword'];
            $body['smtpServer'] = $mailer_configuration['smtpServer'];
            $body['smtpPort'] = $mailer_configuration['smtpPort'];
            $base64text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));

            if ($this->http_client->postRequest('POST', $_ENV['MSMAIL_URL'] . '/email/send', $base64text)) {
                $resp = json_decode($this->http_client->getContent(), true, 512, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
                $status = $resp['success'];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
        return $status;
    }


    /**
     * @return string[]
     * @author     Pradeep
     * @deprecated replaced by MsMailInterfaceTypes, which gets configuration of MSMAIL
     */
    private function fetchCIOMailerConfiguration(int $companyId): array
    {
        // Default Email Hosting server
        $config = [
            // 'senderEmailAddress' => 'support@dev-campaign-in-one.net', // user in mscio TODO: change!!!
            // 'senderPassword' => 'gKsxYmr2cKB10@=rV4UPr', // password in mscio
            // 'smtpServer' => 'lvps92-51-150-31.dedicated.hosteurope.de', // host in mscio
            // 'smtpPort' => '25', // port in mscio

            'senderEmailAddress' => $this->senderEmailAddress,
            'senderPassword' => $this->senderPassword,
            'smtpServer' => $this->smtpServer,
            'smtpPort' => $this->smtpPort,
        ];
        try {
            $result = $this->accountIntegrations->getEMailServerConfiguration($companyId);
            $this->logger->info('MailConfiguration', [$result]);
            if ($result !== null) {
                $config['senderEmailAddress'] = $result['user'];
                $config['senderPassword'] = $result['password'];
                $config['smtpServer'] = $result['host'];
                $config['smtpPort'] = $result['port'];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $config;
        }

        return $config;
    }


    /**
     * todo: implement
     * @param int $campaign_id
     * @return array
     * @author jsr | 2020-03-22, modified 2021-11-03
     */
    public function fetchMailerConfiguration(int $campaign_id = null): array
    {
        return [
            // 'senderEmailAddress' => 'support@dev-campaign-in-one.net', // user in mscio TODO: change!!!
            // 'senderPassword' => 'gKsxYmr2cKB10@=rV4UPr', // password in mscio
            // 'smtpServer' => 'lvps92-51-150-31.dedicated.hosteurope.de', // host in mscio
            // 'smtpPort' => '25', // port in mscio

            'senderEmailAddress' => $this->senderEmailAddress,
            'senderPassword' => $this->senderPassword,
            'smtpServer' => $this->smtpServer,
            'smtpPort' => $this->smtpPort,
        ];

        // https://mscio.dev-api-in-one.net/api/integrations?companyId=20&configurationTargetId=7
        /*    response:
              {
                "id": "18",  // take this id
                "companyId": "20",
                "configurationTargetId": "7",   // check for 7 === Email
                "created": "2020-03-19T16:16:18+00:00",
                "updated": "2020-03-19T16:16:18+00:00"
              }
         */

        // https://mscio.dev-api-in-one.net/api/integrations_metas?integrationsId=18 // todo: request on "integrationsId" to be implemented in mscio
        /*    response:
              {
                    "@id": "/api/integrations_metas/32",
                    "@type": "IntegrationsMeta",
                    "id": "32",
                    "metaKey": "settings",
         >>>>       "metaValue": "{\"host\":\"lvps92-51-150-31.dedicated.hosteurope.de\",\"id\":\"\",\"password\":\"gKsxYmr2cKB10@=rV4UPr\",\"port\":\"25\",\"smtpAuth\":null,\"secureProtocol\":null,\"user\":\"support@dev-campaign-in-one.net\"}",
                    "created": "2020-03-19T16:16:18+00:00",
                    "updated": "2020-03-19T16:16:18+00:00",
                    "integrations": {
                        "@id": "/api/integrations/18",
                        "@type": "Integrations",
                        "id": "18",
                        "companyId": "20",
                        "configurationTargetId": "7",
                        "created": "2020-03-19T16:16:18+00:00",
                        "updated": "2020-03-19T16:16:18+00:00"
                    }
                },
         */ //        ADD IMPLEMENTATION HERE:
//        $email_configuration = [];
//        if ($this->http_client === null || $this->campaignServices === null) {
//            return $email_configuration;
//        }
//        $result = $this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Email');
//        if (!empty($result) && !empty($result['value'])) {
//            $email_configuration = json_decode($result['value'], true, 512, JSON_UNESCAPED_SLASHES);
//        }
//        return $email_configuration;
    }


    /**
     * @param int $campaign_id
     * @return array
     * @author     Pradeep
     * @deprecated 2021-10-29, jsr
     */
    private function fetchEmailConfiguration(int $campaign_id): array
    {
        $email_configuration = [];
        if ($this->http_client === null || $this->campaignServices === null) {
            return $email_configuration;
        }
        $result = $this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Email');
        if (!empty($result) && !empty($result['value'])) {
            $email_configuration = json_decode($result['value'], true, 512, JSON_UNESCAPED_SLASHES);
        }
        return $email_configuration;
    }


    /**
     * @param $contactArray
     * @return array|null
     * @author Aki
     */
    private function generateMessageBodyFromContactArray($contactArray): ?array
    {
        $standard = $contactArray['contact']['standard'];
        $custom = $contactArray['contact']['custom'] ?? [];
        $msgBody = [];
        foreach ($standard as $st) {
            $fieldName = $st['field_name'];
            foreach ($st as $key => $val) {
                if($key === $fieldName){
                    $msgBody[$key] = $val;
                }
            }
        }

        if (!empty($custom)) {
            foreach ($custom as $st) {
                $fieldName = $st['field_name'];
                foreach ($st as $key => $val) {
                    if($key === $fieldName){
                        $msgBody[$key] = $val;
                    }
                }
            }
        }

        $this->logger->info('$msgBody ' . __FUNCTION__, [$msgBody, __METHOD__, __LINE__]);
        return $msgBody;
    }


    /**
     * @param array $contactDetails
     * @return array
     * @author aki
     */
    private function removeParamsInForwardMail(array $contactDetails): array
    {
        //add unnecessary params hear
        //todo:take which parameters to send form UI
        $unnecessaryParams = array(ContactFieldTypes::Device, ContactFieldTypes::DeviceBrowser, ContactFieldTypes::DeviceOs);
        foreach ($contactDetails as $key => $value) {
            if (!empty($value['standard'])) {
                foreach ($value['standard'] as $a => $b) {
                    if(isset($b['field_name'])){
                        $paramName = $b['field_name'];
                        if (in_array($paramName, $unnecessaryParams, true)) {
                            unset($contactDetails['contact']['standard'][$a][$paramName]);
                        }
                    }else{
                        unset($contactDetails['contact']['standard'][$a]);
                    }
                }
            }
        }
        return $contactDetails;
    }

}

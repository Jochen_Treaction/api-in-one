<?php


namespace App\Services\MSHubspotServices;


use App\Services\HttpMicroServices\HttpMicroService;
use App\Services\AIOServices\MSCIOCampaignServices;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MSHubspotServices
{
    public const SUCCESS = 200;
    /**
     * @var HttpMicroService
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var MSCIOCampaignServices
     * @author Pradeep
     */
    private $campaignServices;
    /**
     * @var string
     * @author Pradeep
     */
    private $logger;
    /**
     * @var string
     * @author Pradeep
     */
    private string $apikey;
    private HttpClientInterface $httpClient;

    public function __construct( LoggerInterface $logger)
    {
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
        $this->logger = $logger;
    }

    /**
     * @param string $apikey
     * @return bool
     * @author Pradeep
     */
    public function setAPIKey(string $apikey):bool
    {
        $status = false;
        if(!empty($apikey)){
            $this->apikey = $apikey;
            $status = true;
        }
        return $status;
    }

    /**
     * @return string
     * @author Pradeep
     */
    public function getAPIKey():?string
    {
        return $this->apikey ?? null;
    }


    /**
     * @param array $hbContact
     * @return int
     * @author Pradeep
     */
    public function createContact(array $hbContact):int
    {
        $hbContactId = 0;
        $method = 'POST';
        $endPoint = $_ENV[ 'MSHB_URL' ] . '/contact/create';
        $apikey = $this->getAPIKey();

        try {
            if (empty($hbContact) || empty($apikey)) {
                throw new RuntimeException('Invalid Hubspot Contact Properties Or APIKey');
            }
            $body['apikey'] = $apikey;
            $body['contact'] = $hbContact;
            $body = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
            $this->logger->info('MSHB '.$body);
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if($response->getStatusCode() === self::SUCCESS && !empty($response->toArray())) {
                $hbContactId = $response->toArray()['id'];
            }
            return $hbContactId;
        } catch (\JsonException | RuntimeException | TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $hbContactId;
        }
    }

//
//    /**
//     * @param string $apikey
//     * @param int $campaign_id
//     * @return bool
//     * @author Pradeep
//     */
//    public function isHubspotEnabledForCampaign(string $apikey, int $campaign_id): bool
//    {
//        $status = false;
//        if ($this->http_client === null || $this->campaignServices === null || empty($apikey)) {
//            return $status;
//        }
//        if ($this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Hubspot') !== null) {
//            $status = true;
//            $this->apikey = $apikey;
//        }
//
//        return $status;
//    }
//
//    /**
//     * @param int $campaign_id
//     * @param array $lead_data
//     * @return bool
//     * @author Pradeep
//     */
//    public function IntegrateLeadToHubspot(int $campaign_id, array $lead_data): bool
//    {
//        $status = false;
//        if ($this->http_client === null || $this->campaignServices === null || $campaign_id <= 0 || empty($lead_data)) {
//            return $status;
//        }
//        // get campaign related HB configuration
//        $hb_config = $this->fetchHubspotConfiguration($campaign_id);
//        // Convert lead params from CIO to HB mapping
//        $hb_contact_properties = $this->fetchContactProperties($lead_data, $hb_config[ 'mapping' ]);
//        // Convert Lead params from CIO to HB mapping
//        $hb_company_properties = $this->fetchCompanyProperties($lead_data);
//        $contact_id = $this->createContact($hb_config, $hb_contact_properties);
//        $company_id = $this->createCompany($hb_company_properties);
//        $deal_id = $this->createDeal( $hb_config, $hb_contact_properties, $contact_id, $company_id);
//        if($contact_id > 0 || $company_id > 0 || $deal_id > 0) {
//            $status = true;
//        }
//        return $status;
//    }
//
//    /**
//     * @param int $campaign_id
//     * @return array
//     * @author Pradeep
//     */
//    private function fetchHubspotConfiguration(int $campaign_id): array
//    {
//        $hb_configuration = [];
//        if ($this->http_client === null || $this->campaignServices === null) {
//            return $hb_configuration;
//        }
//        $result = $this->campaignServices->getCampaignParmsForTarget($campaign_id, 'Hubspot');
//        if (!empty($result) && !empty($result[ 'value' ])) {
//            $hb_configuration = json_decode($result[ 'value' ], true, 512, JSON_UNESCAPED_SLASHES);
//            $hb_configuration[ 'mapping' ] = $this->fetchHubspotMapping($hb_configuration[ 'mappingTable' ]);
//        }
//
//        return $hb_configuration;
//    }
//
//    /**
//     * @param string $encoded_mapping_table
//     * @return array
//     * @author Pradeep
//     */
//    private function fetchHubspotMapping(string $encoded_mapping_table): array
//    {
//        $mapping = [];
//        if (empty($encoded_mapping_table)) {
//            return $mapping;
//        }
//        $mapping_decoded = base64_decode($encoded_mapping_table);
//        $result = json_decode($mapping_decoded, true, 512, JSON_UNESCAPED_SLASHES);
//        if (!empty($result)) {
//            foreach ($result as $res) {
//                $mapping[ $res[ 'hubspot' ] ] = $res;
//            }
//        }
//        return $mapping;
//    }
//
//    /**
//     * @param array $lead_params
//     * @param array $hb_mapping
//     * @return array
//     * @author Pradeep
//     */
//    private function fetchContactProperties(array $lead_params, array $hb_mapping): array
//    {
//        $hb_lead = [];
//        if (empty($lead_params) || empty($hb_mapping) || empty($this->apikey)) {
//            return $hb_lead;
//        }
//
//        foreach ($lead_params as $lead_key => $lead_value) {
//            foreach ($hb_mapping as $hb_key => $hb_value) {
//                if ($hb_value[ 'campaign-in-one' ] === $lead_key) {
//                    $hb_lead[ 'contact' ][ $hb_value[ 'hubspot' ] ] = $lead_value;
//                }
//            }
//        }
//
//        $hb_lead[ 'apikey' ] = $this->apikey;
//        return $hb_lead;
//    }
//
//    /**
//     * @param array $lead_params
//     * @return array
//     * @author Pradeep
//     */
//    private function fetchCompanyProperties(array $lead_params): array
//    {
//        $hb_company = [];
//        if (empty($lead_params) || empty($lead_params[ 'email' ]) || empty($this->apikey)) {
//            return $hb_company;
//        }
//        $hb_company[ 'company' ][ 'name' ] = explode('@', $lead_params[ 'email' ])[ 1 ];
//        $hb_company[ 'apikey' ] = $this->apikey;
//        return $hb_company;
//    }
//
//    /**
//     * @param array $hb_config_data
//     * @param array $lead
//     * @param int $contact_id
//     * @param int $company_id
//     * @return int
//     * @author Pradeep
//     */
//    private function createDeal(array $hb_config_data, array $lead, int $contact_id, int $company_id): int
//    {
//        $id = 0;
//        if (empty($hb_config_data) || empty($lead) || $contact_id <= 0 || $company_id <= 0) {
//            return $id;
//        }
//        $hb_deal_properties = $this->fetchDealParameters($hb_config_data, $lead, $contact_id, $company_id);
//        $base64_text = base64_encode(json_encode($hb_deal_properties));
//        $this->logger->info('create deal '.$base64_text, [__METHOD__, __LINE__]);
//        if ($this->http_client->postRequest('POST', $_ENV[ 'MSHB_URL' ] . '/create_deal', $base64_text)) {
//            $resp = json_decode($this->http_client->getContent(), true, 512, JSON_UNESCAPED_SLASHES);
//            if (!empty($resp) && !empty($resp[ 'id' ])) {
//                $id = $resp[ 'id' ];
//            }
//        }
//
//        return $id;
//    }
//
//    /**
//     * @param array $hb_deal_config
//     * @param array $deal_params
//     * @param int $contact_id
//     * @param int $company_id
//     * @return array
//     * @author Pradeep
//     */
//    private function fetchDealParameters(
//        array $hb_deal_config,
//        array $deal_params,
//        int $contact_id,
//        int $company_id
//    ): array {
//        $hb_deal = [];
//        if (empty($hb_deal_config) || empty($deal_params) || empty($hb_deal_config[ 'hb_dl_name' ])) {
//            return $hb_deal;
//        }
//
//        $deal_name = $hb_deal_config[ 'hb_dl_name' ];
//        if ($hb_deal_config[ 'sync_app_frt_name' ] === 'on' && !empty($deal_params[ 'contact' ][ 'firstname' ])) {
//            $deal_name .= ' ' . $deal_params[ 'contact' ][ 'firstname' ];
//        }
//        if ($hb_deal_config[ 'sync_app_lst_name' ] === 'on' && !empty($deal_params[ 'contact' ][ 'lastname' ])) {
//            $deal_name .= ' ' . $deal_params[ 'contact' ][ 'lastname' ];
//        }
//        if ($hb_deal_config[ 'sync_app_city' ] === 'on' && !empty($deal_params[ 'contact' ][ 'city' ])) {
//            $deal_name .= ' ' . $deal_params[ 'contact' ][ 'city' ];
//        }
//        $hb_deal[ 'deal' ][ 'dealname' ] = $deal_name;
//        $hb_deal[ 'deal' ][ 'amount' ] = $hb_deal_config[ 'hb_dl_amount' ];
//        $hb_deal[ 'deal' ][ 'segment' ] = $hb_deal_config[ 'hb_dl_segment' ];
//        $hb_deal[ 'deal' ][ 'dealtype' ] = $hb_deal_config[ 'hb_dl_type' ];
//        $hb_deal[ 'deal' ][ 'dealstage' ] = $hb_deal_config[ 'hb_dl_stage' ];
//        $hb_deal[ 'deal' ][ 'closedate' ] = $hb_deal_config[ 'hb_dl_clsedate' ];
//        $hb_deal[ 'deal' ][ 'pipeline' ] = $hb_deal_config[ 'hb_dl_pipeline' ];
//        $hb_deal[ 'deal' ][ 'hubspot_owner_id' ] = $hb_deal_config[ 'hubspotOwnerId' ];
//        $hb_deal[ 'deal' ][ 'contact_id' ] = $contact_id;
//        $hb_deal[ 'deal' ][ 'company_id' ] = $company_id;
//        $hb_deal[ 'apikey' ] = $this->apikey;
//
//        return $hb_deal;
//    }
//
//    private function fetchDealName(): string
//    {
//
//    }
//
//    /**
//     * @param array $hb_config_data
//     * @param array $hb_contact_properties
//     * @return int
//     * @author Pradeep
//     */
//    private function createContact(array $hb_config_data, array $hb_contact_properties): int
//    {
//        $id = 0;
//        if (empty($hb_config_data) || empty($hb_contact_properties)) {
//            return $id;
//        }
//        $base64_text = base64_encode(json_encode($hb_contact_properties));
//        if ($this->http_client->postRequest('POST', $_ENV[ 'MSHB_URL' ] . '/create_contact', $base64_text)) {
//            $resp = json_decode($this->http_client->getContent(), true, 512, JSON_UNESCAPED_SLASHES);
//            if (!empty($resp) && !empty($resp[ 'id' ])) {
//                $id = $resp[ 'id' ];
//            }
//        }
//        return $id;
//    }
//
//    /**
//     * @param array $company_properties
//     * @return int
//     * @author Pradeep
//     */
//    private function createCompany(array $company_properties): int
//    {
//        $id = 0;
//        if (empty($company_properties)) {
//            return $id;
//        }
//        $base64_text = base64_encode(json_encode($company_properties));
//        if ($this->http_client->postRequest('POST', $_ENV[ 'MSHB_URL' ] . '/create_company', $base64_text)) {
//            $resp = json_decode($this->http_client->getContent(), true, 512, JSON_UNESCAPED_SLASHES);
//            if (!empty($resp) && !empty($resp[ 'id' ])) {
//                $id = $resp[ 'id' ];
//            }
//        }
//
//        return $id;
//    }

}
<?php


namespace App\Services\HttpMicroServices;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
// use Psr\Log\LoggerInterface;
// use Psr\Log\LoggerAwareInterface;
// use Psr\Log\LoggerAwareTrait;

class HttpMicroService // implements LoggerAwareInterface
{
    // use LoggerAwareTrait;

    /**
     * @var HttpClientInterface
     */
    private $http_client;
    /**
     * @var string
     */
    private $api_base_url;
    private $response;

    public function __construct()
    {
        $this->http_client = HttpClient::create(['http_version' => '2.0']);
    }

    /**
     * @param string $method
     * @param string $end_point
     * @param array|null $body
     * @return bool
     */
    public function sendRequestToMSCIO(string $method, string $end_point, array $body=null): bool
    {
        // $this->logger->info(json_encode($body),[__METHOD__,__LINE__]);
        $status = false;
        $http_client = $this->getHttpClient();
        try {
            if(empty($body)) {
                $response = $http_client->request($method, $end_point);
            }else {
                $response = $http_client->request($method, $end_point, ['json' => $body]);
            }

            $this->setResponse($response);
            $status = true;
        } catch (TransportExceptionInterface $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            exit;
        }
        return $status;
    }


    public function getHttpClient()
    {
        return $this->http_client;
    }

    /**
     * @param Response\CurlResponse $response
     */
    private function setResponse(Response\CurlResponse $response): void
    {
        $this->response = $response;
    }

    /**
     * @param string $method
     * @param string $end_point
     * @param string $body
     * @return bool
     */
    public function postRequest(string $method, string $end_point, string $body): bool
    {
        $status = false;
        $http_client = $this->getHttpClient();
        try {
            $response = $http_client->request($method, $end_point, ['body'=> $body]);
            $this->setResponse($response);
            $status = true;
        } catch (TransportExceptionInterface $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            exit;
        }
        return $status;
    }

    /**
     * @param string $url
     * @param array $body
     * @return bool
     */
    public function postToExternalURL(string $url, array $body): bool
    {
        $status = false;
        $http_client = $this->getHttpClient();
        if (empty($url) || empty($body)) {
            return $status;
        }
        try {
            $response = $http_client->request('POST', $url, ['json' => $body]);

            $this->setResponse($response);
            $status = true;
        } catch (TransportExceptionInterface $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            exit;
        }
        return $status;
    }

    public function getArray()
    {
        return $this->response->toArray();
    }


    /**
     * @return mixed
     */
    public function respToArray()
    {
        $resp_arr =[];
        try {
            $resp_arr = $this->response->toArray();

            if (array_key_exists('hydra:member', $resp_arr)) {
                return $resp_arr['hydra:member'];
            }
            return $resp_arr;

        } catch (\Exception $e) {
            echo $e->getMessage();
            return $resp_arr;
        }
    }

    /**
     * @return mixed
     */
    public function respToJson()
    {
        return $this->response->toJson()[ 'hydra:member' ];
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    public function getContent()
    {
        return $this->response->getContent();
    }

}
